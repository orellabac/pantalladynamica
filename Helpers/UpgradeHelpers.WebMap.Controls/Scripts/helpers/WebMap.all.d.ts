/// <reference path="kendo.all.d.ts" />
/// <reference path="jquery.blockUI.d.ts" />
/// <reference path="jquery.d.ts" />
/// <reference path="jquery.caret.d.ts" />
declare module UpgradeHelpers {
    class ControlArray {
        static indexOf(ctrlArray: any, e: any): number;
    }
    class Events {
        static getEventSender(e: any): any;
    }
    class EventKey {
        static getKeyChar(e: any): string;
        static setKeyChar(e: any, key: string): void;
        static getKeyCode(e: any): number;
        static setKeyCode(e: any, key: number): void;
        static handleEvent(e: any, value: boolean): void;
    }
    enum Keys {
        A = 65,
        Add = 43,
        Alt = 18,
        B = 66,
        Back = 8,
        C = 67,
        Capital = 20,
        CapsLock = 20,
        Clear = 46,
        Control = 17,
        ControlKey = 17,
        D = 68,
        D0 = 48,
        D1 = 49,
        D2 = 50,
        D3 = 51,
        D4 = 52,
        D5 = 53,
        D6 = 54,
        D7 = 55,
        D8 = 56,
        D9 = 57,
        Decimal = 44,
        Delete = 46,
        Divide = 47,
        Down = 40,
        E = 69,
        End = 35,
        Enter = 13,
        Escape = 27,
        F = 70,
        F1 = 112,
        F10 = 121,
        F11 = 122,
        F12 = 123,
        F2 = 113,
        F3 = 114,
        F4 = 115,
        F5 = 116,
        F6 = 117,
        F7 = 118,
        F8 = 119,
        F9 = 120,
        G = 71,
        H = 72,
        Home = 36,
        I = 73,
        Insert = 45,
        J = 74,
        K = 75,
        L = 76,
        LControlKey = 17,
        Left = 37,
        LShiftKey = 16,
        M = 77,
        Multiply = 42,
        N = 78,
        NumLock = 144,
        NumPad0 = 48,
        NumPad1 = 49,
        NumPad2 = 50,
        NumPad3 = 51,
        NumPad4 = 52,
        NumPad5 = 53,
        NumPad6 = 54,
        NumPad7 = 55,
        NumPad8 = 56,
        NumPad9 = 57,
        O = 79,
        P = 80,
        PageDown = 34,
        PageUp = 33,
        Q = 81,
        R = 82,
        RControlKey = 17,
        Return = 13,
        Right = 39,
        RShiftKey = 16,
        S = 83,
        Shift = 16,
        ShiftKey = 16,
        Space = 32,
        Subtract = 45,
        T = 84,
        Tab = 9,
        U = 85,
        Up = 38,
        V = 86,
        W = 87,
        X = 88,
        Y = 89,
        Z = 90,
    }
    class Sound {
        static getBeep(): Sound;
        Play(): void;
    }
    class Strings {
        static convertToString(obj: any): string;
        static format(obj: any, format: string): string;
    }
}
declare module WebMap.Client {
    function debug(e: any): void;
    function IsNewView(id: string, VD: any): boolean;
    function showMessage(msg: string): void;
}
declare module WebMap.Utils {
    function IsClassSet(classes: string, cls: string): boolean;
    function RemoveClass(classes: string, cls: string): string;
}
declare module WebMap.Kendo {
    function setupBinding(binding: kendo.data.Binder, previousBinding: kendo.data.Binder, setups: BindingSetup[]): void;
    class BindingSetup {
        setup(binding: kendo.data.Binder): boolean;
    }
}
declare module WebMap.Client {
    class Container implements IIocContainer {
        static Current: Container;
        static Init(): void;
        Resolve(options?: any): any;
    }
}
declare module WebMap.Client {
    interface TryGetValueResult<T> {
        HasValue: boolean;
        Value: T;
    }
    interface IStorageSerializer {
        Serialize(obj: any): any;
    }
    interface IPromise {
    }
    interface DictionaryStringTo<TV> {
        [uniqueID: string]: TV;
    }
    interface AppActionOptions {
        controller?: string;
        action?: string;
        mainobj: IStateObject;
        parameters?: DictionaryStringTo<any>;
        useblockui?: boolean;
        dialogResult?: string;
    }
    interface Message {
        UniqueID: string;
        Text: string;
        Buttons?: number;
        Icons?: number;
        Continuation_UniqueID?: string;
        Caption?: string;
    }
    interface IIocContainer {
        Resolve(options?: any): any;
    }
    interface IStateObject {
        UniqueID: string;
    }
    interface IViewModel extends IStateObject {
        Name: string;
    }
    interface ILogicView<T extends IViewModel> {
        ViewModel: T;
        Init(): JQueryPromise<any>;
        Close(): void;
    }
    interface IViewManager {
        /**
          * Closes a view in the client side.  This method handles form closing when the user clicks
          * the close box control and no binding to server side events has been added.
          */
        CloseView(view: IViewModel): any;
        /**
          * Fills up the JSONWebMapRequest object with the delta information provided by the
          * ViewManager
          */
        PrepareDelta(requestInfo: JSONWebMapRequest): any;
        RemoveViews(data: any): void;
        UpdateView(view: IViewModel, viewInfo: any): void;
        NavigateToView(viewModel: IViewModel): JQueryPromise<any>;
        ShowMessage(message: string, caption: string, buttons: any, boxIcons: any): IPromise;
        LoadView(logic: ILogicView<IViewModel>, domElement: any): JQueryPromise<any>;
        getConstructor(vm: IViewModel): () => ILogicView<IViewModel>;
    }
}
declare module WebMap.Client {
}
declare module WebMap.Client {
    class ViewState {
        LoadedViews: IViewModel[];
    }
    class JSONWebMapRequest {
        JSONWebMapRequest(): void;
        dirty: DictionaryStringTo<any>;
        vm: string;
        closedViews: string[];
        parameters: DictionaryStringTo<any>;
        dialogResult: string;
    }
    class ViewInfo {
        UniqueID: string;
        ZOrder: number;
        Visible: boolean;
    }
    class ViewsState {
        LoadedViews: ViewInfo[];
    }
}
declare module WebMap.Client {
    class App {
        static Current: App;
        ViewManager: IViewManager;
        inActionExecution: boolean;
        clientSynchronizationActive: boolean;
        private updateListeners;
        private eventsQueue;
        static Init(): void;
        Init(models: any, viewsState: ViewState): void;
        isSynchronizingClient(): any;
        addModelUpdateListener(l: () => any): void;
        removeModelUpdateListener(l: () => any): void;
        private callModelUpdateListeners();
        sendAction(options: AppActionOptions): JQueryPromise<any>;
        private doServerCall(that, url, actionParamStr, forced);
        private applyDelta(model, delta);
        private buildActionUrl(options);
        private buildJSONRequest(options);
        private checkFlag(data, flagName);
        private extractItemsCollectionInfo(data);
        InitModels(models: any[]): void;
        private InitUI(viewState);
        private removeLoadingSplash();
        private showLoadingSplash();
        private updateMessages(viewData);
        private showGenericMessage(messageText);
        private showMessageDialog(msg);
        private preparteMessageBoxTemplate(msg);
        private getMessageBoxIconCssClass(id);
        private extractItemsCollectionInfoFromDeltas(MD);
        private syncControlArraysSize(controlArrays);
        private getDeltaWithId(uniqueID, deltaData);
        private processRemovedIds(data);
        private processSwitchedIds(data, deltaData);
        private updateClientSide(data);
        private showSeesionExpiredMessage();
    }
}
declare module WebMap.Client {
    /**
      * (Not implementet yet!) Provides an storage area where to serialize IStateObject objects to.
      * The default persistent layer would be the WebMap server side object.
      */
    class StorageManager {
        _storageSerializer: IStorageSerializer;
        /**
          * Tries the gets an object from the storage area.
          * @param uniqueId The id of the object to get.
          */
        TryGetValue(uniqueId: string): JQueryPromise<TryGetValueResult<IStateObject>>;
        /**
          * Persists the StateCache object into the storage area.
          * @param stateCache The StateCache object to persist.
          */
        Persist(stateCache: StateCache): void;
        /**
          * Saves an object to the storage area.
          * @param uniqueId  The id of ghe object to serialize
          * @param serializedValue The value to serialize.
          */
        SaveEntry(uniqueID: string, serializedValue: any): void;
    }
}
declare module WebMap.Client {
    class DeltaTracker {
        attachedObjs: DictionaryStringTo<IStateObject>;
        dirtyTable: DictionaryStringTo<boolean>;
        dirtyPropertiesTable: DictionaryStringTo<DictionaryStringTo<boolean>>;
        isDirty(obj: IStateObject): boolean;
        constructor();
        updateDirtyPropertiesTable(fieldName: any, uid: any): void;
        changeTracker(e: any): any;
        _changeDelegate: any;
        attachModel(obj: IStateObject, markAsDirty?: boolean): void;
        reset(): void;
        start(): void;
        getDeltas(): void;
        wasModified(variable: IStateObject): boolean;
        getJSONFromFullObject(obj: any): any;
        getCalculatedDeltaFor(variable: IStateObject): any;
    }
}
declare module WebMap.Client {
    class ViewManager implements IViewManager {
        private _logic;
        closedViews: string[];
        constructor();
        CloseView(view: IViewModel): void;
        PrepareDelta(requestInfo: JSONWebMapRequest): void;
        UpdateView(view: IViewModel, viewInfo: any): void;
        RemoveViews(data: any): void;
        NavigateToView(viewModel: IViewModel): JQueryPromise<any>;
        ShowMessage(message: string, caption: string, buttons: any, boxIcons: any): IPromise;
        private static DESIGNERSUPPORT;
        static removeDesignerSupportSnippet(document: string): string;
        LoadView(logic: ILogicView<IViewModel>, domElement?: any): JQueryPromise<any>;
        static AddCssIfMissing(href: any): void;
        private DisposeView(viewModelId);
        getConstructor(vm: IViewModel): () => ILogicView<IViewModel>;
        static getViewHtmlURL(viewName: any, extension: string, isLib?: boolean): string;
        private IsRemovedView(id, VD);
    }
}
declare module WebMap.Client {
    class StateCache {
        static Current: StateCache;
        private _storageManager;
        private _cache;
        private _tracker;
        static Init(): void;
        constructor();
        organize(options?: any): void;
        processObject(obj: IStateObject): void;
        addNewObject(obj: IStateObject, markAsDirty?: boolean): any;
        getDirty(): DictionaryStringTo<any>;
        getObjectLocal(uniqueId: string): IStateObject;
        clearDirty(): void;
        getObject(uniqueId: string): JQueryPromise<any>;
        RemoveView(id: string): void;
    }
}
declare module WebMap.Client {
    class BaseLogic<TViewModel extends IViewModel> implements ILogicView<TViewModel> {
        ViewModel: TViewModel;
        private timersToCleanup;
        Close(): void;
        Init(domElement?: any): JQueryPromise<any>;
        RegisterTimer(timerInfo: any): void;
        generic_Click(event: any): JQueryPromise<any>;
        private getDOMID();
        CloseWindowLocally(e: any): void;
        private CoreInit(domElement);
        private CleanupTimers();
    }
}
interface Window {
    app: any;
}
declare module kendo.data {
    var binders: any;
}
declare module WebMap.Client {
    function checkedListBoxProc(element: any): void;
}
declare function maskToRegex(newChar: any, maskChar: any): any;
