using System;
using System.Web.Mvc;
using UpgradeHelpers.DB.ADO;
using UpgradeHelpers.DB.ADO.Events;
using UpgradeHelpers.Events;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.WebMap.Server;

namespace UpgradeHelpers.WebMap.Controls
{
	public class ADODataControlHelper : IUserControl
	{
		public void Build(IIocContainer ctx)
		{
			Recordset = ctx.Resolve<ADORecordSetHelper>();
			Text = "";
		}

		public string UniqueID { get; set; }
		public IViewManager ViewManager { get; set; }

		#region Data Members
		/// <summary>
		/// Text displayed in box at the center of the ADODC controls
		/// </summary>
		public virtual String Text { get; set; }

		private String _connectionString;
		/// <summary>
		/// Conection String used to connect to the data source
		/// </summary>
		public virtual String ConnectionString
		{
			get
			{
				return _connectionString;
			}
			set
			{
				_connectionString = value;
				SyncRecordset();
			}
		}

		/// <summary>
		/// Database name to connect at the corresponding db connection
		/// </summary>
		public virtual String DatabaseName { get; set; }

		/// <summary>
		/// The Factory Name being used for connecting to the database. 
		/// </summary>
		public virtual String FactoryName { get; set; }

		/// <summary>
		/// Name for this control
		/// </summary>
		public virtual string Name { get; set; }

		/// <summary>
		/// Password to be used for connecting to the database
		/// </summary>
		public virtual String Password { get; set; }

		/// <summary>
		/// The data source (ADORecordSetHelper) used to connect to the database
		/// </summary>
		public virtual ADORecordSetHelper Recordset { get; set; }

		/// <summary>
		/// The record source (or Table) to query when connecting to the database
		/// </summary>
		private String _recordSource;
		public virtual String RecordSource
		{
			get
			{
				return _recordSource;
			}
			set
			{
				_recordSource = value;
				SyncRecordset();
			}
		}

		/// <summary>
		/// User name to be used for connecting to the database
		/// </summary>
		public virtual String UserName { get; set; }

		/// <summary>
		/// Indicates if control is visually Visible
		/// </summary>
		public virtual bool Visible { get; set; }
		#endregion

		#region Methods

		/// <summary>
		/// Method used to syncronize the database when an important property for this control is changed, and
		/// a syncronization is needed when a connection proerty is changed.
		/// </summary>
		private void SyncRecordset()
		{
			if (Recordset != null)
			{
				if (string.IsNullOrEmpty(Recordset.ConnectionString) && !string.IsNullOrEmpty(ConnectionString))
				{
					Recordset.ConnectionString = ConnectionString;
				}
				if (!string.IsNullOrEmpty(RecordSource))
				{
					Recordset.Source = RecordSource;
					if (!Recordset.Opened) Recordset.Open();
				}
			}
		}

		#endregion

		#region Events

		/// <summary>
		/// Delegate to perform the Move operation after a Move event is executed in the data source. Normally, it executes
		/// the Bind method for Bindings property to syncronize the bindings with the data source.
		/// </summary>
		/// <param name="sender">Record set being moved</param>
		/// <param name="e">Move event args</param>
		public void Recordset_MoveComplete(Object sender, MoveCompleteEventArgs e)
		{
			//TODO: how to get View containing this ADODC
			foreach (IViewModel v in ViewManager.LoadedViews)
			{
				var dataBindingsPi = v.GetType().GetProperty("DataBindings");
				if (dataBindingsPi != null)
				{
					var dataBindings = dataBindingsPi.GetValue(v, null);
					var bindMi = dataBindings.GetType().GetMethod("Bind");
					bindMi.Invoke(dataBindings, new[] { v, sender });
				}
			}
		}
		#endregion

		[Handler()]
		internal void b_first_Click(Object eventSender, EventArgs eventArgs)
		{
			if (Recordset.RecordCount > 0)
			{
				Recordset.MoveComplete += Recordset_MoveComplete;
				Recordset.MoveFirst();
			}
		}

		[Handler()]
		internal void b_prev_Click(Object eventSender, EventArgs eventArgs)
		{
			if (Recordset.CanMovePrevious)
			{
				Recordset.MoveComplete += Recordset_MoveComplete;
				Recordset.MovePrevious();
			}
		}

		[Handler()]
		internal void b_next_Click(Object eventSender, EventArgs eventArgs)
		{
			if (Recordset.CanMoveNext)
			{
				Recordset.MoveComplete += Recordset_MoveComplete;
				Recordset.MoveNext();
			}
		}

		[Handler()]
		internal void b_last_Click(Object eventSender, EventArgs eventArgs)
		{
			if (Recordset.RecordCount > 0)
			{
				Recordset.MoveComplete += Recordset_MoveComplete;
				Recordset.MoveLast();
			}
		}
	}

	public class ADODataControlHelperController : Controller
	{
		/// <summary>
		/// Handles the Click event for first button for ADODC control
		/// </summary>
		/// <param name="usercontrol"></param>
		/// <returns>The ActionResult</returns>
		public ActionResult b_first_Click(ADODataControlHelper usercontrol)
		{
			usercontrol.b_first_Click(null, null);
			return new AppChanges();
		}

		/// <summary>
		/// Handles the Click event for previous button for ADODC control
		/// </summary>
		/// <returns>The ActionResult</returns>
		public ActionResult b_prev_Click(ADODataControlHelper usercontrol)
		{
			usercontrol.b_prev_Click(null, null);
			return new AppChanges();
		}

		/// <summary>
		/// Handles the Click event for next button for ADODC control
		/// </summary>
		/// <returns>The ActionResult</returns>
		public ActionResult b_next_Click(ADODataControlHelper usercontrol)
		{
			usercontrol.b_next_Click(null, null);
			return new AppChanges();
		}

		/// <summary>
		/// Handles the Click event for last button for ADODC control
		/// </summary>
		/// <returns>The ActionResult</returns>
		public ActionResult b_last_Click(ADODataControlHelper usercontrol)
		{
			usercontrol.b_last_Click(null, null);
			return new AppChanges();
		}
	}
}
