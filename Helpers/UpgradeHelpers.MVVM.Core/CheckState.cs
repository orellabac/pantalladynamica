﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UpgradeHelpers.Helpers
{
    public enum CheckState
    {
        Unchecked,
        Checked,
		Indeterminate
    }
}
