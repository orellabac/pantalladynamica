﻿using System;
using UpgradeHelpers.Interfaces;
namespace UpgradeHelpers.Helpers
{
    public class Control
    {
        public static int Get_ModifierKeys()
        {
            return default(int);
        }
    }

    /// <summary>
    /// Stores a set of four integers that represent the location and size of a rectangle
    /// </summary>
    [Obsolete("Extension only provided for compilation purposes")]
    public class Rectangle
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }

    [Obsolete("Extension only provided for compilation purposes")]
    public static class FormViewModelExtensions
    {
        [Obsolete("Extension only provided for compilation purposes")]
        public static System.IntPtr Get_Handle(this IViewModel instance)
        {
            return default(System.IntPtr);
        }

        [Obsolete("Extension only provided for compilation purposes")]
        public static Rectangle Get_ClientRectangle(this IViewModel instance)
        {
            return new Rectangle();
        }

        [Obsolete("Extension only provided for compilation purposes")]
        public static void Set_Cursor(this IViewModel instance,Cursor cursor)
        {
            
        }

    }


	[Obsolete("Extension only provided for compilation purposes")]
	public static class ILogicExtensions
	{
		[Obsolete("Extension only provided for compilation purposes")]
		public static void Set_Cursor<TVM>(this ILogicWithViewModel<TVM> instance, Cursor cursor) where TVM : IViewModel 
		{

		}

	}


    public static class ControlExtensions
    {
        public static void Set_Cursor(this IControl instance, Cursor argName)
        {
        }
        public static System.IntPtr Get_Handle(this IControl instance)
        {
            return default(System.IntPtr);
        }
        public static Cursor Get_Cursor(this IControl instance)
        {
            return default(Cursor);
        }

        public static void Call_Refresh(this IControl instance)
        {
            
        }
    }
}