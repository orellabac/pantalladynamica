﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Drawing;

//public class PrinterHelper
//{
//    private static PrinterHelper _printer;
//    private double _currentX;
//    private double _currentY;
//    private HTML5Migration.Helpers.Font _font;

//    public PrinterHelper(Brush brush)
//        : this(null, brush)
//    {
//    }

//    protected PrinterHelper(string printerName, Brush brush)
//    {
//        //_bufferPage = new PageInfo();
//        //_port = GetPort();
//        //_driverName = GetDriverName();
//        //_innerPrinter.PrinterSettings.PrinterName = printerName;
//        //_innerPrinter.PrintController = new StandardPrintController();
//        //_innerPrinter.PrintPage += PrinterPrintPage;
//        //_innerPrinter.EndPrint += PrinterEndPrint;
//        //_brush = brush;
//    }
	
//    /// <summary>
//    /// Gets the default <code>PrinterHelper</code> object
//    /// </summary>
//    public static PrinterHelper Printer
//    {
//        get
//        {
//            if (_printer == null)
//                _printer = new PrinterHelper(null);
//            return _printer;
//        }
//        set
//        {
//            _printer = value;
//        }
//    }

//    /// <summary>
//    /// Returns or set the horizontal coordinate for the next printing or drawing method
//    /// Coordinates are expressed in the current unit of measurement defined by ScaleHeight, ScaleWidth, ScaleLeft,
//    /// ScaleTop and ScaleMode
//    /// </summary>
//    public double CurrentX
//    {
//        get { return _currentX; }
//        set { _currentX = value; }
//    }

//    /// <summary>
//    /// Returns or set the vertical coordinate for the next printing or drawing method
//    /// Coordinates are expressed in the current unit of measurement defined by ScaleHeight, ScaleWidth, ScaleLeft,
//    /// ScaleTop and ScaleMode
//    /// </summary>
//    public double CurrentY
//    {
//        get { return _currentY; }
//        set { _currentY = value; }
//    }

//    public HTML5Migration.Helpers.Font Font
//    {
//        get { return _font; }
//        set
//        {

//            //_font = value;
//            //_internalTextHeight = -1;
//            //SetFontToHdc(_font.Size);
//        }
//    }

//    public string DeviceName
//    {
//        //get { return _innerPrinter.PrinterSettings.PrinterName; }
//        get { return default(string); }
//    }

//}
