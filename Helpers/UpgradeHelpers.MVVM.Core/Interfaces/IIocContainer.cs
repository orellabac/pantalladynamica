﻿using System;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.Interfaces
{
    /// <summary>
    ///     Defines an object that implments the Dependency Injection pattern in order to provide WebMap
    ///     object instances.
    ///     <para>
    ///         All objects that are involved in their synchronization must be created by
    ///         calling any of the Resolve methods of this object.  Depending on its type, several new object properties
    ///         are injected with its values.
    ///     </para>
    ///     <para>
    ///         All created object are given an unique identifier, that helps in keeping track of the new object state for
    ///         synchronization purposes.
    ///     </para>
    ///     <para>Following are the types that can be be resolved using this object:</para>
    ///     <list type="bullet">
    ///         <item>
    ///             <see cref="IViewManager" />
    ///         </item>
    ///         <item>
    ///             <see cref="IDefaults" />
    ///         </item>
    ///         <item>
    ///             <see cref="ILogic" />.  Following properties are injected for new object:
    ///             <list type="bullet">
	///                 <item>Defaults, if the logic class implements <c>IDefaultInstances</c> interface</item>
	///                 <item>ViewModel, if the logic class implements <c>ILogicWithViewModel</c> interface</item>
	///                 <item>ViewManager, if the logic class implements <c>IInteractsWithView</c> interface</item>
	///                 <item>Container, if the logic class implements <c>ICreatesObjects</c> interface</item>
    ///             </list>
    ///             Additionally, if the concrete object type defines an Init method it is invoked./>
    ///         </item>
    ///         <item>
    ///             <see cref="IStateObject" />Object initialization depends on other interfaces implementation:
    ///             <list type="bullet">
    ///                 <item>
	///                     <see cref="IStateObjectWithInitialization" /> when it represents an <see cref="IModelList{T}" /> collection of <c>IStateObjectWithInitialization</c> objects then its size and <c>Container</c> properties
    ///                     are set.  Additionally if it is not an <see cref="IDependantViewModel"/> object then <c>Build</c> method is invoked.
    ///                 </item>
    ///                 <item>
    ///                     <see cref="IDependantViewModel" />Depending in other implemented interfaces:
    ///                     <list type="bullet">
    ///                         <item>
    ///                             <see cref="IInteractsWithView" /> then <c>ViewManager</c> property is injected.
    ///                         </item>
    ///                         <item>
    ///                             <see cref="ICreatesObjects" /> then <c>Container</c> property is injected.
    ///                         </item>
    ///                     </list>
    ///                 </item>
    ///             </list>
    ///         </item>
    ///     </list>
    /// </summary>
    public interface IIocContainer
    {
        /// <summary>
        ///     Creates a new object of the given type <c>t</c> with the given unique id generator.  New object properties are
        ///     injected according to the type of object being created.
        /// </summary>
        /// <param name="t">Type of the desired object.</param>
        /// <param name="getUniqueIdentifier">A function that generates object identifiers.</param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        /// <seealso cref="UpgradeHelpers.WebMap.Server.UniqueIDGenerator" />
        object Resolve(Type t, Func<string> getUniqueIdentifier, params object[] parameters);


        /// <summary>
        ///     Creates a new object of the given type <c>t</c>.  New object properties are
        ///     injected according to the type of object being created.
        ///     <para>
        ///         Object id is assigned by using the unique id generated returned by <c>UniqueIDGenerator.Current</c>
        ///     </para>
        /// </summary>
        /// <param name="t">Type of the desired object.</param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        /// <seealso cref="UpgradeHelpers.WebMap.Server.UniqueIDGenerator" />
        object Resolve(Type t, params object[] parameters);

        /// <summary>
        ///     Creates a new object of the given type <c>T</c>.  New object properties are
        ///     injected according to the type of object being created.
        ///     <para>
        ///         Object id is assigned by using the unique id generated returned by <c>UniqueIDGenerator.Current</c>
        ///     </para>
        /// </summary>
        /// <typeparam name="T">Type of the desired object.</typeparam>
        /// <param name="parameters"></param>
        /// <returns></returns>s
        /// <seealso cref="UpgradeHelpers.WebMap.Server.UniqueIDGenerator" />
        T Resolve<T>(params object[] parameters);


        /// <summary>
        /// Binds a property from <c>obj</c> to a property of <c>ds</c>.  Once property is bound then its values
        /// are synchronized.
        /// </summary>
		/// <param name="objproperty">Name of the <see cref="IStateObject"/> object property to bind.  This property can be sent to the client tier in order to
        /// show value to customers.</param>
		/// <param name="obj">The <c>IStateObject</c> object owning the property to bind.</param>
        /// <param name="dsproperty">Name of the datasource property to bind.</param>
        /// <param name="ds">Datasource object</param>
		void Bind(string objproperty, IStateObject obj, string dsproperty, object ds);
    }
}

namespace UpgradeHelpers.BasicViewModels
{
  
    public class IocContainerDataBindings
    {
        private readonly IIocContainer _ctx;
		private readonly IStateObjectWithInitialization _model;

		public IocContainerDataBindings(IStateObjectWithInitialization model, IIocContainer ctx)
        {
            _ctx = ctx;
            _model = model;
        }

        public void Add(string objproperty, object ds, string dsproperty)
        {
            _ctx.Bind(objproperty, _model, dsproperty, ds);
        }
    }
}