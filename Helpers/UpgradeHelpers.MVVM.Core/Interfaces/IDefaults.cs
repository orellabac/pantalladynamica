﻿namespace UpgradeHelpers.Interfaces
{
    /// <summary>
    /// Marks an object to indicate that this objects holds default instances.  Default instances have special handling thru 
    /// all WebMap application cycle including automatic injection of an instance of <c>IDefaults</c> to all objects implementing
    /// either "ILogicClass{TD}" or "ILogicView{T,TD}" where TD is IDefaults.
    /// <para>At conversion time, WebMap detects usages of default instances (VB6 inherited concept) and it generates a new IDefaults class containing
    /// a property for every default instance object.  This class is instantiated just ones and its property values shared by
    /// all ILogicView or ILogicClass objects</para>
    /// <seealso cref="ILogicClass{TD}"/>
    /// <seealso cref="ILogicView{T,TD}"/>
    /// </summary>
    public interface IDefaults
    {
        
    }
}