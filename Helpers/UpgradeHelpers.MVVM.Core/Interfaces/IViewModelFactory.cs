﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//AIS-TODO: This namespace must be changed according to each client
namespace UpgradeHelpers.Interfaces
{
    public interface IViewModelFactory
    {
        T New<T>(params object[] createParams) where T : IView, new();
    }
}
