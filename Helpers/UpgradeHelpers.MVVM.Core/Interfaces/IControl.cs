﻿namespace UpgradeHelpers.Interfaces
{
	public interface IControl 
    {
        string UniqueID { get; }

        string Name { get; set; }

		bool Enabled { get; set; }
		
		string Tag { get; set; }

		bool Visible { get; set; }

		Helpers.Font Font { get; set; }

		int Top { get; set; }

		int Left { get; set; }

		int Height { get; set; }

		int Width { get; set; }

		int TabIndex { get; set; }

        Helpers.Color BackColor { get; set; }

        Helpers.Color ForeColor { get; set; }

		void BringToFront();
    }
}
