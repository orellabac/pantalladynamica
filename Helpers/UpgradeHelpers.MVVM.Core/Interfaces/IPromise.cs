﻿namespace UpgradeHelpers.Interfaces
{
    /// <summary>
    /// Defines a delegate containing the code that will be registered as promise.
    /// </summary>
    public delegate void PromisedCode();


	public delegate void PromisedCodeWithLogic(ILogicWithViewModel<IViewModel> logic);


    /// <summary>
    /// Defines an object that can generate promises to be executed in next server request. A promise
    /// allows to cut code execution in order to allow end user interaction and then execute a piece of
    /// code depending on user input.  Common usages of promises include:
    /// <list type="bullet">
    /// <item>Modal or dialog like views.</item>
    /// <item>Messages box requesting for user input such as action confirmiations.</item>
    /// </list> 
    /// 
    /// </summary>
    /// <seealso cref="IViewManager"/>
    /// <seealso cref="UpgradeHelpers.WebMap.Controls.Controllers.ResumeOperationController"/>
    public interface IPromise
    {
        /// <summary>
        /// Creates a new promise for the given code. 
        /// </summary>
        /// <param name="code">The code to execute next server request.</param>
        /// <returns>An <c>IPromise</c> object so a new promises can be chained to just created one.</returns>
        IPromise Then(PromisedCode code);

		IPromise ThenWithLogic(PromisedCodeWithLogic code);
    }
}