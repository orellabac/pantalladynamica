﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.WebMap.Server.Helpers;
using UpgradeHelpers.WebMap.Server.Interfaces;

namespace UpgradeHelpers.Interfaces
{
	public delegate DataTable DataSourceProviderDelegate();

    public interface IViewManager : IPromise
    {

		string RequestedInput { get; set; }
		DialogResult InteractionResult { get; set; }

		IEnumerable<IViewModel> LoadedViews { get; }
		IPromise NavigateToView(ILogicWithViewModel<IViewModel> logic, bool isModal = false);
		IPromise ShowMessage(string message, string caption = "", BoxButtons buttons = BoxButtons.OK, BoxIcons boxIcons = BoxIcons.None,
			string promptMessage = "", bool inputRequest = false);

		void DisposeView(ILogicWithViewModel<IViewModel> logic);

        void SetCurrent(IDependantViewModel item);
        void SetCurrentView(IViewModel item);

        IEventAggregator Events { get; }
	    string CurrentCulture { get; set; }

        void SetInfoForDownload(string filename, string contentType);

		/// <summary>
		/// Indicates whether the application is currenly running a state management phase or not.
		/// </summary>
		bool IsInStateManagementPhase { get; }

		/// <summary>
		/// Hides the give view
		/// </summary>
		/// <param name="view">The view to hide</param>
		void HideView(ILogicWithViewModel<IViewModel> view);


		void ResumePendingOperation(string dialogResult, string requestedInput);


		/// <summary>
		/// Gets the <c>IDataPageableViewModel</c> object matching the given control id.
		/// </summary>
		/// <param name="controlId">The id of the control to get</param>
		/// <returns></returns>
		IDataPageableViewModel GetDataPageableViewModel(string controlId);

		IStateObject[] GetCollectionPage(string collectionUid, int take, int skip,out int total);

        /// <summary>
        /// Get the ParentViewModel
        /// </summary>
        /// <param name="getUniqueIdentifier">Function to calculate the parent ViewModel unique id</param>
        /// <returns></returns>
        IStateObject GetParentViewModel(Func<string> getUniqueIdentifier);
	}
}
