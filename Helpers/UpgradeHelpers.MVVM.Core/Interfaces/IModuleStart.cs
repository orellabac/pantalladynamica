﻿namespace UpgradeHelpers.Interfaces
{
    public interface IModuleStart
    {
        void Main(IIocContainer ctx);
    }
}