using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server.Interfaces
{
	public interface IViewModelCollection : IDependantViewModel, IInteractsWithView, ICreatesObjects
	{
		IModelList<IStateObject> _items { get; set; }
	}
}