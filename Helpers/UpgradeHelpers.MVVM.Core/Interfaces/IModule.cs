﻿namespace UpgradeHelpers.Interfaces
{
    /// <summary>
    /// Defines an object that is mainly a static class that holds functions but can also hold 
    /// global variables.
    /// This kind of programming is not usually found on native .net applications but it was
    /// found on programming platforms like vb6
    /// </summary>
    /// <typeparam name="TDefaults"></typeparam>
    public interface IModule<out TDefaults> where TDefaults : IDefaults
    {
        TDefaults Defaults { get; }
    }
}