﻿namespace UpgradeHelpers.Interfaces
{

    /// <summary>
    /// Defines an object containing state that must be kept between server requests.  An <c>IStateObject</c> object must be
    /// uniquely identified in order to be able to store/restore its state between requests.  
    /// <para>Unique ids are handled by <see cref="IIocContainer"/> object, it implies that all instances of objects implementing
    /// this interface must be created by one of <c>IIocContainer.Resolve</c> methods.
    /// </para>
    /// 
    /// </summary>
    /// <seealso cref="IIocContainer"/> 
    /// <seealso cref="IViewModel"/>
    /// <seealso cref="IDependantViewModel"/>
	/// <seealso cref="IStateObjectWithInitialization"/>
	public interface IStateObject
	{
        /// <summary>
        /// Gets/Sets the unique identifier of this <c>IStateObject</c> object.
        /// </summary>
        string UniqueID { get; set; }
	}
}
