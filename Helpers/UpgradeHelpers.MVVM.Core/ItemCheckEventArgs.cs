﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UpgradeHelpers.Helpers;

namespace UpgradeHelpers.Events
{
    public class ItemCheckEventArgs : EventArgs
    {
        public System.Int32 Index {get;set;}
        public CheckState NewCheckValue { get; set; }
        public CheckState CurrentValue { get; set; }

        public ItemCheckEventArgs(int index, CheckState newCheckValue, CheckState currentValue)
        {
            Index = index;
            NewCheckValue = newCheckValue;
            CurrentValue = currentValue;
        }

    }
}
