﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if !PORTABLE
using System.Runtime.Serialization;
#endif

using System.ComponentModel;
using UpgradeHelpers.Utils;

namespace UpgradeHelpers.Helpers
{
	public class DataGridRow : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged Members

		private void PropertyChanged_Handler(object sender, PropertyChangedEventArgs e)
		{
			var PropertyName = this.AssemblePropertyName(sender, e.PropertyName);
			PropertyName = PropertyName.Replace("SubItems.Items[", "SubItems["); 
			OnPropertyChanged(PropertyName);
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string property)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}
		#endregion

		/// <summary>
		/// Keeps track of which is the container
		/// </summary>
		protected internal DataGridRowCollection Owner { get; set; }

		/// <summary>
		/// Current index of this element
		/// </summary>
		public int Index { get { return Owner != null ? Owner.DataGridRows.IndexOf(this) : -1; } }

		private DataGridCellCollection _cells = null;
		/// <summary>
		/// Collection of subitems
		/// </summary>
		public DataGridCellCollection Cells
		{
			get
			{
				if (_cells == null)
				{
					_cells = new DataGridCellCollection();
					_cells.PropertyChanged += PropertyChanged_Handler;
					_cells.Owner = this;
				}
				return _cells;
			}
			set
			{
				if (_cells != value) 
				{
					if (_cells != null)
						_cells.PropertyChanged -= PropertyChanged_Handler;
					_cells = value;
					if (_cells != null)
					{
						_cells.PropertyChanged -= PropertyChanged_Handler;
						_cells.PropertyChanged += PropertyChanged_Handler;
						_cells.Owner = this;
					}
					OnPropertyChanged("SubItems");
				}
			}
		}


		/// <summary>
		/// Returns the forecolor of the elements
		/// </summary>
		private Color _ForeColor = new Color();
#if !PORTABLE
		[DataMember]
#endif
		public Color ForeColor
		{
			get
			{
				return _ForeColor;
			}
			set
			{
				if (_ForeColor != value)
				{
					_ForeColor = value ?? new UpgradeHelpers.Helpers.Color();
					OnPropertyChanged("ForeColor");
				}
			}
		}

		/// <summary>
		/// It is the element selected
		/// </summary>
		private bool _selected;
        public bool Selected
        {
            get { return _selected; }
            set
            {
                if (_selected != value)
                {
                    _selected = value;
                    OnPropertyChanged("Selected");
                    if (this.Owner.Owner != null && value)
                        this.Owner.Owner.FocusedRowIndex = this.Index;
                }
            }
        }

		/// <summary>
		/// Text of the datagrid row
		/// </summary>
		public string Text
		{
			get
			{
				return Owner != null ? (Owner.Data[Index].Count > 0 ? Convert.ToString(Owner.Data[Index][0]) : string.Empty) : string.Empty;
			}
			set
			{
				if (Owner != null)
				{
					if (!object.Equals(Owner.Data[Index][0], value))
					{
						Owner.Data[Index][0] = value;
						OnPropertyChanged("Text");
					}
				}
			}
		}
	}
}
