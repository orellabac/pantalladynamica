﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UpgradeHelpers.Helpers;
using System.ComponentModel;

namespace UpgradeHelpers.Helpers
{
	public class DataGridColumn : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string property)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}
		#endregion

		private string _text = string.Empty;
		/// <summary>
		/// Keeps the text of the column
		/// </summary>
		public string Text
		{
			get { return _text; }
			set
			{
				if (_text != value)
				{
					_text = value ?? string.Empty;
					OnPropertyChanged("Text");
				}
			}
		}

		private int _width;
		/// <summary>
		/// Keeps the width of the column
		/// </summary>
		public int Width
		{
			get { return _width; }
			set
			{
				if (_width != value)
				{
					_width = value;
					OnPropertyChanged("Width");
				}
			}
		}

	}
}
