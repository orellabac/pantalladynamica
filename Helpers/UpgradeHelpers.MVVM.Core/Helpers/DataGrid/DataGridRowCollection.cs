﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using UpgradeHelpers.Utils;

namespace UpgradeHelpers.Helpers
{
	public class DataGridRowCollection : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		private void PropertyChanged_Handler(object sender, PropertyChangedEventArgs e)
		{
			var PropertyName = this.AssemblePropertyName(sender, e.PropertyName);
			OnPropertyChanged(PropertyName);

			if (e.PropertyName.EndsWith("Text"))
				OnPropertyChanged("data");
		}

		protected void OnPropertyChanged(string property)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}

		#endregion

        /// <summary>
        /// Keeps track of which is the container
        /// </summary>
        protected internal DataGrid Owner { get; set; }

		/// <summary>
		/// Plain row data
		/// </summary>
		private List<List<object>> _data;
		public List<List<object>> Data
		{
			get
			{
				if (_data == null)
					_data = new List<List<object>>();

				return _data;
			}
			set //This should never happen unless it is during serialization
			{
				if (_data != value)
				{
					_data = value;
					OnPropertyChanged("data");
					SynchronizeDataGridRows();

				}
			}
		}

		/// <summary>
		/// DataGrid rows collection associated to the data
		/// </summary>
		private List<DataGridRow> _datagridRows;
		public List<DataGridRow> DataGridRows
		{
			get
			{
				if (_datagridRows == null)
				{
					_datagridRows = new List<DataGridRow>();
				}
				return _datagridRows;
			}
			set //This should never happen unless it is during serialization
			{
				if (_datagridRows != value)
				{
					if (_datagridRows != null && _datagridRows.Count > 0)
						CleanEvents();
					_datagridRows = value;
					SynchronizeDataGridRows();
                    OnPropertyChanged("DataGridRows");
				}
			}
		}

		private int _columnCount = 0;
		/// <summary>
		/// Count of columns (This should be the only way to add columns to the items)
		/// </summary>
		public int ColumnCount
		{
			get
			{
				if (Data.Count != 0)
					_columnCount = Data[0].Count;
				return _columnCount;
			}
			set
			{
				if (value != ColumnCount && value >= 0)
				{
					_columnCount = value;
					if (Data.Count == 0)
						return;

					var diff = value - Data[0].Count;
					foreach (var item in Data)
					{
						if (diff > 0)
							for (int i = 0; i < diff; i++) item.Add(null);
						else if (diff < 0)
							for (int i = diff; i < 0; i++) item.RemoveAt(item.Count - 1);
					}

					SynchronizeDataGridRows();
				}
			}
		}

		/// <summary>
		/// Count of items (This should be the only way to add or remove items)
		/// </summary>
		public int Count
		{
			get { return Data.Count; }
			set
			{
				if (value != Count && value >= 0)
				{
					if (value == 0)
					{
						Data = null;
						DataGridRows = null;
						return;
					}

					var diff = value - Data.Count;
					if (diff > 0)
					{
						for (int i = 0; i < diff; i++)
						{
							var newRow = new List<object>();
							for (int j = 0; j < ColumnCount; j++)
							{
								newRow.Add(null);
							}
							Data.Add(newRow);
						}
					}
					else if (diff < 0)
						for (int i = diff; i < 0; i++)
						{
							Data.RemoveAt(Data.Count - 1);
						}

					SynchronizeDataGridRows();
				}
			}
		}

		/// <summary>
		/// Returns a DataGrid row in a specific position
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public DataGridRow this[int index]
		{
			get
			{
				return DataGridRows[index];
			}
		}

		/// <summary>
		/// Add a new DataGrid row
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public DataGridRow Add(string text)
		{
			if (ColumnCount <= 0) ColumnCount = 1;
			Count++;

			

            string[] cellsText = text.Split('\t');
            if (cellsText.Length <= 1)
            {
                this[Count - 1].Text = text;
            }
            else
            {
                for (int i = 0; i <= cellsText.Length - 1; i++)
                {
                    this[Count - 1].Cells[i].Text = cellsText[i];
                }
            }
            
			return this[Count - 1];
		}

		/// <summary>
		/// Add a new DataGrid row
		/// </summary>
		/// <param name="text"></param>
		/// <param name="p_2"></param>
		/// <returns></returns>
		public DataGridRow Add(string text, string p_2)
		{
			return Add(text);
		}

		/// <summary>
		/// Clears the list of items
		/// </summary>
		public void Clear()
		{
			Count = 0;
			ColumnCount = 0;
		}

		/// <summary>
		/// Syncronize the DataGrid rows list with the data
		/// </summary>
		private void SynchronizeDataGridRows()
		{
			CleanEvents();
			//Syncronize the number of items
			int diff = Count - DataGridRows.Count;
			if (diff > 0)
			{
				for (int i = 0; i < diff; i++)
				{
					var newViewItem = new DataGridRow();
					DataGridRows.Add(newViewItem);
				}
			}
			else if (diff < 0)
			{
				for (int i = diff; i < 0; i++)
				{
					DataGridRows.RemoveAt(DataGridRows.Count - 1);
				}
			}

			SynchronizeOwnerAndEvents();
			//Syncronize the subitems
			if (Count > 0)
			{
				for (int i = 0; i < Count; i++)
				{
					DataGridRows[i].Cells.Count = ColumnCount;
				}
			}
		}

		/// <summary>
		/// Synchronize the owner property of the items
		/// </summary>
		private void SynchronizeOwnerAndEvents()
		{
			foreach(var item in DataGridRows)
			{
				item.Owner = this;
				item.PropertyChanged -= PropertyChanged_Handler;
				item.PropertyChanged += PropertyChanged_Handler;
			};
		}

		/// <summary>
		/// Clean the events attached to the items
		/// </summary>
		private void CleanEvents()
		{
			foreach(var item in DataGridRows)
			{
                item.PropertyChanged -= PropertyChanged_Handler;
            }
		}
	}
}
