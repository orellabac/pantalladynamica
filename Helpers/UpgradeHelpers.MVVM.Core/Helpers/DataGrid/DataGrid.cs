﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UpgradeHelpers.Interfaces;
#if !PORTABLE
using System.Runtime.Serialization;
#endif
using System.ComponentModel;
using UpgradeHelpers.Utils;

//AIS-TODO: This namespace must be changed according to each client
namespace UpgradeHelpers.Helpers
{
    public class DataGrid : IControl
    {
        public string UniqueID { get; set; }
        public DataGrid()
        {
            var propertyHandler = new PropertyChangedEventHandler(PropertyChanged_Handler);
            UniqueID = Guid.NewGuid().ToString().Replace("-","");
        }
        private void PropertyChanged_Handler(object sender, PropertyChangedEventArgs e)
        {
            var PropertyName = this.AssemblePropertyName(sender, e.PropertyName);
            PropertyName = PropertyName.Replace("Rows.DataGridRows[", "DataGridRows[");
            PropertyName = PropertyName.Replace("Columns.Items[", "Columns[");
            OnPropertyChanged(PropertyName);
        }
        #region Data Members
        private string _Name = string.Empty;
#if !PORTABLE
        [DataMember]
#endif
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (!object.Equals(_Name, value))
                {
                    _Name = value ?? string.Empty;
                    OnPropertyChanged("Name");
                }
            }
        }

        private bool _Enabled;
#if !PORTABLE
        [DataMember]
#endif
        public bool Enabled
        {
            get
            {
                return _Enabled;
            }
            set
            {
                if (!object.Equals(_Enabled, value))
                {
                    _Enabled = value;
                    OnPropertyChanged("Enabled");
                }
            }
        }

        private bool _Visible;
#if !PORTABLE
        [DataMember]
#endif
        public bool Visible
        {
            get
            {
                return _Visible;
            }
            set
            {
                if (!object.Equals(_Visible, value))
                {
                    _Visible = value;
                    OnPropertyChanged("Visible");
                }
            }
        }

        private Font _Font = new Font();
#if !PORTABLE        
        [DataMember]
#endif
        public Font Font
        {
            get
            {
                return _Font;
            }
            set
            {
                if (!object.Equals(_Font, value))
                {
                    _Font = value ?? new Font();
                    OnPropertyChanged("Font");
                }
            }
        }

        private int _Top;
#if !PORTABLE
        [DataMember]
#endif
        public int Top
        {
            get
            {
                return _Top;
            }
            set
            {
                if (!object.Equals(_Top, value))
                {
                    _Top = value;
                    OnPropertyChanged("Top");
                }
            }
        }

        
        
        private int _Left;
#if !PORTABLE
        [DataMember]
#endif
        public int Left
        {
            get
            {
                return _Left;
            }
            set
            {
                if (!object.Equals(_Left, value))
                {
                    _Left = value;
                    OnPropertyChanged("Left");
                }
            }
        }

        private int _Height;
#if !PORTABLE
        [DataMember]
#endif
        public int Height
        {
            get
            {
                return _Height;
            }
            set
            {
                if (!object.Equals(_Height, value))
                {
                    _Height = value;
                    OnPropertyChanged("Height");
                }
            }
        }

        private int _Width;
#if !PORTABLE
        [DataMember]
#endif
        public int Width
        {
            get
            {
                return _Width;
            }
            set
            {
                if (!object.Equals(_Width, value))
                {
                    _Width = value;
                    OnPropertyChanged("Width");
                }
            }
        }

        private int _TabIndex;
#if !PORTABLE
        [DataMember]
#endif
        public int TabIndex
        {
            get
            {
                return _TabIndex;
            }
            set
            {
                if (!object.Equals(_TabIndex, value))
                {
                    _TabIndex = value;
                    OnPropertyChanged("TabIndex");
                }
            }
        }

        private Color _BackColor = new Color();
#if !PORTABLE
        [DataMember]
#endif
        public Color BackColor
        {
            get
            {
                return _BackColor;
            }
            set
            {
                if (!object.Equals(_BackColor, value))
                {
                    _BackColor = value ?? new Color();
                    OnPropertyChanged("BackColor");
                }
            }
        }

        private Color _ForeColor = new Color();
#if !PORTABLE
        [DataMember]
#endif
        public Color ForeColor
        {
            get
            {
                return _ForeColor;
            }
            set
            {
                if (!object.Equals(_ForeColor, value))
                {
                    _ForeColor = value ?? new UpgradeHelpers.Helpers.Color();
                    OnPropertyChanged("ForeColor");
                }
            }
        }

        private string _Tag = string.Empty;
#if !PORTABLE
        [DataMember]
#endif
        public string Tag
        {
            get
            {
                return _Tag;
            }
            set
            {
                if (!object.Equals(_Tag, value))
                {
                    _Tag = value ?? string.Empty;
                    OnPropertyChanged("Tag");
                }
            }
        }

        private DataGridRowCollection _Rows;
#if !PORTABLE
        [DataMember]
#endif
        public DataGridRowCollection Rows
        {
            get
            {
                if (_Rows == null)
                {
                    _Rows = new DataGridRowCollection();
                    _Rows.Owner = this;
                    _Rows.PropertyChanged += PropertyChanged_Handler;
                }
                return _Rows;
            }
            set
            {
                if (!object.Equals(_Rows, value))
                {
                    if (_Rows != null)
                        _Rows.PropertyChanged -= PropertyChanged_Handler;
                    _Rows = value;
                    if (_Rows != null)
                    {
                        _Rows.PropertyChanged -= PropertyChanged_Handler;
                        _Rows.PropertyChanged += PropertyChanged_Handler;
                        _Rows.Owner = this;
                    }
                    OnPropertyChanged("Rows");
                }
            }
        }

        private DataGridColumnCollection _Columns;
#if !PORTABLE
        [DataMember]
#endif
        public DataGridColumnCollection Columns
        {
            get
            {
                if (_Columns == null)
                {
                    _Columns = new DataGridColumnCollection();
                    _Columns.PropertyChanged += PropertyChanged_Handler;
                }
                return _Columns;
            }
            set
            {
                if (!object.Equals(_Columns, value))
                {
                    if (_Columns != null)
                        _Columns.PropertyChanged -= PropertyChanged_Handler;
                    _Columns = value;
                    if (_Columns != null)
                    {
                        _Columns.PropertyChanged -= PropertyChanged_Handler;
                        _Columns.PropertyChanged += PropertyChanged_Handler;
                    }
                    OnPropertyChanged("Columns");
                }
            }
        }
        private int _FocusedRowIndex;
#if !PORTABLE
      [DataMember]
#endif
      public int FocusedRowIndex
      {
         get
         {
             return _FocusedRowIndex;
         }
         set
         {
             if (!object.Equals(_FocusedRowIndex, value))
            {
                _FocusedRowIndex = value;
                OnPropertyChanged("FocusedItemIndex");
            }
         }
      }
        #endregion
        public void RemoveItem(int index)
		{
            Rows.Data.RemoveAt(index);
            Rows.DataGridRows.RemoveAt(index);
		}

		public void SetColumnWidth(int pos, double value)
		{
            Columns[pos].Width = Convert.ToInt32(value);
		}
        public int ColumnCount
        {
            get
            {
                return Columns.Items.Count;
            }
            set
            {
                if (value > Columns.Items.Count)
                {
                    for (int i = Columns.Items.Count; i <= value; i++)
                    {
                        Columns.Add();
                    }
                }
            }
        }
            

		public void AddItem(string vsItem)
		{
            Rows.Add(vsItem);
		}

		public void SetValue(int row, int column, object value)
		{
            if (row == 0)
            {
                Columns[column].Text = value.ToString();
            }
            else
            {
                Rows[row].Cells[column].Text = value.ToString();
            }
            
		}
        public void BringToFront()
        {
            throw new NotImplementedException();
        }
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
        protected void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
