﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using UpgradeHelpers.Utils;

namespace UpgradeHelpers.Helpers
{
	public class DataGridCellCollection : INotifyPropertyChanged
	{
		/// <summary>
		/// Flag to indicate that a sync is pending
		/// </summary>
		private bool PendingSynchronizationWithDataOwner;

		#region INotifyPropertyChanged Members

		private void PropertyChanged_Handler(object sender, PropertyChangedEventArgs e)
		{
			var PropertyName = this.AssemblePropertyName(sender, e.PropertyName);
			OnPropertyChanged(PropertyName);
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string property)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}
		#endregion

		private DataGridRow _owner;
		/// <summary>
		/// Owner class of this one
		/// </summary>
		protected internal DataGridRow Owner
		{
			get { return _owner; }
			set
			{
				if (_owner != value)
				{
					_owner = value;
					SynchronizeOwnerAndEvents();
					if (PendingSynchronizationWithDataOwner)
						SynchronizeWithDataOwner();
				}
			}
		}

		private List<DataGridCell> _items;
		/// <summary>
		/// Internal list of SubItems, it should not be used directly
		/// </summary>
		public List<DataGridCell> Items
		{
			get
			{
				if (_items == null)
					_items = new List<DataGridCell>();
				return _items;
			}
			set
			{
				if (_items != value)
				{
					ClearEvents();
					_items = value;
					SynchronizeOwnerAndEvents();
					SynchronizeWithDataOwner();
					OnPropertyChanged("Items");
				}
			}
		}

		/// <summary>
		/// Returns/sets the count of subitems
		/// </summary>
		public int Count
		{
			get
			{
				return Items.Count;
			}
			set
			{
				if (value >= 0 && value != Items.Count)
				{
					ClearEvents();
					var diff = value - Items.Count;
					if (diff > 0)
					{
						for (int i = 0; i < diff; i++)
						{
							var newSubItem = new DataGridCell();
							Items.Add(newSubItem);
						}
					}
					else if (diff < 0)
					{
						for (int i = diff; i < 0; i++)
						{
							Items.RemoveAt(Items.Count - 1);
						}
					}
					SynchronizeOwnerAndEvents();
					SynchronizeWithDataOwner();
					OnPropertyChanged("Count");
				}
			}
		}

		/// <summary>
		/// Returns/sets a item
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public DataGridCell this[int index]
		{
			get
			{
				if (Count <= index)
				{
					Count = index + 1;
				}
				return Items[index];
			}
			set
			{
				if (value != null)
				{
					index--;
					if (Count <= index)
					{
						Count = index + 1;
					}

					if (value != Items[index])
					{
						value.Owner = this.Owner;
						OnPropertyChanged("Item");
					}
				}
			}
		}

		/// <summary>
		/// Add a new DataGrid cell
		/// </summary>
        /// <param name="DataGridCell"></param>
        public void Add(DataGridCell dataGridCell)
		{
            Items.Add(dataGridCell);
            dataGridCell.Owner = this.Owner;
            dataGridCell.PropertyChanged -= PropertyChanged_Handler;
            dataGridCell.PropertyChanged += PropertyChanged_Handler;
			SynchronizeWithDataOwner();
		}

		/// <summary>
		/// Synchronize with the data of the owner
		/// </summary>
		private void SynchronizeWithDataOwner()
		{
			if (Owner != null && Owner.Owner != null)
			{
				if (Owner.Owner.ColumnCount < (Count))
				{
					Owner.Owner.ColumnCount = Count;
				}
				PendingSynchronizationWithDataOwner = false;
			}
			else
				PendingSynchronizationWithDataOwner = true;
		}

		/// <summary>
		/// Synchronize the owner property of the subItems
		/// </summary>
		private void SynchronizeOwnerAndEvents()
		{
			foreach(var item in Items)
			{
				item.Owner = this.Owner;
				item.PropertyChanged -= PropertyChanged_Handler;
				item.PropertyChanged += PropertyChanged_Handler;
			};
		}

		/// <summary>
		/// Clear the events attached to the items
		/// </summary>
		private void ClearEvents()
		{
            foreach (var item in Items)
            {
                item.PropertyChanged -= PropertyChanged_Handler;
            }
		}

	}
}
