﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if !PORTABLE
using System.Runtime.Serialization;
#endif
using System.ComponentModel;

namespace UpgradeHelpers.Helpers
{
	public class DataGridCell : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string property)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}


		#endregion

		/// <summary>
		/// Forecolor of a datagrid cell
		/// </summary>
		private Color _ForeColor = new Color();
#if !PORTABLE
		[DataMember]
#endif 
		public Color ForeColor
		{
			get
			{
				return _ForeColor;
			}
			set
			{
				if (_ForeColor != value)
				{
					_ForeColor = value ?? new Color();
					OnPropertyChanged("ForeColor");
				}
			}
		}

		/// <summary>
		/// DataGrid Row owner of this Cell
		/// </summary>
		protected internal DataGridRow Owner { get; set; }

		private Font _Font = new Font();
		/// <summary>
		/// Font of the DataGridCell
		/// </summary>
		public Font Font
		{
			get
			{
				return _Font;
			}
			set
			{
				if (_Font != value)
				{
					_Font = value ?? new Font();
					OnPropertyChanged("Font");
				}
			}
		}

		/// <summary>
		/// Index of this sub item
		/// </summary>
		public int Index { get { return Owner != null ? Owner.Cells.Items.IndexOf(this) : -1; } }

		/// <summary>
		/// Text of the DataGrid Cell
		/// </summary>
		public string Text
		{
			get
			{
				if (Owner != null && Owner.Owner != null)
					return Convert.ToString(Owner.Owner.Data[Owner.Index][Index]);
				return string.Empty;
			}
			set
			{
				if (Owner != null && Owner.Owner != null && !object.Equals(Owner.Owner.Data[Owner.Index][Index], value))
				{
					Owner.Owner.Data[Owner.Index][Index] = value;
					OnPropertyChanged("Text");
				}
			}
		}

	}
}
