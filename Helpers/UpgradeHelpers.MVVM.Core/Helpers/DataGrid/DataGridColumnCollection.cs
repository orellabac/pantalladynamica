﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Utils;

namespace UpgradeHelpers.Helpers
{
	public class DataGridColumnCollection : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged Members

		private void PropertyChanged_Handler(object sender, PropertyChangedEventArgs e)
		{
			var PropertyName = this.AssemblePropertyName(sender, e.PropertyName);
			OnPropertyChanged(PropertyName);
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string property)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}
		#endregion

		/// <summary>
		/// DataGrid rows collection associated to the data
		/// </summary>
		private List<DataGridColumn> _items;
		public List<DataGridColumn> Items
		{
			get
			{
				if (_items == null)
					_items = new List<DataGridColumn>();
				return _items;
			}
			set //This should never happen unless it is during serialization
			{
				if (_items != value)
				{
					if (_items != null)
						ClearEvents();
					_items = value;
                    if (_items != null)
                        AttachEvents();
					OnPropertyChanged("Items");
				}
			}
		}


        /// <summary>
        /// Returns a Column item in a specific position
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public DataGridColumn this[int index]
        {
            get
            {
                return Items[index];
            }
        }


		/// <summary>
		/// Clears the list of columns
		/// </summary>
		public void Clear()
		{
			Items = null;
		}

		/// <summary>
		/// Add a new column to the collection
		/// </summary>
		/// <param name="text"></param>
		/// <param name="width"></param>
		/// <param name="horizontalAlignment"></param>
		public void Add(string text = "", int width = 0)
		{
			var item = new DataGridColumn();
            Items.Add(item);
            item.PropertyChanged += PropertyChanged_Handler;
            item.Text = text;
            item.Width = width;
            OnPropertyChanged("Items");
		}

		/// <summary>
		/// Clear the events attached to the items
		/// </summary>
		private void ClearEvents()
		{
            foreach (var item in Items)
            {
                item.PropertyChanged -= PropertyChanged_Handler;
            }
		}

        /// <summary>
        /// Attach events to each item
        /// </summary>
        private void AttachEvents()
        {
            foreach(var item in Items)
            { 
                item.PropertyChanged -= PropertyChanged_Handler;
                item.PropertyChanged += PropertyChanged_Handler; 
            };
        }

	}
}
