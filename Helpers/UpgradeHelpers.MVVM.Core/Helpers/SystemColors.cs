﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//AIS-TODO: This namespace must be changed according to each client
namespace UpgradeHelpers.Helpers
{
	public class SystemColors
	{
		public static Color ActiveBorder = "rgb(180, 180, 180)";
		public static Color ActiveCaption = "rgb(153, 180, 209)";
		public static Color ActiveCaptionText = "rgb(0, 0, 0)";
		public static Color AppWorkspace = "rgb(171, 171, 171)";
		public static Color ButtonFace = "rgb(240, 240, 240)";
		public static Color ButtonHighlight = "rgb(255, 255, 255)";
		public static Color ButtonShadow = "rgb(160, 160, 160)";
		public static Color Control = "rgb(240, 240, 240)";
		public static Color ControlDark = "rgb(160, 160, 160)";
		public static Color ControlDarkDark = "rgb(105, 105, 105)";
		public static Color ControlLight = "rgb(227, 227, 227)";
		public static Color ControlLightLight = "rgb(255, 255, 255)";
		public static Color ControlText = "rgb(0, 0, 0)";
		public static Color Desktop = "rgb(0, 0, 0)";
		public static Color GradientActiveCaption = "rgb(185, 209, 234)";
		public static Color GradientInactiveCaption = "rgb(215, 228, 242)";
		public static Color GrayText = "rgb(109, 109, 109)";
		public static Color Highlight = "rgb(51, 153, 255)";
		public static Color HighlightText = "rgb(255, 255, 255)";
		public static Color HotTrack = "rgb(0, 102, 204)";
		public static Color InactiveBorder = "rgb(244, 247, 252)";
		public static Color InactiveCaption = "rgb(191, 205, 219)";
		public static Color InactiveCaptionText = "rgb(67, 78, 84)";
		public static Color Info = "rgb(255, 255, 225)";
		public static Color InfoText = "rgb(0, 0, 0)";
		public static Color Menu = "rgb(240, 240, 240)";
		public static Color MenuBar = "rgb(240, 240, 240)";
		public static Color MenuHighlight = "rgb(51, 153, 255)";
		public static Color MenuText = "rgb(0, 0, 0)";
		public static Color ScrollBar = "rgb(200, 200, 200)";
		public static Color Window = "rgb(255, 255, 255)";
		public static Color WindowFrame = "rgb(100, 100, 100)";
		public static Color WindowText = "rgb(0, 0, 0)";
	}
}
