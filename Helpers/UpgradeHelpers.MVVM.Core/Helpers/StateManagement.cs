﻿using System;
using System.Reflection;

namespace UpgradeHelpers.Helpers
{
	/// <summary>
	/// Used to mark object as a singlenton
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class Singlenton : Attribute
	{
	}

	public enum StateManagementValues
	{
		Undefined,
		None,
		ClientOnly,
		ServerOnly,
		Both
	}

	/// <summary>
	/// This attribute is used to mark a method as an event handler
	/// If no parameter are given then it is assumed that the method follow the convention
	/// controlName_EventId
	/// </summary>
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class StateManagement : Attribute
	{
		private readonly bool _include;
		private readonly StateManagementValues _value;


		public bool Include
		{
			get { return _include; }

		}

		public StateManagementValues Value
		{
			get
			{
				return _value;
			}
		}

		public StateManagement(bool include = true)
		{
			_include = include;
			_value = StateManagementValues.Undefined;
		}


		public StateManagement(StateManagementValues value)
		{
			_value = value;
		}

		public bool RequiresStateManagement()
		{
			return (_value == StateManagementValues.Undefined && _include)
			       || (_value == StateManagementValues.Both) || (_value == StateManagementValues.ClientOnly)
			       || (_value == StateManagementValues.ServerOnly);

		}
	}

	public class PreserveUniqueId : Attribute
	{
		
	}

	/// <summary>
	/// This attribute is used to mark a property ViewModel as a Pointer
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class Pointer : Attribute
	{
	}

	public static class StateManagementUtils
	{
		public static bool MustIgnoreMember(bool forClient, MemberInfo member)
		{
			var atts = member.GetCustomAttributes(typeof(StateManagement), false);
			if (atts.Length <= 0) return false;

			var att = ((StateManagement)atts[0]);
			var value = att.Value;
			return value == StateManagementValues.Undefined
				? !att.Include
				: (forClient
					? value == StateManagementValues.None || value == StateManagementValues.ServerOnly
					: value == StateManagementValues.None || value == StateManagementValues.ClientOnly);
		}

		public static bool MustIgnoreMember(MemberInfo member)
		{
			var atts = member.GetCustomAttributes(typeof(StateManagement), false);
			return atts.Length > 0 && !((StateManagement) atts[0]).Include;
		}
	}
}
