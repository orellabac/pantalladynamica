﻿using System;


namespace UpgradeHelpers.Helpers
{
	/// <summary>
	/// Class to be used for the viewModels to be bound 
	/// </summary>
	public class Font
	{
		#region Constructors

		/// <summary>
		/// Default constructor for serialization
		/// </summary>
		public Font() { }

		/// <summary>
		/// Constructor with familyName, emSize and FontStyle
		/// </summary>
		/// <param name="familyName"></param>
		/// <param name="emSize"></param>
		/// <param name="style"></param>
		public Font(string familyName, float emSize, FontStyle style)
		{
			this.Name = familyName;
			this.Size = emSize;
			this.Style = style;
		}

        /// <summary>
        /// Constructor with familyName, emSize 
        /// </summary>
        /// <param name="familyName"></param>
        /// <param name="emSize"></param>
        public Font(string familyName, float emSize)
        {
            this.Name = familyName;
            this.Size = emSize;
        }

		/// <summary>
		/// Constructor with prototype and FontStyle
		/// </summary>
		/// <param name="prototype"></param>
		/// <param name="newStyle"></param>
		public Font(Font prototype, FontStyle newStyle)
		{
			this.Name = prototype.Name;
			this.Size = prototype.Size;
			this.Style = newStyle;
		}

		/// <summary>
		/// Constructor with familyName, size, style, unit and gdiCharSet
		/// </summary>
		/// <param name="familyName"></param>
		/// <param name="emSize"></param>
		/// <param name="style"></param>
		/// <param name="unit"></param>
		/// <param name="gdiCharSet"></param>
		public Font(string familyName, float emSize, FontStyle style, GraphicsUnit unit, byte gdiCharSet)
		{
			this.Name = familyName;
			this.Size = emSize;
			this.Style = style;
		}

		#endregion

		#region Properties to bind

		/// <summary>
		/// Only client-side code should change this values
		/// </summary>
		public string Font_Decoration
		{
			get { return _Font_Decoration; }
			set { _Font_Decoration = value ?? string.Empty; }
		}
		private string _Font_Decoration = string.Empty;
		

		private string _Font_family = string.Empty;
		/// <summary>
		/// Only client-side code should change this values
		/// </summary>
		public string Font_family
		{
			get { return _Font_family; }
			set { _Font_family = value ?? string.Empty; }
		}
		private string _Font_size = string.Empty;
		/// <summary>
		/// Only client-side code should change this values
		/// </summary>
		public string Font_size
		{
			get { return _Font_size; }
			set
			{
				_Font_size = value ?? string.Empty;
				if (!string.IsNullOrEmpty(_Font_size))
				{
					var tmp = value;
					if (tmp.EndsWith("px"))
					{
						tmp = tmp.Substring(0, tmp.Length - 2);

					}
					float floatValue;
					if (float.TryParse(tmp, out floatValue))
					{
						this._Size = (floatValue * 60f) / 72f;
					}
				}
				else
				{
					this._Size = 8.25f;
				}
			}
		}
		private string _Font_style = string.Empty;
		/// <summary>
		/// Only client-side code should change this values
		/// </summary>
		public string Font_style
		{
			get { return _Font_style; }
			set { _Font_style = value ?? string.Empty; }
		}
		private string _Font_variant = string.Empty;
		/// <summary>
		/// Only client-side code should change this values
		/// </summary>
		public string Font_variant
		{
			get { return _Font_variant; }
			set { _Font_variant = value ?? string.Empty; }
		}
		private string _Font_weight = string.Empty;
		/// <summary>
		/// Only client-side code should change this values
		/// </summary>
		public string Font_weight
		{
			get { return _Font_weight; }
			set { _Font_weight = value ?? string.Empty; }
		}

		#endregion

		/// <summary>
		/// This properties will be serialized but can't be used for binding
		/// </summary>
		#region .NET properties
		private FontStyle _Style;
		/// <summary>
		/// Only server-side code should change this values
		/// </summary>
        public virtual FontStyle Style
        {
            get { return _Style; }
            private set
            {
                bool hasFontDecoration = false;
                _Style = value;
                if ((_Style & FontStyle.Bold) == FontStyle.Bold)
                {
                    this._Font_weight = "bold";
                    this.Bold = true;
                }
                else
                    this._Font_weight = "normal";

                if ((_Style & FontStyle.Italic) == FontStyle.Italic)
                {
                    this._Font_style = "italic";
                    this.Italic = true;
                }
                else
                    this._Font_style = "normal";

                if ((_Style & FontStyle.Strikeout) == FontStyle.Strikeout)
                {
                    this.Font_Decoration = "line-through";
                    this.Strikeout = true;
                    hasFontDecoration = true;
                }

                else
                    this.Font_Decoration = "none";

                if ((_Style & FontStyle.Underline) == FontStyle.Underline)
                {
                    this.Font_Decoration = "underline";
                    this.Underline = true;
                }
                else if (!hasFontDecoration)
                    this.Font_Decoration = "none";
            }
        }
		private string _Name = string.Empty;
		/// <summary>
		/// Only server-side code should change this values
		/// </summary>
        public virtual string Name
		{
			get { return _Name; }
			set { _Name = value ?? string.Empty; }
		}
		private float _Size;
		/// <summary>
		/// Only server-side code should change this values
		/// </summary>
        public virtual float Size
		{
			get { return _Size; }
			private set
			{
				_Size = value;
                SizeInPoints = value;
				if (_Size == 8.25 || _Size == 0)
					_Font_size = string.Empty;
				else
					_Font_size = ((_Size * 72F) / 60F) + "px";
			}
		}

        public virtual bool Bold {  set; get; }
        public virtual bool Italic { set; get; }
        public virtual bool Strikeout { set; get; }
        public virtual bool Underline { set; get; }
        public virtual float SizeInPoints { set; get; }

		#endregion
	}

	// Summary:
	//     Specifies style information applied to text.
	[Flags]
	public enum FontStyle
	{
		// Summary:
		//     Normal text.
		Regular = 0,
		//
		// Summary:
		//     Bold text.
		Bold = 1,
		//
		// Summary:
		//     Italic text.
		Italic = 2,
		//
		// Summary:
		//     Underlined text.
		Underline = 4,
		//
		// Summary:
		//     Text with a line through the middle.
		Strikeout = 8,
	}

	// Summary:
	//     Specifies the unit of measure for the given data.
	public enum GraphicsUnit
	{
		// Summary:
		//     Specifies the world coordinate system unit as the unit of measure.
		World = 0,
		//
		// Summary:
		//     Specifies the unit of measure of the display device. Typically pixels for
		//     video displays, and 1/100 inch for printers.
		Display = 1,
		//
		// Summary:
		//     Specifies a device pixel as the unit of measure.
		Pixel = 2,
		//
		// Summary:
		//     Specifies a printer's point (1/72 inch) as the unit of measure.
		Point = 3,
		//
		// Summary:
		//     Specifies the inch as the unit of measure.
		Inch = 4,
		//
		// Summary:
		//     Specifies the document unit (1/300 inch) as the unit of measure.
		Document = 5,
		//
		// Summary:
		//     Specifies the millimeter as the unit of measure.
		Millimeter = 6,
	}

}