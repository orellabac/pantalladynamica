﻿using System.Collections.Generic;
using UpgradeHelpers.Interfaces;


namespace UpgradeHelpers.Helpers
{
#if PORTABLE
    static class TypeExtentions
    {
        /// <summary>Searches for the interface with the specified name.</summary>
        /// <param name="objectType">Type on which the interface will be searched</param>
        /// <param name="interfaceName">The <see cref="T:System.String" /> containing the name of the interface to get. For generic interfaces, this is the mangled name.</param>
        /// <returns>A <see cref="T:System.Type" /> object representing the interface with the specified name, implemented or inherited by the current <see cref="T:System.Type" />, if found; otherwise, null.</returns>
        public static Type GetInterface(this Type objectType, string interfaceName)
        {
            foreach (var iinterface in objectType.GetInterfaces())
            {
                if (iinterface.FullName.Equals(interfaceName))
                    return iinterface;
            }
            return null;
        }
    }
#endif

	public class ContainerHelper
	{


		public static IList<IControl> Controls(IViewModel form)
		{
			var result = new List<IControl>();
			foreach (var prop in form.GetType().GetProperties())
			{
				var propType = prop.PropertyType;
				if (propType.GetInterface(typeof(IControl).FullName) != null)
				{
					var value = prop.GetValue(form, null);
					if (value != null)
					{
						result.Add(value as IControl);
					}
				}
				else if (propType.GetInterface(typeof(System.Collections.IList).FullName) != null)
				{
					var elementType = typeof(System.Collections.IList);
					if (propType.IsArray)
					{
						elementType = propType.GetElementType();
					}
					else
					{
						elementType = propType.GetProperty("Item").PropertyType;
					}
					if (elementType.GetInterface(typeof(IControl).FullName)!=null)
					{
						foreach (IControl ctl in (System.Collections.IList)prop.GetValue(form, null))
						{
							if (ctl != null)
							{
								result.Add(ctl);
							}
						}
					}
				}
			}
			return result;
		}
	}
}
