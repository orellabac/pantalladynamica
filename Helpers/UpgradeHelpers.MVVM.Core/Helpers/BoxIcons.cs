﻿namespace UpgradeHelpers.Helpers
{
	public enum BoxIcons
	{
		None,
		Question,
		Exclamation,
		Error,
		Information
	}
}
