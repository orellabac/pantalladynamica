﻿using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.Helpers
{
	public class ControlHelper
	{
		public static void SetEnabled(IControl ctl, bool bEnable)
		{
			ctl.Enabled = bEnable;
		}

		public static bool GetEnabled(IControl ctl)
		{
			return ctl.Enabled;
		}

		public static object GetTag(IControl ctl)
		{
			return ctl.Tag;
		}
	}
}