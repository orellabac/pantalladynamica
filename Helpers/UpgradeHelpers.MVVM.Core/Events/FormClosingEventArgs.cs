﻿using System;
using System.ComponentModel;

namespace UpgradeHelpers.Events
{
    /// <summary>
    ///     This class defines the closing event args event for compilation purposes only.
    /// </summary>
    public class FormClosingEventArgs : CancelEventArgs
    {
        [Obsolete("only generated for compilation")]
        public FormClosingEventArgs(CloseReason closeReason, bool cancel)
        {
            throw new NotImplementedException();
        }

        public CloseReason CloseReason { get; set; }

        [Obsolete("only generated for compilation")]
        public int Get_CloseReason()
        {
            throw new NotImplementedException();
        }
    }
}