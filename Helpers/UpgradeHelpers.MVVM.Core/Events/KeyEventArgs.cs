﻿using System;

namespace UpgradeHelpers.Events
{
    /// <summary>
    ///     Event args class for key event handlers.  This class is provided for compilation purposes only,
    ///     because key events are not server propagated to server side, instead they are
    ///     converted to client side events (such as JavaScript)
    /// </summary>
    public class KeyEventArgs : EventArgs
    {
        public KeyEventArgs(Keys keyData)
        {
            // TODO: Complete member initialization
            KeyData = keyData;
        }

        public KeyEventArgs(int keyCode)
        {
            KeyData = (Keys) keyCode;
        }

        public Keys KeyData { get; set; }


        public bool Shift { get; set; }

        public int KeyCode { get; set; }
    }
}