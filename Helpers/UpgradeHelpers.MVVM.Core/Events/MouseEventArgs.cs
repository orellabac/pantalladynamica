﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UpgradeHelpers.Events
{
    /// <summary>
    ///     Event args class for Mouse event handlers.  This class is provided for compilation purposes only,
    ///     because mouse events are not server propagated to server side, instead they are
    ///     converted to client side events (such as JavaScript)
    /// </summary>
    public class MouseEventArgs: EventArgs
    {

        public MouseEventArgs(object button, int clicks, int x, int y, int delta) {
            this.Button = button;
            this.Clicks = clicks;
            this.X = x;
            this.Y = y;
            this.Delta = delta;
        }

       
        public object Button { get; set; }
        
        public int Clicks { get; set; }
        
        public int Delta { get; set; }
      
        public object Location { get; set; }
       
        public int X { get; set; }
       
        public int Y { get; set; }
    }
}