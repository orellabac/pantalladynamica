﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using Fasterflect;

namespace UpgradeHelpers.WebMap.Server
{
	public static class Bootstrapper
	{
		public static IUnityContainer Initialise()
		{
			IUnityContainer container = BuildUnityContainer();
			DependencyResolver.SetResolver(new WebMapResolver(container));
			ConventionBasedHelper.Container = IocContainerImplWithUnity.Current;
			return container;
		}

		internal static IUnityContainer BuildUnityContainer()
		{
			IocContainerImplWithUnity container = IocContainerImplWithUnity.Current;
			AppChanges.AppChangesDelegate = IocContainerImplWithUnity.GenerateAppChanges;
			return container.Container;
		}

		#region Nested type: WebMapResolver

		public class WebMapResolver : IDependencyResolver
		{
			private UnityDependencyResolver _resolver;

			public WebMapResolver(IUnityContainer container)
			{
				_resolver = new UnityDependencyResolver(container);
			}

			#region IDependencyResolver Members

			public object GetService(Type serviceType)
			{
				object obj = _resolver.GetService(serviceType);

				if (typeof (Controller).IsAssignableFrom(serviceType))
				{
					DoDefaultInjectionInController(obj, serviceType);
				}
				return obj;
			}

			public IEnumerable<object> GetServices(Type serviceType)
			{
				return _resolver.GetServices(serviceType);
			}

			#endregion

			internal static void DoDefaultInjectionInController(object controller, Type controllerType)
			{
				PropertyInfo logicProperty = controllerType.Property("logic");
				if (logicProperty != null)
				{
					object logic = IocContainerImplWithUnity.Current.Resolve(logicProperty.PropertyType, new[] {IocContainerImplWithUnity.NoView});
					logicProperty.SetValue(controller, logic, null);
				}

				var viewManagerProperty = controllerType.Property("viewManager");
				if (viewManagerProperty != null)
				{
					viewManagerProperty.SetValue(controller, ViewManager.Instance, null);
				}
				else
				{
					viewManagerProperty = controllerType.Property("ViewManager");
					if (viewManagerProperty != null)
					{
						viewManagerProperty.SetValue(controller, ViewManager.Instance, null);
					}
				}

				var  currentCulture = ViewManager.Instance.CurrentCulture;
				if (currentCulture != null)
				{
					Thread.CurrentThread.CurrentCulture = new CultureInfo(currentCulture);
					Thread.CurrentThread.CurrentUICulture = new CultureInfo(currentCulture);
				}

			}
		}

		#endregion
	}
}