﻿using System;
using System.Linq;
using System.Reflection;
using Fasterflect;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
	internal class StateObjectSerializer : JsonConverter
	{
		private readonly bool writeTypeInfo;
		private readonly bool writeUniqueID;

		public StateObjectSerializer(bool writeTypeInfo, bool writeUniqueID)
		{
			this.writeTypeInfo = writeTypeInfo;
			this.writeUniqueID = writeUniqueID;
		}

		public StateObjectSerializer() : this(true, true)
		{
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			writer.WriteStartObject();
			Type valueType = value.GetType();
			if (writeTypeInfo)
			{
				writer.WritePropertyName("__type");

				if (IsGeneratedType(valueType))
				{
					valueType = valueType.BaseType;
				}
				string assemblyQualifiedName = valueType.AssemblyQualifiedName;
				writer.WriteValue(assemblyQualifiedName);
			}

			foreach (PropertyInfo prop in value.GetType().GetProperties())
			{

				if (typeof (IDefaults).IsAssignableFrom(prop.PropertyType) || typeof (IStateObject).IsAssignableFrom(prop.PropertyType) ||
					typeof (IViewManager).IsAssignableFrom(prop.PropertyType) 
					|| typeof (IIocContainer).IsAssignableFrom(prop.PropertyType) || SurrogatesDirectory.IsSurrogateRegistered(prop.PropertyType)
					|| StateManagementUtils.MustIgnoreMember(true, prop))
				{
					continue;
				}

				if (!typeof (IDependantViewModel).IsAssignableFrom(prop.PropertyType))
				{
					//This check might be innecessary if we are serializing Deltas because the delta should not include the UniqueID
					//if (!writeUniqueID && prop.Name == "UniqueID") continue;
					writer.WritePropertyName(prop.Name);
					serializer.Serialize(writer, prop.GetValue(value, null));
				}
			}

			writer.WriteEndObject();
		}

		private bool IsGeneratedType(Type valueType)
		{
			return valueType.Assembly.FullName.StartsWith("Unity");
		}

		public bool IsControlArray(Type type)
		{
			if (type.IsGenericType &&
			    (type.GetGenericTypeDefinition() == typeof (IocContainerImplWithUnity.ObservableCollectionEx<>) ||
			     (type.BaseType != null && type.BaseType.GetGenericTypeDefinition() == typeof (IocContainerImplWithUnity.ObservableCollectionEx<>))))
			{
				return true;
			}
			return false;
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			JObject jobj = JObject.Load(reader);
			var assemblyQualifiedType = jobj["__type"].Value<string>();
			Type targetType = Type.GetType(assemblyQualifiedType, false, false);
			if (IsControlArray(targetType))
			{
				var size = jobj["Count"].Value<int>();
				var uid = jobj["UniqueID"].Value<string>();
				dynamic value = IocContainerImplWithUnity.Current.ResolveUnPrepared(targetType);
				value.SetupLazy(size, uid);
				return value;
			}
			object newInstance = IocContainerImplWithUnity.Current.ResolveUnPrepared(targetType);
			foreach (JProperty prop in jobj.Properties().ToList())
			{
				if (prop.Name == "__type") continue;
				PropertyInfo reflectedProperty = targetType.Property(prop.Name);
				object adapterValue = Convert.ChangeType(prop.Value, reflectedProperty.PropertyType);
				reflectedProperty.SetValue(newInstance, adapterValue, null);
			}
			return newInstance;
		}

		public override bool CanConvert(Type objectType)
		{
			return typeof (IStateObject).IsAssignableFrom(objectType);
		}
	}

	internal class StateObjectSerializerIndex : StateObjectSerializer
	{
		public StateObjectSerializerIndex(bool writeTypeInfo, bool writeUniqueID) : base(writeTypeInfo, writeUniqueID)
		{
		}

		public StateObjectSerializerIndex() : this(true, true)
		{
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			Type valueType = value.GetType();
			if (IsControlArray(valueType))
			{
				writer.WriteStartObject();
				dynamic coll = value;
				writer.WritePropertyName("UniqueID");
				writer.WriteValue((string) coll.UniqueID);
				writer.WritePropertyName("Count");
				writer.WriteValue((int) coll.Count);
				writer.WritePropertyName("@arr");
				writer.WriteValue("1");
				writer.WriteEndObject();
			}
			else
			{
				base.WriteJson(writer, value, serializer);
			}
		}
	}
}