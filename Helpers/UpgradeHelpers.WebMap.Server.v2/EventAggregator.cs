﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Fasterflect;
using UpgradeHelpers.Events;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
	internal class EventAggregator : IEventAggregator
	{

		static Dictionary<Type, Type> MapHandlers = new Dictionary<Type, Type>();

		static EventAggregator()
		{
			MapHandlers.Add(typeof(KeyPressEventArgs), typeof(KeyPressEventHandler));
			MapHandlers.Add(typeof(KeyEventArgs), typeof(KeyEventHandler));
			MapHandlers.Add(typeof(EventArgs), typeof(EventHandler));
			MapHandlers.Add(typeof(ListViewItemSelectionChangedEventArgs), typeof(ListViewItemSelectionChangedEventHandler));
            MapHandlers.Add(typeof(MouseEventArgs), typeof(MouseEventHandler));
            MapHandlers.Add(typeof(TypeValidationEventArgs), typeof(TypeValidationEventHandler));
            MapHandlers.Add(typeof(MaskInputRejectedEventArgs), typeof(MaskInputRejectEventHandler));
		}



		private readonly UniqueIDGenerator _uniqueIdGenerator;
		private readonly StateCache _cache;
		public EventAggregator(UniqueIDGenerator uniqueIdGenerator, StateCache cache)
		{
			this._uniqueIdGenerator = uniqueIdGenerator;
			this._cache = cache;

		}

        public void Subscribe<T>(string eventId, IStateObject component, ILogicWithViewModel<T> logic, string methodName)
             where T : IStateObject
        {
            var type = logic.GetType();
            var method = type.Method(methodName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            RegisterMethodForEvent(logic, method, component.ToString(), eventId, component);

        }


		public void Subscribe(string eventId, IStateObject obj, Delegate handler)
		{
			eventId = eventId.ToUpper();
			object instance = handler.Target;
			if (!(instance is ILogicWithViewModel<IViewModel>))
			{
				throw new NotSupportedException("Handler can only be defined on classes implemented ILogicView<IViewModel,IDefaults>");
			}
			var eventSubscriptionId = _uniqueIdGenerator.GetRelativeUniqueID(obj, "%" + eventId);
			Debug.Assert(StateCache.Current.GetObject(eventSubscriptionId) == null);
			var declaringType = handler.Target.GetType();
			var methodName = handler.Method.Name;
			var eventHandlerInfo = new ContinuationInfo
			{
				DeclaringType = declaringType.AssemblyQualifiedName,
				IStateObject_UniqueID = eventId,
				MethodName = methodName,
				UniqueID = eventSubscriptionId
			};
			_cache.AddNewObject(eventHandlerInfo);
		}

		public void Publish(string eventId, IStateObject source, EventArgs args)
		{
			if (suspendedState.Any()) return;
			eventId = eventId.ToUpper();
			var eventSubscriptionId = _uniqueIdGenerator.GetRelativeUniqueID(source, "%" + eventId);
			var eventHandlerInfo = StateCache.Current.GetObject(eventSubscriptionId) as ContinuationInfo;
			if (eventHandlerInfo != null)
			{
				var type = Type.GetType(eventHandlerInfo.DeclaringType);
				var method = type.Method(eventHandlerInfo.MethodName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
				IViewModel parent = StateCache.Current.GetParentViewModel(source);
				var logic = (ILogicWithViewModel<IViewModel>)IocContainerImplWithUnity.Current.Resolve(type, new [] { parent });
				method.Invoke(logic, new object[] { source, args });
			}
		}

		public void UnSubscribe(string eventId, IStateObject source)
		{
			eventId = eventId.ToUpper();
			var eventSubscriptionId = _uniqueIdGenerator.GetRelativeUniqueID(source, "%" + eventId);
			_cache.RemoveObject(eventSubscriptionId);
		}

		public void AutoWireEvents<T>(ILogicWithViewModel<T> logic) where T : IStateObject
		{
			foreach (MethodInfo method in logic.GetType().Methods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
			{
				var attr = (Handler)Attribute.GetCustomAttribute(method, typeof(Handler));
				if (attr != null)
				{
					string componentName;
					string eventId;
					if (attr.IsDefault)
					{
						var methodNameParts = SplitMethodAndEventParts(method.Name);//method.Name.Split(new char[] {'_'},StringSplitOptions.RemoveEmptyEntries);

						Debug.Assert(methodNameParts.Count() == 2);
						componentName = methodNameParts[0];
						eventId = methodNameParts[1];
					}
					else
					{
						componentName = attr.GetControl();
						eventId = attr.GetEvent();
					}
                    var componentProp = logic.ViewModel.GetType().Property(componentName);
                    if (componentProp != null)
                    {
                        var component = componentProp.GetValue(logic.ViewModel, null) as IStateObject;

                        RegisterMethodForEvent<T>(logic, method, componentName, eventId, component);
                    }

				}
			}
		}

        private void RegisterMethodForEvent<T>(ILogicWithViewModel<T> logic, MethodInfo method, string componentName, string eventId, IStateObject component) where T : IStateObject
        {
					var parameters = method.GetParameters();
					if (parameters.Length == 2)
					{
						var eventArgsType = parameters[1].ParameterType;
						Type eventHandlerType = null;
						if (MapHandlers.TryGetValue(eventArgsType, out eventHandlerType))
						{
								Debug.Assert(component != null, "Invalid AutoWire for event handler " + method.Name + " component " + componentName + " was not found in viewmodel " + logic.ViewModel.GetType());
								if (component != null)
									Subscribe(eventId, component,
										Delegate.CreateDelegate(eventHandlerType, logic, method));
							}
						else
						{
							Debug.Fail("Support has not been implement for this handler type:" + eventArgsType.FullName);
						}
					}
					else
					{
						Debug.Fail("Invalid Handler signature for method " + method.ToString() + " it should be in the form of handler(Object sender,<ArgsType> args)");

					}
				}

		private string[] SplitMethodAndEventParts(string eventHandlerName)
		{
			var match = Regex.Match(eventHandlerName, "^(.+)_([^_]+)$");
			if (match.Success)
			{
				return new string[] { match.Groups[1].Value, match.Groups[2].Value };
			}
			else
			{
				return new string[] { };
			}

		}



		Stack<bool> suspendedState = new Stack<bool>();
		public void Suspend()
		{
#if EVENTS_DEBUG
            Debug.WriteLine("Events Suspended");
#endif
			suspendedState.Push(true);

		}

		public void Resume()
		{
			if (suspendedState.Any())
				suspendedState.Pop();
#if EVENTS_DEBUG
            if (suspendedState.Count==0)
                 Debug.WriteLine("Events Resumed");
#endif
		}

		public bool IsSuspended()
		{
			return suspendedState.Any();
		}
	}
}
