﻿using System;
using System.Linq;
using System.Reflection;
using Fasterflect;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
	internal class LogicSingletonSerializer : JsonConverter
	{
		private readonly bool _writeTypeInfo;
		private readonly bool _writeUniqueId;

		public LogicSingletonSerializer(bool writeTypeInfo, bool writeUniqueId)
		{
			_writeTypeInfo = writeTypeInfo;
			_writeUniqueId = writeUniqueId;
		}

		public LogicSingletonSerializer()
			: this(true, true)
		{
		}

		/// <summary>
		/// Test if the given object has state to serialize or not
		/// </summary>
		/// <param name="value">The object to test for</param>
		/// <returns><c>true</c> if the given object has state to serialize, <c>false</c> otherwise</returns>
		public static bool HasSerializableState(object value)
		{
			foreach (PropertyInfo prop in value.GetType().GetProperties())
			{
				if (IsExcludedProperty(prop) || prop.Name == "UniqueID")
					continue;
				return true;
			}
			return false;
		}


		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			writer.WriteStartObject();
			if (_writeTypeInfo)
			{
				writer.WritePropertyName("__type");
				var valueType = value.GetType();
				if (IsGeneratedType(valueType))
				{
					valueType = valueType.BaseType;
				}
				if (valueType != null)
				{
					var assemblyQualifiedName = valueType.AssemblyQualifiedName;
					writer.WriteValue(assemblyQualifiedName);
				}
			}

			foreach (PropertyInfo prop in value.GetType().GetProperties())
			{
				if (IsExcludedProperty(prop))
					continue;
				//This check might be innecessary if we are serializing Deltas because the delta should not include the UniqueID
				if (!_writeUniqueId && prop.Name == "UniqueID") continue;
				writer.WritePropertyName(prop.Name);
				serializer.Serialize(writer, prop.GetValue(value, null));
			}
			writer.WriteEndObject();
		}

		private bool IsGeneratedType(Type valueType)
		{
			return valueType.Assembly.FullName.StartsWith("Unity");
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			JObject jobj = JObject.Load(reader);
			var assemblyQualifiedType = jobj["__type"].Value<string>();
			Type targetType = Type.GetType(assemblyQualifiedType, false, false);
			object newInstance = IocContainerImplWithUnity.Current.ResolveUnPrepared(targetType);
			foreach (JProperty prop in jobj.Properties().ToList())
			{
				if (prop.Name == "__type") continue;
				PropertyInfo reflectedProperty = targetType.Property(prop.Name);
				object adapterValue = Convert.ChangeType(prop.Value, reflectedProperty.PropertyType);
				reflectedProperty.SetValue(newInstance, adapterValue, null);
			}
			return newInstance;
		}

		public override bool CanConvert(Type objectType)
		{
			return typeof(ILogicClass).IsAssignableFrom(objectType);
		}

		/// <summary>
		/// Indicates if a property must be exlcuded from serialization or not
		/// </summary>
		/// <param name="prop"><c>PropertyInfo</c> object containing the information of the property to test for.</param>
		/// <returns><c>true</c> if the property must be excluded, <c>false</c> otherwise.</returns>
		private static bool IsExcludedProperty(PropertyInfo prop)
		{
			return typeof(IDependantViewModel).IsAssignableFrom(prop.PropertyType) ||
				   typeof(IViewModel).IsAssignableFrom(prop.PropertyType) ||
				   typeof(IDefaults).IsAssignableFrom(prop.PropertyType) ||
				   typeof(IIocContainer).IsAssignableFrom(prop.PropertyType) ||
				   typeof(IViewManager).IsAssignableFrom(prop.PropertyType);
		}

	}
}