﻿using System.Collections.Generic;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
	internal interface IStorageManager
	{
		IEnumerable<string> GetAllItems();
		/// <summary>
		/// Gets children for the object identified by given unique id
		/// </summary>
		/// <param name="uniqueId">The id of the object to get children for</param>
		/// <returns>An enumeration with object children</returns>
		IEnumerable<string> GetChildren(string uniqueId);
		IStateObject GetObject(string uniqueID);
		void SaveObject(IStateObject obj);
		void RemoveObject(string uniqueId);
	    void SaveRaw(string uniqueId,object rawData);
	    object GetRaw(string uniqueId);
	    void SaveSurrogate(StateObjectSurrogate surrogate);
		void SwitchUniqueIDsFrom(string itemUID, string lastUID);
	}

	internal class StorageManagerFactory
	{
		internal static bool UseTestImpl = false;

		internal static IStorageManager obj;

		internal static void ResetSingleton()
		{
			obj = null;
		}

		public static IStorageManager CreateInstance()
		{
			if (UseTestImpl)
				return obj ?? (obj = new TempDictStorageManager());
			else
				return obj ?? (obj = new SessionStorageManager());
		}
	}
}