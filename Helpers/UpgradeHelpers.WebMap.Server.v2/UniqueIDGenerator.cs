﻿using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
	/// <summary>
	///     NOTE: probable should use state cache not session because
	///     storage mechanism could not be the storage
	/// </summary>
	public class UniqueIDGenerator : IStateObject, IInternalData
	{
		internal const string UniqueIdGeneratorCacheId = "@@UniqueIDGenerator";


		public static string GetSinglentonUniqueId(System.Type t)
		{
			return t.Name + "_Default";
		}
		public UniqueIDGenerator()
		{
			Seed = 1;
			UniqueID = UniqueIdGeneratorCacheId;
		}

		public static UniqueIDGenerator Current
		{
			get
			{
				var item = RequestTimeStorage.Current[UniqueIdGeneratorCacheId] as UniqueIDGenerator;
				if (item == null)
				{
					item =  StateCache.Current.GetObject(UniqueIdGeneratorCacheId) as UniqueIDGenerator;
					if (item == null)
					{
						item = new UniqueIDGenerator();
						StateCache.Current.AddNewObject(item);
					}
					RequestTimeStorage.Current[UniqueIdGeneratorCacheId] = item;
					
				}
				return item;
			}
		}


		public int Seed { get; set; }

		#region IStateObject Members

		public string UniqueID { get; set; }

		#endregion

/*
        /// <summary>
        /// Returns a new unique ID which can someone be related to the object parent
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        public string GetRelativeUniqueIDAttached(IStateObject parent, string fieldID)
        {
            return fieldID + "###" + parent.UniqueID;
        } */

		/// <summary>
		///     Returns a new unique ID which can someone be related to the object parent
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		public string GetRelativeUniqueID(IStateObject parent, string id)
		{
			return id + "###" + parent.UniqueID;
		}

		public string GetRelativeUniqueID(IStateObject parent)
		{
			return GetRelativeUniqueID(parent, GetUniqueID());
		}

		/// <summary>
		///     Returns a new unique ID
		/// </summary>
		/// <returns></returns>
		public string GetUniqueID()
		{
			int nextValue = GetNextSeed();
			string res = string.Format("{0:0000}", nextValue);
			return res;
		}


		private int GetNextSeed()
		{
			Seed = Seed + 1;
			return Seed;
		}
	}
}