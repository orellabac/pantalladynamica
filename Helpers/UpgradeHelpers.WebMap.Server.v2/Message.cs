﻿using UpgradeHelpers.Helpers;

namespace UpgradeHelpers.WebMap.Server
{
	public struct Message
	{
		public string UniqueID { get; set; }
		public string Text { get; set; }
        public string InsertText { get; set; }
		public BoxButtons Buttons { get; set; }
		public BoxIcons Icons { get; set; }
		public string Continuation_UniqueID { get; set; }

		public string Caption { get; set; }
		public string PromptText { get; set; }
		public bool InputRequest { get; set; }
	}
}