﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
    /// <summary>
    /// Keeps track of changes occurred to the viewmodels during a request processing
    /// </summary>
    internal class DeltaTracker
    {
        internal readonly HashSet<IStateObject> _dirtyModels = new HashSet<IStateObject>();
        internal readonly HashSet<IStateObject> _attachedModels = new HashSet<IStateObject>();

        public DeltaTracker()
        {
            Debug.WriteLine("DeltaTracker created");
        }

        public IEnumerable<DirtyEntry> DirtyEntries
        {
            get
            {
                return from variable in _attachedModels
                       let delta = GetCalculatedDeltaFor(variable)
                       where delta != null
                       select new DirtyEntry()
                           {
                               Value = variable,
                               Delta = delta
                           };
            }
        }


        public void AttachModel(IStateObject obj,bool markAsDirty=false)
        {
            _attachedModels.Add(obj);
            if (markAsDirty)
                _dirtyModels.Add(obj);
        }

        public void Reset()
        {
            _dirtyModels.Clear();
        }

        public void Start()
        {
            Reset();
        }

        internal bool WasModified(IStateObject variable)
        {
            return true;
        }

        internal object GetCalculatedDeltaFor(IStateObject variable)
        {
            return variable;
        }
    }
}