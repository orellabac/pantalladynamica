﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
	internal class TempDictStorageManager : IStorageManager
	{
		private StorageSerializerUsingJsonnet _serializer;
		internal Hashtable table = new Hashtable();


		public TempDictStorageManager()
		{
			_serializer = new StorageSerializerUsingJsonnet();
		}

		#region IStorageManager Members

		public IEnumerable<string> GetAllItems()
		{
			return table.Keys.Cast<string>();
		}

		public IEnumerable<string> GetChildren(string uniqueId)
		{
			foreach (var id in GetAllItems())
			{
				if (id.EndsWith(uniqueId))
					yield return id;
			}
		}


		public IStateObject GetObject(string uniqueID)
		{
			object rawData = table[uniqueID];
			if (rawData == null)
				return null;
			object res = _serializer.RawToObject(uniqueID, rawData);
			return res as IStateObject;
		}

		public void SaveObject(IStateObject obj)
		{
			object rawData = _serializer.ObjectToRaw(obj);
			table[obj.UniqueID] = rawData;
		}

		public void RemoveObject(string uniqueId)
		{
			table.Remove(uniqueId);
		}


		public void SwitchUniqueIDsFrom(string itemUID, string lastUID)
		{
			throw new NotImplementedException();
		}

		#endregion

		internal void Dump(TextWriter writer)
		{
			writer.WriteLine("STORAGE");
			writer.WriteLine("=================================");
			writer.WriteLine("Entries:" + table.Count);
			foreach (object key in table.Keys)
			{
				writer.WriteLine("[ {0}, {1} ]", key, table[key]);
			}
		}


        public void SaveRaw(string uniqueId, object rawData)
        {
            throw new NotImplementedException();
        }

        public object GetRaw(string uniqueId)
        {
            throw new NotImplementedException();
        }

        public void SaveSurrogate(StateObjectSurrogate surrogate)
        {
            throw new NotImplementedException();
        }
	}
}