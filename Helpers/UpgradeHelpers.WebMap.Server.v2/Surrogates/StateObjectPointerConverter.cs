﻿using System;
using Newtonsoft.Json;

namespace UpgradeHelpers.WebMap.Server
{
    public class StateObjectPointerConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var pointer = (StateObjectPointer)value;
            writer.WriteStartObject();
            writer.WritePropertyName("$type");
            writer.WriteValue("StateObjectPointer");
            writer.WritePropertyName("UniqueID");
            writer.WriteValue(pointer.UniqueID);
            writer.WritePropertyName("TargetUniqueId");
            writer.WriteValue(pointer.Target.UniqueID);
            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof (StateObjectPointer).IsAssignableFrom(objectType);
        }
    }
}