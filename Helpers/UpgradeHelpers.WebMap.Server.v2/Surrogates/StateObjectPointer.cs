﻿using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
    public class StateObjectPointer : IStateObject, IInternalData
    {
        public string UniqueID { get; set; }

        private string _targetUniqueId;
        internal string TargetUniqueId
        {
            get { return _targetUniqueId; }
            set
            {
                if (_targetUniqueId != value)
                {
                    _targetUniqueId = value;
                    if (_target != null)
                    {
                        Target = null;
                    }
                }
            }
        }

        private IStateObject _target;

        public IStateObject Target
        {
            get
            {
                if (_target == null)
                {
                    _target = (IStateObject)StateCache.Current.GetObject(TargetUniqueId);
                }
                return _target;
            }
            set 
            { 
                _target = value; 
                TrackerHelper.MarkAsModified(this,"Target",true);
            }
        }
    }
}