﻿using System;
using Newtonsoft.Json;

namespace UpgradeHelpers.WebMap.Server
{
    public class StateObjectSurrogateConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var surrogate = (StateObjectSurrogate) value;
            writer.WriteStartObject();
            writer.WritePropertyName("UniqueID");
            writer.WriteValue(surrogate.UniqueID);
            writer.WritePropertyName("$type");
            writer.WriteValue("StateObjectSurrogate");
            writer.WritePropertyName("SurrogateType");
            writer.WriteValue(surrogate.Value.GetType().AssemblyQualifiedName);
            writer.WritePropertyName("Refs");
            writer.WriteStartArray();
            foreach (var v in surrogate.objectRefs)
            {
                writer.WriteValue(v.UniqueID);
            }
            writer.WriteEndArray();
            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
           
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof (StateObjectSurrogate).IsAssignableFrom(objectType);
        }
    }
}