﻿using System;

namespace UpgradeHelpers.WebMap.Server
{
    internal class SurrogatesInfo
    {
        public Type SupportedType { get; set; }
        public Action<object> InstantionDelegate { get; set; }
        public Action<object, object> Serialize { get; set; }
        public Action<object, object> DeSerialize { get; set; }

    }
}