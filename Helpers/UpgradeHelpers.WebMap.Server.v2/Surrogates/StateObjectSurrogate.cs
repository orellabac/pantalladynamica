﻿using System;
using System.Collections.Generic;
using System.Linq;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
    internal class StateObjectSurrogate : IStateObject, IInternalData
    {

        private object _value;
        public object Value
        {
            get
            {
                if (_value == null)
                {
                    var uid = UniqueIDGenerator.Current.GetRelativeUniqueID(this, "Value");
                    _value = Parent.GetObjectRaw(uid);
                }
                return _value;
            }
            set { _value = value; }
        }
        public string UniqueID { get; set; }
        public Type SurrogateType { get; set; }

        internal List<StateCache.LazyStateObjectReference> objectRefs = new List<StateCache.LazyStateObjectReference>();

        internal void AddReference(StateCache.LazyStateObjectReference newValue)
        {
            objectRefs.Add(newValue);
        }

        internal void RemoveReference(StateCache.LazyStateObjectReference oldValue)
        {
            objectRefs.Remove(oldValue);
        }

        internal void RemoveReference(string uniqueId)
        {
            var result = new List<StateCache.LazyStateObjectReference>();
            foreach (var lazyStateObjectReference in objectRefs)
            {
                if (lazyStateObjectReference.UniqueID.Equals(uniqueId))
                    result.Add(lazyStateObjectReference);
            }
            foreach (var lazyStateObjectReference in result)
            {
                objectRefs.Remove(lazyStateObjectReference);
            }
        }

		/// <summary>
		/// Indicates whether this <c>StateObjectSurrogate</c> object has references or not.
		/// </summary>
		/// <returns><code>true</code> if this object has references, <code>false</code> otherwise</returns>
	    internal bool HasReferences()
	    {
		    return objectRefs.Any();
	    }

        public StateCache Parent { get; set; }
    }
}