﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace UpgradeHelpers.WebMap.Server
{
    public class SurrogatesDirectory
    {
        static readonly Dictionary<Type,SurrogatesInfo> Surrogates = new Dictionary<Type,SurrogatesInfo>();

        //static SurrogatesDirectory()
        //{
        //    RegisterSurrogate(typeof(System.Data.SqlClient.SqlConnection));
        //    RegisterSurrogate(typeof(UpgradeHelpers.VB6.DB.ADO.ADORecordSetHelper));
        //}

        //public static void RegisterSurrogate(Type supportedType)
        //{
        //    Surrogates.Add(supportedType,null);
        //}

        public static bool IsSurrogateRegistered(Type supportedType)
        {
            return Surrogates.ContainsKey(supportedType);
        }


        internal static object CreateInstanceFor(Type type)
        {
            return Activator.CreateInstance(type, true);
        }

        internal static object ObjectToRaw(object obj)
        {
            if (obj is UpgradeHelpers.WebMap.Server.StateCache.DataBinding)
            {
                var databinding = obj as UpgradeHelpers.WebMap.Server.StateCache.DataBinding;
                var ms = new MemoryStream();
                var textWriter = new StreamWriter(ms);
                textWriter.Write(new char[] { 'd', 'b', 'i', 'n' });
                JsonWriter writer = new JsonTextWriter(textWriter);
                writer.WriteStartObject();
                
                writer.WritePropertyName("DataSource");
                writer.WriteValue(databinding.DataSourceReference.UniqueID);
                writer.WritePropertyName("DataSourceProperty");
                writer.WriteValue(databinding.DataSourceProperty);
                writer.WritePropertyName("Object");
                writer.WriteValue(databinding.ObjReference.UniqueID);
                writer.WritePropertyName("ObjectProperty");
                writer.WriteValue(databinding.ObjProperty);

                writer.WriteEndObject();
                textWriter.Flush();
                return ms.ToArray();    
            }
            if (obj is StateObjectSurrogate)
            {
                var surrogate = obj as StateObjectSurrogate;
                var ms = new MemoryStream();
                var textWriter = new StreamWriter(ms);
                textWriter.Write(new char[] { 's', 'u', 'r', 'r' });
                JsonWriter writer = new JsonTextWriter(textWriter);
                writer.WriteStartObject();

                writer.WritePropertyName("UniqueID");
                writer.WriteValue(surrogate.UniqueID);
                writer.WritePropertyName("Refs");
                writer.WriteStartArray();
                foreach (var v in surrogate.objectRefs)
                {
                    writer.WriteValue(v.UniqueID);
                }
                writer.WriteEndArray();
                writer.WriteEndObject();
                textWriter.Flush();
                return ms.ToArray();    
            }
            else if (obj is System.Data.Common.DbConnection)
            {
                var conn = obj as System.Data.Common.DbConnection;
                var ms = new MemoryStream();
                var textWriter = new StreamWriter(ms);
                textWriter.Write(new char[] { 'c', 'o', 'n', 'n' });
                JsonWriter writer = new JsonTextWriter(textWriter);
                writer.WriteStartObject();

                writer.WritePropertyName("ConnectionString");
                writer.WriteValue(conn.ConnectionString);
                writer.WritePropertyName("State");
                writer.WriteValue(conn.State);
                writer.WriteEndObject();
                textWriter.Flush();
                return ms.ToArray();
            }
			else if (obj is UpgradeHelpers.VB6.DB.ADO.ADORecordSetHelper)
			{
				var rs = obj as UpgradeHelpers.VB6.DB.ADO.ADORecordSetHelper;
				var ms = new MemoryStream();
				var textWriter = new StreamWriter(ms);
				textWriter.Write(new char[] { 'r', 'e', 'c', 's' });
				JsonWriter writer = new JsonTextWriter(textWriter);
				writer.WriteStartObject();

				writer.WritePropertyName("ConnectionString");
				writer.WriteValue(rs.ConnectionString);
				writer.WritePropertyName("Source");
				writer.WriteValue(rs.Source);
				writer.WritePropertyName("CurrentPosition");
				writer.WriteValue(rs.CurrentPosition);
				writer.WriteEndObject();
				textWriter.Flush();
				return ms.ToArray();
			}
			else if (obj is DataSet)
            {
                var ds = obj as DataSet;
                var ms = new MemoryStream();
                var textWriter = new StreamWriter(ms);
                textWriter.Write(new char[] { 'd', 's', 'e', 't' });
                textWriter.Write(ds.GetXml());
                textWriter.Flush();
                return ms.ToArray();
            }
            throw new NotSupportedException();
        }

        internal static void RegisterBinding(object ds, string dataSourceProperty, Action<object> applyBindingChangedAction, bool firstTime = false )
        {
            if (ds is UpgradeHelpers.VB6.DB.ADO.ADORecordSetHelper)
            {
                var rs = ds as UpgradeHelpers.VB6.DB.ADO.ADORecordSetHelper;
                rs.RecordsetChangeComplete += (obj,e) =>
                    {
                        var value = rs[dataSourceProperty];
                        applyBindingChangedAction(value);
                    };
                if (firstTime)
                {
                    var value = rs[dataSourceProperty];
                    applyBindingChangedAction(value);
                }
                //rs.MoveComplete += (sender, args) =>
                //    {
                //        var value = rs[dataSourceProperty];
                //        applyBindingChangedAction(value);
                //    };
            }
        }

        internal static object RawToObject(object raw)
        {
            var rawBytes = (byte[]) raw;
            var ms = new MemoryStream(rawBytes);
            var textReader = new StreamReader(ms);
            var signature = new char[4];
            textReader.Read(signature, 0, 4);

            var sigStr = new string(signature);
            if (sigStr == "dbin")
            {
                var jsonReader = new JsonTextReader(textReader);
                JObject jobj = JObject.Load(jsonReader);
                var dataBinding  = new StateCache.DataBinding();
                var idDataSource = jobj["DataSource"].Value<string>();
                dataBinding.DataSourceReference = (UpgradeHelpers.Interfaces.IModel)StateCache.Current.GetObject(idDataSource);
                var idObj = jobj["Object"].Value<string>();
                dataBinding.ObjReference = (UpgradeHelpers.Interfaces.IModel)StateCache.Current.GetObject(idObj);
                dataBinding.DataSourceProperty = jobj["DataSourceProperty"].Value<string>();
                dataBinding.ObjProperty = jobj["ObjectProperty"].Value<string>();
                return dataBinding;

            }
            if (sigStr == "surr")
            {
                var jsonReader = new JsonTextReader(textReader);
                JObject jobj = JObject.Load(jsonReader);
                var surrogate = new StateObjectSurrogate();
                surrogate.UniqueID = jobj["UniqueID"].Value<string>();
                foreach (var v in jobj["Refs"].Values<string>())
                {
                    StateCache.Current.AddSurrogateReference(surrogate,v);
                }
                return surrogate;
            }
            else if (sigStr == "conn")
            {
                var jsonReader = new JsonTextReader(textReader);
                JObject jobj = JObject.Load(jsonReader);
                var conn = new System.Data.SqlClient.SqlConnection();
                conn.ConnectionString = jobj["ConnectionString"].Value<string>();
                var state = jobj["State"].Value<int>();
                if (state == 1)
                    conn.Open();
                return conn;
            }
			else if (sigStr == "recs")
			{
				var jsonReader = new JsonTextReader(textReader);
				JObject jobj = JObject.Load(jsonReader);
				var rs = new UpgradeHelpers.VB6.DB.ADO.ADORecordSetHelper();
				rs.ConnectionString = jobj["ConnectionString"].Value<string>();
				if (jobj["Source"].Type.ToString().ToLower() == "string")
				{
					rs.Source = jobj["Source"].Value<string>();
				}
				else
				{
					rs.Source = jobj["Source"].Value<object>();
				}
				if (!rs.Opened)
					rs.Open();
				rs.CurrentPosition = jobj["CurrentPosition"].Value<int>();
				return rs;
			}
			else if (sigStr == "dset")
            {
                var ds = new DataSet();
                ds.ReadXml(textReader);
                return ds;
            }
            throw new NotSupportedException();

        }
    }
}