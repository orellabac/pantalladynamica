﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Fasterflect;
using UpgradeHelpers.Interfaces;
namespace UpgradeHelpers.WebMap.Server
{
	public class ContinuationInfo : IStateObject,IInternalData
	{
		public string UniqueID { get; set; }
		public string IStateObject_UniqueID { get; set; } /* For viewmodel associated with logic containing method */

		public string ContinuationFields { get; set; }

		public string Modal_UniqueID { get; set; }
		public string DeclaringType { get; set; }
		public string MethodName { get; set; }
		public string TargetType { get; set; }

		/// <summary>
		/// Executes the continuation
		/// </summary>
		public object Execute(ILogicWithViewModel<IViewModel> modelParam = null)
		{
			var classType = Type.GetType(DeclaringType);
			var targetType = classType;
			if (TargetType != null)
			{
				var oldTargetType = targetType;
				targetType = Type.GetType(TargetType);
				if (targetType == null)
					targetType = oldTargetType;
			}
			object[] parameters = { };


			if (typeof(ILogicWithViewModel<IStateObject>).IsAssignableFrom(classType))
			{
				var vm = StateCache.Current.GetObject(IStateObject_UniqueID);
				var logic = IocContainerImplWithUnity.Current.Resolve(targetType, vm);
				try {
					if (((ILogicWithViewModel<IStateObject>)logic).ViewModel == null) 
					{
						PropertyInfo pInfo = logic.GetType().Property("ViewModel");
						if (pInfo != null && pInfo.CanWrite)
						{
							pInfo.SetValue(logic, vm, null);
						}
					}
				}
				catch { }
				var method = classType.Method(MethodName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
				if (method.GetParameters().Length > 0)
				{
					parameters = new object[] { modelParam };
				}
				return method.Invoke(logic, parameters);
			}
			if (typeof(ILogicClass).IsAssignableFrom(classType))
			{
				var logic = StateCache.Current.GetObject(this.IStateObject_UniqueID);
				if (logic == null)
					logic = (ILogicClass)IocContainerImplWithUnity.Current.Resolve(classType, IocContainerImplWithUnity.NoView);
				var method = classType.Method(MethodName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                if (method.GetParameters().Length > 0)
			{
					parameters = new object[] { modelParam };
				}
				return method.Invoke(logic, parameters);
			}
				var classTypeName = classType.Name;
				if (classTypeName.Contains("DisplayClass"))
				{

				Dictionary<string, FieldInfo> theFields = new Dictionary<string, FieldInfo>();
				byte[] bytes = null;
				MemoryStream memstream = null;
				BinaryReader reader = null;
				var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

				if (this.ContinuationFields!=null)  {
					bytes = Convert.FromBase64String(this.ContinuationFields);
					memstream = new MemoryStream(bytes);
					reader = new BinaryReader(memstream);
				}



					var delegateHiddenClassType = classType;
					classType =  classType.DeclaringType;
				object logic;
				if (typeof(ILogicClass).IsAssignableFrom(classType))
				{
					logic = StateCache.Current.GetObject(this.IStateObject_UniqueID);
					if (logic == null)
						logic = (ILogicClass)IocContainerImplWithUnity.Current.Resolve(classType, IocContainerImplWithUnity.NoView);
				}
				else
				{
					 logic = IocContainerImplWithUnity.Current.Resolve(classType, IocContainerImplWithUnity.NoView);
					var vm = StateCache.Current.GetObject(IStateObject_UniqueID);
					classType.Property("ViewModel").SetValue(logic, vm, null);
				}

					var delegateHiddenClass = Activator.CreateInstance(delegateHiddenClassType);

					var fieldList = delegateHiddenClassType.Fields(BindingFlags.Public | BindingFlags.Instance);
					bool thisWasFound = false;
					foreach (var field in fieldList)
					{
						if (classType == field.FieldType && field.Name.EndsWith("this"))
						{
							field.SetValue(delegateHiddenClass, logic);
						thisWasFound = true;
							break;
						}
					else if (this.ContinuationFields != null)
					{
						if (!field.Name.Contains("locals"))
						{
							//System.Diagnostics.Debug.Assert(fieldName == field.Name);
							object fieldValue = null;
							if (typeof(IStateObject).IsAssignableFrom(field.FieldType))
							{
								var fieldName = reader.ReadString();
								var uid = reader.ReadString();
								fieldValue = StateCache.Current.GetObject(uid);
								field.SetValue(delegateHiddenClass, fieldValue);
							}
							else
							{
								if (field.FieldType.IsSerializable)
								{
									var fieldName = reader.ReadString();
									fieldValue = formatter.Deserialize(memstream);
									field.SetValue(delegateHiddenClass, fieldValue);
								}
							}
						}
					}
				}
				if (!thisWasFound)
				{

					foreach(var field in fieldList)
					{
						var fieldName = field.Name;
						if (fieldName.Contains("<>") && fieldName.Contains("locals"))
						{
							var localInstance = Activator.CreateInstance(field.FieldType);
							field.SetValue(delegateHiddenClass, localInstance);
							var fieldOfLocals = field.FieldType.Fields(BindingFlags.Public | BindingFlags.Instance);
							foreach (var fieldOfLocalsField in fieldOfLocals)
							{
								var valueForLocalsDisplayClass = field.GetValue(delegateHiddenClass);
								if (classType == fieldOfLocalsField.FieldType && fieldOfLocalsField.Name.EndsWith("this"))
								{
									fieldOfLocalsField.SetValue(valueForLocalsDisplayClass, logic);
									thisWasFound = true;
								   //break;
								}
								else if (this.ContinuationFields != null)
								{
									if (typeof(IStateObject).IsAssignableFrom(fieldOfLocalsField.FieldType) || fieldOfLocalsField.FieldType.IsSerializable)
									{


										var localsFieldName = reader.ReadString();
										//System.Diagnostics.Debug.Assert(localsFieldName == fieldOfLocalsField.Name);
										object fieldValue = null;
										if (typeof(IStateObject).IsAssignableFrom(fieldOfLocalsField.FieldType))
										{
											var uid = reader.ReadString();
											fieldValue = StateCache.Current.GetObject(uid);
										}
										else
										{
											fieldValue = formatter.Deserialize(memstream);
										}
										fieldOfLocalsField.SetValue(valueForLocalsDisplayClass, fieldValue);
									}
								}
							}
						   // if (thisWasFound) break;
						}
					}
					}
					var method = delegateHiddenClassType.Method(MethodName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
					if (method.GetParameters().Length > 0)
					{
						parameters = new object[] { modelParam };
					}
				return method.Invoke(delegateHiddenClass, parameters);
			}
			return null;
		}

	}


}