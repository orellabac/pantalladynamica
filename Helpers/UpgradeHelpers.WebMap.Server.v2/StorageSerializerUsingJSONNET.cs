﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using Fasterflect;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.WebMap.Server.Common.Config;

namespace UpgradeHelpers.WebMap.Server
{
	/// <summary>
	///     This class is used to serialize the view models using json as representation
	/// </summary>
	internal class StorageSerializerUsingJsonnet : IStorageSerializer
	{
		private static readonly JsonSerializerSettings settings = new JsonSerializerSettings
		{
			Binder = new TypeNameSerializationBinder(),
			TypeNameHandling = TypeNameHandling.All,
			TypeNameAssemblyFormat = FormatterAssemblyStyle.Full,
			NullValueHandling = NullValueHandling.Ignore,
			DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate,
			ContractResolver = new PropertySkiperContractResolver(skipUniqueId: true, server: true),
			Converters = new JsonConverter[] { new ObservableCollectionExConverter(), new StateObjectPointerConverter(), new StateObjectSurrogateConverter() }
		};


		#region IStorageSerializer Members

		public object ObjectToRaw(object obj)
		{
			string res = JsonConvert.SerializeObject(obj, settings);
			return res;
		}

		public object RawToObject(string uniqueId, object rawData)
		{
			if (rawData != null && ((string)rawData).IndexOf("IocContainerImplWithUnity+ObservableCollectionEx") != -1)
			{
				JObject jobj = JObject.Load(new JsonTextReader(new StringReader((string)rawData)));

				var typeName = jobj["$type"].Value<string>();
				Type type = Type.GetType(typeName, false);
				if (type != null)
				{
					dynamic actualColl = IocContainerImplWithUnity.Current.Resolve(type, IocContainerImplWithUnity.RecoveredFromStorage, jobj["Count"].Value<int>());
					actualColl.UniqueID = uniqueId; //jobj["UniqueID"].Value<string>();
					return actualColl;
				}
			}
			if (rawData != null && ((string)rawData).IndexOf("StateObjectPointer") != -1)
			{
				JObject jobj = JObject.Load(new JsonTextReader(new StringReader((string)rawData)));
				var pointer = new StateObjectPointer();
				pointer.UniqueID = uniqueId;//jobj["UniqueID"].Value<string>();
				pointer.TargetUniqueId = jobj["TargetUniqueId"].Value<string>();
				return pointer;
			}
			if (rawData != null && ((string)rawData).IndexOf("Surrogate") != -1)
			{
				JObject jobj = JObject.Load(new JsonTextReader(new StringReader((string)rawData)));
				var surrogate = new StateObjectSurrogate();
				return surrogate;
			}
			object res = JsonConvert.DeserializeObject((string)rawData, settings);
			object actualRes = IocContainerImplWithUnity.Current.Resolve(res.GetType(), IocContainerImplWithUnity.RecoveredFromStorage);
			Copy(res, actualRes, uniqueId);
			return actualRes;
		}

		#endregion

		/// <summary>
		/// Copies one object into another by setting its public instance properties with reflection.  This method is used
		/// just to copy a serialized restored instance into another one created by the container (Serialized objects are not
		/// instrumented).
		/// <p>
		/// Properties which type is listed below are not copied because current values must be preserve so they
		/// are injected by the container when it created the <c>target</c> object.
		/// </p>
		/// <list type="bullet">
		/// <item>UpgradeHelpers.Interfaces.IViewManager</item>
		/// <item>UpgradeHelpers.Interfaces.IIocContainer</item>
		/// <item>UpgradeHelpers.Interfaces.IDefaults</item>
		/// </list>
		/// </summary>
		/// <param name="source">Source object to get property values from</param>
		/// <param name="target">Target object to set property values to</param>
		private static void Copy<T>(T source, T target, string uniqueId = null)
		{

			var isStateObject = target is IStateObject;
			if (isStateObject && uniqueId != null)
			{
				((IStateObject)target).UniqueID = uniqueId;
			}

			var props = from p in source.GetType().Properties(BindingFlags.Public | BindingFlags.Instance)
						where p.CanWrite &&
						!SurrogatesDirectory.IsSurrogateRegistered(p.PropertyType) &&
						!typeof(IViewManager).IsAssignableFrom(p.PropertyType) &&
						!typeof(IIocContainer).IsAssignableFrom(p.PropertyType) &&
			!typeof(IDefaults).IsAssignableFrom(p.PropertyType)
						select p;
			foreach (var prop in props)
			{
				if (!StateManagementUtils.MustIgnoreMember(false, prop) &&
					/* Is this an indexer Property? */
					!(prop.GetIndexParameters().Length > 0))
				{
					var value = prop.GetValue(source, null);
					prop.SetValue(target, value, null);

					if (isStateObject && value is INotifyCollectionChanged)
					{
						((INotifyCollectionChanged)value).CollectionChanged += (sender, args) =>
						{
							TrackerHelper.MarkAsModified((IStateObject)target, prop.Name, false);
						};
					}
				}
			}

			var fields = from p in source.GetType().Fields(BindingFlags.Public | BindingFlags.Instance)
						 where !p.IsInitOnly &&
						 !SurrogatesDirectory.IsSurrogateRegistered(p.FieldType) &&
						 !typeof(IViewManager).IsAssignableFrom(p.FieldType) &&
						 !typeof(IIocContainer).IsAssignableFrom(p.FieldType) &&
			 !typeof(IDefaults).IsAssignableFrom(p.FieldType)
						 select p;
			foreach (var field in fields)
			{
				if (!StateManagementUtils.MustIgnoreMember(false, field))
				{
					var value = field.GetValue(source);
					field.SetValue(target, value);

					if (isStateObject && value is INotifyCollectionChanged)
					{
						var field1 = field;
						((INotifyCollectionChanged)value).CollectionChanged += (sender, args) =>
						{
							TrackerHelper.MarkAsModified((IStateObject)target, field1.Name, false);
						};
					}
				}
			}
		}



		#region Nested type: ObservableCollectionExConverter

		internal class ObservableCollectionExConverter : JsonConverter
		{
			private readonly bool writeTypeInfo;
			private readonly bool writeUniqueID;

			public ObservableCollectionExConverter(bool writeTypeInfo, bool writeUniqueID)
			{
				this.writeTypeInfo = writeTypeInfo;
				this.writeUniqueID = writeUniqueID;
			}

			public ObservableCollectionExConverter()
				: this(true, true)
			{
			}

			public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
			{
				writer.WriteStartObject();
				Type valueType = value.GetType();

				writer.WritePropertyName("$type");

				if (writeTypeInfo)
				{
					if (IsGeneratedType(valueType))
					{
						valueType = valueType.BaseType;
					}
					string assemblyQualifiedName = valueType.AssemblyQualifiedName;
					writer.WriteValue(assemblyQualifiedName);
				}
				writer.WritePropertyName("Count");
				var count = (int)valueType.Property("Count").GetValue(value, null);
				writer.WriteValue(count);
				writer.WritePropertyName("@arr");
				writer.WriteValue("1");
				if (writeUniqueID)
				{
					writer.WritePropertyName("UniqueID");
					writer.WriteValue(((IStateObject)value).UniqueID);
				}


				writer.WriteEndObject();
			}

			private bool IsGeneratedType(Type valueType)
			{
				return valueType.Assembly.FullName.StartsWith("Unity");
			}

			public bool IsControlArray(Type type)
			{
				if (type.IsGenericType)
				{
					Type[] interfaces = type.GetInterfaces();
					return interfaces.Any(x => x.Name == typeof(IModelList<>).Name);
				}

				return false;
			}

			public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
			{
				JObject jobj = JObject.Load(reader);
				var size = jobj["Count"].Value<int>();
				var uid = jobj["UniqueID"].Value<string>();

				dynamic value = IocContainerImplWithUnity.Current.ResolveUnPrepared(objectType);
				value.SetupLazy(size, uid);
				return value;
			}

			public override bool CanConvert(Type objectType)
			{
				return IsControlArray(objectType);
			}
		}

		#endregion

		#region Nested type: TypeNameSerializationBinder

		private class TypeNameSerializationBinder : SerializationBinder
		{
			private bool IsGeneratedType(Type valueType)
			{
				return valueType.Assembly.FullName.StartsWith("Unity");
			}

			private static IDictionary<string, string> _typeToWMType;
			private static IDictionary<string, string> TypeToWMType
			{
				get
				{
					if (_typeToWMType == null)
					{
						var config = WebMAPConfiguration.KnownTypesConfiguration;
						_typeToWMType = new Dictionary<string, string>();
						if (config != null && config.Enabled)
						{
							foreach (KnownType knownType in config.KnownTypes)
							{
								_typeToWMType[knownType.FullClassName + "@" + knownType.AssemblyName] =
									knownType.ShortName;
							}
						}
					}
					return _typeToWMType;
				}
			}

			private static IDictionary<string, string[]> _wmTypeToType;

			private static IDictionary<string, string[]> WMTypeToType
			{
				get
				{
					if (_wmTypeToType == null)
					{
						var config = WebMAPConfiguration.KnownTypesConfiguration;
						_wmTypeToType = new Dictionary<string, string[]>();
						if (config != null && config.Enabled)
						{
							foreach (KnownType knownType in config.KnownTypes)
							{
								_wmTypeToType[knownType.ShortName] = new[] { knownType.FullClassName, knownType.AssemblyName };
							}
						}
					}
					return _wmTypeToType;
				}
			}

			public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
			{
				if (IsGeneratedType(serializedType))
				{
					assemblyName = serializedType.BaseType.Assembly.FullName;
					typeName = serializedType.BaseType.FullName;
				}
				else
				{
					assemblyName = serializedType.Assembly.FullName;
					typeName = serializedType.FullName;
				}
				var key = typeName + "@" + assemblyName;
				if (TypeToWMType.ContainsKey(key))
				{
					assemblyName = "WM";
					typeName = TypeToWMType[key];
				}
				else
				{
					Debug.WriteLine("KEY:" + key);
				}
			}

			public override Type BindToType(string assemblyName, string typeName)
			{
				if (assemblyName.Equals("WM") && typeName.StartsWith("WMKT") &&
					WMTypeToType.ContainsKey(typeName))
				{
					var info = WMTypeToType[typeName];
					typeName = info[0];
					assemblyName = info[1];
				}

				string resolvedTypeName = string.Format("{0},{1}", typeName, assemblyName);
				Type type = Type.GetType(resolvedTypeName, true);
				if (type.IsArray)
				{
				}
				return type;
			}
		}

		#endregion
	}
}
