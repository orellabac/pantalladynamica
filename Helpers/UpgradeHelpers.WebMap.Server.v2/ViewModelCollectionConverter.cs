﻿using System;
using Fasterflect;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.WebMap.Server;
using UpgradeHelpers.WebMap.Server.Interfaces;

internal class ViewModelCollectionConverter : JsonConverter
{
    public ViewModelCollectionConverter()
    {
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        var viewModelCollection = (IViewModelCollection)value;
        writer.WriteStartObject();
        writer.WritePropertyName("UniqueID");
        writer.WriteValue(viewModelCollection.UniqueID);

        writer.WritePropertyName("Items");
        writer.WriteValue(viewModelCollection._items.UniqueID);

        writer.WritePropertyName("Parent");
        writer.WriteValue(viewModelCollection.UniqueID);
        var valueType = viewModelCollection.GetType();
        if (IsGeneratedType(valueType))
        {
            valueType = valueType.BaseType;
        }
        var props = valueType.Properties(System.Reflection.BindingFlags.Public |
                                              System.Reflection.BindingFlags.Instance );
		//;|
		//									  System.Reflection.BindingFlags.DeclaredOnly);
        foreach (var propertyInfo in props)
        {
			if (!propertyInfo.Name.Equals("_items") && !StateManagementUtils.MustIgnoreMember(propertyInfo))
			{
            writer.WritePropertyName(propertyInfo.Name);
            serializer.Serialize(writer,propertyInfo.GetValue(value, null));
			}
        }
        writer.WritePropertyName("@k");
        writer.WriteValue(1);



        writer.WriteEndObject();
    }

    private bool IsGeneratedType(Type valueType)
    {
        return valueType.Assembly.FullName.StartsWith("Unity");
    }
  

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        JObject jobj = JObject.Load(reader);
        var size = jobj["Count"].Value<int>();
        var uid = jobj["UniqueID"].Value<string>();

        dynamic value = IocContainerImplWithUnity.Current.ResolveUnPrepared(objectType);
        value.SetupLazy(size, uid);
        return value;
    }

    public override bool CanConvert(Type objectType)
    {
        return typeof (IViewModelCollection).IsAssignableFrom(objectType);
    }
}