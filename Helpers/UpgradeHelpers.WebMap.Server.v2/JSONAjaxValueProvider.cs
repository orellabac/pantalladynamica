﻿using System;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.WebMap.Server;
using IViewModel = UpgradeHelpers.Interfaces.IViewModel;
using System.Reflection;
using Fasterflect;

public class JSONAjaxValueProvider : ValueProviderFactory
{
	private static JsonValueProviderFactory _oldFactory = new JsonValueProviderFactory();

	public override IValueProvider GetValueProvider(ControllerContext controllerContext)
	{
		// first make sure we have a valid context
		if (controllerContext == null)
			throw new ArgumentNullException("controllerContext");

		// now make sure we are dealing with a json request
		if (!controllerContext.HttpContext.Request.ContentType.StartsWith("application/json", StringComparison.OrdinalIgnoreCase))
			return null;

		if (!SessionExpireFilterAttribute.StillValidSession(controllerContext.RequestContext.HttpContext.Session, controllerContext.HttpContext.Request))
			return null;

		string wmHeader = controllerContext.RequestContext.HttpContext.Request.Headers["WM"];
		if (wmHeader == null)
			return _oldFactory.GetValueProvider(controllerContext);


		Stream inputStream = controllerContext.HttpContext.Request.InputStream;
		return ProcessWebMapAction(inputStream, controllerContext.Controller);
	}

	internal IValueProvider ProcessWebMapAction(Stream inputStream, ControllerBase controller)
	{
		// get a generic stream reader (get reader for the http stream)
		var streamReader = new StreamReader(inputStream);
		// convert stream reader to a JSON Text Reader
		var jsonReader = new JsonTextReader(streamReader);
		// tell JSON to read
		if (!jsonReader.Read())
			return null;
		JObject jobj = JObject.Load(jsonReader);
		SyncDirtyModels(jobj);
		UpdateViewManager(jobj);


		return new WebMapAction(jobj, controller);
	}

	private void UpdateViewManager(JObject jsonObj)
	{
		var viewManager = ViewManager.Instance;
		var closedViews = jsonObj["closedViews"];
		if (closedViews != null)
		{
			var array = (JArray)closedViews;
			foreach (var view in array)
			{
				viewManager.State.DisposeViewInternal(view.Value<String>());
			}
		}
	}

	private void SyncDirtyModels(JObject jobj)
	{
		JToken dirty = jobj["dirty"];
		if (dirty != null)
		{
			ViewManager.Instance.Events.Suspend();
			foreach (JToken viewmodel in dirty)
			{
				StateCache.Current.SyncDirtyModel(viewmodel);
			}
			ViewManager.Instance.Events.Resume();
		}
	}

	private class WebMapAction : IValueProvider
	{
		private readonly JObject jobj;
		private readonly ControllerBase _controller;

		public WebMapAction(JObject jobj, ControllerBase controller)
		{
			this.jobj = jobj;
			_controller = controller;
		}

		public bool ContainsPrefix(string prefix)
		{
			if ("viewmodel".Equals(prefix, StringComparison.CurrentCultureIgnoreCase) ||
				"viewFromClient".Equals(prefix, StringComparison.CurrentCultureIgnoreCase) ||
				"usercontrol".Equals(prefix, StringComparison.CurrentCultureIgnoreCase))
			{
				return true;
			}
			JToken parameters = jobj["parameters"];
			if (parameters != null)
			{
				JToken elem = parameters[prefix];
				if (elem != null)
					return true;
			}
			if (prefix == "dialogResult")
			{
				return true;
			}
			return false;
		}


		public ValueProviderResult GetValue(string key)
		{
			if ("viewmodel".Equals(key, StringComparison.CurrentCultureIgnoreCase) ||
				"viewFromClient".Equals(key, StringComparison.CurrentCultureIgnoreCase) ||
				"usercontrol".Equals(key, StringComparison.CurrentCultureIgnoreCase))
			{
				//Breakpoint
				JToken tok = jobj["vm"];

				string vmID = tok != null ? tok.Value<string>() : null;
				if (vmID != null)
				{
					var actualObject = StateCache.Current.GetObject(vmID);
					// lets update the container of controller logic with view model context
					UpdateContainerWithViewModel(actualObject);

					return new ValueProviderResult(actualObject, "json", CultureInfo.CurrentCulture);
				}
				return null;
			}
			else if (key == "dialogResult" && jobj["dialogResult"] != null)
			{
				var tok = jobj["dialogResult"];
				return new ValueProviderResult(tok.Value<string>(), tok.Value<string>(), CultureInfo.CurrentCulture);
			}
			else
			{
				//look into params
				JToken parameters = jobj["parameters"];
				JToken param = parameters[key];
				if (param != null)
				{
					return new ValueProviderResult(param.Value<string>(), param.Value<string>(), CultureInfo.CurrentCulture);
				}
			}
			return null;
		}

		private void UpdateContainerWithViewModel(IStateObject viewModel)
		{
			var controllerType = _controller.GetType();
			PropertyInfo logicProperty = controllerType.Property("logic");
			if (logicProperty != null)
			{
				object logic = IocContainerImplWithUnity.Current.Resolve(logicProperty.PropertyType, new object[] { viewModel, });
				logicProperty.SetValue(_controller, logic, null);
			}
		}
	}
}