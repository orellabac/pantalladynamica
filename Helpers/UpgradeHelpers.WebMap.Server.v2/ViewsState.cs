﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Fasterflect;
using Newtonsoft.Json;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

#endregion

namespace UpgradeHelpers.WebMap.Server
{
	[JsonObject(MemberSerialization.OptIn)]
	internal class ViewsState : IStateObject, IInternalData
	{
		internal const string UniqueIdViewmanagerState = "@@ViewsStateSinglenton";
		[NonSerialized] [JsonIgnore] private ViewsStateDelta _viewsDelta;

		public ViewsState()
		{
			LoadedViews = new List<ViewInfo>();
			Messages = new List<Message>();
			Delegates = new List<ContinuationInfo>();
			InModalExecution = false;
			ModalViews = new List<string>();
		}

		[JsonProperty]
		public string CurrentCulture { get; set; }

		[JsonProperty]
		public List<ViewInfo> LoadedViews { get; set; }

		[JsonProperty]
		public List<Message> Messages { get; set; }

		[JsonProperty]
		public List<ContinuationInfo> Delegates { get; set; }

		[JsonProperty]
		internal bool InModalExecution { get; set; }

        [JsonProperty]
        public string CurrentFocusedControl { get; set; }

		[JsonProperty]
		public List<string> ModalViews { get; set; }

		#region IStateObject Members

		public string UniqueID
		{
			get { return UniqueIdViewmanagerState; }
			set { }
		}

		#endregion

		public void Start()
		{
			_viewsDelta = new ViewsStateDelta();
		}

		public ViewsStateDelta End()
		{
			ViewsStateDelta res = _viewsDelta;
			_viewsDelta = null;
			return res;
		}

        /// <summary>
        /// Sets the UniqueId of the control that will be focused
        /// </summary>
        /// <param name="uniqueID">Unique ID of the control</param>
        public void SetCurrentFocusedControl(string uniqueID)
        {
            CurrentFocusedControl = uniqueID;
            _viewsDelta.CurrentFocusedControl = uniqueID;
        }

		internal void NavigateToView(ILogicWithViewModel<IViewModel> logic, bool isModal = false )
		{
			IViewModel view = logic.ViewModel;
			view.Visible = true;
			if (!LoadedViews.Any(x => x.UniqueID == view.UniqueID))
			{
				_viewsDelta.AddNewView(view, isModal);

				if (isModal)
				{
					this.ModalViews.Add(view.UniqueID);
				}

				// lets fire the load event
				//Load can have no parameters or some parameters
				MethodInfo method = logic.GetType().Method("Load", new Type[0]);
				if (method != null)
				{
					method.Invoke(logic, new object[0]);
				}
				else
				{
						object p1 = null;
						System.EventArgs p2 = null;
						method = logic.GetType().Method("Load", new Type[] { typeof(object), typeof(System.EventArgs) });
						if (method != null)
						{
							method.Invoke(logic, new object[] { p1, p2 });
						}
				}
				LoadedViews.Add(new ViewInfo { UniqueID = view.UniqueID, Visible = view.Visible, ZOrder = 0 });
			}
			else
			{
				var currentView = LoadedViews.FirstOrDefault(v => v.UniqueID == view.UniqueID);
				currentView.Visible = view.Visible;
				currentView.ZOrder = 0;
			}
			TrackerHelper.MarkAsModified(this, "LoadedViews", false);
		}

		internal void HideView(ILogicWithViewModel<IViewModel> logicObjectWithView)
		{
			IViewModel view = logicObjectWithView.ViewModel;
			view.Visible = false;
		}

		/// <summary>
		/// Disposes the view bound to the given <c>ILogicView</c> object.
		/// </summary>
		/// <param name="logic">The <c>ILogicView</c> object bound to the view to dispose.</param>
		internal void DisposeView(ILogicWithViewModel<IViewModel> logic)
		{
			IViewModel view = logic.ViewModel;
			Debug.Assert(LoadedViews.Any(x => x.UniqueID == view.UniqueID), "Cannot remove a view which is not loaded");
			int count = LoadedViews.RemoveAll(x => x.UniqueID == view.UniqueID);
			_viewsDelta.RemoveView(view);
			StateCache.Current.RemoveObject(view.UniqueID);
			TrackerHelper.MarkAsModified(this, "LoadedViews", false);
			if (this.ModalViews.Exists(x => x == view.UniqueID))
			{
				this.ModalViews.Remove(view.UniqueID);
			}
			Debug.Assert(count == 1, "Exactly one view must be removed");
		}

		internal void DisposeViewInternal(string viewId)
		{
			LoadedViews.RemoveAll(x => x.UniqueID == viewId);
			StateCache.Current.RemoveObject(viewId);
			TrackerHelper.MarkAsModified(this, "LoadedViews", false);
		}

		internal string AddMessage(string message, string caption, BoxButtons buttons, BoxIcons icons,
			string promptText, bool inputRequest)
		{
			string id = UniqueIDGenerator.Current.GetUniqueID();
			var newMessage = new Message {
				Text = message, 
				Caption = caption, 
				Buttons = buttons, 
				Icons = icons, 
				UniqueID = id,
				PromptText = promptText,
				InputRequest = inputRequest
				};
			Messages.Add(newMessage);
			_viewsDelta.Messages.Add(newMessage);
			return id;
		}


		internal void RegisterContinuation(ContinuationInfo continuation)
		{
			Delegates.Insert(0, continuation);
			TrackerHelper.MarkAsModified(this, "Delegates", false);
		}


        internal void RemoveTopContinuation(ContinuationInfo oldContinuation)
        {
            var index = Delegates.FindIndex(x => x == oldContinuation);
            if (index!=-1)
            {
                Delegates.RemoveAt(index);
            }
            TrackerHelper.MarkAsModified(this, "Delegates", false);
        }


		internal void RemoveTopContinuation()
		{
			if (Delegates.Count > 0)
			{
				Delegates.RemoveAt(0);
			}
			TrackerHelper.MarkAsModified(this, "Delegates", false);
		}
	}
}