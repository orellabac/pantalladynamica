﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Fasterflect;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
	public class RequestTimeStorage
	{
		internal static RequestTimeStorage Wrapper = new RequestTimeStorage();

#if DEBUG
		public virtual object this[string key]
#else
        public object this[string key]
#endif
		{
			get { return HttpContext.Current.Items[key]; }
			set { HttpContext.Current.Items[key] = value; }
		}

#if DEBUG
		public virtual bool Contains(string key)
#else
        public bool Contains(string key)
#endif
		{
			return HttpContext.Current.Items.Contains(key);
		}


		public virtual void Remove(string key)
		{
			HttpContext.Current.Items.Remove(key);
		}

		public static RequestTimeStorage Current
		{
			get { return Wrapper; }
		}


	}

	/// <summary>
	///     This object only lives during the request execution lifecycle
	///     This is mostly like a managed memory.
	///     All object used by the bussiness logic will be cached in this object during a request process
	/// </summary>
	internal class StateCache
	{
		internal const string Statecachekey = "@@STATECACHE";
		private const string UniqueIdSeparator = "###";
		internal const string BindingKey = "^Binding";
		private readonly Dictionary<string, IStateObject> _cache = new Dictionary<string, IStateObject>();
		private readonly Dictionary<string, IStateObject> _tempcache = new Dictionary<string, IStateObject>();

		private readonly Dictionary<object, StateObjectSurrogate> _surrogates = new Dictionary<object, StateObjectSurrogate>();

		private readonly Dictionary<string, StateObjectSurrogate> _surrogatesIdsToSurrogates =
			new Dictionary<string, StateObjectSurrogate>();

		private readonly List<Tuple<string, string>> _switchedIds = new List<Tuple<string, string>>();
		private readonly List<string> _elementsToRemove = new List<string>();

		internal IStorageManager StorageManager;
		//internal readonly DeltaTracker _tracker = new DeltaTracker();
		internal DeltaTracker Tracker = new DeltaTracker();

		private StateCache()
		{
			StorageManager = StorageManagerFactory.CreateInstance();
			Tracker.Start();
		}


		public static bool IsAvailable
		{
			get { return RequestTimeStorage.Current.Contains(Statecachekey); }
		}

		public static StateCache Current
		{
			get
			{
				var item = RequestTimeStorage.Current[Statecachekey] as StateCache;
				if (item == null)
				{
					RequestTimeStorage.Current[Statecachekey] = (item = new StateCache());
				}
				return item;
			}
		}

		public IEnumerable<DirtyEntry> DirtyEntries
		{
			get { return Tracker.DirtyEntries; }
		}

		public IEnumerable<string> GetAllEntries()
		{
			return _cache.Keys.AsEnumerable();
		}

		public static bool IsRootLevelObject(IStateObject obj)
		{
			return obj is ITopLevelStateObject || obj is IShared;
		}

		public static bool IsAttachedChild(IStateObject obj)
		{
			return AllBranchesAttached(obj);
			//obj.UniqueID.IndexOf("###") != -1 && obj.UniqueID.IndexOf("$$$")==-1;
		}

		/// <summary>
		/// This method is to verify that we are not trying to promote an object
		/// whose parent has not been promoted yet
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static bool AllBranchesAttached(IStateObject obj)
		{
			return AllBranchesAttached(obj.UniqueID);
		}

		public static bool AllBranchesAttached(string uniqueId)
		{
			return !uniqueId.Contains("$");
		}

		public void PromoteObject(IStateObject obj)
		{
			// If promoting an id that is marked to be deleted, 
			// then remove it from the elements-to-remove list
			UndoRemove(obj.UniqueID);

			string keyToRemove = null;
			foreach (var pair in _tempcache)
			{
				if (pair.Value == obj)
				{
					keyToRemove = pair.Key;
				}
			}
			Debug.Assert(keyToRemove != null);
			_tempcache.Remove(keyToRemove);
			//Find all children of this key
			UpdateAllDependends(keyToRemove, obj.UniqueID);
			AddInCache(obj.UniqueID, obj);
		}

		public void UpdateAllDependends(string keyToRemove, string parentUniqueId, bool promote = true)
		{
			foreach (var pair in GetAllDependendsKeys(keyToRemove))
			{

				_tempcache.Remove(pair.Key);
                pair.Value.UniqueID = ComputeNewDependantUniqueID(keyToRemove, pair.Key, parentUniqueId);
				if (promote && AllBranchesAttached(pair.Value.UniqueID))
				{
					UpdateDependantPointer(pair.Key, pair.Value.UniqueID);
					UndoRemove(pair.Value.UniqueID);
					AddInCache(pair.Value.UniqueID, pair.Value);
				}
				else
					_tempcache[pair.Value.UniqueID] = pair.Value;
			}
		}

		/// <summary>
		/// Replaces the surrogate pointer references from one pointer to another.  This method is called 
		/// everytyime a pointer is promoted because its parent object has been attached to a view model object.
		/// </summary>
		/// <param name="pointerOldUniqueId">Unique id of the object that was not attached.</param>
		/// <param name="pointerNewUniqueId">Uniqued Id of the object pointer once attached.</param>
		private void UpdateDependantPointer(string pointerOldUniqueId, string pointerNewUniqueId)
		{
			// lets check if it is a pointer if it is registered as surrogate object pointer.
			if (IsPointer(pointerOldUniqueId) && _surrogatedObjects.ContainsKey(pointerOldUniqueId))
			{
				var pointedSurrogate = _surrogatedObjects[pointerOldUniqueId];
				// lets upadte the surrogate referenced objects
				var surrogateIds = pointedSurrogate.ToList();
				foreach (var surrogateId in surrogateIds)
				{
					var surrogate = (StateObjectSurrogate) GetObject(surrogateId);
					AddSurrogateReference(surrogate, pointerNewUniqueId);
					RemoveSurrogateReference(surrogate, pointerOldUniqueId);
				}
				// removes the old pointer from surrogated objects list
				_surrogatedObjects.Remove(pointerOldUniqueId);
			}
		}

		/// <summary>
		///     It is assumed that this method is ONLY called for new objects
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		internal IStateObject AddNewObject(IStateObject obj)
		{
			if (!IsRootLevelObject(obj) && !IsAttachedChild(obj))
			{
				_tempcache.Add(obj.UniqueID, obj);
			}
			else
			{
				AddInCache(obj.UniqueID, obj);
			}
			UndoRemove(obj.UniqueID);
			Tracker.AttachModel(obj, true);
			return obj;
		}

		internal IStateObject AddNewObjectWithSurrogate(object obj, string uniqueId)
		{
			var surrogate = CreateObjectSurrogate(obj, uniqueId);
			return AddNewObject(surrogate);
		}

		internal IStateObject CreateObjectSurrogate(object obj, string uid)
		{
			return new StateObjectSurrogate {UniqueID = uid, Value = obj};
		}


		public void Persist()
		{
			/// Step 1 : Apply id switches
			foreach (var valuePair in _switchedIds)
			{
				StorageManager.SwitchUniqueIDsFrom(itemUID: valuePair.Item1, lastUID: valuePair.Item2);
			}

			// Step 2: Apply property changes to storage
			foreach (DirtyEntry dirtyEntry in DirtyEntries)
			{
				if (dirtyEntry.Value is StateObjectSurrogate)
					continue;
				StorageManager.SaveObject(dirtyEntry.Value);
			}

			// Step 3: Remove elements from storage
			foreach (var idOfElementToRemove in _elementsToRemove)
			{
				StorageManager.RemoveObject(idOfElementToRemove);
			}


			var currentSurrogates = new List<StateObjectSurrogate>();
			currentSurrogates.AddRange(_surrogates.Values);

			// Step 4: Save surrogates
			foreach (var surrogate in currentSurrogates)
			{
				if (surrogate.HasReferences())
				StorageManager.SaveSurrogate(surrogate);
			}

			//Step 5. Save any new surrogates
			var currentSurrogatesAfterFirstPass = new List<StateObjectSurrogate>();
			currentSurrogatesAfterFirstPass.AddRange(_surrogates.Values.Where(x => !currentSurrogates.Contains(x)));

			foreach (var surrogate in currentSurrogatesAfterFirstPass)
			{
				StorageManager.SaveSurrogate(surrogate);
			}

			StorageManager.SaveObject(UniqueIDGenerator.Current);
			RequestTimeStorage.Current.Remove(UniqueIDGenerator.UniqueIdGeneratorCacheId);
		}

		/// <summary>
		/// Calculates the delta object that requires client synchronization.
		/// <p>Objects that does not qualified to be sinchronized are:</p>
		/// <list type="bullet">
		/// <item>IInternalData objects</item>
		/// <item>Objects contained by an IInternalData object</item>
		/// <Item>ILogincSinglenton objects that has no serializable state</Item>
		/// </list>
		/// </summary>
		/// <returns></returns>
		internal IEnumerable<object> GetDeltasForClientSync()
		{
			return DirtyEntries.
				Where(x =>
					!(x.Value is IInternalData) && !(BelongsToInternalData(x.Value)) &&
					(!(x.Value is ILogicClass) || LogicSingletonSerializer.HasSerializableState(x.Value))).
				Select(e => new {UniqueID = e.Value.UniqueID, e.Delta});
		}

		internal IEnumerable<string> GetElementsToRemove()
		{
			return this._elementsToRemove;
		}

		internal IEnumerable<string[]> GetSwitchedIds()
		{
			return this._switchedIds.Select(switchedIdPair => new string[] {switchedIdPair.Item1, switchedIdPair.Item2});
		}

		internal bool HasSwichedIds
		{
			get { return this._switchedIds.Count > 0; }
		}


		internal void RemoveObject(string uniqueId, bool deep = true)
		{
            IStateObject objToRemove;
			if (_tempcache.ContainsKey(uniqueId))
			{
				_tempcache.Remove(uniqueId);
			}
			else if (_cache.TryGetValue(uniqueId, out objToRemove)
					 && Tracker.IsDirtyModel(objToRemove))
			{
				this.Tracker.DetachModel(objToRemove);
				_cache.Remove(uniqueId);
			}
			else
			{
				// lets check for surrogates and binding objects first
				if (IsBindingObject(uniqueId))
				{
					RemoveBindingObject(uniqueId);
				}
				// letch check for state object pointers 
				if (IsPointer(uniqueId))
				{
					var pointer = (StateObjectPointer)GetObject(uniqueId);
					if (IsSurrogate(pointer.TargetUniqueId))
					{
						// lets get the surrogate just to remove reference
						GetObject(pointer.TargetUniqueId);
					}
				}

				RemoveFromAllSurrugateReferences(uniqueId);
				if (_cache.ContainsKey(uniqueId))
				{
					Tracker.DetachModel(_cache[uniqueId]);
					_cache.Remove(uniqueId);
				}
				//Avoid removing the object at this moment
				if (!_elementsToRemove.Contains(uniqueId))
				{
					_elementsToRemove.Add(uniqueId);
				}
			}

            if (deep)
            {
                var dependants = StorageManager.GetChildren(uniqueId).ToList();
                foreach (var idToDelete in dependants)
                {
                    if (idToDelete != uniqueId)
                    {
                        RemoveObject(idToDelete, deep: false);
                    }
                }
            }
		}

		private void RemoveBindingObject(string uniqueId)
		{
					var pointer = GetObject(uniqueId) as StateObjectPointer;
					if (pointer != null && pointer.TargetUniqueId != null)
					{
						var surrogate = pointer.Target as StateObjectSurrogate;
						if (surrogate == null  && pointer.Target is StateObjectPointer)
						{
							RemoveBindingObject(pointer.Target.UniqueID);
						}
						else
						{
							var dataBindingToDelete = surrogate.Value as DataBinding;
							// simple case, removing the only binding left
							if (dataBindingToDelete.PreviousBinding != null ||
							    dataBindingToDelete.NextBinding != null)
							{
								// lets adjust the previous  pointer in order to update previous element
								// next pointer
								if (dataBindingToDelete.PreviousBinding != null)
								{
									var pPointer = (StateObjectPointer) GetObject(dataBindingToDelete.PreviousBinding);
									// checking if deleting first element (previous pointing to "first element pointer"
									if (pPointer.Target is StateObjectPointer)
									{
										pPointer.TargetUniqueId = dataBindingToDelete.NextBinding;
										if (pPointer.TargetUniqueId == null)
										{
											RemoveObject(pPointer.UniqueID);
										}
										else
										{
											StorageManager.SaveObject(pPointer);
										}
									}
									else
									{
										((DataBinding) ((StateObjectSurrogate) pPointer.Target).Value).NextBinding = dataBindingToDelete.NextBinding;
									}
								}
								if (dataBindingToDelete.NextBinding != null)
								{
									var pPointer = GetObject(dataBindingToDelete.NextBinding);
									((DataBinding) ((StateObjectSurrogate) ((StateObjectPointer) pPointer).Target).Value).PreviousBinding =
										dataBindingToDelete.PreviousBinding;
								}
							}
							_surrogates.Remove(surrogate.Value);
							_surrogatesIdsToSurrogates.Remove(surrogate.UniqueID);
							RemoveObject(surrogate.UniqueID);
						}
					}
				}


		internal IStateObject GetObject(string uniqueId)
		{
			IStateObject obj;
			if (_tempcache.TryGetValue(uniqueId, out obj))
			{
				return obj;
			}
			if (_cache.TryGetValue(uniqueId, out obj))
			{
				return obj;
			}
			//Cache failed!. Go to storage to see if can be recovered from there    
			//When retrieving from storage Events must be disabled

			var isViewStateSingletonsRelated = ViewsState.UniqueIdViewmanagerState.Equals(uniqueId) ||
			                                   ViewManager.ViewManagerStateCacheUid.Equals(uniqueId) ||
			                                   UniqueIDGenerator.UniqueIdGeneratorCacheId.Equals(uniqueId);

			try
			{
				if (!isViewStateSingletonsRelated)
				{
					ViewManager.Instance.Events.Suspend();
				}
				if (IsSurrogate(uniqueId))
				{
					StateObjectSurrogate surrogate = null;
					if (!_surrogatesIdsToSurrogates.TryGetValue(uniqueId, out surrogate))
					{
						surrogate = (StateObjectSurrogate) GetObjectRaw(uniqueId);
						surrogate.Parent = this;
						_surrogates.Add(surrogate.Value, surrogate);
						_surrogatesIdsToSurrogates.Add(uniqueId, surrogate);
					}
					return surrogate;
				}
				obj = StorageManager.GetObject(uniqueId);
				//Workaround for module
				if (obj != null)
					obj.UniqueID = uniqueId;
			}
			finally
			{
				if (!isViewStateSingletonsRelated)
				{
					ViewManager.Instance.Events.Resume();
				}
			}
			if (obj != null)
			{
				AddInCache(uniqueId, obj);
				Tracker.AttachModel(obj);
				return obj;
			}
			return null;
		}

		/// <summary>
		/// Indicates whether the object identified by the given id is a surrogate or not.
		/// </summary>
		/// <param name="uniqueId">The id of the object to test for.</param>
		/// <returns><c>true</c> if the object is a surrogate.</returns>
		private static bool IsSurrogate(string uniqueId)
		{
			return uniqueId.Contains("!") && !uniqueId.Contains("#");
		}

		internal void SyncDirtyModel(JToken jobj)
		{
			string uniqueId = ((JProperty) (jobj)).Name;
			IStateObject obj = GetObject(uniqueId);
			Debug.Assert(obj != null, "Model to sync was not found ID:" + uniqueId);
			JToken val = jobj.FirstOrDefault();
			if (val != null && (!(val is JArray) || (((JArray) val).Count > 0 && ((JArray) val)[0].HasValues)))
			{
				JsonConvert.PopulateObject(val.ToString(), obj,
					new JsonSerializerSettings
					{
						TypeNameHandling = TypeNameHandling.None,
						ContractResolver = new PropertySkiperContractResolver(),
						Converters =
							new JsonConverter[]
							{new ViewsStateDeltaConverter(), new ObservableCollectionExSerializer(), new ViewModelCollectionConverter()}
					});
			}
		}

		internal object GetModelEntries()
		{
			IEnumerable<IStateObject> res = from v in _cache
				where (v.Value is IStateObject
				       && !(v.Value is IInternalData)
				       && !(v.Value is StateObjectSurrogate))
				select v.Value;
			return res;
		}

		/// <summary>
		/// This method is used to make only when the users press F5 and the state of the cache needs to load
		/// the view models that need to be in sync between frontend and backend
		/// </summary>
		internal void LoadAllEntriesThanNeedToBeOnFrontEnd()
		{
			foreach (string key in StorageManager.GetAllItems())
			{
				if (!IsEntryThatIsPrimaryForBackend(key))
				{
					if (!_cache.ContainsKey(key))
					{
						GetObject(key); //This will load this entry in the StateCache
					}
				}
			}
		}

		internal bool IsEntryThatIsPrimaryForBackend(string uniqueId)
		{
			if (uniqueId.Contains("!") || uniqueId.Contains("^") || uniqueId.Contains("@"))
				return true;
			return false;
		}
        internal void SwitchToNotExistingUniqueId(string currentId, string newId)
        {
            IStateObject currentObject, newObject;

            if (_cache.TryGetValue(currentId, out currentObject)
                     && this.Tracker.IsDirtyModel(currentObject))
            {
                _cache[newId] = currentObject;
                currentObject.UniqueID = newId;
                UpdateAllDependends(currentId, newId, false);
                return;
            }
            else
            {
                RegisterIdSwitch(currentId, newId);
            }
        }

		internal void SwitchUniqueIds(string currentId, string newId)
		{
            IStateObject currentObject, newObject;

			if (!AllBranchesAttached(currentId))
			{
				//This entry is in the _tempcache
				var tempEntry = _tempcache[currentId];
				_tempcache.Remove(currentId);
				_tempcache.Add(newId, tempEntry);
				UpdateAllDependends(currentId, tempEntry.UniqueID, false);
				return;
			}
            else if (_cache.TryGetValue(currentId, out currentObject)
                     && _cache.TryGetValue(newId, out newObject)
                     && this.Tracker.IsDirtyModel(currentObject)
                     && this.Tracker.IsDirtyModel(newObject))
            {

                //_cache[currentId] = _cache[newId];
                //_cache[currentId].UniqueID = currentId;
                _cache[newId] = currentObject;
                _cache[newId].UniqueID = newId;
                UpdateAllDependends(currentId, newObject.UniqueID, false);
                return;
            }
            else
			{
                RegisterIdSwitch(currentId, newId);
			}
		}

        private void RegisterIdSwitch(string currentId, string newId)
        {
            Tuple<string, string> existingSwitchToId;
            // Verify if we are switching from an id that is the target of another switch
            if ((existingSwitchToId = _switchedIds.FirstOrDefault(x => x.Item2 == currentId)) != null)
            {
                var idx = this._switchedIds.IndexOf(existingSwitchToId);
                if (existingSwitchToId.Item1 != newId)
                {
                    // If switching from a -> b and from b -> c then just leave
                    // the switch from a -> c
                    var newSwitch = Tuple.Create(existingSwitchToId.Item1, newId);
                    this._switchedIds[idx] = newSwitch;
                }
                else
                {
                    // If switching to itself just remove the switch
                    this._switchedIds.Remove(existingSwitchToId);
                }
            }
            else
            {
                // If it's no target just add the switch
                _switchedIds.Add(Tuple.Create(currentId, newId));
                // Switch its children elements too
                foreach(var related in StorageManager.GetChildren(currentId))
                {
                    if (related != currentId)
                    {
                        _switchedIds.Add(Tuple.Create(related, related.Replace(currentId, newId)));
                    }
                }
            }

            // if we are switching to an id to be removed then, 
            // remove the id from  the elements to be removed
			UndoRemove(newId);
        }

		internal IViewModel GetParentViewModel(IStateObject source)
		{
			var parentId = source.UniqueID.Split(new[] {UniqueIdSeparator}, StringSplitOptions.None).Last();
			return (IViewModel) GetObject(parentId);
		}

		private IDictionary<string, IList<string>> _surrogatedObjects = new Dictionary<string, IList<string>>();

		internal void AddSurrogateReference(StateObjectSurrogate surrogate, string uniqueId)
		{
			surrogate.AddReference(new LazyStateObjectReference(this, uniqueId));
			RegisterSurrogatedObject(surrogate, uniqueId);
		}

		internal void AddSurrogateReference(StateObjectSurrogate surrogate, StateObjectPointer pointer, bool isNewPointer = true)
		{
			surrogate.AddReference(new LazyStateObjectReference(this, pointer));
			RegisterSurrogatedObject(surrogate, pointer.UniqueID);
			pointer.Target = surrogate;
			pointer.TargetUniqueId = surrogate.UniqueID;
			if (isNewPointer)
				AddNewObject(pointer);
			else if (AllBranchesAttached(pointer))
				StorageManager.SaveObject(pointer);
			UndoRemove(pointer.UniqueID);
		}

		private void RegisterSurrogatedObject(StateObjectSurrogate surrogate, string uniqueId)
		{
			if (!_surrogatedObjects.ContainsKey(uniqueId))
			{
				_surrogatedObjects.Add(uniqueId, new List<string>());
			}
			_surrogatedObjects[uniqueId].Add(surrogate.UniqueID);
		}

		internal void RemoveSurrogateReference(StateObjectSurrogate surrogate, IStateObject oldValue)
		{
			//surrogate.RemoveReference(oldValue);
		}

		internal void RemoveSurrogateReference(StateObjectSurrogate surrogate, string uniqueId)
		{
			surrogate.RemoveReference(uniqueId);
			if (!surrogate.HasReferences())
				RemoveObject(surrogate.UniqueID, true);
			_surrogatedObjects[uniqueId].Remove(surrogate.UniqueID);
		}

		internal void RemoveFromAllSurrugateReferences(string uniqueId)
		{
			if (_surrogatedObjects.ContainsKey(uniqueId))
			{
				var cloned = _surrogatedObjects[uniqueId].ToList();
				foreach (var surrogatedObject in cloned)
				{
					StateObjectSurrogate surrogate;
					if (_surrogatesIdsToSurrogates.TryGetValue(surrogatedObject, out surrogate))
						RemoveSurrogateReference(surrogate, uniqueId);
				}
			}
		}

		internal StateObjectSurrogate GetSurrogateFor(object newValueObject, bool generateIfNotFound)
		{
			StateObjectSurrogate surrogate;
			if (_surrogates.TryGetValue(newValueObject, out surrogate))
			{
				return surrogate;
			}
			if (generateIfNotFound)
			{
				_surrogates.Add(newValueObject,
					surrogate =
						new StateObjectSurrogate
						{
							UniqueID = "!" + UniqueIDGenerator.Current.GetUniqueID(),
							Value = newValueObject
						});
				AddNewObject(surrogate); //TODO Add in cache not not mark as dirty because all surrogates are dirty
			}
			return surrogate;
		}


		internal object GetObjectRaw(string uniqueId)
		{
			var raw = StorageManager.GetRaw(uniqueId);
			if (raw == null)
				throw new Exception("No data was found for the given uniqueId");
			var obj = SurrogatesDirectory.RawToObject(raw);
			return obj;
		}

		/// <summary>
		/// Builds a pointer unique id from another unique id.
		/// </summary>
		/// <param name="uniqueId">The base element to build a unique id from.</param>
		/// <returns></returns>
		internal static  string GetPointerUniqueId(string uniqueId)
		{
			return "->" + uniqueId;
		}

		/// <summary>
		/// Indicates whether the given unique id is a state pointer one.
		/// </summary>
		/// <param name="uniqueId"></param>
		/// <returns></returns>
		internal static bool IsPointer(string uniqueId)
		{
			return uniqueId.StartsWith("->");
		}

		/// <summary>
		/// Indicates whether the given state object is contained by an <c>IStateObject</c> element or not.
		/// </summary>
		/// <param name="stateObject">The object to test for</param>
		/// <returns></returns>
		private bool BelongsToInternalData(IStateObject stateObject)
		{
			if (stateObject is IModelList<IStateObject>)
			{
				var modelList = (IModelList<IStateObject>) stateObject;
				var parent = GetParent(modelList);
				return parent is IInternalData;
			}
			return false;
		}

		/// <summary>
		/// Gets the parent object for the given state object
		/// </summary>
		/// <param name="stateObject">State object to get parent for</param>
		/// <returns>The state object</returns>
		private IStateObject GetParent(IStateObject stateObject)
		{
			var index = stateObject.UniqueID.IndexOf(UniqueIdSeparator);
			if (index != -1)
			{
				var parentId = stateObject.UniqueID.Substring(index + 3);
				return GetObject(parentId);
			}
			return null;
		}

		/// <summary>
		/// Gets the elements from the temporary cache that belongs to the object identified by the given id
		/// </summary>
		/// <param name="uniqueId">Id of the object to get dependencies for.</param>
		/// <returns></returns>
		private IEnumerable<KeyValuePair<string, IStateObject>> GetAllDependendsKeys(string uniqueId)
		{
			var keys = _tempcache.ToList();
			foreach (var key in keys)
			{
				//We do not want to send as a dependend the same key
				if (key.Key != uniqueId && key.Key.EndsWith(uniqueId))
					yield return key;
			}
		}

		/// <summary>
		/// Adds the given object to the cache
		/// </summary>
		/// <param name="uniqueId">The id identifying the object to add</param>
		/// <param name="stateObject">The state object to add.</param>
		private void AddInCache(string uniqueId, IStateObject stateObject)
		{
			Debug.Assert(uniqueId != null && AllBranchesAttached(stateObject));
			_cache.Add(uniqueId, stateObject);
		}


		/// <summary>
		/// Checks if the given id matches a first level object
		/// </summary>
		/// <param name="uniqueId">The id identifying the object to test for</param>
		/// <returns><c>true</c> if the object is a first level one, otherwise <c>false</c></returns>
		private static bool IsRootLevelObject(string uniqueId)
		{
			return !uniqueId.Contains(UniqueIdSeparator);
		}

		/// <summary>
		/// Indicates whether the given unique id identifies a binding object or not.
		/// </summary>
		/// <param name="uniqueId">The unique id to test for</param>
		/// <returns><c>true</c> if the given unique id matches a binding object.</returns>
		private static bool IsBindingObject(string uniqueId)
		{
			return uniqueId.StartsWith(BindingKey);
		}


		/// <summary>
		/// Creates a dependand unique id for the object identified by <c>uniqueId</c>
		/// </summary>
		/// <param name="keyToRemove"></param>
		/// <param name="uniqueId"></param>
		/// <param name="parent"></param>
		/// <returns></returns>
		private string ComputeNewDependantUniqueID(string keyToRemove, string uniqueId, string newParentUniqueId)
		{
            return uniqueId.Replace(keyToRemove, newParentUniqueId);
		}


		internal class LazyStateObjectReference
		{
			private StateCache _parent;

			public LazyStateObjectReference(StateCache parent, string uniqueId)
			{
				_uniqueId = uniqueId;
				this._parent = parent;
			}

			public LazyStateObjectReference(StateCache parent, IStateObject target)
			{
				_uniqueId = target.UniqueID;
				_target = target;
				this._parent = parent;
			}

			private string _uniqueId;

			public String UniqueID
			{
				get
				{
					//if (_target == null)
					//{
					//	_target = _parent._cache[_uniqueId];
					//}
					//if (_target != null)
					//{
					//	return _target.UniqueID;
					//}
					return _uniqueId;
				}
				set { _uniqueId = value; }
			}

			private IStateObject _target;

			public IStateObject Target
			{
				get
				{
					if (_target != null)
					{
						return _target;
					}
					return null;
				}

			}
		}


		public class DataBinding
		{
			internal string _objReferenceUID;
			private IStateObject _objReference;

			internal IStateObject ObjReference
			{
				get
				{
					if (_objReference == null && _objReferenceUID != null)
					{
						_objReference = StateCache.Current.GetObject(_objReferenceUID) as IStateObject;
					}
					return _objReference;
				}
				set
				{
					_objReference = value;
					if (_objReference != null)
					{
						_objReferenceUID = _objReference.UniqueID;
					}
					else
					{
						_objReferenceUID = null;
					}
				}
			}

			internal string ObjProperty;
			internal IStateObject DataSourceReference;
			internal string DataSourceProperty;
			internal string NextBinding { get; set; }
			internal string PreviousBinding { get; set; }

			internal string ReferencedUniqueID
			{
				get { return _objReferenceUID; }
				set { _objReferenceUID = value; }
			}
		}

		public IDictionary<IStateObject, List<DataBinding>> Bindings = new Dictionary<IStateObject, List<DataBinding>>();

		public void Bind(IIocContainer container, IStateObject obj, string objProperty, object dataSource,
			string dataSourceProperty)
		{
			var uuidgen = UniqueIDGenerator.Current;

			if (dataSource == null)
				throw new ArgumentException("Datasource argument cannot be null");

			if (obj == null)
				throw new ArgumentException("Binded object cannot be null");

			if (string.IsNullOrWhiteSpace(objProperty) || string.IsNullOrWhiteSpace(dataSourceProperty))
				throw new ArgumentException("Missing or invalid property names for binding");

			//The DataSource can be a surrogate.
			//When this happens then we need to create two pointers.
			//One that will be used from the surrogate to the Databinding
			if (SurrogatesDirectory.IsSurrogateRegistered(dataSource.GetType()))
			{
				var dsSurrogate = GetSurrogateFor(dataSource, false);
				if (dsSurrogate == null)
					throw new InvalidOperationException("Invalid state a surrogate should exist for object at this point");

				var uidBindingForDsSurrogate = uuidgen.GetRelativeUniqueID(dsSurrogate, BindingKey);

				//First lets find if there is alread a binding 
				//We need a deterministic ID that we can use to find the databinding from the surrogate

				var pointerToFirst = GetObject(uidBindingForDsSurrogate) as StateObjectPointer;
				DataBinding currentLastBinding = null;
				StateObjectPointer currentLastBindingPointer = null;
				bool firstElement = false;
				if (pointerToFirst == null)
				{
					// the pointer to the first element was not found, lets create it and point it to first element
					pointerToFirst = container.Resolve<StateObjectPointer>();
					pointerToFirst.UniqueID = uidBindingForDsSurrogate;
					firstElement = true;
				}
				else
				{
					if (pointerToFirst.TargetUniqueId == null)
					{
						firstElement = true;
					}
					else
					{

						// lets find the last node
						currentLastBindingPointer = (StateObjectPointer) pointerToFirst.Target;
						bool doWhile;
						do
						{
							var surrogate = (StateObjectSurrogate) currentLastBindingPointer.Target;
							currentLastBinding = (DataBinding) surrogate.Value;
							doWhile = (currentLastBinding.NextBinding != null);
							if (doWhile)
								currentLastBindingPointer =
									(StateObjectPointer) GetObject(currentLastBinding.NextBinding);
						} while (doWhile);
					}
				}

				// lets create the binding and its surrogate object
				var newDatabinding = new DataBinding
				{
					ObjReference = obj,
					ObjProperty = objProperty,
					DataSourceProperty = dataSourceProperty,
					PreviousBinding = firstElement ? null : currentLastBindingPointer.UniqueID,
					DataSourceReference = dsSurrogate
				};
				var newDatabindingSurrogate = GetSurrogateFor(newDatabinding, true);

				// lets create the new pointer to the binder
				var newBindingPointer = container.Resolve<StateObjectPointer>();
				newBindingPointer.UniqueID = uuidgen.GetRelativeUniqueID(pointerToFirst);
				//Now keep track that the DS surrogate has a pointer to the DataBinding surrogate (this is for reference counting)
				AddSurrogateReference(newDatabindingSurrogate, newBindingPointer);

				if (firstElement)
				{
					pointerToFirst.TargetUniqueId = newBindingPointer.UniqueID;
					newDatabinding.PreviousBinding = pointerToFirst.UniqueID;
					AddNewObject(pointerToFirst);
				}
				else
				{
					currentLastBinding.NextBinding = newBindingPointer.UniqueID;
					newDatabinding.PreviousBinding = currentLastBindingPointer.UniqueID;
				}

				////Now we need also reference counting but for the datasource surrogate
				var pointerObjectToBinding = container.Resolve<StateObjectPointer>();
				pointerObjectToBinding.UniqueID = uuidgen.GetRelativeUniqueID(obj, objProperty + BindingKey);
				AddSurrogateReference(newDatabindingSurrogate, pointerObjectToBinding);



				var getter = SurrogatesDirectory.GetPropertyGetter(dataSource);
				if (getter == null)
					throw new InvalidOperationException("No getter was registered for dataSource " + dataSource.GetType());
				var value = getter(dataSource, newDatabinding.DataSourceProperty);
				var target = newDatabinding.ObjReference;
				var propInfo = target.GetType().Property(newDatabinding.ObjProperty);
				propInfo.SetValue(target, Convert.ChangeType(value, propInfo.PropertyType), null);


				int count = RegisterBinding(newDatabinding, dsSurrogate);
				if (count == 1)
				{
					this.AttachBindingBehaviourForSurrogate(dsSurrogate, dataSource);
				}
				return;
			}
			throw new NotImplementedException();
		}

		internal HashSet<IStateObject> skip = null;

		internal bool IsSkip(IStateObject model)
		{
			if (skip == null)
				return false;
			else
				return skip.Contains(model);
		}

		private void AddSkip(IStateObject model)
		{
			if (skip == null)
			{
				skip = new HashSet<IStateObject>();
			}
			skip.Add(model);
		}

		internal bool ContainsSkip(IStateObject model)
		{
			if (skip == null)
				return false;
			return skip.Contains(model);
		}

		private void RemoveSkip(IStateObject model)
		{
			if (skip != null)
			{
				skip.Remove(model);
			}
		}

		//Currently there is a limitation that bindings added this method will not be seen	
		private void RestoreDataBindings(StateObjectSurrogate surrogate)
		{
			StateCache stateCache = this;
			string relativeUidForBinding = UniqueIDGenerator.Current.GetRelativeUniqueID(surrogate, "^Binding");
			var pointerToFirstBinding = stateCache.GetObject(relativeUidForBinding) as StateObjectPointer;
			if (pointerToFirstBinding != null)
			{
				// 
				var pointerToDataBinding = pointerToFirstBinding.Target as StateObjectPointer;
				bool more = true;
				//
				do
				{
					var dataBindingSurrogate = pointerToDataBinding.Target as StateObjectSurrogate;
					more = dataBindingSurrogate != null;
					if (more)
					{
						var dataBinding = dataBindingSurrogate.Value as StateCache.DataBinding;
						stateCache.RegisterBinding(dataBinding, surrogate);
						if (dataBinding.NextBinding != null)
						{
							pointerToDataBinding =
								stateCache.GetObject(dataBinding.NextBinding) as StateObjectPointer;
						}
						else
							more = false;
					}

				} while (more);
			}
		}

		internal void AttachBindingBehaviourForSurrogate(StateObjectSurrogate surrogate, object cachedValue)
		{
			if (!SurrogatesDirectory.SupportsBinding(cachedValue)) return;
			StateCache stateCache = this;
			var getter = SurrogatesDirectory.GetPropertyGetter(cachedValue);

			var setter = SurrogatesDirectory.GetPropertySetter(cachedValue);
			//var setter = SurrogatesDirectory.GetPropertySetter(cachedValue);
			Func<Action<bool>> temp = () =>
			{
				string relativeUidForBinding = UniqueIDGenerator.Current.GetRelativeUniqueID(surrogate, "^Binding");
				bool firstTime = true;
				return (bool isAfter) =>
				{
					if (firstTime)
					{
						//Load bindings
						firstTime = false;
						RestoreDataBindings(surrogate);
					}
					var bindings = stateCache.GetRegisteredBindings(surrogate);
					if (!isAfter)
					{
						foreach (var binding in bindings)
						{
							var target = binding.ObjReference;
							try
							{
								stateCache.AddSkip(target);
								var propInfo = target.GetType().Property(binding.ObjProperty);
								object val = propInfo.GetValue(target, null);
								setter(cachedValue, binding.DataSourceProperty, val);
							}
							finally
							{
								stateCache.RemoveSkip(target);
							}
						}
					}
					else
					{
						foreach (var binding in bindings)
						{
							var target = binding.ObjReference;
							try
							{
								stateCache.AddSkip(target);
								var propInfo = target.GetType().Property(binding.ObjProperty);
								object val = getter(cachedValue, binding.DataSourceProperty);
								object valueToSet;
								if (DBNull.Value.Equals(val) && !propInfo.PropertyType.IsValueType)
								{
									valueToSet = null;
								}
								else
								{
									valueToSet = Convert.ChangeType(val, propInfo.PropertyType);
								}
								propInfo.SetValue(target, valueToSet, null);
							}
							finally
							{
								stateCache.RemoveSkip(target);
							}
						}
					}
					//TriggerEvent
				};
			};
			SurrogatesDirectory.ApplyBindingHandlers(cachedValue, temp());
			//Is there a binding?
		}



		internal int RegisterBinding(DataBinding dataBinding, IStateObject ds)
		{
			Debug.Assert(ds != null);
			Debug.Assert(dataBinding != null);
			List<DataBinding> bindings = null;
			if (!Bindings.TryGetValue(ds, out bindings))
			{
				bindings = new List<DataBinding>();
				Bindings[ds] = bindings;
			}
			bindings.Add(dataBinding);
			return bindings.Count();
		}

		internal IEnumerable<DataBinding> GetRegisteredBindings(IStateObject ds)
		{

			List<DataBinding> bindings = null;
			if (!Bindings.TryGetValue(ds, out bindings))
			{
				bindings = new List<DataBinding>();
			}
			return bindings;
		}

		internal static bool IsChild(string parentId, string childId)
		{
			return childId.EndsWith(parentId);
		}

		internal void RemoveEntryFromCache(string uniqueId)
		{
			this._cache.Remove(uniqueId);
		}

		internal string GetSubViewId(IDependantViewModel item)
		{
			var id = item.UniqueID.Split('#').First();
			return id;
		}

		private void UndoRemove(string uniqueId)
		{
			if (this._elementsToRemove.Contains(uniqueId))
			{
				this._elementsToRemove.Remove(uniqueId);
			}
		}

	}

	public class DirtyEntry
	{
		public IStateObject Value { get; set; }
		public object Delta { get; set; }
	}
}
