﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Fasterflect;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
	/// <summary>
	///     The representation of a object of change
	/// </summary>
	public class Delta : Dictionary<string, object>
	{
		public void Print(TextWriter writer, int indentSize = 0)
		{
			var indent = new string(' ', indentSize);
			writer.WriteLine(indent + "{");
			foreach ( var pair in this )
			{
				writer.Write(indent + "\"" + pair.Key + "\" = ");
				object value = pair.Value;
				if ( value is Delta )
				{
					writer.WriteLine();
					((Delta)(value)).Print(writer, indentSize + 4);
				}
				else
				{
					writer.WriteLine("\"" + pair.Value + "\",");
				}
			}
			writer.WriteLine(indent + "}");
		}
	}

	/// <summary>
	///     The Class responsible of the Tracking of Deltas
	/// </summary>
	public class DeltaTracker
	{
		private List<IStateObject> attachedModels = new List<IStateObject>();
		private VisitingState context = new VisitingState();
		private HashSet<IStateObject> dirtyModels = new HashSet<IStateObject>();
        Dictionary<string, Tuple<WeakReference, FieldInfo, byte[]>> watchedFields;
        Dictionary<string, Tuple<WeakReference, PropertyInfo, byte[]>> watchedProperties;

        internal void WatchField(IStateObject parent, FieldInfo prop, object theStruct, Type watcherType)
        {
            if (!dirtyModels.Contains(parent))
            {
                watchedFields = watchedFields ?? new Dictionary<string, Tuple<WeakReference, FieldInfo, byte[]>>();
                var key = parent.UniqueID + "--" + prop.Name;
                if (!watchedFields.ContainsKey(key))
                {
                    var watcher = (IDeltaWatcher)Activator.CreateInstance(watcherType);
                    watchedFields.Add(key, Tuple.Create(new WeakReference(parent), prop, watcher.GetAsBytes(theStruct)));
                }
            }
        }

        internal void WatchProperty(IStateObject parent, PropertyInfo prop, object obj, Type watcherType)
        {
            if (!dirtyModels.Contains(parent))
            {
                watchedProperties = watchedProperties ?? new Dictionary<string, Tuple<WeakReference, PropertyInfo, byte[]>>();
                var key = parent.UniqueID + "--" + prop.Name;
                if (!watchedProperties.ContainsKey(key))
                {
                    var watcher = (IDeltaWatcher)Activator.CreateInstance(watcherType);
                    watchedProperties.Add(key, Tuple.Create(new WeakReference(parent), prop, watcher.GetAsBytes(obj)));
                }
            }
        }

		public IEnumerable<DirtyEntry> DirtyEntries
		{
			get
			{
                CheckWatchedFields();
                CheckWatchedProperties();
                //We need to clone this collection to avoid modifications to it while
                //calculating the deltas. This problem can occur when modifying an object
                //with complex properties not used in the same session
                var attachedModelsClone = this.attachedModels.ToArray();
				IEnumerable<DirtyEntry> first = from variable in attachedModelsClone
				                                let delta = GetCalculatedDeltaFor(variable)
				                                where delta != null
				                                select new DirtyEntry { Value = variable, Delta = delta };
				IEnumerable<DirtyEntry> second =
					from d in dirtyModels
					let delta = d
					select new DirtyEntry { Value = d, Delta = delta };

				return first.Union(second);
			}
		}


        private void CheckWatchedFields()
        {
            //Check watches
            if (watchedFields != null)
            {
                foreach (var entry in this.watchedFields.Keys)
                {
                    var contents = watchedFields[entry];
                    if (dirtyModels.Contains(contents.Item1.Target)) continue; //Parent already marked as dirty there is nothing else to do
                    var obj = contents.Item2.GetValue(contents.Item1.Target);
					var watcherAtt = contents.Item2.GetCustomAttributes(typeof(Watchable), true).FirstOrDefault() as Watchable;
                    if (watcherAtt != null)
                    {
                        var watcher = (IDeltaWatcher)Activator.CreateInstance(watcherAtt.WatcherType);
                        var oldBytes = contents.Item3;
                        var currentBytes = watcher.GetAsBytes(obj);
                        bool isDirty = false;
                        for (int i = 0; i < currentBytes.Length; i++)
                        {
                            if (currentBytes[i] != oldBytes[i])
                            {
                                isDirty = true;
                                break;
                            }
                        }
                        if (isDirty) dirtyModels.Add((IStateObject)contents.Item1.Target);
                    }
                }
            }
        }

        private void CheckWatchedProperties()
        {
            //Check watches
            if (watchedProperties != null)
            {
                foreach (var entry in this.watchedProperties.Keys)
                {
                    var contents = watchedProperties[entry];
                    if (dirtyModels.Contains(contents.Item1.Target)) continue; //Parent already marked as dirty there is nothing else to do
                    var obj = contents.Item2.GetValue(contents.Item1.Target, null);
					var watcherAtt = contents.Item2.GetCustomAttributes(typeof(Watchable), true).FirstOrDefault() as Watchable;
                    if (watcherAtt != null)
                    {
                        var watcher = (IDeltaWatcher)Activator.CreateInstance(watcherAtt.WatcherType);
                        var oldBytes = contents.Item3;
                        var currentBytes = watcher.GetAsBytes(obj);
                        bool isDirty = false;
                        for (int i = 0; i < currentBytes.Length; i++)
                        {
                            if (currentBytes[i] != oldBytes[i])
                            {
                                isDirty = true;
                                break;
                            }
                        }
                        if (isDirty) dirtyModels.Add((IStateObject)contents.Item1.Target);
                    }
                }
            }
        }

        private void RegisterWatchablePropertiesAndFields(IStateObject model)
        {
            //TODO: Probably catching type fields and properties is needed
            var fields = model.GetType().Fields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            var fieldsWithWatches = from field in fields
									let att = field.GetCustomAttributes(typeof(Watchable), true).FirstOrDefault() as Watchable
                                    where att != null
                                    select new { Field = field, WatcherType = att.WatcherType };
            foreach (var f in fieldsWithWatches)
            {
                var field = f.Field;
                var isStruct = field.FieldType.IsValueType && !field.FieldType.IsPrimitive && !field.FieldType.IsEnum;
                if (isStruct)
                    WatchField(model, f.Field, f.Field.GetValue(model), f.WatcherType);
            }

            var props = model.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            var propertiesWithWatches = from p in props
										let att = p.GetCustomAttributes(typeof(Watchable), true).FirstOrDefault() as Watchable
                                        where att != null
                                        select new { Property = p, WatcherType = att.WatcherType };
            foreach (var p in propertiesWithWatches)
            {
                var prop = p.Property;
                WatchProperty(model, p.Property, p.Property.GetValue(model, null), p.WatcherType);
            }

        }

		/// <summary>
		///     It resets the context and each of the subscribed model
		/// </summary>
		public void Start()
		{
			context.Reset();

			foreach ( IStateObject model in attachedModels )
			{
				TrackerHelper.ResetDeltas(model, ref context);
			}
		}

		/// <summary>
		///     It tries to get the index in the List of subscribed models, of the given model
		/// </summary>
		/// <param name="model">The instance of IChangesTrackeable</param>
		/// <returns>Index if model found, -1 otherwise</returns>
		private int IndexOfModel(IStateObject model)
		{
			int length = attachedModels.Count();
			for ( int i = 0; i < length; i++ )
			{
				if ( model.UniqueID == attachedModels[i].UniqueID )
				{
					return i;
				}
			}
			return -1;
		}

		/// <summary>
		///     It tries to subscribe to given model to the list of subscribed models
		///     If it succeeds, it resets the given model and adds a bit array of its properties
		/// </summary>
		/// <param name="model">The instance of IChangesTrackeable</param>
		public void AttachModel(IStateObject model, bool isDirty = false)
		{
			if ( isDirty )
			{
				dirtyModels.Add(model);
				return;
			}
            RegisterWatchablePropertiesAndFields(model);
			// Adding model to list of models
			int index = IndexOfModel(model);
			if ( index != -1 )
			{
				attachedModels.RemoveAt(index);
			}
			attachedModels.Add(model);

			// Adding data of model to bitArrays
			TrackerHelper.AddsBitArray(model);
			TrackerHelper.ResetDeltas(model, ref context);
		}

		/// <summary>
		///     It tries to removes the given model of the list of the subscribed models and the
		///     bit array of the data arrays
		/// </summary>
		/// <param name="model"></param>
		/// <returns>True if is succeeds, false otherwise</returns>
		public void DetachModel(IStateObject model)
		{
			// Removing model from list of models
			int index = IndexOfModel(model);
			if ( index != -1 )
			{
				attachedModels.RemoveAt(index);
				TrackerHelper.RemovesBitArray(model);
			}
				// It is possible that it is in the dirty models
			else
			{
				dirtyModels.Remove(model);
			}
		}

		/// <summary>
		///     When this method is called, all the changes occured so far,since it was started,
		///     in each of the subscribed model, are collected and storage in the context (visitating state)
		/// </summary>
		public void CollectDeltas()
		{
			context.Reset();

			foreach ( IStateObject model in attachedModels )
			{
#if Debug && Verbose
				Debug.WriteLine("Id of attached model: {0}", model.UniqueID);
#endif
				Delta internalDelta = TrackerHelper.GetDeltas(ref context, model);
				if ( internalDelta != null )
				{
					context.AddDelta(model.UniqueID, internalDelta);
				}
			}

			foreach ( IStateObject model in dirtyModels )
			{
				var modelDelta = new Delta();
				modelDelta.Add(model.UniqueID, model);
				context.AddDelta(model.UniqueID, modelDelta);
			}
		}

		/// <summary>
		///     Given the stream to be written, its prints the deltas collected so far in it
		/// </summary>
		/// <param name="writer">The stream to be written</param>
		public void PrintDeltas(TextWriter writer)
		{
			CollectDeltas();
			context.Print(writer);
		}

		/// <summary>
		///     Returns the Dictionary where all deltas are stored
		/// </summary>
		/// <returns>The dictionary of Deltas</returns>
		public Dictionary<string, Delta> GetResults()
		{
			return context.GetResult();
		}

		public void Reset()
		{
			dirtyModels.Clear();
			attachedModels.Clear();
			context.Reset();
		}

		internal object GetCalculatedDeltaFor(IStateObject variable)
		{
			return TrackerHelper.GetDeltas(ref context, variable);
		}

		public bool IsDirtyModel(IStateObject model)
		{
			return dirtyModels.Contains(model);
		}

		public bool IncludePropertyResolution(Object propValue)
		{
			if ( propValue is IStateObject )
			{
				return false;
			}
			return true;
		}
	}

	/// <summary>
	///     This class will hold al the Deltas collected in the subcribed viewModels of the delta tracker.
	///     It is like a context,
	/// </summary>
	public class VisitingState
	{
		private Dictionary<string, Delta> result;
		private List<string> visitedSharedNodes = new List<string>();

		public VisitingState()
		{
			result = new Dictionary<string, Delta>();
		}

		/// <summary>
		///     Checks if the given id was alredy visited by this context
		///     It assumes that the given id belongs to a IShared instance
		/// </summary>
		/// <param name="id">The id of the IShared instance</param>
		/// <returns>True is it was already visited, false otherwise</returns>
		public bool AlreadyVisited(string id)
		{
			return visitedSharedNodes.Contains(id);
		}

		/// <summary>
		///     It adds the given id to the list of visitated ids
		///     It assumes that the given id belongs to a IShared instance
		/// </summary>
		/// <param name="id">The id of a IShared instance</param>
		public void AddSharedNode(string id)
		{
			visitedSharedNodes.Add(id);
		}

		/// <summary>
		///     Adds the given delta, to its dictionary of deltas
		/// </summary>
		/// <param name="id">The id of the IChangesTrackeable instance</param>
		/// <param name="delta">The delta of the instance with the given id</param>
		public void AddDelta(string id, Delta delta)
		{
			result.Add(id, delta);
		}

		/// <summary>
		///     Prints , in a Json-like format, all the deltas collected
		/// </summary>
		/// <param name="writer">The stream where to write</param>
		public void Print(TextWriter writer)
		{
			writer.WriteLine("[");
			foreach ( var item in result )
			{
				writer.WriteLine();
				writer.WriteLine(" [" + item.Key + ",");
				item.Value.Print(writer, 4);
				writer.WriteLine(" ]");
			}
			writer.WriteLine();
			writer.WriteLine("]");
		}

		/// <summary>
		///     It clears the visited shared nodes ids, and the deltas collected
		/// </summary>
		public void Reset()
		{
			visitedSharedNodes.Clear();
			result.Clear();
		}

		/// <summary>
		///     Returns the Dictionary where all the deltas collected so far are stored
		/// </summary>
		/// <returns>The dictionary of deltas</returns>
		public Dictionary<string, Delta> GetResult()
		{
			return result;
		}
	}
}