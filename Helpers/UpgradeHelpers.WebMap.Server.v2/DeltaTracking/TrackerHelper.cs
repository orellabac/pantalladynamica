﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Fasterflect;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
	public static class TrackerHelper
	{
		//                                PropName tupla:{index, property info}

		public delegate bool IncludePropertyResolution(Object propValue);

		public const int NO_CHANGES_MADE_ON_SOURCE_COLLECTION = -1;

		private static Dictionary<Type, Dictionary<string, Tuple<int, PropertyInfo>>> cachedProperties =
			new Dictionary<Type, Dictionary<string, Tuple<int, PropertyInfo>>>();

		private static Dictionary<string, BitArray> bitArrays = new Dictionary<string, BitArray>();

		/// <summary>
		///     It clears the Dictionary of properties and the bit arrays dictionayr
		/// </summary>
		public static void Clear()
		{
			cachedProperties.Clear();
			bitArrays.Clear();
		}

		/// <summary>
		///     It tries to get the index of the given property, from the dictionary of its container's class
		/// </summary>
		/// <param name="property">Name of property</param>
		/// <param name="type">Type of instance</param>
		/// <returns>if property found and index, otherwise -1</returns>
		public static int IndexOfProperty(string property, Type type)
		{
			Dictionary<string, Tuple<int, PropertyInfo>> typeProperties = GetPropertiesOfType(type);
			Tuple<int, PropertyInfo> tupleProperty;

			if ( !typeProperties.TryGetValue(property, out tupleProperty) )
			{
				return -1;
			}

			return tupleProperty.Item1;
		}


		/// <summary>
		///     It collects all the delta objects int the given instance
		/// </summary>
		/// <param name="context">The visiting state</param>
		/// <param name="instance">The instance</param>
		/// <returns>The changes found in the instance</returns>
		public static Delta GetDeltas(ref VisitingState context, IStateObject instance)
		{
			BitArray data;
			var delta = new Delta();

			// if model has been subscribed to any delta tracker
			if ( bitArrays.TryGetValue(instance.UniqueID, out data) )
			{
				Dictionary<string, Tuple<int, PropertyInfo>> properties = GetPropertiesOfType(instance.GetType());

				foreach ( var property in properties )
				{
					// Propiedad local, if is modified then
					int index = property.Value.Item1;
					if ( index < data.Count )
					{
						if ( data.Get(index) ) //It is a modified property
						{
							object propValue = property.Value.Item2.GetValue(instance, null);
							if ( propValue is IShared )
							{
								var shared = (IShared)(propValue);
								if ( !context.AlreadyVisited(shared.UniqueID) )
								{
									context.AddSharedNode(shared.UniqueID);
									Delta internalDelta = GetDeltas(ref context, shared);
									if ( internalDelta != null )
									{
										context.AddDelta(shared.UniqueID, internalDelta);
									}
								}
								continue;
							}
							if ( propValue is IStateObject )
							{
								continue;
							}
							if (SurrogatesDirectory.IsSurrogateRegistered(property.Value.Item2.PropertyType))
							{
								continue; //Surrogate properties must be skipped they cannot be used for deltas with Client Sync
							}
							/*
							if (propValue is IStateObject)
							{
								var trackeable = (IStateObject) (propValue);
								Delta internalDelta = GetDeltas(ref context, trackeable);
								if (internalDelta != null)
								{
									delta.Add(property.Key, internalDelta);
								}
								continue;
							}
							*/
							delta.Add(property.Key, propValue);
						}
					}
				}
			}
			return delta.Count == 0 ? null : delta;
		}


		/// <summary>
		///     It resets to false all the changes tracked in instance, and its containing
		///     IChangesTrackeable properties
		/// </summary>
		/// <param name="instance">The instance implementing the IchangesTrackeable Interface</param>
		/// <param name="context">The visitating state</param>
		public static void ResetDeltas(IStateObject instance, ref VisitingState context)
		{
			BitArray data;
			// if model has been subscribed to any delta tracker
			if ( bitArrays.TryGetValue(instance.UniqueID, out data) )
			{
				data.SetAll(false);
			}

			//ITrackeableCollection collectionTrackeable = instance as ITrackeableCollection;
			//if (collectionTrackeable != null)
			//{
			//    //TODO ResetDeltasArrays(collectionTrackeable, ref context);
			//    return;
			//}
		}



		/// <summary>
		///     The oposite of Reset Deltas, it sets every property of the instance as if it was changed
		/// </summary>
		/// <param name="instance">The instance implementing the IchangesTrackeable Interface </param>
		public static void MarkAllAsModified(IStateObject instance)
		{
			BitArray data;
			// if model has been subscribed to any delta tracker
			if ( bitArrays.TryGetValue(instance.UniqueID, out data) )
			{
				data.SetAll(true);
			}

			//ITrackeableCollection collectionTrackeable = instance as ITrackeableCollection;
			//if (collectionTrackeable != null)
			//{
			//    MarkAllAsModifiedArrays(collectionTrackeable);
			//    return;
			//}
		}


		/// <summary>
		///     It tries to get the dictionary of properties of the given class. If not found, using reflection
		///     it add this dictionary to the Dictionary of dictionary
		/// </summary>
		/// <param name="viewModelType">The type of the class</param>
		/// <returns>A dictionary of the name of property - {its index, its propertyInfo}</returns>
		public static Dictionary<string, Tuple<int, PropertyInfo>> GetPropertiesOfType(Type viewModelType)
		{
			Dictionary<string, Tuple<int, PropertyInfo>> propertyNamesDict;
			lock ( cachedProperties )
			{
				if ( !cachedProperties.TryGetValue(viewModelType, out propertyNamesDict) )
				{
					//Not found. Adding new Dictionary
					propertyNamesDict = new Dictionary<string, Tuple<int, PropertyInfo>>();
					IList<PropertyInfo> props = viewModelType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
					int length = props.Count;
					for ( int i = 0; i < length; i++ )
					{
						// If an indexer
						if ( props[i].GetIndexParameters().Length > 0 )
						{
							continue;
						}
						propertyNamesDict.Add(props[i].Name, new Tuple<int, PropertyInfo>(i, props[i]));
					}
					cachedProperties.Add(viewModelType, propertyNamesDict);
				}
			}
			return propertyNamesDict;
		}

		/// <summary>
		///     Given the instance it tries to add the bit array to the dictionary of bit arrays
		///     representing the state (modified or not) of the model properties.
		/// </summary>
		/// <param name="model">The instance of IChangesTrackeable</param>
		public static void AddsBitArray(IStateObject model)
		{
			Dictionary<string, Tuple<int, PropertyInfo>> props = GetPropertiesOfType(model.GetType());
			int numProperties = props.Count;

			// If a shared object is tried to be added more than one time
			if ( !bitArrays.ContainsKey(model.UniqueID) )
			{
				bitArrays.Add(model.UniqueID, new BitArray(numProperties));
			}

			//if (model is ITrackeableCollection)
			//{
			//    //var collection = (ITrackeableCollection)(model);
			//    //TODO AddsBitArrayOfCollection(collection);
			//}
		}


		/// <summary>
		///     It tries to remove from the dictionary of bit arrays the one
		///     representing the given instance
		/// </summary>
		/// <param name="model">The instance of IChangesTrackeable</param>
		/// <returns>True if it was removed from the dictionary, false otherwise</returns>
		public static bool RemovesBitArray(IStateObject model)
		{
			return model != null ? bitArrays.Remove(model.UniqueID) : false;
		}

		/// <summary>
		///     Given the property name and its instance, it sets in the corresponding bit array at
		///     the corresponding index (of the property) the value to true (modified)
		///     Assumming that model is not a dirty model
		/// </summary>
		/// <param name="model">The instance of IChangesTrackeable</param>
		/// <param name="property">The name of the property</param>
		/// <param name="isPropIStateObject">true if property is IStateObject</param>
		public static void MarkAsModified(IStateObject model, string property, bool isPropIStateObject)
		{
			int index = IndexOfProperty(property, model.GetType());

			if ( index != -1 )
			{
				BitArray data;
				if ( model.UniqueID != null && bitArrays.TryGetValue(model.UniqueID, out data) )
				{
					data.Set(index, true);
				}
			}

			//// going to be removed
			//if (isPropIStateObject)
			//{
			//    PropertyInfo propInfo = model.GetType().GetProperty(property);
			//    MarkAllAsModified((IStateObject) propInfo.GetValue(model, null));
			//}
		}

		/// <summary>
		///     Using reflection it performs a deep copy of the object.
		/// </summary>
		/// <param name="instance">The instance to be copied</param>
		/// <returns>A copy of the given instance</returns>
		public static IStateObject Clone(IStateObject instance)
		{

			Type instanceType = instance.GetType();
			// It create an instance of the given type without calling any constructors of the class
			// ... creepy
			var copy = (IStateObject)FormatterServices.GetUninitializedObject(instanceType);

			BitArray data = null;
			String uniqueID = "**NOT_A-VALID_ID**";
			copy.UniqueID = uniqueID;

			Dictionary<string, Tuple<int, PropertyInfo>> properties = GetPropertiesOfType(instanceType);

			// We change the UniqueID because if the correct value is set to the copy instance
			// then all the properties that are going to be set, are going to be reported as modified... 
			foreach ( var pair in properties )
			{
				PropertyInfo property = pair.Value.Item2;
				IStateObject viewModelProperty = null;

				if ( property.Name.Equals("UniqueID") )
				{
					uniqueID = property.GetValue(instance, null).ToString();
					continue;
				}

				if ( property.CanWrite && property.CanRead )
				{
					Object propValue = property.GetValue(instance, null);

					// When an IChangesTrackeable property is changed the MarkAllAsModified method is called
					// We need the BitArray of that property to be set to the copy of it, before this method
					// is called
					if ( propValue is IStateObject )
					{
						viewModelProperty = (IStateObject)(propValue);
						propValue = Clone(viewModelProperty);

						BitArray bitArray;
						if ( !bitArrays.TryGetValue(viewModelProperty.UniqueID, out data) )
						{
							AddsBitArray(viewModelProperty);
							bitArrays.TryGetValue(viewModelProperty.UniqueID, out data);
						}

						bitArray = (BitArray)data.Clone();
						property.SetValue(copy, propValue, null);
						bitArrays[viewModelProperty.UniqueID] = bitArray;
					}
					else
					{
						property.SetValue(copy, propValue, null);
					}
				}
			}

			copy.UniqueID = uniqueID;

			return (copy);
		}
	}
}