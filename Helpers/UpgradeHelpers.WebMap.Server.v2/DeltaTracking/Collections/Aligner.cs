﻿using System;
using UpgradeHelpers.Interfaces;

/*
 Global Alignment
 NeedlemanWunsch algorithm implementation.
 We want to align the sourceArray (left) to the targetArray (up)
 
 Eval
    mat[i][j] = max (mat[i-1][j-1] + Eval(i,j),
                     mat[i-1][j] + GAP,
                     mat[i][j-1] + GAP )
  )
   
 */

namespace UpgradeHelpers.WebMap.Server
{
	public class Aligner
	{
		#region Nested type: ChoiceOrder

		private enum ChoiceOrder
		{
			DIAGONAL = 1,
			UP = 2,
			LEFT = 3
		}

		#endregion

		#region Properties

		private int GAP_VALUE = -1;
		private int MATCH_VALUE = 0;
		private int MISMATCH_VALUE = -1;
		public ActivityList actionsList = new ActivityList();
		private ITrackeableCollection collection;
		private VisitingState context;
		private int i;
		private bool isCollectionOfIStateObject;
		private bool isWholeArrayBetter;
		private int j;
		private int m;
		private int[,] matrix;
		private int n;
		private SizeTracker sizeTracker;

		#endregion

		#region Methods

		public Aligner(ITrackeableCollection collection, VisitingState context = null)
		{
			this.context = (context == null) ? new VisitingState() : context;
			this.collection = collection;

			n = collection.SourceCount + 1;
			m = collection.Count + 1;

			Type modelType = collection.GetType();

			isCollectionOfIStateObject = false;

			if (modelType.IsGenericType)
			{
				Type withinType = modelType.GetGenericArguments()[0].GetType();
				if (typeof (IStateObject).IsAssignableFrom(withinType))
				{
					isCollectionOfIStateObject = true;
				}
			}
		}

		private int Eval(int i, int j, ref bool match)
		{
			if (i == 0 || j == 0)
			{
				return matrix[0, 0];
			}
			if (isCollectionOfIStateObject)
			{
				var targetValue = (IStateObject) collection[j - 1];
				if (collection.IsSameReference(i - 1, targetValue))
				{
					match = true;
					Delta fieldDelta = TrackerHelper.GetDeltas(ref context, targetValue);
					if (fieldDelta != null)
					{
						AddDeltaActivity(0, new Update(i - 1, fieldDelta), ref sizeTracker);
					}
					return MATCH_VALUE;
				}
				match = false;
				return MISMATCH_VALUE;
			}

			if (collection.IsSameReference(i - 1, collection[j - 1]))
			{
				match = true;
				return MATCH_VALUE;
			}

			match = false;
			return MISMATCH_VALUE;
		}

		private int Max(int a, int b, int c)
		{
			return (a >= b && a >= c) ? a : (b >= a && b >= c) ? b : c;
		}

		// Algorithm formula
		private int ValueForField(int i, int j, ref bool isMatch)
		{
			return Max(matrix[i - 1, j - 1] + Eval(i, j, ref isMatch), matrix[i - 1, j] + GAP_VALUE, matrix[i, j - 1] + GAP_VALUE);
		}

		private void FillMatrix()
		{
			matrix = new int[n,m];
			bool isMatch = false;

			// Fill firts row
			for (i = 1; i < n; i++)
			{
				matrix[i, 0] = GAP_VALUE*i;
			}

			// Fill firts column
			for (j = 1; j < m; j++)
			{
				matrix[0, j] = GAP_VALUE*j;
			}

			// Fill the rest
			for (i = 1; i < n; i++)
			{
				for (j = 1; j < m; j++)
				{
					matrix[i, j] = ValueForField(i, j, ref isMatch);
				}
			}
		}

		private bool ComesFromDiagonal(int i, int j, ref bool isMatch)
		{
			int eval = Eval(i, j, ref isMatch);
			return (i > 0 && j > 0) ? matrix[i - 1, j - 1] + eval == matrix[i, j] : false;
		}

		private bool ComesFromUp(int i, int j)
		{
			return (i > 0) ? matrix[i - 1, j] + GAP_VALUE == matrix[i, j] : false;
		}

		private bool ComesFromLeft(int i, int j)
		{
			return (j > 0) ? matrix[i, j - 1] + GAP_VALUE == matrix[i, j] : false;
		}

		/// <summary>
		///     Returns and array of ChoiceOrder, sorted by the value
		/// </summary>
		/// <returns>An array of choice order</returns>
		private ChoiceOrder[] GetOrderChoice()
		{
			return (ChoiceOrder[]) Enum.GetValues(typeof (ChoiceOrder));
		}

		private void AddDeltaActivity(int index, Activity activity, ref SizeTracker sizeTracker)
		{
			if (!actionsList.Insert(index, activity, ref sizeTracker))
			{
				isWholeArrayBetter = true;
			}
		}

		// Make a Match or a Mismatch
		private void AddMovementFromDiagonal(int row, int column, bool isMatch)
		{
			if (!isMatch)
			{
				AddDeltaActivity(0, new Replace(column - 1, collection[column - 1]), ref sizeTracker);
			}
		}

		// Makes a gap in newTargetArray, up
		private void AddMovementFromUp(int row, int column)
		{
			AddDeltaActivity(0, new Removal(column), ref sizeTracker);
		}

		// Makes a gap in newSourceArray, left
		private void AddMovementFromLeft(int row, int column)
		{
			AddDeltaActivity(0, new Insertion(column - 1, collection[column - 1]), ref sizeTracker);
		}

		private void BestAlignment()
		{
			actionsList.Clear();
			i = n - 1;
			j = m - 1;

			bool comesFromDiagonal, comesFromUp, comesFromLeft, choiceTaken = false, isMatch = false;
			ChoiceOrder[] choiceArray = GetOrderChoice();
			ChoiceOrder currentChoice;
			int currentChoiceIndex = 0;

			while (i != 0 || j != 0)
			{
				if (isWholeArrayBetter)
				{
					return;
				}

				comesFromDiagonal = ComesFromDiagonal(i, j, ref isMatch);
				comesFromUp = ComesFromUp(i, j);
				comesFromLeft = ComesFromLeft(i, j);
				choiceTaken = false;

				for (currentChoiceIndex = 0; currentChoiceIndex < 3; currentChoiceIndex++)
				{
					currentChoice = choiceArray[currentChoiceIndex];
					switch (currentChoice)
					{
						case ChoiceOrder.DIAGONAL:
							if (comesFromDiagonal)
							{
								AddMovementFromDiagonal(i, j, isMatch);
								i--;
								j--;
								choiceTaken = true;
							}
							break;
						case ChoiceOrder.UP:
							if (comesFromUp)
							{
								AddMovementFromUp(i, j);
								i--;
								choiceTaken = true;
							}
							break;
						case ChoiceOrder.LEFT:
							if (comesFromLeft)
							{
								AddMovementFromLeft(i, j);
								j--;
								choiceTaken = true;
							}
							break;
					}

					if (choiceTaken)
					{
						break;
					}
				}
			}
		}

		public Delta GetDelta()
		{
			sizeTracker = new SizeTracker();
			sizeTracker.StartSizeTracking(collection);
			FillMatrix();
			BestAlignment();

			Delta delta = null;

			if (isWholeArrayBetter)
			{
				delta = new Delta();
				delta.Add("Whole_Instance", collection);
			}
			else
			{
				if (actionsList.Count > 0)
				{
					delta = new Delta();
					actionsList.arrayLength = m - 1;
					delta.Add("Array", actionsList);
				}
			}

			return delta;
		}

		#endregion
	}
}