﻿using System;
using System.Collections.Generic;
using System.IO;

namespace UpgradeHelpers.WebMap.Server
{
	internal enum ExecutionOrder
	{
		Insertion = 1,
		Removal = 2,
		Movement = 3,
		Exchange = 4,
		Replace = 5,
		Update = 6
	}

	public abstract class Activity
	{
		#region Properties

		protected int _sourceIndex;

		#endregion

		#region Methods

		public int SourceIndex
		{
			get { return _sourceIndex; }
			set { _sourceIndex = value; }
		}

		public abstract int GetOrder();

		#endregion
	}

	public abstract class DeltaActivity : Activity
	{
		#region Properties

		protected Delta _sourceDelta;

		#endregion

		#region Methods

		public Delta SourceDelta
		{
			get { return _sourceDelta; }
			set { _sourceDelta = value; }
		}

		#endregion
	}

	public class Update : DeltaActivity
	{
		#region Properties

		#endregion

		#region Constructors

		public Update(int sourceIndex, Delta sourceDelta)
		{
			_sourceIndex = sourceIndex;
			_sourceDelta = sourceDelta;
		}

		#endregion

		#region Methods

		public override int GetOrder()
		{
			return (int) ExecutionOrder.Update;
		}

		public override string ToString()
		{
			return "Update, sourceIndex: " + SourceIndex + " Delta: " + SourceDelta + "\n";
		}

		#endregion
	}

	public class Movement : DeltaActivity
	{
		#region Properties

		protected int _targetIndex;

		#endregion

		#region Constructors

		public Movement(int sourceIndex, int targetIndex)
		{
			_sourceIndex = sourceIndex;
			_targetIndex = targetIndex;
		}

		public Movement(int sourceIndex, int targetIndex, Delta delta) : this(sourceIndex, targetIndex)
		{
			_sourceDelta = delta;
		}

		#endregion

		#region Methods

		public int TargetIndex
		{
			get { return _targetIndex; }
			set { _targetIndex = value; }
		}

		public override int GetOrder()
		{
			return (int) ExecutionOrder.Movement;
		}

		public override string ToString()
		{
			return "Movement, sourceIndex: " + SourceIndex + " targetIndex: " + TargetIndex + "\n";
		}

		#endregion
	}

	public class Exchange : Movement
	{
		#region Properties

		#endregion

		#region Constructors

		public Exchange(int sourceIndex, int targetIndex) : base(sourceIndex, targetIndex)
		{
		}

		public Exchange(int sourceIndex, int targetIndex, Delta delta) : base(sourceIndex, targetIndex, delta)
		{
		}

		public Exchange(int sourceIndex, int targetIndex, Delta delta, Delta delta2) : this(sourceIndex, targetIndex, delta)
		{
			TargetDelta = delta2;
		}

		#endregion

		#region Methods

		public Delta TargetDelta { get; set; }

		public override int GetOrder()
		{
			return (int) ExecutionOrder.Exchange;
		}

		public override string ToString()
		{
			return "Exchange, sourceIndex: " + SourceIndex + " targetIndex: " + TargetIndex + "\n";
		}

		#endregion
	}

	public class Replace : Activity
	{
		#region Properties

		protected Object _newObject;

		#endregion

		#region Constructors

		public Replace(int sourceIndex, Object newObject)
		{
			_sourceIndex = sourceIndex;
			_newObject = newObject;
		}

		#endregion

		#region Methods

		public Object NewObject
		{
			get { return _newObject; }
			set { _newObject = value; }
		}

		public override int GetOrder()
		{
			return (int) ExecutionOrder.Replace;
		}

		public override string ToString()
		{
			return "Replace, sourceIndex: " + SourceIndex + " newValue: {" + _newObject + "}\n";
		}

		#endregion
	}

	public class Insertion : Replace
	{
		#region Properties

		#endregion

		#region Constructors

		public Insertion(int sourceIndex, Object newObject) : base(sourceIndex, newObject)
		{
		}

		#endregion

		#region Methods

		public override int GetOrder()
		{
			return (int) ExecutionOrder.Insertion;
		}

		public override string ToString()
		{
			return "Insertion, sourceIndex: " + SourceIndex + " newValue: {" + _newObject + "}\n";
		}

		#endregion
	}

	public class Removal : Activity
	{
		#region Properties

		#endregion

		#region Constructors

		public Removal(int sourceIndex)
		{
			_sourceIndex = sourceIndex;
		}

		#endregion

		#region Methods

		public override int GetOrder()
		{
			return (int) ExecutionOrder.Removal;
		}

		public override string ToString()
		{
			return "Removal, sourceIndex: " + SourceIndex + "\n";
		}

		#endregion
	}

	public class ActivityList : List<Activity>
	{
		#region Properties

		public int arrayLength;

		#endregion

		#region Methods

		public void CatogorizeByActivity(ref List<Activity> updates, ref List<Activity> exchanges, ref List<Activity> movements, ref List<Activity> replaces,
		                                 ref List<Activity> insertions, ref List<Activity> removals)
		{
			foreach (Activity activity in this)
			{
				var executionOrder = (ExecutionOrder) (activity.GetOrder());
				switch (executionOrder)
				{
					case ExecutionOrder.Update:
						updates.Add(activity);
						break;
					case ExecutionOrder.Exchange:
						exchanges.Add(activity);
						break;
					case ExecutionOrder.Movement:
						movements.Add(activity);
						break;
					case ExecutionOrder.Replace:
						replaces.Add(activity);
						break;
					case ExecutionOrder.Insertion:
						insertions.Add(activity);
						break;
					case ExecutionOrder.Removal:
						removals.Add(activity);
						break;
				}
			}
		}

		public void PrettyPrint(TextWriter writer, int indentSize = 2)
		{
			var indent = new string(' ', indentSize);

			// Categorize the actions by its Enum Activity, just for printing reasons
			List<Activity> exchanges = new List<Activity>(),
			               updates = new List<Activity>(),
			               movements = new List<Activity>(),
			               replaces = new List<Activity>(),
			               insertions = new List<Activity>(),
			               removals = new List<Activity>();
			CatogorizeByActivity(ref updates, ref exchanges, ref movements, ref replaces, ref insertions, ref removals);

			writer.WriteLine(indent + "{");
			writer.WriteLine(indent + "Current Array Length: " + arrayLength);
			writer.WriteLine(indent + "Number of actions: " + Count);

			var newIndent = new string(' ', indentSize + 2);

			#region Print Updates

			if (updates.Count > 0)
			{
				writer.WriteLine(indent + "Updates: [");
				foreach (Update update in updates)
				{
					writer.WriteLine(newIndent + "{sourceIndex: " + update.SourceIndex + ", Delta: ");
					update.SourceDelta.Print(writer, indentSize + 4);
					writer.WriteLine(newIndent + "},");
				}
				writer.WriteLine(indent + "]");
			}

			#endregion

			#region Print Exchanges

			if (exchanges.Count > 0)
			{
				writer.WriteLine(indent + "Exchanges: [");
				foreach (Exchange exchange in exchanges)
				{
					writer.WriteLine(newIndent + "{sourceIndex: " + exchange.SourceIndex + ", targetIndex: " + exchange.TargetIndex + "},");
					if (exchange.SourceDelta != null)
					{
						writer.WriteLine("  " + newIndent + "Source Delta: ");
						exchange.SourceDelta.Print(writer, indentSize + 4);
					}
					if (exchange.TargetDelta != null)
					{
						writer.WriteLine("  " + newIndent + "Target Delta: ");
						exchange.TargetDelta.Print(writer, indentSize + 4);
					}
				}
				writer.WriteLine(indent + "]");
			}

			#endregion

			#region Print Insertion

			if (insertions.Count > 0)
			{
				writer.WriteLine(indent + "Insertions: [");
				foreach (Insertion insertion in insertions)
				{
					writer.WriteLine(newIndent + "{sourceIndex: " + insertion.SourceIndex + ", newValue: {" + insertion.NewObject + "} },");
				}
				writer.WriteLine(indent + "]");
			}

			#endregion

			#region Print Removals

			if (removals.Count > 0)
			{
				writer.WriteLine(indent + "Removals: [");
				foreach (Removal removal in removals)
				{
					writer.WriteLine(newIndent + "{sourceIndex: " + removal.SourceIndex + "} },");
				}
				writer.WriteLine(indent + "]");
			}

			#endregion

			#region Print Movements

			if (movements.Count > 0)
			{
				writer.WriteLine(indent + "Movements: [");
				foreach (Movement movement in movements)
				{
					writer.WriteLine(newIndent + "{sourceIndex: " + movement.SourceIndex + ", targetIndex: " + movement.TargetIndex + "},");
					if (movement.SourceDelta != null)
					{
						writer.WriteLine("  " + newIndent + "Source Delta: ");
						movement.SourceDelta.Print(writer, indentSize + 4);
					}
				}
				writer.WriteLine(indent + "]");
			}

			#endregion

			#region Print Replaces

			if (replaces.Count > 0)
			{
				writer.WriteLine(indent + "Replaces: [");
				foreach (Replace replace in replaces)
				{
					writer.WriteLine(newIndent + "{sourceIndex: " + replace.SourceIndex + ", newValue: {" + replace.NewObject + "} },");
				}
				writer.WriteLine(indent + "]");
			}

			#endregion

			writer.WriteLine(indent + "}");
		}

		public void PrettyPrint2(TextWriter writer, int indentSize = 2)
		{
			foreach (Activity act in this)
			{
				Console.WriteLine(act.ToString());
			}
			Console.WriteLine();
		}

		public bool Insert(int index, Activity activity, ref SizeTracker sizeTracker)
		{
			sizeTracker.Add_Activity_Size(activity);
			if (sizeTracker.IsTrackingSenseless())
			{
				return false;
			}
			base.Insert(index, activity);
			return true;
		}

		#endregion
	}
}