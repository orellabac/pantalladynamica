﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.DeltaTracking
{
    /// <summary>
    /// The representation of a object of change
    /// </summary>
    public class Delta : Dictionary<string, object>
    {
        #region Methods
        public void Print(TextWriter writer, int indentSize = 0)
        {
            var indent = new string(' ', indentSize);
            writer.WriteLine(indent + "{");
            foreach (KeyValuePair<string, object> pair in this)
            {
                writer.Write(indent + "\"" + pair.Key + "\" = ");
                var value = pair.Value;
                if (value is Delta)
                {
                    writer.WriteLine();
                    ((Delta)(value)).Print(writer, indentSize + 4);
                }
                else
                {
                    if (value is ActivityList)
                    {
                        writer.WriteLine();
                        ((ActivityList)(value)).PrettyPrint(writer, indentSize + 4);
                    }
                    else
                    {
                        writer.WriteLine("\"" + pair.Value + "\",");
                    }
                }
            }
            writer.WriteLine(indent + "}");
        }

        #endregion
    }

    public class DeltaTracker
    {
        #region Properties
        List<IChangesTrackeable> subscribed_models = new List<IChangesTrackeable>();
        static Dictionary<string, ICollection> _originalCollections = null;
        VisitingState context = new VisitingState();
        #endregion

        #region Methods
        public static Dictionary<string, ICollection> originalCollections
        {
            get
            {
                if (_originalCollections == null)
                {
                    _originalCollections = new Dictionary<string, ICollection>();
                }
                return _originalCollections;
            }
        }

        /// <summary>
        /// It resets the context and each of the subscribed model 
        /// </summary>
        public void Start()
        {
            // reset all viewmodel 
            context.Reset();

            originalCollections.Clear();

            // not yet implemented -> check in each model if it has observable collections 
            // and adds them 
            foreach (IChangesTrackeable model in subscribed_models)
            {
                model.ResetDeltas(ref context);
            }
        }

        /// <summary>
        /// It tries to get the index in the List of subscribed models, of the given model
        /// </summary>
        /// <param name="model">The instance of IChangesTrackeable</param>
        /// <returns>Index if model found, -1 otherwise</returns>
        private int IndexOFModel(IChangesTrackeable model)
        {
            int length = subscribed_models.Count();
            for (int i = 0; i < length; i++)
            {
                if (model.ID == subscribed_models[i].ID)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// It tries to subscribe to given model to the list of subscribed models
        /// If it succeeds, it resets the given model and adds a bit array of its properties
        /// </summary>
        /// <param name="model">The instance of IChangesTrackeable</param>
        public void AttachModel(IChangesTrackeable model)
        {
            context.Reset();

            // Adding model to list of models
            int index = IndexOFModel(model);
            if (index != -1)
            {
                subscribed_models.RemoveAt(index);
            }
            subscribed_models.Add(model);

            // Adding data of model to bitArrays
            TrackerHelper.AddsBitArray(model);

            model.ResetDeltas(ref context);
        }

        /// <summary>
        /// It tries to removes the given model of the list of the subscribed models and the 
        /// bit array of the data arrays
        /// </summary>
        /// <param name="model"></param>
        /// <returns>True if is succeeds, false otherwise</returns>
        public void DetachModel(IChangesTrackeable model)
        {
            // Removing model from list of models
            int index = IndexOFModel(model);
            if (index != -1)
            {
                subscribed_models.RemoveAt(index);
                TrackerHelper.RemovesBitArray(model);

                // We need to check if model has IObservableCollection, they need to be removed as well
                Dictionary<string, Tuple<int, PropertyInfo>> properties =
                    TrackerHelper.GetPropertiesOfType(model.GetType());
                foreach (KeyValuePair<string, Tuple<int, PropertyInfo>> pair in properties)
                {
                    PropertyInfo property = pair.Value.Item2;
                    Object value = property.GetValue(model);
                    if (value is ICollectionTrackeable)
                    {
                        string valueID = ((ICollectionTrackeable)(value)).ID;
                        RemoveOriginalCollection(valueID);
                    }
                }
            }
        }

        /// <summary>
        /// When this method is called, all the changes occured so far,since it was started,
        /// in each of the subscribed model, are collected and storage in the context (visitating state)
        /// </summary>
        public void CollectDeltas()
        {
            context.Reset();

            Delta delta = new Delta();

            if (subscribed_models.Count() != 0)
            {
                foreach (IChangesTrackeable model in subscribed_models)
                {
                    Delta internalDelta = model.GetDeltas(ref context);

                    if (internalDelta != null)
                    {
                        context.AddDelta(model.ID.ToString(), internalDelta);
                    }
                }
            }
        }

        /// <summary>
        /// Given the stream to be written, its prints the deltas collected so far in it
        /// </summary>
        /// <param name="writer">The stream to be written</param>
        public void PrintDeltas(TextWriter writer)
        {
            CollectDeltas();
            context.Print(writer);
        }

        /// <summary>
        /// Given the Observable Collection it adds it to the orignal collections dictionary,
        /// if a previos version of the collection has already been added, it removes it and 
        /// adds the new one
        /// </summary>
        /// <param name="collection"></param>
        public static void AddOriginalCollection(string key, ICollection collection)
        {
            if (originalCollections.ContainsKey(key))
            {
                originalCollections.Remove(key);
            }

            originalCollections.Add(key, collection);
        }

        /// <summary>
        /// Given the ID of an orginal collection, this methods seeks to remove the collection
        /// of the OriginalCollections dictionary
        /// </summary>
        /// <param name="key">The ID of the original collection to be removed</param>
        public static void RemoveOriginalCollection(string key)
        {
            if (originalCollections.ContainsKey(key))
            {
                originalCollections.Remove(key);
            }
        }

        /// <summary>
        /// It returns the original collection at OriginalCollections Dictionary that matchs
        /// the given key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns>The Collection corresponding to the given key... if not found then null</returns>
        public static ICollection GetOriginalCollection(string key)
        {
            ICollection collection = null;
            if (originalCollections.TryGetValue(key, out collection))
            {
                return collection;
            }
            return null;
        }

        /// <summary>
        /// Returns the Dictionary where all deltas are stored
        /// </summary>
        /// <returns>The dictionary of Deltas</returns>
        public Dictionary<string, Delta> GetResults()
        {
            return this.context.GetResult();
        }
        #endregion
    }

    /// <summary>
    /// This class will hold al the Deltas collected in the subcribed viewModels of the delta tracker.
    /// It is like a context, 
    /// </summary>
    public class VisitingState
    {
        #region Properties
        Dictionary<string, Delta> result;
        List<string> visitedSharedNodes = new List<string>();
        #endregion

        #region Constructors

        public VisitingState()
        {
            result = new Dictionary<string, Delta>();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Checks if the given id was alredy visited by this context
        /// It assumes that the given id belongs to a IShared instance
        /// </summary>
        /// <param name="id">The id of the IShared instance</param>
        /// <returns>True is it was already visited, false otherwise</returns>
        public bool AlreadyVisited(string id)
        {
            return visitedSharedNodes.Contains(id);
        }

        /// <summary>
        /// It adds the given id to the list of visitated ids
        /// It assumes that the given id belongs to a IShared instance
        /// </summary>
        /// <param name="id">The id of a IShared instance</param>
        public void AddSharedNode(string id)
        {
            visitedSharedNodes.Add(id);
        }

        /// <summary>
        /// Adds the given delta, to its dictionary of deltas
        /// </summary>
        /// <param name="id">The id of the IChangesTrackeable instance</param>
        /// <param name="delta">The delta of the instance with the given id</param>
        public void AddDelta(string id, Delta delta)
        {
            result.Add(id, delta);
        }

        /// <summary>
        /// Prints , in a Json-like format, all the deltas collected
        /// </summary>
        /// <param name="writer">The stream where to write</param>
        public void Print(TextWriter writer)
        {
            writer.WriteLine("[");
            foreach (var item in result)
            {
                writer.WriteLine();
                writer.WriteLine(" [" + item.Key + ",");
                item.Value.Print(writer, 4);
                writer.WriteLine(" ]");
            }
            writer.WriteLine();
            writer.WriteLine("]");
        }

        /// <summary>
        /// It clears the visited shared nodes ids, and the deltas collected
        /// </summary>
        public void Reset()
        {
            visitedSharedNodes.Clear();
            result.Clear();
        }

        /// <summary>
        /// Returns the Dictionary where all the deltas collected so far are stored
        /// </summary>
        /// <returns>The dictionary of deltas</returns>
        public Dictionary<string, Delta> GetResult()
        {
            return this.result;
        }
        #endregion
    }
}
