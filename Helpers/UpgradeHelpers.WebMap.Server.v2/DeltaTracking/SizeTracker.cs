﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using Fasterflect;

namespace UpgradeHelpers.WebMap.Server
{
	public class SizeTracker
	{
		private int AVG_STRING_LENGTH = 15;
		private float CHANGES_AVG_PERCENTAGE = 0.6F; // the percentage of how much an instance is to be modified
		private int INDEX_SIZE = 4;

		private int instanceSize;

		private double avgDeltaSize;

		public double MaxAllowedSize { get; set; }

		public double CurrentSize { get; set; }

		public int SizeInBytes(Object instance, bool withPropNames)
		{
			int size = 0;
			// Returns the size of the pointer, 4 or 8 bytes
			if ( instance == null )
			{
				return IntPtr.Size;
			}
			Type currentType = instance.GetType();

			if ( currentType.IsPrimitive )
			{
				return Marshal.SizeOf(instance);
			}

			if ( instance is string )
			{
				return AVG_STRING_LENGTH * sizeof(Char);
			}

			// Using Reflextion
			IList<PropertyInfo> props = currentType.Properties();
			int length = props.Count;
			for ( int i = 0; i < length; i++ )
			{
				// If an indexer
				if ( props[i].GetIndexParameters().Length > 0 )
				{
					continue;
				}
				//Console.WriteLine("Name of prop: {0}", props[i].Name);
				object value = props[i].GetValue(instance, null);
				size += (withPropNames) ? SizeInBytes(props[i].Name, withPropNames) : 0;
				size += SizeInBytes(value, withPropNames);
			}

			return size;
		}

		public void StartSizeTracking(IList target)
		{
			int length = target.Count;

			// Size of element in array
			// should the withPropNames parameter be true???
			instanceSize = length > 0 ? SizeInBytes(target[0], false) : -1;

			// Target Array Empty
			if ( instanceSize == -1 )
			{
				MaxAllowedSize = 0;
				return;
			}

			// Size of sending whole array, 
			MaxAllowedSize = instanceSize * length;

			// Size of all properties modified, stored in a Delta (propName + value)
			int worstDeltaSize = SizeInBytes(target[0], true);

			// Average Delta Size
			avgDeltaSize = (worstDeltaSize * CHANGES_AVG_PERCENTAGE);
			CurrentSize = 0;

			Console.WriteLine("Instance: {0}  MAX: {1}  AVG_DELTA_SIZE: {2}", instanceSize, MaxAllowedSize, avgDeltaSize);
		}

		public bool IsTrackingSenseless()
		{
			return CurrentSize >= MaxAllowedSize;
		}

	}
}