﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Fasterflect;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.Helpers;

namespace UpgradeHelpers.WebMap.Server
{
	internal class PropertySkiperContractResolver : DefaultContractResolver
	{
		bool serverSide = false;
		private bool skipUniqueId = false;
		public PropertySkiperContractResolver(bool skipUniqueId = false, bool server = false)
			: base()
		{
			serverSide = server;
			this.skipUniqueId = skipUniqueId;
		}


		protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
		{
			IList<JsonProperty> properties = base.CreateProperties(type, memberSerialization);
			var typeIsStruct = !type.IsEnum && !type.IsPrimitive && type.IsValueType;
			if (typeIsStruct) return properties;

			properties = properties.Where(
				p =>
				{

					if (typeof(IDefaults).IsAssignableFrom(p.PropertyType) ||
						(typeof(IStateObject).IsAssignableFrom(p.PropertyType) && !typeof(ViewsState).IsAssignableFrom(p.PropertyType)) ||
						typeof(IViewManager).IsAssignableFrom(p.PropertyType) ||
						typeof(IIocContainer).IsAssignableFrom(p.PropertyType) ||
						SurrogatesDirectory.IsSurrogateRegistered(p.PropertyType) ||
						(IsIgnorableUniqueId(type, p)))
						return false;
					var prop = type.Property(p.PropertyName);
					if (prop == null)
					{
						var isStruct = p.PropertyType.IsValueType && !p.PropertyType.IsPrimitive && !p.PropertyType.IsEnum;
						if (isStruct)
						{
							var field = type.GetField(p.PropertyName);
							if (field != null) return !StateManagementUtils.MustIgnoreMember(!serverSide, field);
						}
						return false;
					}

					else
						return !StateManagementUtils.MustIgnoreMember(!serverSide, prop);
				}).ToList();



			return properties;
		}

		/// <summary>
		/// Indicates whether the given property matches am UniqueId property or not.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="p"></param>
		/// <returns></returns>
		private bool IsIgnorableUniqueId(Type type, JsonProperty p)
		{
			if (typeof(IStateObject).IsAssignableFrom(p.DeclaringType) && skipUniqueId && p.PropertyName == "UniqueID")
			{
				var prop = type.Property(p.PropertyName);
				if (prop != null)
				{
					var attributes = prop.GetCustomAttributes(typeof(PreserveUniqueId), true);
					return attributes.Any();
				}
			}
			return false;
		}
	}
}