﻿using System;
using System.Diagnostics;
using System.Web.Mvc;

namespace UpgradeHelpers.WebMap.Server
{
	public class ViewManagerRequestStartAndEndActionFilter : ActionFilterAttribute, IExceptionFilter
	{


		//Init is not required because on first access StateCache will be created
		//And it is a per-request item
		public override void OnResultExecuted(ResultExecutedContext filterContext)
		{

			if (filterContext.Exception == null)
			{
			if (StateCache.IsAvailable)
			{
				StateCache.Current.Persist();
				base.OnResultExecuted(filterContext);
			}
		}
	}

		public void OnException(ExceptionContext filterContext)
		{
			filterContext.ExceptionHandled = true;
			filterContext.Result = new JsonResult() { Data = new { ErrorOcurred = true, ExMessage = filterContext.Exception.Message, ExStackTrace = filterContext.Exception.StackTrace } };
		}
	}
}