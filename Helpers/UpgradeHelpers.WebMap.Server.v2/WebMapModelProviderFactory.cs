﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace UpgradeHelpers.WebMap.Server
{
	public class WebMapModelProviderFactory : ValueProviderFactory
	{
		public override IValueProvider GetValueProvider(ControllerContext controllerContext)
		{
			// first make sure we have a valid context
			if (controllerContext == null)
				throw new ArgumentNullException("controllerContext");
			if (controllerContext.HttpContext.Request.Params["$uniqueID"] == null)
			{
				return null;
			}
			var uniqueID = controllerContext.HttpContext.Request.Params["$uniqueID"].ToString();
			return new WebMapAction(uniqueID);
		}

		private class WebMapAction : IValueProvider
		{
			readonly string _uniqueId;

			public WebMapAction(string uniqueId)
			{
				_uniqueId = uniqueId;
			}

			public bool ContainsPrefix(string prefix)
			{
				if (prefix == "viewmodel" || prefix == "viewFromClient" || "uniqueid".Equals(prefix, StringComparison.CurrentCultureIgnoreCase))
				{
					return true;
				}
				return false;
			}

			public ValueProviderResult GetValue(string key)
			{
				if ("viewmodel".Equals(key, StringComparison.CurrentCultureIgnoreCase) ||
						   "viewFromClient".Equals(key, StringComparison.CurrentCultureIgnoreCase))
				{
					var actualObject = StateCache.Current.GetObject(this._uniqueId);
					return new ValueProviderResult(actualObject, "json", CultureInfo.CurrentCulture);
				}
				if ("uniqueid".Equals(key, StringComparison.CurrentCultureIgnoreCase))
				{
					return new ValueProviderResult(_uniqueId, _uniqueId, CultureInfo.CurrentCulture);
				}
				return null;
			}
		}
	}

}
