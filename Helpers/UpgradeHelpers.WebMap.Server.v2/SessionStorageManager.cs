﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
	internal class SessionStorageManager : IStorageManager
	{
		private readonly StorageSerializerUsingJsonnet _serializer;
        const string WM_PREFIX = "WM@";

		public SessionStorageManager()
		{
			_serializer = new StorageSerializerUsingJsonnet();
		}

		#region IStorageManager Members

		public IEnumerable<string> GetAllItems()
		{
            return HttpContext.Current.Session.Keys.Cast<string>().Where(x => x.StartsWith(WM_PREFIX));
		}

		public IEnumerable<string> GetChildren(string uniqueId)
		{
            var uniqueIdToCheck = "###" + uniqueId;
			foreach (var id in GetAllItems())
			{
                if (id.EndsWith(uniqueIdToCheck))
				{
                    yield return id.StartsWith("WM@")
                                  ? id.Substring(3)
                                  : id;
				}
			}
			
		}

		public IStateObject GetObject(string uniqueID)
		{
			object rawData = GetRaw(uniqueID);
			if (rawData == null) return null;
			return _serializer.RawToObject(uniqueID,rawData) as IStateObject;
		}

		public void SaveObject(IStateObject obj)
		{
			object rawData = _serializer.ObjectToRaw(obj);
            SaveRaw(obj.UniqueID,rawData);
		}

		public void RemoveObject(string uniqueId)
		{
            HttpContext.Current.Session.Remove(WM_PREFIX + uniqueId);
		}

	    public void SaveRaw(string uniqueId, object rawData)
	    {
			if (HttpContext.Current != null && HttpContext.Current.Session != null)
				 HttpContext.Current.Session[WM_PREFIX + uniqueId] = rawData;
	    }

	    public object GetRaw(string uniqueId)
	    {
			if (HttpContext.Current != null && HttpContext.Current.Session != null)
				return HttpContext.Current.Session[WM_PREFIX + uniqueId];
			else
			{
				return null;
			}
	    }

	    public void SaveSurrogate(StateObjectSurrogate surrogate)
	    {
	        var raw = SurrogatesDirectory.ObjectToRaw(surrogate);
			SaveRaw(surrogate.UniqueID, raw);
            var uid = UniqueIDGenerator.Current.GetRelativeUniqueID(surrogate,"Value");
	        raw =  SurrogatesDirectory.ObjectToRaw(surrogate.Value);
			SaveRaw(uid, raw);
            
	    }

        //public void GetSurrogate(StateObjectSurrogate surrogate)
        //{
        //    var uid = UniqueIDGenerator.Current.GetRelativeUniqueID(surrogate, "Value");
        //    var raw = HttpContext.Current.Session[uid];
        //    surrogate.Value = SurrogatesDirectory.RawToObject(raw);
        //}


		public void SwitchUniqueIDsFrom(string itemUID, string lastUID)
		{
            if (!itemUID.StartsWith("WM@"))
            {
                itemUID = "WM@" + itemUID;
            }
            if (!lastUID.StartsWith("WM@"))
            {
                lastUID = "WM@" + lastUID;
            }

			var tmp = HttpContext.Current.Session[itemUID];
			HttpContext.Current.Session[lastUID] = tmp;
		}

		#endregion
	}
}