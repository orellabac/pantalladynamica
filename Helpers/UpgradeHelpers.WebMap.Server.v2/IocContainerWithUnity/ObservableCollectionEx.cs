﻿using System;
using System.Collections;
using System.Collections.Generic;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
    public partial class IocContainerImplWithUnity
    {

        #region Nested type: ObservableCollectionEx



        public class ObservableCollectionEx<T> : IModelList<T>, IList, IDependantViewModel
        {
            private FieldInfo[] _fieldInfoArray;
            private BitArray _lazyitems;
            public bool isCollectionOfIStateObject;
            internal List<T> items = new List<T>();
            public int maxIndex;
            internal List<T> sourceArray = null;

            public ObservableCollectionEx()
            {
                PageSize = 10000;
                if (typeof (IStateObject).IsAssignableFrom(typeof (T))) _lazyitems = new BitArray(0);
                else _lazyitems = null;

                isCollectionOfIStateObject = typeof (IStateObject).IsAssignableFrom(typeof (T));
            }

            internal IIocContainer container { get; set; }
            public int PageSize { get; set; }
            public int PreviousPageIndex { get; set; }
            public string NextPageUniqueID { get; set; }

            // lazy property
            internal FieldInfo[] FieldInfoArray
            {
                get
                {
                    if (isCollectionOfIStateObject && _fieldInfoArray == null)
                    {
                        int length = items.Count;
                        _fieldInfoArray = new FieldInfo[length];
                        var parentInstance = this as IDependantViewModel;
                        for (int i = 0; i < length; i++)
                        {
                            string ID = UniqueIDGenerator.Current.GetRelativeUniqueID(parentInstance, "" + i);
                            _fieldInfoArray[i] = new FieldInfo(ID);
                        }
                    }

                    return _fieldInfoArray;
                }
            }

            private bool IsLazy
            {
                get { return _lazyitems != null; }
            }

            /// <summary>
            ///     Indicates the actual number of items on the page
            /// </summary>
            internal int CollectionSize
            {
                set
                {
                    if (typeof (IStateObject).IsAssignableFrom(typeof (T)))
                    {
                        _lazyitems = new BitArray(value);
                    }
                    for (int i = 0; i < value; i++)
                    {
                        items.Add(default(T));
                    }
                }
            }

            #region IList Members

            public int Add(object value)
            {
                Add((T) value);
                return 0;
            }

            public bool Contains(object value)
            {
                return Contains((T) value);
            }

            public int IndexOf(object value)
            {
                return IndexOf((T) value);
            }

            public void Insert(int index, object value)
            {
                Insert(index, (T) value);
            }

            public bool IsFixedSize
            {
                get { return false; }
            }

            public void Remove(object value)
            {
                Remove((T) value);
            }

            object IList.this[int index]
            {
                get { return this[index]; }
                set { this[index] = (T) value; }
            }

            public void CopyTo(Array array, int index)
            {
                throw new NotImplementedException();
            }

            public bool IsSynchronized
            {
                get { return false; }
            }

            public object SyncRoot
            {
                get { return null; }
            }

            #endregion

            #region IModelList<T> Members

            public string UniqueID { get; set; }

            public void Build(IIocContainer ctx)
            {
                if (!typeof(T).IsInterface)
                {
                    var size = items.Count;
                    for (int i = 0; i < size; i++)
                    {
                        var newItem = (T) ctx.Resolve(typeof (T));
                        this[i] = newItem;
                    }
                }

            }

            public int IndexOf(T item)
            {
                if (IsLazy)
                {
                    //First Look in the currently loaded items if where are lucky the element might be there. 
                    //We are assuming no duplicates for example no cases like [a ,b,c,a] 
                    var lazyIndexes = new List<int>();
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (!_lazyitems[i])
                        {
                            lazyIndexes.Add(i);
                            continue;
                        }
                        var currentItem = (IStateObject) items[i];
                        var itemAsStateObject = (IStateObject) item;
                        if (currentItem.UniqueID == itemAsStateObject.UniqueID)
                            return i;
                    }
                    foreach (int index in lazyIndexes)
                    {
                        var currentItem = (IStateObject) LoadItem(index);
                        var itemAsStateObject = (IStateObject) item;
                        if (currentItem.UniqueID == itemAsStateObject.UniqueID)
                            return index;
                    }
                }
                else
                {
                    int res = items.IndexOf(item);
                    if (res != -1)
                        return res;
                }
                if (!string.IsNullOrWhiteSpace(NextPageUniqueID))
                {
                    var nextPage = (ObservableCollectionEx<T>) StateCache.Current.GetObject(NextPageUniqueID);
                    int res = nextPage.IndexOf(item);
                    if (res != -1)
                        return items.Count + res;
                }
                return -1;
            }

			public void Insert(int index, T item)
			{
				if (items.Count + 1 > PageSize)
				{
					ProcessOverflow();
				}
				var parentInstance = this as IDependantViewModel;
				string lastUID = UniqueIDGenerator.Current.GetRelativeUniqueID(parentInstance, "" + items.Count);

				var newLazyItems = new BitArray(_lazyitems.Length + 1);
				for (int i = 0; i < index; i++)
				{
					newLazyItems[i] = _lazyitems[i];
				}
				newLazyItems[index] = false;

				// Switch the ids of the elements located after the new element's position
				for (int i = items.Count - 1; i >= index; i--)
				{
					newLazyItems[i + 1] = _lazyitems[i];
					if (!typeof(T).IsPrimitive && !(items[i] == null))
					{
						((IStateObject)items[i]).UniqueID = lastUID;
					}
					string itemUID = UniqueIDGenerator.Current.GetRelativeUniqueID(parentInstance, "" + (i));

                    if (i == items.Count - 1)
                    {
                        StateCache.Current.SwitchToNotExistingUniqueId(itemUID, lastUID);
                    }
                    else
                    {
                        StateCache.Current.SwitchUniqueIds(itemUID, lastUID);
                    }
					lastUID = itemUID;
				}
				// Insert the element in the local collection
				items.Insert(index, item);
				// Remove the entry where the new element will be stored
				StateCache.Current.RemoveEntryFromCache(
					UniqueIDGenerator.Current.GetRelativeUniqueID(parentInstance, "" + index));
				AttachValue(index, item);
				this._lazyitems = newLazyItems;
				TrackerHelper.MarkAsModified(this, "Count", false);
			}

			public void RemoveAt(int index)
			{
				if (index > items.Count)
				{
					if (string.IsNullOrWhiteSpace(NextPageUniqueID))
					{
						throw new IndexOutOfRangeException();
					}
					var nextPage = (ObservableCollectionEx<T>)StateCache.Current.GetObject(NextPageUniqueID);
					nextPage.RemoveAt(index);
					TrackerHelper.MarkAsModified(this, "Count", false);
					return;
				}

				if (IsLazy)
				{
					var parentInstance = this as IDependantViewModel;
					string lastUID = UniqueIDGenerator.Current.GetRelativeUniqueID(parentInstance, "" + index);

					SetAsRemoved(lastUID);
					RemoveLazyItem(index, lastUID, parentInstance);
				}
				items.RemoveAt(index);
				TrackerHelper.MarkAsModified(this, "Count", false);
			}

			/// <summary>
			/// Removes the lazy item from the collection.  Removing the item implies synchronizing the lazy items tracker, 
			/// rebuild the cache unique id and remove the element from the cache.
			/// </summary>
			/// <param name="index">Index of the element to remove</param>
			/// <param name="uniqueId">Id of the element to remove</param>
			/// <param name="parentInstance">IDependantViewModel object containing the element to delete</param>
			private void RemoveLazyItem(int index, string uniqueId, IDependantViewModel parentInstance)
			{
				var newLazyItems = new BitArray(_lazyitems.Length - 1);
				if (newLazyItems != null)
				{
					for (int i = 0; i < index; i++)
					{
						newLazyItems[i] = _lazyitems[i];
					}
				}
				for (int i = index + 1; i < items.Count; i++)
				{
					newLazyItems[i - 1] = _lazyitems[i];
					if (_lazyitems[i]) 
					{
						((IStateObject)items[i]).UniqueID = uniqueId;

					}
					string itemUID = UniqueIDGenerator.Current.GetRelativeUniqueID(parentInstance, "" + i);
					StateCache.Current.SwitchUniqueIds(itemUID, uniqueId);
					SwitchUniqueIDs(itemUID, uniqueId);
					uniqueId = itemUID;
				}
				_lazyitems = newLazyItems;
				StateCache.Current.RemoveObject(uniqueId);
			}

            public T this[int index]
            {
                get
                {
                    if (IsLazy)
                    {
                        if (!_lazyitems[index])
                        {
                            LoadItem(index);
                        }
                    }
                    return items[index];
                }
                set
                {
                    if (!typeof (T).IsPrimitive && typeof (IStateObject).IsAssignableFrom(typeof (T)))
                    {
                        AttachValue(index, value);
                        items[index] = value;
                    }
                    else
                    {
                        items[index] = value;
                    }
                }
            }

            public void Add(T item)
            {
                if (items.Count == PageSize)
                {
                    if (!string.IsNullOrWhiteSpace(NextPageUniqueID))
                    {
                        var next = (ObservableCollectionEx<T>) StateCache.Current.GetObject(NextPageUniqueID);
                        next.Add(item);
                    }
                    else
                    {
                        ProcessOverflow();
                    }
                }
                int newIndex = items.Count;


                items.Add(item);
                ResizeLazyItems(items.Count);
                AttachValue(newIndex, item);
                TrackerHelper.MarkAsModified(this, "Count", false);
            }

            public void AddRange(IList<T> list)
            {
                foreach (var T in list)
                {
                    Add(T);
                }
            }

            public void Clear()
            {
                // lets remove objects from cache
                for (int i = 0; i < items.Count; i++)
                {
                    var item = this[i] as IStateObject;
                    if (item != null)
                    {
                        StateCache.Current.RemoveObject(((IStateObject)item).UniqueID);
                    }
                }
                items.Clear();
               if (_lazyitems!=null) _lazyitems = new BitArray(0);
                TrackerHelper.MarkAsModified(this, "Count", false);
            }

            public bool Contains(T item)
            {
                return items.Contains(item);
            }

            public void CopyTo(T[] array, int arrayIndex)
            {
                throw new NotImplementedException();
            }

            public int Count
            {
                get
                {
                    if (IsLazy)
                    {
                        return _lazyitems.Length;
                    }
                    else
                    {
                        return items.Count;
                    }
                }
            }

            public bool IsReadOnly
            {
                get { return false; }
            }

            public bool Remove(T item)
            {
                bool underflow = false;
                bool res = false;
                if (typeof (IStateObject).IsAssignableFrom(typeof (T)))
                {
                    var itemAsIStateObject = (IStateObject) item;
                    res = RemoveItem(itemAsIStateObject, out underflow);
                }
                else
                {
                    //This is primitive type so just find it
                    res = RemoveItemPrimitive(item, out underflow);
                }
                if (underflow) ProcessUnderFlow();
                return res;
            }

            public IEnumerator<T> GetEnumerator()
            {
                if (IsLazy)
                    return new LazyIterator<T>(this);
                //TODO Modify to return a lazy aware enumerator
                return items.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            #endregion

            #region ITrackeableCollection Members

            public bool IsSameReference(int index, object targetItem)
            {
                if (typeof (T).IsPrimitive || typeof (T) == typeof (string))
                {
                    return sourceArray[index].Equals((T) targetItem);
                }
                else
                {
                    if (typeof (T) is IStateObject)
                    {
                        var viewModel = (IStateObject) (targetItem);
                        return FieldInfoArray[index].NewId.Equals(viewModel.UniqueID);
                    }
                }
                return false;
            }

            /// <summary>
            ///     Returns the length of the source array. If this is null, then it returns the length of the fieldInfo array
            ///     if this is null (meaning no changes have ocurred to the collection) it returns the
            ///     TrackerHelper.NO_CHANGES_MADE_ON_SOURCE_COLLECTION constant
            /// </summary>
            /// <returns>The FieldInfoArray.Count</returns>
            public int SourceCount
            {
                get
                {
                    return (sourceArray != null)
                               ? sourceArray.Count
                               : (FieldInfoArray != null)
                                     ? FieldInfoArray.Length
                                     : TrackerHelper.NO_CHANGES_MADE_ON_SOURCE_COLLECTION;
                }
            }

            #endregion

            public void SetAsRemoved(string uniqueID)
            {
                if (isCollectionOfIStateObject)
                {
                    foreach (FieldInfo fielInfo in FieldInfoArray)
                    {
                        if (fielInfo.NewId.Equals(uniqueID))
                        {
                            fielInfo.IsRemoved = true;
                            return;
                        }
                    }
                }
            }

            private void SwitchUniqueIDs(string itemUID, string lastUID)
            {
                if (isCollectionOfIStateObject)
                {
                    int length = FieldInfoArray.Length;
                    int foundElements = 0;

                    foreach (FieldInfo fieldInfo in FieldInfoArray)
                    {
                        if (fieldInfo.IsRemoved)
                        {
                            continue;
                        }

                        if (fieldInfo.NewId.Equals(itemUID))
                        {
                            fieldInfo.NewId = lastUID;
                            if (++foundElements == 2)
                                break;
                        }
                        else
                        {
                            if (fieldInfo.NewId.Equals(lastUID))
                            {
                                fieldInfo.NewId = itemUID;
                                if (++foundElements == 2)
                                    break;
                            }
                        }
                    }
                }
            }

            public void PushItem()
            {
            }

            public T PopItem()
            {
                return default(T);
            }

            private void ProcessOverflow()
            {
                throw new NotImplementedException();
            }

            private void AttachValue(int index, T item)
            {
				if (item is IStateObject)
				{
					var newValue = (IStateObject)item;
					if (!StateCache.IsAttachedChild(newValue))
					{
						//if current UniqueID is null then we need to sync the viewmodel uniqueID
						//with its parent
						var parentInstance = this as IDependantViewModel;
						string relativeUid = UniqueIDGenerator.Current.GetRelativeUniqueID(this, "" + index);
						var oldId = newValue.UniqueID;
						newValue.UniqueID = relativeUid;
						if (StateCache.AllBranchesAttached(relativeUid))
						{
							StateCache.Current.PromoteObject(newValue);
						}
						else
							StateCache.Current.SwitchUniqueIds(oldId, relativeUid);
					}
					else
					{
						if (!IsAttachedToThisCollection(newValue))
						{
							throw new NotImplementedException("Global objects are not supported as collection elements!");
						}
					}
				}
            }

			/// <summary>
			/// Tests if the given object is already attached to this collection.
			/// </summary>
			/// <param name="stateObject">The <c>stateObject</c></param>
			/// <returns></returns>
	        private bool IsAttachedToThisCollection(IStateObject stateObject)
	        {
				return stateObject.UniqueID.EndsWith(UniqueID);
	        }

	        // modify the isLoader property... old or new??
            private T LoadItem(int index)
            {
                //First lookforit
                string relativeUid = UniqueIDGenerator.Current.GetRelativeUniqueID(this, "" + index);
                var cachedValue = (T) StateCache.Current.GetObject(relativeUid);
                if (cachedValue == null)
                {
                    //We need to create it
                    cachedValue = container.Resolve<T>();
                    items[index] = cachedValue;
                    AttachValue(index, cachedValue);
                }
                else
                {
                    items[index] = cachedValue;
                }
                _lazyitems[index] = true;
                return cachedValue;
            }

            private void ResizeLazyItems(int newSize)
            {
                BitArray old_lazyitems = _lazyitems;
                _lazyitems = new BitArray(newSize);
                for (int i = 0; i < old_lazyitems.Length; i++)
                {
                    _lazyitems[i] = old_lazyitems[i];
                }
            }

            public bool RemoveItem(IStateObject item, out bool possibleUnderFlow)
            {
                bool res = false;
                possibleUnderFlow = false;
                if (!StateCache.IsAttachedChild(item))
                {
                    throw new NotSupportedException(
                        "To remove an object from a collection, the item must belong to that collection");
                }

                //UniqueID can be remapped back to index
                int indexAbsolute = MapUniqueIDToIndex(item);
                int indexRelative = indexAbsolute - PreviousPageIndex;
                if (indexRelative > items.Count)
                {
                    if (!string.IsNullOrWhiteSpace(NextPageUniqueID))
                    {
                        var nextPage = (ObservableCollectionEx<T>) StateCache.Current.GetObject(NextPageUniqueID);
                        res = nextPage.RemoveItem(item, out possibleUnderFlow);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    possibleUnderFlow = (items.Count - 1) < ((PageSize/2) - 1);
                    string lastID = item.UniqueID;
                    for (int i = indexRelative; i < items.Count; i++)
                    {
                        string actualID = UniqueIDGenerator.Current.GetRelativeUniqueID(this, "" + (i + 1));
                        StateCache.Current.SwitchUniqueIds(currentId: lastID, newId: actualID);
                        lastID = actualID;
                    }
                    RemoveFromItemsAndLazyFlags(indexRelative);
                    return true;
                }
                return false;
            }

            public bool RemoveItemPrimitive(T item, out bool possibleUnderFlow)
            {
                bool res = false;
                possibleUnderFlow = false;
                int index = items.IndexOf(item);
                possibleUnderFlow = (items.Count - 1) < ((PageSize/2) - 1);
                if (index != -1)
                {
                    //It was found we need to remove
                    RemoveFromItemsAndLazyFlags(index);
                    res = true;
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(NextPageUniqueID))
                    {
                        var nextPage = (ObservableCollectionEx<T>) StateCache.Current.GetObject(NextPageUniqueID);
                        res = nextPage.RemoveItemPrimitive(item, out possibleUnderFlow);
                    }
                }
                return res;
            }

            private void RemoveFromItemsAndLazyFlags(int indexRelative)
            {
                items.RemoveAt(indexRelative);
                var newLazyItems = new BitArray(_lazyitems.Length - 1);
                int j = 0;
                for (int i = 0; i < _lazyitems.Length; i++)
                {
                    if (i == indexRelative) continue;
                    newLazyItems[j++] = _lazyitems[i];
                }
            }

            private int MapUniqueIDToIndex(IStateObject itemAsIStateObject)
            {
                throw new NotImplementedException();
            }

            private void ProcessUnderFlow()
            {
                throw new NotImplementedException();
            }

            #region Nested type: LazyIterator

            public class LazyIterator<T> : IEnumerator<T>
            {
                private int index = -1;
                private ObservableCollectionEx<T> observableCollectionEx;

                public LazyIterator(ObservableCollectionEx<T> observableCollectionEx)
                {
                    // TODO: Complete member initialization
                    this.observableCollectionEx = observableCollectionEx;
                }

                #region IEnumerator<T> Members

                public T Current
                {
                    get { return observableCollectionEx[index]; }
                }

                public void Dispose()
                {
                }

                object IEnumerator.Current
                {
                    get { return Current; }
                }

                public bool MoveNext()
                {
                    index++;
                    if (index < observableCollectionEx.Count)
                    {
                        return true;
                    }
                    return false;
                }

                public void Reset()
                {
                    index = -1;
                }

                #endregion
            }

            #endregion
        }

        #endregion
    }
}