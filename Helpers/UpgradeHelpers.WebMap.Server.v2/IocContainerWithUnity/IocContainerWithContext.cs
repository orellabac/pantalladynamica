﻿using System;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
    /// <summary>
    ///     This class is just a wrapper which holds the context for the container
    ///     The context is important to determine parent - child relationships.
    ///     These relationships can even be related to the way uniqueIDs are generated
    /// </summary>
    public class IocContainerWithContext : IIocContainer
    {
        private readonly IStateObject _current;
        private readonly IIocContainer _wrappedContainer;

        public IocContainerWithContext(IIocContainer _containerToWrap, IStateObject current)
        {
            _wrappedContainer = _containerToWrap;
            _current = current;
        }

        #region IIocContainer Members

        public object Resolve(Type t, params object[] parameters)
        {
            return Resolve(t, GetUniqueIdentifierContexted, parameters);
        }

        public T Resolve<T>(params object[] parameters)
        {
            return (T) Resolve(typeof (T), parameters);
        }

        public object Resolve(Type t, Func<string> getUniqueIdentifier, params object[] parameters)
        {
            return _wrappedContainer.Resolve(t, getUniqueIdentifier, parameters);
        }

        #endregion

        private string GetUniqueIdentifierContexted()
        {
            return UniqueIDGenerator.Current.GetRelativeUniqueID(_current);
        }

		public void Bind(string objproperty, IStateObject obj, string dsproperty, object ds)
        {
            StateCache.Current.Bind(this,obj,objproperty,ds,dsproperty);
        }
    }
}