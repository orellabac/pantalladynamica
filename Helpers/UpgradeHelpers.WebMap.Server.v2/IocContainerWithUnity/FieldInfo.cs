﻿
namespace UpgradeHelpers.WebMap.Server
{
    public partial class IocContainerImplWithUnity
    {

        #region Nested type: FieldInfo

        public class FieldInfo
        {
            public FieldInfo(string id)
            {
                OldId = NewId = id;
            }

            public string OldId { get; set; }
            public string NewId { get; set; }
            public bool IsLoaded { get; set; }
            public bool IsRemoved { get; set; }

            public override string ToString()
            {
                return "{ " + IsRemoved + "," + IsLoaded + "," + OldId + "," + NewId + " }";
            }
        }

        #endregion

    }
}