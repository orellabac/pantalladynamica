using System;
using Newtonsoft.Json;

namespace UpgradeHelpers.WebMap.Server
{
    public class ObservableCollectionExSerializer : JsonConverter
    {

        private bool IsGeneratedType(Type valueType)
        {
            return valueType.Assembly.FullName.StartsWith("Unity");
        }

        public bool IsControlArray(Type type)
        {
            if (type.IsGenericType &&
                (type.GetGenericTypeDefinition() == typeof(IocContainerImplWithUnity.ObservableCollectionEx<>) ||
                 (type.BaseType != null && type.BaseType.IsGenericType && type.BaseType.GetGenericTypeDefinition() == typeof(IocContainerImplWithUnity.ObservableCollectionEx<>))))
            {
                return true;
            }
            return false;
        }


        public override bool CanConvert(Type objectType)
        {
            return IsControlArray(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteStartObject();
            dynamic coll = value;
            writer.WritePropertyName("UniqueID");
            writer.WriteValue((string)coll.UniqueID);
            writer.WritePropertyName("Count");
            writer.WriteValue((int)coll.Count);
            writer.WritePropertyName("@arr");
            writer.WriteValue("1");
            writer.WriteEndObject();
        }
    }
}