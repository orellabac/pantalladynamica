﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.Helpers;
using System.Data.Common;
using System.Reflection;
using Fasterflect;

namespace UpgradeHelpers.WebMap.Server
{
	public partial class IocContainerImplWithUnity : IIocContainer
	{
		internal const string RecoveredFromStorage = "RecoveredFromStorageKeyStringWasUsedBecauseStringsAreAlwaysSinglentons";
		internal const string SingletonWithUIDGenerator = "SingletonWithUIDGeneratorKeyStringMustUseTheGenerateIdDelegate";
		private static IocContainerImplWithUnity _current;


		internal static JsonSerializer _serializer = JsonSerializer.Create(
						new JsonSerializerSettings
						{
							TypeNameHandling = TypeNameHandling.None,
							ContractResolver = new PropertySkiperContractResolver(server:false),
							Converters = new JsonConverter[] { new ViewsStateDeltaConverter(), new ObservableCollectionExSerializer(), new ViewModelCollectionConverter() }
						});

		internal static JsonSerializer _serializerIndex = JsonSerializer.Create(
				new JsonSerializerSettings
				{
					TypeNameHandling = TypeNameHandling.None,
					ContractResolver = new PropertySkiperContractResolver(),
					Converters = new JsonConverter[] { new ViewsStateDeltaConverter(), new ObservableCollectionExSerializer(), new ViewModelCollectionConverter() }
				});
		internal static object NoView = new object();
		public readonly UnityContainer Container = new UnityContainer();


		static IocContainerImplWithUnity()
		{
			//_serializer.Converters.Add(new StateObjectSerializer(writeUniqueID: false, writeTypeInfo: false));

			_serializer.Converters.Add(new UpgradeHelpers.WebMap.Server.LogicSingletonSerializer(writeUniqueId: false, writeTypeInfo: false));
			_serializerIndex.Converters.Add(new StateObjectSerializerIndex(writeTypeInfo: false, writeUniqueID: true));
			_serializerIndex.Converters.Add(new UpgradeHelpers.WebMap.Server.LogicSingletonSerializer(writeTypeInfo: false, writeUniqueId: true));
			RegisterStandardSurrogates();

		}

		private IocContainerImplWithUnity()
		{
			RegisterRules();
		}

		/// <summary>
		/// Register basic surrogates
		/// </summary>
		private static void RegisterStandardSurrogates()
		{
			SurrogatesDirectory.defaultApplyBindingChangedAction = (string targetUniqueId, string targetProperty, object value) =>
			{
				var stateCache = StateCache.Current;
				var target = (IStateObject)stateCache.GetObject(targetUniqueId);
				var property = target.GetType().Property(targetProperty);
				if (property != null)
				{
					var convertedValue = System.Convert.ChangeType(value, property.PropertyType);
					property.SetValue(target, convertedValue, null);
				}
			};
			SurrogatesDirectory.GetUniqueIDForSurrogateDelegate = (obj) =>
			{
				var sur = StateCache.Current.GetSurrogateFor(obj, true);
				return sur.UniqueID;
			};

			SurrogatesDirectory.RestoreSurrogateDelegate = (uniqueId) =>
			{
				var sur = StateCache.Current.GetObject(uniqueId) as StateObjectSurrogate;
				if(sur != null)
				{
					return sur.Value;
				}
				return null;
			};
			RegisterSurrogateForConnection();
			RegisterSurrogateForCommand();
			RegisterSurrogateForSurrogate();
			RegisterSurrogateForDataBinding();
		}

		private static void RegisterSurrogateForCommand()
		{
			var signature = SurrogatesDirectory.ValidSignature("cmd");
			Func<object,object> serialize = (obj) =>
					{
						var cmd = obj as System.Data.Common.DbCommand;
						if (cmd != null)
						{
							var ms = new MemoryStream();
							var writer = new BinaryWriter(ms);
							Debug.Assert(signature.Length == SurrogatesDirectory.SignatureMaxlenght);
							writer.Write(signature);
							writer.Write(cmd.GetType().Namespace); //Used for provider
							writer.Write(cmd.CommandText);
							writer.Write((Int32)cmd.CommandType);
							if (cmd.Connection==null)
							{
								writer.Write(String.Empty);
							}
							else
							{
								var surrogate = StateCache.Current.GetSurrogateFor(cmd.Connection, false);
								writer.Write(surrogate.UniqueID);
							}
							writer.Flush();
							return ms.ToArray();
						}
						throw new ArgumentException(
								"SurrogateSerialize Error: Invalid object type expected System.Data.Common.DbConnection");
					};

			Func<BinaryReader,object> deserialize = (textReader) =>
					{
						var binaryReader = new BinaryReader(textReader.BaseStream);
						var connString = binaryReader.ReadString();
						var commandReflectionTypeName = binaryReader.ReadString();

						var providerName = binaryReader.ReadString();
						var commandText = binaryReader.ReadString();
						int commandType = binaryReader.ReadInt32();
						var connectionUniqueId = binaryReader.ReadString();
						DbConnection conn = null;
						if (connectionUniqueId != String.Empty)
						{
							var connectionSurrogate = StateCache.Current.GetObject(connectionUniqueId) as StateObjectSurrogate;
							if (connectionSurrogate !=null)
							{
								conn = connectionSurrogate.Value as DbConnection;
							}
						}
						

						var factory = DbProviderFactories.GetFactory(providerName);
						var cmd = factory.CreateCommand();
						cmd.Connection = conn;
						cmd.CommandText = commandText;
						cmd.CommandType = (System.Data.CommandType)commandType;
						return cmd;
					};



			SurrogatesDirectory.RegisterSurrogate(signature,
					typeof(System.Data.Common.DbCommand),
					serialize,
					deserialize, null);
			signature = SurrogatesDirectory.ValidSignature("cmd1");
			SurrogatesDirectory.RegisterSurrogate(signature,
					typeof(System.Data.OleDb.OleDbCommand),
					serialize,
					deserialize, null);
			signature = SurrogatesDirectory.ValidSignature("cmd2");
			SurrogatesDirectory.RegisterSurrogate(signature,
					typeof(System.Data.Odbc.OdbcCommand),
					serialize,
					deserialize, null);
			signature = SurrogatesDirectory.ValidSignature("cmd3");
			SurrogatesDirectory.RegisterSurrogate(signature,
					typeof(System.Data.SqlClient.SqlCommand),
					serialize,
					deserialize, null);
		}

		private static void RegisterSurrogateForDataBinding()
		{
			var signature = SurrogatesDirectory.ValidSignature("dbin");
			SurrogatesDirectory.RegisterSurrogate(signature, typeof(StateCache.DataBinding),
																						(obj) =>
																						{
																							var databinding =
																									obj as UpgradeHelpers.WebMap.Server.StateCache.DataBinding;
																							if (databinding != null)
																							{
																								var ms = new MemoryStream();
																								var writer = new BinaryWriter(ms);

																								var signatureChars = signature.ToCharArray();
																								Debug.Assert(signatureChars.Length == SurrogatesDirectory.SignatureMaxlenght);
																								writer.Write(signature);
																								//DataSource
																								writer.Write(databinding.DataSourceReference.UniqueID);
																								//DataSourceProperty
																								writer.Write(databinding.DataSourceProperty);
																								//Object
																								writer.Write(databinding.ReferencedUniqueID);
																								//ObjectProperty
																								writer.Write(databinding.ObjProperty);
																								//Next
																								writer.Write(databinding.NextBinding ?? "");
																								//Previous
																								writer.Write(databinding.PreviousBinding ?? "");
																								writer.Flush();
																								return ms.ToArray();
																							}
																							throw new NotImplementedException(
																									"SurrogateSerialize Error: Invalid object type expected StateCache.DataBinding");
																						},
																						(textReader) =>
																						{
																							var stateCache = StateCache.Current;
																							var reader = new BinaryReader(textReader.BaseStream);
																							var dataBinding = new StateCache.DataBinding();
																							//DataSource
																							var idDataSource = reader.ReadString();

																							dataBinding.DataSourceReference =(IStateObject)stateCache.GetObject(idDataSource);
																							//DataSourceProperty
																							dataBinding.DataSourceProperty = reader.ReadString();
																							//Object
																							var idObj = reader.ReadString();
																							dataBinding._objReferenceUID = idObj;
																							//ObjectProperty
																							dataBinding.ObjProperty = reader.ReadString();
																							//Next
																							var next = reader.ReadString();
																							dataBinding.NextBinding = next == "" ? null : next;
																							//Previous
																							var previous = reader.ReadString();
																							dataBinding.PreviousBinding = previous == "" ? null : previous;
																							return dataBinding;
																						}, null);
		}

		private static void RegisterSurrogateForSurrogate()
		{
			var signature = SurrogatesDirectory.ValidSignature("surr");
			SurrogatesDirectory.RegisterSurrogate(signature, typeof(StateObjectSurrogate),
					(obj) =>
					{
						var surrogate = obj as StateObjectSurrogate;
						if (surrogate != null)
						{
							var ms = new MemoryStream();
							var writer = new BinaryWriter(ms);
							Debug.Assert(signature.Length == SurrogatesDirectory.SignatureMaxlenght);
							writer.Write(signature);
							//"UniqueID");
							writer.Write(surrogate.UniqueID);
							//RefCount
							writer.Write((Int32)surrogate.objectRefs.Count);
							//Refs
							foreach(var v in surrogate.objectRefs)
							{
								writer.Write(v.UniqueID);
							}
							writer.Flush();
							return ms.ToArray();
						}
						throw new ArgumentException(
								"SurrogateSerialize Error: Invalid object type expected Surrogate");
					},
					(binaryReader) =>
					{
						
						var surrogate = new StateObjectSurrogate
						{
							UniqueID = binaryReader.ReadString()
						};
						var refCount = binaryReader.ReadInt32();
						for (int i = 0; i < refCount;i++ )
						{
							var referenceId = binaryReader.ReadString();
							StateCache.Current.AddSurrogateReference(surrogate, referenceId);
						}
						return surrogate;
					}, null);
		}

		private static void RegisterSurrogateForConnection()
		{
			var signature = SurrogatesDirectory.ValidSignature("conn");

			Func<object,object> serialize = (obj) =>
					{
						var conn = obj as System.Data.Common.DbConnection;
						if (conn != null)
						{
							var ms = new MemoryStream();
							var writer = new BinaryWriter(ms);
							Debug.Assert(signature.Length == SurrogatesDirectory.SignatureMaxlenght);
							writer.Write(signature);
							writer.Write(conn.GetType().Namespace); //Used for provider

							writer.Write(conn.ConnectionString);
							writer.Write((Int32)conn.State);
							writer.Flush();
							return ms.ToArray();
						}
						throw new ArgumentException(
								"SurrogateSerialize Error: Invalid object type expected System.Data.Common.DbConnection");
					};

			Func<BinaryReader,object> deserialize = (textReader) =>
					{
						var binaryReader = new BinaryReader(textReader.BaseStream);
						var providerName = binaryReader.ReadString();
						var factory = DbProviderFactories.GetFactory(providerName);
						var conn = factory.CreateConnection();
						var connString = binaryReader.ReadString();
						var state = (System.Data.ConnectionState)binaryReader.ReadInt32();
						conn.ConnectionString = connString;
						if (state == System.Data.ConnectionState.Open)
							conn.Open();
						return conn;
					};

			SurrogatesDirectory.RegisterSurrogate(signature,
					typeof(System.Data.Common.DbConnection),
					serialize,
					deserialize, null);

			signature = SurrogatesDirectory.ValidSignature("conn2");
			SurrogatesDirectory.RegisterSurrogate(signature,
					typeof(System.Data.OleDb.OleDbConnection),
					serialize,
					deserialize, null);

			signature = SurrogatesDirectory.ValidSignature("conn3");
			SurrogatesDirectory.RegisterSurrogate(signature,
					typeof(System.Data.Odbc.OdbcConnection),
					serialize,
					deserialize, null);

			signature = SurrogatesDirectory.ValidSignature("conn4");
			SurrogatesDirectory.RegisterSurrogate(signature,
					typeof(System.Data.SqlClient.SqlConnection),
					serialize,
					deserialize, null);



		}

		public static IocContainerImplWithUnity Current
		{
			get { return _current ?? (_current = new IocContainerImplWithUnity()); }
		}

		#region IIocContainer Members

		public T Resolve<T>(params object[] parameters)
		{
			return (T)Resolve(typeof(T), parameters);
		}

		public object Resolve(Type t, Func<string> getUniqueIdentifier, params object[] parameters)
		{
			var typeofT = t;
			object newInstance = null;
			if (SurrogatesDirectory.IsSurrogateRegistered(typeofT))
			{
				//Surrogates are created as top level objects
				//however reference tracking is important because surrogates with no references
				//are not persisted
				if (parameters.Length == 0)
				{
					//We must create a new instance, instance creation mechanism must be located from
					//the surrogates directory
					newInstance = SurrogatesDirectory.CreateInstanceFor(typeofT);


				}
				else
				{
					newInstance = parameters[0];
				}
				var surrogate = StateCache.Current.GetSurrogateFor(newInstance, generateIfNotFound: true);
				return newInstance;
			}

			if (!IsRecoveredFromStorage(parameters))
			{
				var attributes = t.GetCustomAttributes(typeof(Singlenton), false);
				if (attributes != null && attributes.Length > 0)
				{
					newInstance = StateCache.Current.GetObject(UniqueIDGenerator.GetSinglentonUniqueId(t));
					//At this point the object was recovered from storage and it the RecoveredFromStorageFlags must
					//added to avoid unnecessary further processing of this object
					if (newInstance != null)
						parameters = AddRecoveredFromStorage(parameters);
				}
			}


			newInstance = newInstance ?? ResolveUnPrepared(t);
			if (newInstance is IViewManager || newInstance is IDefaults)
			{
				//Nothing to do
			}
			else if (newInstance is ILogic)
			{
				InitializeObject((ILogic)newInstance, parameters,t,getUniqueIdentifier);
			}
			else if (newInstance is IStateObject)
			{
				InitializeObject((IStateObject)newInstance, parameters, t, getUniqueIdentifier);
			}
			else
			{
				Debug.Assert(false, "Type to resolved is not an IStateObject, so there is no tracking or serialization available for that object");
			}
			return newInstance;
		}

		//Inserts the recoverred from storage flag
		private object[] AddRecoveredFromStorage(object[] parameters)
		{
			if (parameters == null || parameters.Length == 0)
			{
				return new object[] { RecoveredFromStorage };
			}
			else
			{
				var oldLength = parameters.Length;
				Array.Resize(ref parameters, oldLength + 1);
				parameters[oldLength] = RecoveredFromStorage;
				return parameters;
			}
		}

		public object Resolve(Type t, params object[] parameters)
		{
			if (parameters != null && parameters.FirstOrDefault(x => x == RecoveredFromStorage) != null)
			{
				return Resolve(t, () => "fromstorage", parameters);
			}
			else
				return Resolve(t, UniqueIDGenerator.Current.GetUniqueID, parameters);
		}

		#endregion

		internal static void ResetSingleton()
		{
			_current = null;
		}

		private void RegisterRules()
		{
			Container.RegisterType<IViewManager>(new InjectionFactory(c => ViewManager.Instance));
			Container.RegisterType<IIocContainer>(new InjectionFactory(c => this));
			Container.RegisterType(typeof(IModelList<>), typeof(ObservableCollectionEx<>));
			Container.AddNewExtension<Interception>();
		}


		public object ResolveUnPrepared(Type t)
		{
			if (typeof(IViewModel).IsAssignableFrom(t) || typeof(IDependantViewModel).IsAssignableFrom(t))
			{
				lock (Container)
				{
					if (!Container.IsRegistered(t))
					{
						Container.RegisterType(t, new Interceptor<VirtualMethodInterceptor>(), new InterceptionBehavior<LazyBehaviour>());
					}
				}
			}
			if (typeof(IDefaults).IsAssignableFrom(t))
			{
				lock (Container)
				{
					if (!Container.IsRegistered(t))
					{
						Container.RegisterType(t, new Interceptor<VirtualMethodInterceptor>(), new InterceptionBehavior<LazyBehaviour>());
					}
				}
			}

			if (typeof(ILogicClass).IsAssignableFrom(t))
			{
				lock (Container)
				{
					if (!Container.IsRegistered(t))
					{
						Container.RegisterType(t, new Interceptor<VirtualMethodInterceptor>(), new InterceptionBehavior<LazyBehaviour>());
					}
				}
			}
			object newInstance;
			if (Container.IsRegistered(t))
				newInstance = Container.Resolve(t);
			else
			{
				newInstance = Activator.CreateInstance(t);
				Debug.WriteLine(String.Format("Warning unregistered type instance for type {0}", t.AssemblyQualifiedName));
			}

			return newInstance;
		}

		private static bool IsRecoveredFromStorage(object[] parameters)
		{
			return parameters != null && parameters.FirstOrDefault(x => x == RecoveredFromStorage) != null;
		}

		private static bool IsSingletonWithUIDGenerated(object[] parameters)
		{
			return parameters != null && parameters.FirstOrDefault(x => x == SingletonWithUIDGenerator) != null;
		}

		private void InitializeObject(IStateObject obj, object[] parameters, Type t, Func<string> getUniqueIdentifier)
		{
			bool recoveredFromStorage = IsRecoveredFromStorage(parameters);
			var attributes = t.GetCustomAttributes(typeof(Singlenton), false);
			if (attributes!=null && attributes.Length > 0)
			{
				//Unique ID 
				if (!recoveredFromStorage) obj.UniqueID = UniqueIDGenerator.GetSinglentonUniqueId(t);
				
			}
			else
			{
				if (!recoveredFromStorage)
				{
					if (!StateCache.IsRootLevelObject(obj))
						obj.UniqueID = "$$$" + getUniqueIdentifier();
					else if (IsSingletonWithUIDGenerated(parameters))
					{
						obj.UniqueID = getUniqueIdentifier();
					}
					else
					{
						obj.UniqueID = UniqueIDGenerator.Current.GetUniqueID();
					}
					if (obj is IShared)
					{
						obj.UniqueID = "!" + obj.UniqueID;
					}

				}
			}
			//Initialization for control arrays
			//pending review IModelList add ICreatesObjects
			if (obj is IStateObject && IsControlArray((IStateObject)obj))
			{
				//We need the list to be already in the cache as attached so this operation need to be done after that
				object countParameter = null;
				if (parameters != null && (countParameter = parameters.FirstOrDefault(x => x is int)) != null)
				{
					var list = (IList)obj;
					Type[] args = t.GetGenericArguments();
					var size = (int)countParameter;
					dynamic d = list;
					d.container = new IocContainerWithContext(this, obj);
					d.CollectionSize = size;
				}
			}

			if (obj is IDependantViewModel) 
			{
				var container = new IocContainerWithContext(this, obj);
				if (!recoveredFromStorage)
					((IDependantViewModel)obj).Build(new IocContainerWithContext(container, obj));
				if (obj is IInteractsWithView)
				{
					((IInteractsWithView)obj).ViewManager = ViewManager.Instance;
				}
				if (obj is ICreatesObjects)
				{
					((ICreatesObjects)obj).Container = container;
				}
			}
			if (!recoveredFromStorage)
				StateCache.Current.AddNewObject(obj);
		}

		private void InitializeObject(ILogic logic, object[] parameters, Type logicType, Func<string> getUniqueIdentifier)
		{
			PropertyInfo defaultProp = logicType.Property("Defaults");
			PropertyInfo viewModelProp = logicType.Property("ViewModel"); //This type is fix but arbitrary
			MethodInfo initMethod = logicType.Method("Init");
			IIocContainer container = this;
			
			var isNewLogic = true;
			if (viewModelProp != null)
			{
				IViewModel viewModel = null;
				if (parameters != null && parameters.Length > 0)
				{
					if (parameters[0] == NoView)
					{
						isNewLogic = false;
					}
					else if (parameters[0] is IViewModel)
					{
						viewModel = (IViewModel)parameters[0];
						//In case we are providing aditional parameter it determines if the logic instance is new
						//this case can't be determine within the this method so it needs to be passed as an aditional value in the parameters 
						isNewLogic = parameters.Length > 1 ? (bool)parameters[1] : false;
					}
				}
				else
				{
					viewModel = (IViewModel)Resolve(viewModelProp.PropertyType);
					viewModel.Build(new IocContainerWithContext(this, viewModel));

				}
				viewModelProp.SetValue(logic, viewModel, null);
				if (viewModel != null && isNewLogic)
				{
					if (logic is ILogicView<IViewModel>)
					{
						if (!IsRecoveredFromStorage(parameters))
							ViewManager.Instance.Events.AutoWireEvents((ILogicView<IViewModel>)logic);

					}
				}
				container = new IocContainerWithContext(this, viewModel);
			}
			// lets inject the container
			if (logic is ICreatesObjects)
			{
				((ICreatesObjects)logic).Container = container;
			}
			if (logic is IDefaultInstances<IDefaults> && defaultProp != null)
			{
				object defaultsObject = Resolve(defaultProp.PropertyType);
				defaultProp.SetValue(logic, defaultsObject, null);
			}

			if (logic is IInteractsWithView)
			{
				((IInteractsWithView)logic).ViewManager = ViewManager.Instance;
			}


			// lets inject all IViewModel properties  

			//Check if following code is either correct or necessary
			Type baseType = typeof(ILogicView<IViewModel>);
			IEnumerable<PropertyInfo> iLogicProperties = logicType.Properties().Where(x => baseType.IsAssignableFrom(x.PropertyType));
			foreach (PropertyInfo logicProperty in iLogicProperties)
			{
				if (logicProperty.GetSetMethod() != null)
					logicProperty.SetValue(logic, Resolve(logicProperty.PropertyType), null);
			}

			if (logic is IStateObject)
			{
				InitializeObject((IStateObject)logic, parameters, logicType, getUniqueIdentifier);
			}

			if (isNewLogic && parameters.FirstOrDefault(x => x == RecoveredFromStorage) == null && 
				initMethod != null)
			{
				initMethod.Invoke(logic, new object[] { });
			}

		}

		//private void IntilializeObject(ILogicControl<IDependantViewModel> logic, object[] parameters)
		//{
		//	Type logicType = logic.GetType();

		//	PropertyInfo defaultProp = logicType.GetProperty("Defaults");
		//	PropertyInfo viewModelProp = logicType.GetProperty("ViewModel");
		//	PropertyInfo viewManagerProp = logicType.GetProperty("ViewManager");

		//	if (viewModelProp != null)
		//	{
		//		var isNewLogic = true;
		//		IViewModel viewModel = null;
		//		if (parameters != null && parameters.Length > 0)
		//		{
		//			if (parameters[0] == NoView)
		//			{
		//			}
		//			else if (parameters[0] is IViewModel)
		//			{
		//				viewModel = (IViewModel)parameters[0];
		//				isNewLogic = false;
		//			}
		//		}
		//		else
		//		{
		//			viewModel = (IViewModel)Resolve(viewModelProp.PropertyType);
		//			viewModel.Build(new IocContainerWithContext(this, viewModel));

		//		}
		//		viewModelProp.SetValue(logic, viewModel, null);
		//		if (viewModel != null && isNewLogic)
		//		{
		//			//ViewManager.Instance.Events.AutoWireEvents(logic);
		//		}
		//		// lets inject the view model

		//		// lets inject the viewmanager

		//		if (logic is ICreatesObjects)
		//		{
		//			((ICreatesObjects)logic).Container = viewModel == null ? this : (IIocContainer)new IocContainerWithContext(this, viewModel);
		//		}

		//	}
		//	if (defaultProp != null)
		//	{
		//		object defaultsObject = Resolve(defaultProp.PropertyType);
		//		defaultProp.SetValue(logic, defaultsObject, null);
		//	}
		//	logic.ViewManager = ViewManager.Instance;


		//	// lets inject all IViewModel properties  
		//	Type baseType = typeof(ILogicView<IViewModel>);
		//	IEnumerable<PropertyInfo> iLogicProperties = logicType.GetProperties().Where(x => baseType.IsAssignableFrom(x.PropertyType));
		//	foreach (PropertyInfo logicProperty in iLogicProperties)
		//	{
		//		logicProperty.SetValue(logic, Resolve(logicProperty.PropertyType), null);
		//	}
		//}

		public string GenerateCurrentStateAsJSON()
		{
			ViewsState viewState = ViewManager.Instance.State;
			//var modelStateDelta = StateCache.Current.GetDeltasForClientSync();
			//from m in  viewState.LoadedViews select StateCache.Current.GetObject(
			ViewManager.Instance.Events.Suspend();
			StateCache.Current.LoadAllEntriesThanNeedToBeOnFrontEnd();

			IEnumerable<IStateObject> models = from v in viewState.LoadedViews select (IStateObject)StateCache.Current.GetObject(v.UniqueID);

			foreach (IStateObject model in models)
			{
				LoadRequiredSublevels(model, new HashSet<object>());
			}

			object cacheModelEntries = StateCache.Current.GetModelEntries();
			var res = new { V = viewState, M = cacheModelEntries, };
			var stringWriter = new StringWriter();
			var jsonWriter = new JsonTextWriter(stringWriter);
			_serializerIndex.Serialize(jsonWriter, res);
			var allres = stringWriter.ToString();
			ViewManager.Instance.Events.Resume();
			return allres;

		}

		/// <summary>
		///     This method is meant to make sure that all required levels for a model are loaded
		/// </summary>
		/// <param name="model"></param>
		private void LoadRequiredSublevels(IStateObject model, HashSet<object> visited)
		{
			if (visited.Contains(model)) return;
			if (IsControlArray(model))
			{
				dynamic m = model;
				dynamic count = m.Count;
				for (int i = 0; i < count; i++)
				{
					dynamic testVal = m[i];
					testVal = null;
				}
			}
			else
				foreach (PropertyInfo prop in model.GetType().Properties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static))
				{
					if (typeof(IStateObject).IsAssignableFrom(prop.PropertyType))
					{
						ParameterInfo[] indexParams = prop.GetIndexParameters();
						if (indexParams.Length == 0)
						{
							//This will load the property
							object value = prop.GetValue(model, null);
							if (value != null)
								LoadRequiredSublevels((IStateObject)value, visited);
						}
						else
						{
							/*TODO*/
						}
					}
				}
		}

		private bool IsControlArray(IStateObject model)
		{
			Type modelType;
			return (model != null && (modelType = model.GetType()).IsGenericType &&
							((modelType.GetGenericTypeDefinition() == typeof(ObservableCollectionEx<>)) ||
							 (modelType.BaseType != null && modelType.BaseType.GetGenericTypeDefinition() == typeof(ObservableCollectionEx<>))));
		}


		internal static string GenerateAppChanges(PiggyBackResult piggyBackResult)
		{
			object modelStateDelta = StateCache.Current.GetDeltasForClientSync();
			var switchedIds = StateCache.Current.GetSwitchedIds();
			var idsToRemove = StateCache.Current.GetElementsToRemove();
			ViewsStateDelta viewStateDelta = ViewManager.Instance.EndRequest();

            var res = StateCache.Current.HasSwichedIds
                ? new
                {
	                VD = viewStateDelta,
	                MD = modelStateDelta,
	                SW = switchedIds,
	                RM = idsToRemove,
                }
                : (object)new
                {
                    VD = viewStateDelta,
                    MD = modelStateDelta,
                    RM = idsToRemove
                };
			if (piggyBackResult != null)
			{
				res = piggyBackResult(res);
			}

			var stringWriter = new StringWriter();
			_serializer.Serialize(stringWriter, res);
			return stringWriter.ToString();
		}


		public void Bind(string objproperty, IStateObject obj, string dsproperty, object ds)
		{
			StateCache.Current.Bind(this, obj, objproperty, ds, dsproperty);
		}
	}
}
