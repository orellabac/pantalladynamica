﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using Microsoft.Practices.Unity.InterceptionExtension;
using UpgradeHelpers.Helpers;
using System.Linq;
using Fasterflect;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
	public partial class IocContainerImplWithUnity
	{


		#region Nested type: LazyBehaviour

		public class LazyBehaviour : IInterceptionBehavior
		{
			private static TypePropertiesCache typePropertiesCache = new TypePropertiesCache();


			internal HashSet<object> skipInterceptor = new HashSet<object>();



			#region IInterceptionBehavior Members

			/// This method id called every time a method is called from ViewModel
			public IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
			{
				string methodName = input.MethodBase.Name;
				Type targetType = input.Target.GetType();
				var parentInstance = input.Target;
				PropertyInfo prop = null;
				bool isPropIStateObject = false;
				var stateCache = StateCache.Current;
				
                if (methodName != "GetHashCode" && methodName != "Equals" && !skipInterceptor.Contains(input.Target))
				{

					if (typePropertiesCache.hasSetter(targetType, methodName, out prop))
					{
						ProcessSetter(input, prop, isPropIStateObject, parentInstance);
					}
					else if (typePropertiesCache.hasGetter(targetType, methodName, out prop))
					{
						IMethodReturn invoke;
						if (ProcessGetter(input, getNext, prop, parentInstance, out invoke)) return invoke;
					}
				}
				IMethodReturn result = getNext()(input, getNext);
				return result;
			}

			private bool ProcessGetter(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext, PropertyInfo prop,
									   object parentObject, out IMethodReturn invoke)
			{

				var  parentInstance = parentObject as IStateObject;
				invoke = null;
				Type returnType = prop.PropertyType; //((MethodInfo) (input.MethodBase)).ReturnType;
				string propName = prop.Name; //methodName.Substring(4);

				if (SurrogatesDirectory.IsSurrogateRegistered(returnType))
				{
					IMethodReturn intermediateResult = getNext()(input, getNext);
					invoke = intermediateResult;
					if (intermediateResult.ReturnValue == null)
					{
						//Try to recover
						//1. First recover pointer
						//2. Then get surrogate
						string relativeUid = StateCache.GetPointerUniqueId(UniqueIDGenerator.Current.GetRelativeUniqueID(parentInstance, propName));
						var pointer = (StateObjectPointer)StateCache.Current.GetObject(relativeUid);
						if (pointer != null)
						{
							var cachedValue = TryToRecoverWithPointer(input, prop, parentInstance, true);
							{
								invoke = new VirtualMethodReturn(input, cachedValue, null);
								return true;
							}
						}
					}
					return true;
				}
				else if (typeof(IDependantViewModel).IsAssignableFrom(returnType))
				{
					IMethodReturn intermediateResult = getNext()(input, getNext);
					if (intermediateResult.ReturnValue == null)
					{
						//parentInstance = input.Target as IStateObject;
						if (parentInstance != null)
						{
							string relativeUid = UniqueIDGenerator.Current.GetRelativeUniqueID(parentInstance,
																							   propName);
							IStateObject cachedValue = StateCache.Current.GetObject(relativeUid);
							try
							{
								ViewManager.Instance.Events.Suspend();

								skipInterceptor.Add(parentObject);
								prop.SetValue(input.Target, cachedValue, null);
							}
							finally
							{
								skipInterceptor.Remove(parentObject);
								ViewManager.Instance.Events.Resume();
							}
							{
								invoke = new VirtualMethodReturn(input, cachedValue, null);
								return true;
							}
						}
					}
				}
				else if (typeof(ITopLevelStateObject).IsAssignableFrom(returnType))
				{
					IMethodReturn intermediateResult = getNext()(input, getNext);
					invoke = intermediateResult;
					if (intermediateResult.ReturnValue == null)
					{
						if (typeof(IDefaults).IsAssignableFrom(prop.DeclaringType))
						{
							string defaultUid = UniqueIDGenerator.GetSinglentonUniqueId(returnType);
							object defaultInstance = StateCache.Current.GetObject(defaultUid);
							if (defaultInstance == null)
							{
								defaultInstance = Current.Resolve(returnType, defaultInstance);
							}
							try
							{
								skipInterceptor.Add(parentObject);
								prop.SetValue(input.Target, defaultInstance, null);
							}
							finally
							{
								skipInterceptor.Remove(parentObject);
							}
							invoke = new VirtualMethodReturn(input, defaultInstance, null);
							return true;
						}
						else if (typeof(IStateObject).IsAssignableFrom(prop.DeclaringType))
						{
							var attributes = returnType.GetCustomAttributes(typeof(Singlenton), false);
							if (attributes != null && attributes.Length > 0)
							{
								var cachedValue = StateCache.Current.GetObject(UniqueIDGenerator.GetSinglentonUniqueId(returnType));
								invoke = new VirtualMethodReturn(input, cachedValue, null);
								return true;
							}
							else
							{
								string relativeUid = StateCache.GetPointerUniqueId(UniqueIDGenerator.Current.GetRelativeUniqueID(parentInstance, propName));
								var pointer = (StateObjectPointer)StateCache.Current.GetObject(relativeUid);
								if (pointer != null)
								{
									var cachedValue = TryToRecoverWithPointer(input, prop, parentInstance, false);
									{
										invoke = new VirtualMethodReturn(input, cachedValue, null);
										return true;
									}
								}
							}
						}
					}
					return true;
				}
				else if (typeof(ILogicWithViewModel<IViewModel>).IsAssignableFrom(returnType))
				{
					IMethodReturn intermediateResult = getNext()(input, getNext);
					if (intermediateResult.ReturnValue == null)
					{
						if (typeof(IDefaults).IsAssignableFrom(prop.DeclaringType))
						{
							string defaultUid = UniqueIDGenerator.GetSinglentonUniqueId(returnType);
							IViewModel viewModel = null;
							PropertyInfo viewModelProp = returnType.Property("ViewModel");
							if (viewModelProp != null)
							{
								bool newLogic = false;
								viewModel = (IViewModel)StateCache.Current.GetObject(defaultUid);
								if (viewModel == null)
								{
									newLogic = true;
									viewModel = (IViewModel)Current.Resolve(viewModelProp.PropertyType, () => defaultUid, SingletonWithUIDGenerator);
									viewModel.Build(new IocContainerWithContext(IocContainerImplWithUnity.Current, viewModel));
								}
								object defaultInstance = Current.Resolve(returnType, viewModel, newLogic);
								prop = input.Target.GetType().Property(propName);
								try
								{
									skipInterceptor.Add(parentObject);
									prop.SetValue(input.Target, defaultInstance, null);
								}
								finally
								{
									skipInterceptor.Remove(parentObject);
								}
								invoke = new VirtualMethodReturn(input, defaultInstance, null);
								return true;
							}
						}
					}
					return false;
				}
#if DEBUG && Verbose
					Debug.WriteLine(input.MethodBase.Name + " -- " + input.MethodBase.ReflectedType);
#endif
				return false;
			}

			private object TryToRecoverWithPointer(IMethodInvocation input, PropertyInfo prop, IStateObject parentInstance, bool forSurrogate)
			{

				string relativeUid = StateCache.GetPointerUniqueId(UniqueIDGenerator.Current.GetRelativeUniqueID(parentInstance, prop.Name));
				var pointer = (StateObjectPointer)StateCache.Current.GetObject(relativeUid);
				object cachedValue = null;
				StateObjectSurrogate surrogate = null;
				if (pointer != null)
				{
					if (forSurrogate)
					{
						surrogate = (StateObjectSurrogate)pointer.Target;
						cachedValue = surrogate.Value;
					}
					else
					{
						cachedValue = (IStateObject)pointer.Target;
					}

					try
					{
						var stateCache = StateCache.Current;
						skipInterceptor.Add(parentInstance);
						prop.SetValue(input.Target, cachedValue, null);
						if (forSurrogate)
							stateCache.AttachBindingBehaviourForSurrogate(surrogate, cachedValue);
					}
					finally
					{
						skipInterceptor.Remove(parentInstance);
					}
				}
				return cachedValue;
			}


			private void ProcessSetterDefaults(IMethodInvocation input, PropertyInfo prop, bool isPropIStateObject, IDefaults defaults)
			{
				//To process setter we need to get the target object from the defaults
				//Get PropertyName
				var defaultsType = defaults.GetType();
				var propName = prop.Name;
				var propInDefaults = defaultsType.Property(propName);
				var currentValue = propInDefaults.GetValue(input.Target, null);
				var newValueObject = input.Arguments[0];
				var newValue = newValueObject as IStateObject;
				var stateCache = StateCache.Current;
				if (currentValue != null)
				{
					//if newValue is different we must remove object
					bool ToRemove = newValueObject != currentValue;
					//if Surrogate it must be process different
					if (ToRemove && SurrogatesDirectory.IsSurrogateRegistered(propInDefaults.PropertyType))
					{
						var relativeUid = UniqueIDGenerator.GetSinglentonUniqueId(propInDefaults.PropertyType);
						var pointer = (StateObjectPointer)stateCache.GetObject(relativeUid);
						if (pointer != null) stateCache.RemoveObject(pointer.UniqueID);
					}
					else if (ToRemove)
					{
						if (currentValue is IStateObject)
						{
							//This is the default object and all properties have UID just like Form1_Default 
							var uid = UniqueIDGenerator.GetSinglentonUniqueId(propInDefaults.PropertyType);
							stateCache.RemoveObject(uid);
						}
						else if (currentValue is ILogicView<IViewModel>)
						{
							var formLogic = currentValue as ILogicWithViewModel<IViewModel>;
							var formViewModelUID = formLogic.ViewModel.UniqueID;
							stateCache.RemoveObject(formViewModelUID);
							//This is a defaults so what we need to delete is the ViewInstance
						}
					}
				}
				//At this point if the old instance was present we already deleted it
				try
				{
					skipInterceptor.Add(defaults);
					propInDefaults.SetValue(defaults, newValueObject, null);
				}
				finally
				{
					skipInterceptor.Remove(defaults);
				}

				var uidForProperty = UniqueIDGenerator.GetSinglentonUniqueId(propInDefaults.PropertyType);
				if (newValueObject != null)
				{
					if (SurrogatesDirectory.IsSurrogateRegistered(propInDefaults.PropertyType)) throw new NotImplementedException();
					//At this point newValueObject must be !=null
					newValue.UniqueID = uidForProperty;
					stateCache.AddNewObject(newValue);
				}
			}

			private void ProcessSetter(IMethodInvocation input, PropertyInfo prop, bool isPropIStateObject,
												object parentObject)
			{
				var parentInstance = parentObject as IStateObject;
				var stateCache = StateCache.Current;
				var uuidgen = UniqueIDGenerator.Current;
				string propName = prop.Name;
				var newValueObject = input.Arguments[0];
				var newValue = newValueObject as IStateObject;
				var isPointer = typePropertiesCache.GetIsPointer(prop);
				if (parentInstance == null)
				{
					//parentInstance is not an IStateObject
					//another case is that parentInstance is Defaults
					var parentDefaults = input.Target as IDefaults;
					if (parentDefaults != null)
					{
						ProcessSetterDefaults(input, prop, isPropIStateObject, parentDefaults);
					}
				}
				else
				{
					if (newValue == null && SurrogatesDirectory.IsSurrogateRegistered(prop.PropertyType))
					{
						if (newValueObject == null)
						{
							var relativeUid = StateCache.GetPointerUniqueId(uuidgen.GetRelativeUniqueID(parentInstance, propName));
							var pointer = (StateObjectPointer)stateCache.GetObject(relativeUid);
							if (pointer != null)
								stateCache.RemoveObject(pointer.UniqueID);

						}
						else if (newValueObject != null)
						{
							var relativeUid = StateCache.GetPointerUniqueId(uuidgen.GetRelativeUniqueID(parentInstance, propName));
							var pointer = (StateObjectPointer)stateCache.GetObject(relativeUid);

							if (pointer == null)
							{
								pointer = IocContainerImplWithUnity.Current.Resolve<StateObjectPointer>();
								//There was not a pointer object here, it means is not initialized
								StateObjectSurrogate surrogate = stateCache.GetSurrogateFor(newValueObject, generateIfNotFound: true);
								if (surrogate == null) throw new Exception("Object must be instantiated/registered with resolve");
								stateCache.SwitchUniqueIds(pointer.UniqueID, relativeUid);
								pointer.UniqueID = relativeUid;
								stateCache.AddSurrogateReference(surrogate, pointer, false);
							}
							else
							{
								//First lets see if this is same object
								var surrogate = (StateObjectSurrogate)pointer.Target;
								if (surrogate.Value == newValueObject)
								{
									//Nothing to do
								}
								else
								{
									stateCache.RemoveSurrogateReference(surrogate, pointer);
									surrogate = stateCache.GetSurrogateFor(newValueObject, generateIfNotFound: true);
									stateCache.AddSurrogateReference(surrogate, pointer, false);
								}
							}
						}
					}
					else if (isPointer)
					{
						var x = prop;
					}
					else if (newValue != null)
					{
						if (StateCache.IsAttachedChild(newValue))
						{

							//Antes no se tenia posible que un padre tuviera punteros
							// not supported  yet, future implementation maybe with a reference object.
							if (parentInstance != null)
							{
								if (!StateCache.IsChild(parentInstance.UniqueID, newValue.UniqueID))
								{
									if (typeof(ITopLevelStateObject).IsAssignableFrom(prop.PropertyType))
									{
										var relativeUid = StateCache.GetPointerUniqueId(uuidgen.GetRelativeUniqueID(parentInstance, propName));
										var pointer = (StateObjectPointer)stateCache.GetObject(relativeUid);
										if (pointer == null)
										{
											pointer = IocContainerImplWithUnity.Current.Resolve<StateObjectPointer>();
											//There was not a pointer object here, it means is not initialized
											pointer.Target = newValue;
											stateCache.SwitchUniqueIds(pointer.UniqueID, relativeUid);
											pointer.UniqueID = relativeUid;
										}
										else
										{
											pointer.Target = newValue;
										}
									}
								}
							}
						}
						else
						{
							isPropIStateObject = true;
						//if current UniqueID is null then we need to sync the viewmodel uniqueID
						//with its parent
							if (parentInstance != null)
							{
								var new_relativeUid = uuidgen.GetRelativeUniqueID(parentInstance, propName);
								var oldValue = stateCache.GetObject(new_relativeUid);
								if (oldValue == newValueObject)
								{
									//Nothing to do
								}
								else
								{
									if (oldValue != null)
										stateCache.RemoveObject(oldValue.UniqueID);
									if (typeof(IDependantViewModel).IsAssignableFrom(prop.PropertyType))
									{
										// StateCache.Current.SwitchUniqueIds(newValue.UniqueID,relativeUID);
										var old_Uid = newValue.UniqueID;
										newValue.UniqueID = new_relativeUid;
										if (StateCache.AllBranchesAttached(newValue))
											stateCache.PromoteObject(newValue);
										else
										{
											stateCache.SwitchUniqueIds(old_Uid, new_relativeUid);
										}
									}
									else if (typeof(ITopLevelStateObject).IsAssignableFrom(prop.PropertyType))
									{
										var relativeUid = StateCache.GetPointerUniqueId(uuidgen.GetRelativeUniqueID(parentInstance, propName));
										var pointer = (StateObjectPointer)stateCache.GetObject(relativeUid);
										if (pointer == null)
										{
											pointer = IocContainerImplWithUnity.Current.Resolve<StateObjectPointer>();
											//There was not a pointer object here, it means is not initialized
											pointer.Target = newValue;
											stateCache.SwitchUniqueIds(pointer.UniqueID, relativeUid);
											pointer.UniqueID = relativeUid;
										}
										else
										{
											pointer.Target = newValue;
										}
									}

								}
							}
						}
					}
					else
					{
						if (parentInstance != null)
						{
							if (!stateCache.ContainsSkip(parentInstance))
							{
								var bindinguid = uuidgen.GetRelativeUniqueID(parentInstance, prop.Name + StateCache.BindingKey);
								var bindingPointer = stateCache.GetObject(bindinguid) as StateObjectPointer;
								if (bindingPointer != null)
								{
									var bindingSurrogate = bindingPointer.Target as StateObjectSurrogate;
									if (bindingSurrogate != null)
									{
										var binding = bindingSurrogate.Value as StateCache.DataBinding;
										if (binding != null)
										{
											var dataSource = binding.DataSourceReference;
											if (dataSource is StateObjectSurrogate)
											{
												var surrogate = (StateObjectSurrogate)dataSource;
												var surrogateValue = surrogate.Value;
												var setter = SurrogatesDirectory.GetPropertySetter(surrogate.Value);
												setter(surrogateValue, binding.DataSourceProperty, newValueObject);
											}
											else
												throw new NotImplementedException();
										}
									}
								}
							}
						}
					}

					// Delta Tracking of modified property
					if (prop != null)
					{
						if (!stateCache.Tracker.IsDirtyModel(parentInstance))
						{
							TrackerHelper.MarkAsModified(parentInstance, propName, isPropIStateObject);
							// Not being implemented in case property is IStateObject, review sets before
						}
					}
				}
			}


			public IEnumerable<Type> GetRequiredInterfaces()
			{
				return Type.EmptyTypes;
			}

			public bool WillExecute
			{
				get { return true; }
			}

			#endregion

			#region Nested type: TypePropertiesCache

			private class TypePropertiesCache
			{
				private Dictionary<Type,
							Tuple<
								Dictionary<string, PropertyInfo>,
								Dictionary<string, PropertyInfo>>>
					gettersandsetter =
						new Dictionary<Type,
							Tuple<
							Dictionary<string, PropertyInfo>,
							Dictionary<string, PropertyInfo>>>();

				Dictionary<Type, Dictionary<string, bool>> isPointerTable = new Dictionary<Type, Dictionary<string, bool>>();

				public bool hasGetter(Type t, string getterName, out PropertyInfo prop)
				{
					Tuple<Dictionary<string, PropertyInfo>, Dictionary<string, PropertyInfo>> g_s;
					if (!gettersandsetter.TryGetValue(t, out g_s))
					{
						LoadTable(t);
						LoadPointerTable(t);
						gettersandsetter.TryGetValue(t, out g_s);
					}
					if (g_s != null)
					{
						return g_s.Item1.TryGetValue(getterName, out prop);
					}
					prop = null;
					return false;
				}

				public bool hasSetter(Type t, string setterName, out PropertyInfo prop)
				{
					Tuple<Dictionary<string, PropertyInfo>, Dictionary<string, PropertyInfo>> g_s;
					if (!gettersandsetter.TryGetValue(t, out g_s))
					{
						LoadTable(t);
						LoadPointerTable(t);
						gettersandsetter.TryGetValue(t, out g_s);
					}
					if (g_s != null)
					{
						return g_s.Item2.TryGetValue(setterName, out prop);
					}
					prop = null;
					return false;
				}

				public bool GetIsPointer(PropertyInfo property)
				{
					Type type = property.PropertyType;
					Dictionary<string, bool> dict = null;
					bool result = false;
					if (!isPointerTable.TryGetValue(type, out dict))
					{
						LoadPointerTable(type);
						dict = isPointerTable.ContainsKey(type) ? isPointerTable[type] : null;
					}
					if (dict != null)
						dict.TryGetValue(property.Name, out result);
					return result;
				}

				private void LoadTable(Type t)
				{
					lock (gettersandsetter)
					{
						if (gettersandsetter.ContainsKey(t))
						{
							return;
						}

						var tuple =
							new Tuple<Dictionary<string, PropertyInfo>, Dictionary<string, PropertyInfo>>(
								new Dictionary<string, PropertyInfo>(),
								new Dictionary<string, PropertyInfo>());

						foreach (PropertyInfo prop in t.GetProperties(BindingFlags.Public | BindingFlags.Instance))
						{
							var attr =
									(StateManagement)
									prop.GetCustomAttributes(typeof(StateManagement), true).FirstOrDefault();
							if (attr != null && !attr.RequiresStateManagement()) continue;
							RegisterProperty(prop, tuple);
						}
						gettersandsetter.Add(t, tuple);
					}
				}

				private void LoadPointerTable(Type t)
				{
					lock (isPointerTable)
					{
						if (isPointerTable.ContainsKey(t))
						{
							return;
						}

						foreach (PropertyInfo prop in t.GetProperties())
						{
							var attrPointer =
									(UpgradeHelpers.Helpers.Pointer)
									prop.GetCustomAttributes(typeof(UpgradeHelpers.Helpers.Pointer), true).FirstOrDefault();
							if (attrPointer != null)
							{
								Dictionary<string, bool> pointers;
								if (!isPointerTable.TryGetValue(t, out pointers))
								{
									isPointerTable[t] = pointers = new Dictionary<string, bool>();
								}
								pointers.Add(prop.Name, true);
								continue;
							}
						}
					}
				}

				private static void RegisterProperty(PropertyInfo prop,
													 Tuple
														 <Dictionary<string, PropertyInfo>,
														 Dictionary<string, PropertyInfo>> tuple)
				{
					if (prop.CanRead)
					{
						var sb = new StringBuilder();
						sb.Append("get_");
						sb.Append(prop.Name);
						tuple.Item1.Add(sb.ToString(), prop);
					}
					if (prop.CanWrite)
					{
						var sb = new StringBuilder();
						sb.Append("set_");
						sb.Append(prop.Name);
						tuple.Item2.Add(sb.ToString(), prop);
					}
				}
			}

			#endregion
		}

		#endregion
	}
}