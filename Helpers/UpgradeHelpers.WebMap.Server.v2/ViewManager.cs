﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Fasterflect;
using UpgradeHelpers.Events;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.WebMap.Server.Interfaces;

namespace UpgradeHelpers.WebMap.Server
{
	internal class TrackingVisitingState
	{
		private bool IsDirty(IStateObject obj)
		{
			return true;
		}
	}


	public class ViewManager : IViewManager
	{
		internal HashSet<string> DisposedView = new HashSet<string>();
		internal const string ViewManagerStateCacheUid = "@@ViewManager";
		internal ViewsState _state;



        public void SetInfoForDownload(string filename, string contentType)
        {
            System.Web.HttpContext.Current.Session["filenameForDownload"] = filename;
            System.Web.HttpContext.Current.Session["filenameForDownloadContentType"] = contentType;
        }

		public string CurrentCulture
		{
			get { return State.CurrentCulture; }
            set
            {
                State.CurrentCulture = value;
                TrackerHelper.MarkAsModified(State, "CurrentCulture", false);
            }
		}

	    private IEventAggregator _events;
	    public IEventAggregator Events
	    {
	        get { return _events ?? (_events = new EventAggregator(UniqueIDGenerator.Current, StateCache.Current)); }
	    }

		public DialogResult InteractionResult { get; set; }

		public string RequestedInput { get; set; }

		internal ViewsState State
		{
			get
			{
				if (_state == null)
				{
					_state = (ViewsState) StateCache.Current.GetObject(ViewsState.UniqueIdViewmanagerState);
					if (_state == null)
					{
						//There is no previous one
						_state = new ViewsState {UniqueID = ViewsState.UniqueIdViewmanagerState};
						StateCache.Current.AddNewObject(_state);
					}
				}
				return _state;
			}
		}

		public static ViewManager Instance
		{
			get
			{
				var vm = RequestTimeStorage.Current[ViewManagerStateCacheUid] as ViewManager;
				if (vm == null)
				{
					RequestTimeStorage.Current[ViewManagerStateCacheUid] = (vm = new ViewManager());
					vm.StartRequest();
				}
				return vm;
			}
		}

		#region IPromise Members

		public IPromise Then(PromisedCode code)
		{
			ContinuationInfo continuation = PromiseUtils.BuildContinuationInfo(code);
			//Pushing a new continuation
			State.RegisterContinuation(continuation);
			return this;
		}

		public IPromise ThenWithLogic(PromisedCodeWithLogic code)
		{
			ContinuationInfo continuation = PromiseUtils.BuildContinuationInfo(code);
			//Pushing a new continuation
			State.RegisterContinuation(continuation);
			return this;
		}

	    #endregion

		#region IViewManager Members

		public IPromise NavigateToView(ILogicWithViewModel<IViewModel> view, bool isModal = false)
		{
			State.NavigateToView(view, isModal);
			return new ShowDialogPromise(this, view.ViewModel.UniqueID);
		}

		public void HideView(ILogicWithViewModel<IViewModel> view)
		{
			view.ViewModel.Visible = false;
		}

		public void DisposeView(ILogicWithViewModel<IViewModel> view)
		{
			// lets fire the closing event
			var types = new Type[2] { (typeof(bool)).MakeByRefType(), typeof(CloseReason) };
			MethodInfo method = view.GetType().Method("Closing", types);
			bool cancel = false;
			var parameters = new object[2] { cancel, CloseReason.UserClosing };
			if (method != null)
			{
				method.Invoke(view, parameters);
			}
			// lets fire the form closing event
			method = view.GetType().Method("FormClosing", types);
			if (method != null)
			{
				method.Invoke(view, parameters);
			}
			if (!(bool)parameters[0])
			{
				// lets fired the closed events
				// lets fire the Closed event
				method = view.GetType().Method("Closed", types);
				if (method != null)
				{
					method.Invoke(view, parameters);
				}
				// lets fire the Closed event
				method = view.GetType().Method("FormClosed", types);
				if (method != null)
				{
					method.Invoke(view, parameters);
				}

				// lets close the form
				// lets close the form
				if (!DisposedView.Contains(view.ViewModel.UniqueID))
				{
					DisposedView.Add(view.ViewModel.UniqueID);
					ResumeAfterShowDialog(view);
					State.DisposeView(view);
				}

				DisposedView.Remove(view.ViewModel.UniqueID);
			}

		}

        /// <summary>
        /// Sets the focus to the desired item
        /// </summary>
        /// <param name="item">The item that will be focused</param>
        public void SetCurrent(IDependantViewModel item)
        {
            var fieldid = StateCache.Current.GetSubViewId(item);
            State.SetCurrentFocusedControl(fieldid);
        }

        /// <summary>
        /// Sets the focus to the desired view
        /// </summary>
        /// <param name="item">The item that will be focused</param>
        public void SetCurrentView(IViewModel item)
        {
            var fieldid = item.UniqueID;
            State.SetCurrentFocusedControl(fieldid);
        }

		public IPromise ShowMessage(string message, string caption = "", BoxButtons buttons = BoxButtons.OK, BoxIcons icons = BoxIcons.None,
			string promptMessage = "", bool inputRequest = false)
		{
			///TODO index must always put all messages, all other constructs just send the last messages.
			string messageId = State.AddMessage(message, caption, buttons, icons, promptMessage, inputRequest);
			return new MessagePromise(this, messageId);
		}

		public IEnumerable<IViewModel> LoadedViews
		{
			get
			{
				foreach (IViewModel view in State.LoadedViews.Select(loadedView => (IViewModel) StateCache.Current.GetObject(loadedView.UniqueID)))
				{
					Debug.Assert(view != null, "Abnormal state. Missing view model");
					yield return view;
				}
			}
		}

		#endregion

		public void DisposeView(IViewModel view)
		{
			throw new NotImplementedException();
		}

		public IPromise NavigateToView(IViewModel view)
		{
			throw new NotImplementedException();
		}


		/// <summary>
		///     Must be invoked before starting a request to init tracking of view models and other
		///     structures
		/// </summary>
		public void StartRequest()
		{
			State.Start();
		}


		/// <summary>
		///     Must be invoked after the request has been executed to free any pending resources
		///     structures
		/// </summary>
		public ViewsStateDelta EndRequest()
		{
			return State.End();
		}

		/// <summary>
		/// Tries to resume the execution once a view is closed by checking if top delegate
		/// matches the given unique id.
		/// </summary>
		/// <param name="uniqueId">the unique id of closed view</param>
		private void ResumeAfterShowDialog(ILogicWithViewModel<IViewModel> logic)
		{
			if (State.Delegates.Count > 0)
			{

				var topContinuationInfo = State.Delegates[0];
				if (topContinuationInfo.Modal_UniqueID.Equals(logic.ViewModel.UniqueID))
				{
					State.RemoveTopContinuation(topContinuationInfo);
						topContinuationInfo.Execute(logic);
					}
			}
		}

		void IViewManager.ResumePendingOperation(string dialogResult, string requestedInput)
		{
			var resultValue = (DialogResult)Enum.Parse(typeof(DialogResult), dialogResult, ignoreCase: true);
			InteractionResult = resultValue;
			RequestedInput = requestedInput;
			if (State.Delegates.Count > 0)
			{
                ContinuationInfo topContinuationInfo=null;
                topContinuationInfo = State.Delegates[0];
                State.RemoveTopContinuation(topContinuationInfo);
					topContinuationInfo.Execute();
				}
		else
			{
				Debug.WriteLine("WARNING: trying to resume continuation, but none was available");
			}
		}

	    public IDataPageableViewModel GetDataPageableViewModel(string controlId)
	    {
	        var control = StateCache.Current.GetObject(controlId);
		    return control as IDataPageableViewModel;
	    }


        /// <summary>
        /// Get the ViewModel associated with the Unique ID
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public IStateObject GetParentViewModel(Func<string> getUniqueIdentifier)
        {
            return StateCache.Current.GetObject(getUniqueIdentifier());
        }
        


		public IStateObject[] GetCollectionPage(string collectionUid, int take, int skip, out int total)
		{
			var dynamicObj = (dynamic)StateCache.Current.GetObject(collectionUid);
			dynamic obj = dynamicObj.Items;
			total = obj.Count;
			var objs = new List<IStateObject>();

			for (int i = skip; i < skip + take && i < total; i++)
			{
				objs.Add(obj[i]);
			}
			return objs.ToArray();
		}

		public void RegisterDataProvider(IViewModel viewModel, IStateObject viewObject, DataSourceProviderDelegate dataSourceProvider)
		{

			var providerId = UniqueIDGenerator.Current.GetRelativeUniqueID(viewObject, "%dataSourceProvider");
			var existingProvider = StateCache.Current.GetObject(providerId);
			//if (existingProvider != null)
			//	StateCache.Current.RemoveObject(providerId);
			var declaringType = dataSourceProvider.Target.GetType();
			var methodName = dataSourceProvider.Method.Name;
			var dataProviderContinuation = new ContinuationInfo
			{
				DeclaringType = declaringType.AssemblyQualifiedName,
				IStateObject_UniqueID = viewModel.UniqueID,
				MethodName = methodName,
				UniqueID = providerId
			};
			if (existingProvider == null)
				StateCache.Current.AddNewObject(dataProviderContinuation);
		}

		public object GetDataFromProvider(IStateObject viewObject)
		{
			var providerId = UniqueIDGenerator.Current.GetRelativeUniqueID(viewObject, "%dataSourceProvider");
			var continuationInfo = StateCache.Current.GetObject(providerId);
			if (continuationInfo != null)
			{
				return ((ContinuationInfo)continuationInfo).Execute();
			}
			return null;
		}

		public bool IsInStateManagementPhase
		{
			get
			{
				return Events.IsSuspended();	
			}
		}


		#region Nested type: MessagePromise

		private class MessagePromise : IPromise
		{
			private readonly string _currentMessage;
			private readonly ViewManager _parent;

			public MessagePromise(ViewManager parent, string messageId)
			{
				_parent = parent;
				_currentMessage = messageId;
			}

			#region IPromise Members

			public IPromise Then(PromisedCode code)
			{
				ContinuationInfo continuation = PromiseUtils.BuildContinuationInfo(code);
				_parent.State.RegisterContinuation(continuation);
				Message msg = _parent.State.Messages.Find(x => x.UniqueID == _currentMessage);
				msg.Continuation_UniqueID = continuation.UniqueID;
				return _parent;
			}

			public IPromise ThenWithLogic(PromisedCodeWithLogic code)
			{
				ContinuationInfo continuation = PromiseUtils.BuildContinuationInfo(code);
				_parent.State.RegisterContinuation(continuation);
				Message msg = _parent.State.Messages.Find(x => x.UniqueID == _currentMessage);
				msg.Continuation_UniqueID = continuation.UniqueID;
				return _parent;
			}

		    #endregion
		}

		/// <summary>
		/// Defines a promise object that handles code just after a showing a view as modal.
		/// </summary>
		private class ShowDialogPromise : IPromise
		{
			protected ViewManager _parent;
			protected string _uniqueId;

			public ShowDialogPromise(ViewManager parent, string uniqueId)
			{
				_parent = parent;
				_uniqueId = uniqueId;
			}

			public virtual IPromise Then(PromisedCode code)
			{
				ContinuationInfo continuation = PromiseUtils.BuildContinuationInfo(code, _uniqueId);
				_parent.State.RegisterContinuation(continuation);
				return _parent;
			}

			public IPromise ThenWithLogic(PromisedCodeWithLogic code)
			{
				ContinuationInfo continuation = PromiseUtils.BuildContinuationInfo(code, _uniqueId);
				_parent.State.RegisterContinuation(continuation);
				return _parent;
			}
		}

		#endregion

		#region Nested type: PromiseUtils

		private static class PromiseUtils
		{
			internal static ContinuationInfo BuildContinuationInfo(Delegate code, string continuationid = null, object target = null)
			{

                

				object instance = target ?? code.Target;
				if (instance is ILogicWithViewModel<IStateObject>)
				{
					var instanceTyped = (ILogicWithViewModel<IStateObject>)instance;
					IStateObject viewModel = instanceTyped.ViewModel;
					Type declaringType = code.Method.DeclaringType;
					string methodName = code.Method.Name;
					//Pushing a new continuation
					Debug.Assert(declaringType != null, "declaringType != null");
					var res = new ContinuationInfo
						{
							DeclaringType = declaringType.AssemblyQualifiedName,
							IStateObject_UniqueID = viewModel.UniqueID,
                            TargetType = instanceTyped.GetType().AssemblyQualifiedName,
							MethodName = methodName,
							Modal_UniqueID = continuationid ?? viewModel.UniqueID,
							UniqueID = UniqueIDGenerator.Current.GetUniqueID()
						};
					return res;
				}
                if (instance is ILogicClass) {
                    var instanceTyped = (ILogicClass)instance;
                    IStateObject viewModel = instanceTyped;
                    Type declaringType = code.Method.DeclaringType;
                    string methodName = code.Method.Name;
                    //Pushing a new continuation
                    Debug.Assert(declaringType != null, "declaringType != null");
                    var res = new ContinuationInfo
                    {
                        DeclaringType = declaringType.AssemblyQualifiedName,
                        IStateObject_UniqueID = viewModel.UniqueID,
							MethodName = methodName,
							Modal_UniqueID = continuationid ?? viewModel.UniqueID,
							UniqueID = UniqueIDGenerator.Current.GetUniqueID()
						};
					return res;
				}
				else
				{
                    var streamForFields = new System.IO.MemoryStream();
                    var writer = new System.IO.BinaryWriter(streamForFields);
                    var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
					var codePromiseType = instance.GetType();
					Type declaringType = codePromiseType;
					IStateObject viewModel = null;
					var fieldList = codePromiseType.Fields(BindingFlags.Public | BindingFlags.Instance);
					var logicObjectType = codePromiseType.DeclaringType;
                    bool thisWasFound = false;
					foreach(var field in fieldList)
					{
						Debug.WriteLine("Field" + field.Name);
						var objValue = field.GetValue(instance);

						if (logicObjectType == field.FieldType && field.Name.EndsWith("this"))
						{
							if (objValue is ILogicWithViewModel<IStateObject>)
							{
								//We have the field for the parent class
								var instanceTyped = (ILogicWithViewModel<IStateObject>)objValue;
								viewModel = instanceTyped.ViewModel;
                                thisWasFound = true;
                                break;
                            }
                            else if (objValue is ILogicClass)
                            {
                                //We have the field for the parent class
                                viewModel  = (ILogicClass)objValue;
                                thisWasFound = true;
								break;
							}
							else
							{
								throw new PromisesOnlySupportDelegatesFromClassesImplementingILogic();
							}
						}
                        else
                        {
                            if (!field.Name.Contains("locals"))
                            {
                                //For IStateObject only the UniqueID is needed for the other we would use the ISerialize
                                if (typeof(IStateObject).IsAssignableFrom(field.FieldType))
                                {
                                    //If this field is not the this then we are goint to save its value
                                    writer.Write(field.Name);
                                    writer.Write(((IStateObject)field.GetValue(instance)).UniqueID);
                                }
                                else
                                {
                                    if (field.FieldType.IsSerializable)
                                    {
                                        writer.Write(field.Name);
                                        formatter.Serialize(streamForFields, field.GetValue(instance));
                                    }
                                }
                            }


                        }
					}

                    if (!thisWasFound)
                    {

                        foreach (var field in fieldList)
                        {
                            var fieldName = field.Name;
                            if (fieldName.Contains("<>") && fieldName.Contains("locals"))
                            {
                                var fieldOfLocals = field.FieldType.Fields(BindingFlags.Public | BindingFlags.Instance);
                                foreach (var fieldOfLocalsField in fieldOfLocals)
                                {
                                    var valueForLocalsDisplayClass = field.GetValue(instance);
                                    if (logicObjectType == fieldOfLocalsField.FieldType && fieldOfLocalsField.Name.EndsWith("this"))
                                    {

                                        
                                        var objValue = fieldOfLocalsField.GetValue(valueForLocalsDisplayClass);
                                        var instanceTyped = (ILogicWithViewModel<IStateObject>)objValue;
                                        viewModel = instanceTyped.ViewModel;
                                        thisWasFound = true;
                                        //break;
                                    }
                                    else
                                    {
                                            //If this field is not the this then we are goint to save its value
                                            //For IStateObject only the UniqueID is needed for the other we would use the ISerialize
                                            if (typeof(IStateObject).IsAssignableFrom(fieldOfLocalsField.FieldType))
                                            {
                                                writer.Write(field.Name + "." + fieldOfLocalsField.Name);
                                                writer.Write(((IStateObject)fieldOfLocalsField.GetValue(valueForLocalsDisplayClass)).UniqueID);
                                            }
                                            else
                                            {
                                                if (fieldOfLocalsField.FieldType.IsSerializable)
                                                {
                                                    writer.Write(field.Name + "." + fieldOfLocalsField.Name);
                                                    formatter.Serialize(streamForFields, fieldOfLocalsField.GetValue(valueForLocalsDisplayClass));
                                                }
                                            }
                                        
                                    }
                                }
                                if (thisWasFound) break;
                            }
					}
                    }

					var codePromiseTypeName = codePromiseType.Name;
					if (codePromiseTypeName.Contains("DisplayClass"))
					{
						string methodName = code.Method.Name;
						
						var res = new ContinuationInfo
						{
							DeclaringType = declaringType.AssemblyQualifiedName,
							IStateObject_UniqueID = viewModel.UniqueID,
							MethodName = methodName,
							Modal_UniqueID = continuationid ?? viewModel.UniqueID,
							UniqueID = UniqueIDGenerator.Current.GetUniqueID(),
                            ContinuationFields = System.Convert.ToBase64String(streamForFields.ToArray())
						};
						return res;

					}
				}
				throw new PromisesOnlySupportDelegatesFromClassesImplementingILogic();
			}
			
		}

		#endregion

       
	}
}