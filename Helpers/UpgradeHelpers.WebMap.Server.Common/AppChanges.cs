﻿using System;
using System.Web;
using System.Web.Mvc;

namespace UpgradeHelpers.WebMap.Server
{
	public class AppChanges : JsonResult
	{
		public static CalculateAppChanges AppChangesDelegate = null;

		public PiggyBackResult PiggyBackResult { get; set; }

		public override void ExecuteResult(ControllerContext context)
		{
			// verify we have a contrex
			if (context == null)
				throw new ArgumentNullException("context");

			// get the current http context response
			HttpResponseBase response = context.HttpContext.Response;
			// set content type of response
			response.ContentType = !String.IsNullOrEmpty(ContentType) ? ContentType : "application/json";
			// set content encoding of response
			if (ContentEncoding != null)
				response.ContentEncoding = ContentEncoding;
			if (AppChangesDelegate == null)
				throw new AppChangesResultDelegateHasNotBeenSet();

			response.Write(AppChangesDelegate(PiggyBackResult));
		}
	}
}