﻿using System.Web;

namespace UpgradeHelpers.WebMap.Server
{
	public delegate object CalculateAppChanges(PiggyBackResult piggyBackingResult);

	public delegate object PiggyBackResult(object appChanges);
}