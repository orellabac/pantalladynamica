﻿using System;

namespace UpgradeHelpers.WebMap.Server
{
	public class AppChangesResultDelegateHasNotBeenSet : Exception
	{
		public AppChangesResultDelegateHasNotBeenSet() : base(message: "The static AppChangesDelegate must be set on application init")
		{
		}
	}
}