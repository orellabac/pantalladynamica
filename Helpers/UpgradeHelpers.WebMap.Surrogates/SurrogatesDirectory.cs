﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting;
using UpgradeHelpers.WebMap.Surrogates;

namespace UpgradeHelpers.WebMap
{
	/// <summary>
	/// Centralized directory to register "Surrogates"
	/// </summary>
	public static class SurrogatesDirectory
	{
		static readonly Dictionary<Type, SurrogatesInfo> TypeToSurrogate = new Dictionary<Type, SurrogatesInfo>();
		static readonly Dictionary<string, SurrogatesInfo> SignatureToSurrogate = new Dictionary<string, SurrogatesInfo>();
		public const int SignatureMaxlenght = 8;


		/// <summary>
		/// DEFAULT action for applying binding changes
		/// First parameter is the target ViewModel uniqueID, second parameter is target ViewModel property name, and third parameter is new value
		/// </summary>
		internal static Action<string, string, object> defaultApplyBindingChangedAction;

		internal static Func<object, string> GetUniqueIDForSurrogateDelegate;

		internal static Func<string, object> RestoreSurrogateDelegate;

		public static object RestoreSurrogate(string uniqueId)
		{
			return RestoreSurrogateDelegate(uniqueId);
		}

		public static string GetUniqueIdForSurrogate(object obj)
		{
			return GetUniqueIDForSurrogateDelegate(obj);
		}


		public static void RegisterSurrogate(
				string signature,
				Type supportedType,
				Func<object, object> serialize,
				Func<BinaryReader, object> deSerialize,
				Action<object, Action<bool>> applyBindingAction = null,
				Func<object, string, object> PropertyGetter = null,
				Action<object, string, object> PropertySetter = null
		)
		{
			if (string.IsNullOrWhiteSpace(signature) || signature.Length > SignatureMaxlenght)
			{
				throw new ArgumentException(string.Format("Signature name must be a non null of maximum of {0} characters", SignatureMaxlenght));
			}
			signature = signature.PadLeft(SignatureMaxlenght);
			var surrogateInfo = new SurrogatesInfo()
			{
				Serialize = serialize,
				DeSerialize = deSerialize,
				SupportedType = supportedType,
				ApplyBindingHandlers = applyBindingAction,
				PropertyGetter = PropertyGetter,
				PropertySetter = PropertySetter
			};
			TypeToSurrogate.Add(supportedType, surrogateInfo);
			SignatureToSurrogate.Add(signature, surrogateInfo);
			if (applyBindingAction != null || PropertyGetter != null || PropertySetter != null)
			{
				if (!(applyBindingAction != null && PropertyGetter != null && PropertySetter != null))
				{
					throw new ArgumentException("Invalid surrogate registration. If an ApplyBindingAction is given, then a PropertyGetter and a PropertySetter must be given too");
				}
			}
		}

		public static bool IsSurrogateRegistered(Type supportedType)
		{
			var isStateObject = (typeof (Interfaces.IStateObject).IsAssignableFrom(supportedType));
			var isDefaults = (typeof(Interfaces.IDefaults).IsAssignableFrom(supportedType));
			if (supportedType.IsValueType || isStateObject || isDefaults)
				return false;

			if (TypeToSurrogate.ContainsKey(supportedType)) return true;
			//Generate new Surrogate 
			//Support ISerializable? Register a new surrogate for this type using a new instance of SurrogateByISerializable(type);
			if (typeof (System.Runtime.Serialization.ISerializable).IsAssignableFrom(supportedType))
			{
				var signature = GenerateNewSurrogateFromType(supportedType);
				var newSurrogateByISerializable = new SurrogateByISerializable(supportedType, signature);
				RegisterSurrogate(signature, supportedType, newSurrogateByISerializable.SerializeAction, newSurrogateByISerializable.DeserializeAction);
				return true;
			}
			else
			{
				//Register a new surrogate for this type using a new instance of SurrogateByNewtonsoft(type);
				//var signature = GenerateNewSurrogateFromType(supportedType);
				//var newNewTon = new SurrogateByNewtonsoft(supportedType, signature);
				//RegisterSurrogate(GenerateNewSurrogateFromType(supportedType), supportedType, newNewTon.SerializeAction, newNewTon.DeserializeAction);
				return false;
			}
		}

		private static string GenerateNewSurrogateFromType(Type supportedType)
		{
			var signature = supportedType.GetHashCode().ToString("X");
			if (signature.Length > SignatureMaxlenght)
			{
				throw new ArgumentException(string.Format("Invalid signature length. Max lenght is {0}", SignatureMaxlenght));
			}
			signature = signature.PadRight(SignatureMaxlenght);
			return signature;
		}


		public static object CreateInstanceFor(Type type)
		{
			return Activator.CreateInstance(type, true);
		}

		public static object ObjectToRaw(object obj)
		{
			SurrogatesInfo info = null;
			if (obj != null && TypeToSurrogate.TryGetValue(obj.GetType(), out info))
			{
				return info.Serialize(obj);
			}
			throw new NotSupportedException();
		}

		/// <summary>
		/// Validates that the string that will be used as a signature follows the expected format. Adjust length if necessary
		/// </summary>
		/// <param name="signature"></param>
		/// <returns></returns>
		public static string ValidSignature(string signature)
		{
			if (signature.Length > SignatureMaxlenght)
			{
				throw new ArgumentException(string.Format("Invalid signature length. Max lenght is {0}", SignatureMaxlenght));
			}
			signature = signature.PadRight(SignatureMaxlenght);
			return signature;
		}

		public static object RawToObject(object raw)
		{
			byte[] rawBytes = (byte[])raw;
			var ms = new MemoryStream(rawBytes);
			var reader = new BinaryReader(ms);
			var signature = reader.ReadString();
			SurrogatesInfo info = null;

			if (SignatureToSurrogate.TryGetValue(signature, out info))
			{
				return info.DeSerialize(reader);
			}
			throw new NotSupportedException();
		}



		public static Func<object, string, object> GetPropertyGetter(object obj)
		{
			Debug.Assert(obj != null);
			SurrogatesInfo info = null;
			if (TypeToSurrogate.TryGetValue(obj.GetType(), out info))
			{
				return info.PropertyGetter;
			}
			return null;
		}

		public static Action<object, string, object> GetPropertySetter(object obj)
		{
			SurrogatesInfo info = null;
			Debug.Assert(obj != null);
			if (TypeToSurrogate.TryGetValue(obj.GetType(), out info))
			{
				return info.PropertySetter;
			}
			return null;
		}


		public static void RegisterBinding(object ds, string dataSourceProperty, string viewModelUniqueID, string viewModelProperty, bool firstTime = false)
		{
			if (ds == null)
				throw new ArgumentException("The object for which a binding will be register cannot be null");
			SurrogatesInfo info = null;
			if (TypeToSurrogate.TryGetValue(ds.GetType(), out info))
			{
				info.RegisterBinding(ds, dataSourceProperty, viewModelUniqueID, viewModelProperty, firstTime);
				return;
			}
			throw new InvalidOperationException(string.Format("There is no surrogate information for objects of type {0}", ds.GetType()));

		}

		public static void ApplyBindingHandlers(object surrogateValue, Action<bool> bindingAction)
		{
			SurrogatesInfo info = null;

			if (TypeToSurrogate.TryGetValue(surrogateValue.GetType(), out info))
			{
				Debug.Assert(info.ApplyBindingHandlers != null);
				if (info.ApplyBindingHandlers != null)
					info.ApplyBindingHandlers(surrogateValue, bindingAction);
				return;
			}
		}


		public static bool SupportsBinding(object surrogateValue)
		{
			SurrogatesInfo info = null;

			if (TypeToSurrogate.TryGetValue(surrogateValue.GetType(), out info))
			{
				return info.ApplyBindingHandlers != null;
			}
			return false;
		}
	}
}