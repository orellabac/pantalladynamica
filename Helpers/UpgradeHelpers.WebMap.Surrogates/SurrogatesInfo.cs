﻿using System;
using System.IO;

namespace UpgradeHelpers.WebMap
{
	/// <summary>
	/// Stores information necessary for "Surrogate" functionality in WebMap
	/// </summary>
	internal class SurrogatesInfo
	{
		public Type SupportedType { get; set; }
		public Action<object> InstantionDelegate { get; set; }
		public Func<object, object> Serialize { get; set; }
		public Func<BinaryReader, object> DeSerialize { get; set; }
		public Action<object, string, string, string, bool> RegisterBinding { get; set; }
		public Func<object> CreateInstanceFor { get; set; }

		public Action<object, Action<bool>> ApplyBindingHandlers { get; set; }

		public Func<object, string, object> PropertyGetter { get; set; }

		public Action<object, string, object> PropertySetter { get; set; }
	}
}