﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml.Serialization;

namespace UpgradeHelpers.WebMap.Surrogates
{
	internal class SurrogateByISerializable
	{
		private Type _oType;
		private readonly string _signatureISerializable;
		internal SurrogateByISerializable(Type T, string signature)
		{
			_oType = T;
			_signatureISerializable = signature;
		}

		internal object SerializeAction(object arg)
		{
			var serializableArg = arg as ISerializable;
			if (serializableArg != null)
			{
				var ms = new MemoryStream();
				var binaryWriter = new BinaryWriter(ms);
				//Signature
				binaryWriter.Write(_signatureISerializable);
				Debug.Assert(_signatureISerializable.Length == SurrogatesDirectory.SignatureMaxlenght);
				//ISerializable serialized
				var msIserializable = new MemoryStream();
				var bFormat = new BinaryFormatter();
				bFormat.Serialize(msIserializable, serializableArg);
				binaryWriter.Write(msIserializable.ToArray());
				return ms.ToArray();
			}
			return null;
		}

		internal object DeserializeAction(System.IO.BinaryReader binaryReader)
		{
			var bFormat = new BinaryFormatter();
			var serializableObj = (ISerializable)bFormat.Deserialize(binaryReader.BaseStream);
			return serializableObj;
		}
	}
}
