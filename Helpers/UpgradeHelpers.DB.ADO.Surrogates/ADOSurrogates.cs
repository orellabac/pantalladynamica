﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using UpgradeHelpers.DB.ADO.Events;
using UpgradeHelpers.WebMap;

namespace UpgradeHelpers.DB.ADO.Surrogates
{
	public class ADOSurrogates : ISurrogateRegistration
	{
		public void Register()
		{
			RegisterSurrogateForAdoRecordSetHelper();
			RegisterSurrogateForDataset();
		}

		private static void RegisterSurrogateForDataset()
		{
			string signatureDataSet = SurrogatesDirectory.ValidSignature("DATASET");
			SurrogatesDirectory.RegisterSurrogate(signatureDataSet, typeof(DataSet),
												  (obj) =>
												  {
													  var ds = obj as DataSet;
													  var ms = new MemoryStream();
													  var textWriter = new StreamWriter(ms);
													  textWriter.Write(signatureDataSet);
													  textWriter.Write(ds.GetXml());
													  textWriter.Flush();
													  return ms.ToArray();
												  },
												  (binaryReader) =>
												  {
													  var ds = new DataSet();
													  ds.ReadXml(binaryReader.BaseStream);
													  return ds;
												  }, null);
		}

		private static void RegisterSurrogateForAdoRecordSetHelper()
		{
			string signatureAdoRecordSetHelper = SurrogatesDirectory.ValidSignature("ADORS");
			SurrogatesDirectory.RegisterSurrogate(signatureAdoRecordSetHelper, typeof(ADORecordSetHelper),
												  (obj) =>
												  {
													  var rs = obj as ADORecordSetHelper;
													  string uniqueIDConnection = SurrogatesDirectory.GetUniqueIdForSurrogate(rs.ActiveConnection);
													  var ms = new MemoryStream();
													  var binaryWriter = new BinaryWriter(ms);
													  //Signature
													  binaryWriter.Write(signatureAdoRecordSetHelper);
													  Debug.Assert(signatureAdoRecordSetHelper.Length == SurrogatesDirectory.SignatureMaxlenght);

													  binaryWriter.Write(uniqueIDConnection);
													  //ADORecordSet serialized
													  var ms_adorecordset = new MemoryStream();
													  XmlSerializer SerializerObj = new XmlSerializer(typeof(ADORecordSetHelper));
													  BinaryFormatter bFormat = new BinaryFormatter();
													  bFormat.Serialize(ms_adorecordset, rs);
													  binaryWriter.Write(ms_adorecordset.ToArray());
													  return ms.ToArray();
												  },
												  (binaryReader) =>
												  {

													  var uniqueIDConnection = binaryReader.ReadString();
													  var connection = SurrogatesDirectory.RestoreSurrogate(uniqueIDConnection) as DbConnection;
													  BinaryFormatter bFormat = new BinaryFormatter();
													  ADORecordSetHelper rs = (ADORecordSetHelper)bFormat.Deserialize(binaryReader.BaseStream);
													  rs.ActiveConnection = connection;
													  return rs;
												  },
												  (ds, applyAction) =>
												  {
													  var rs = ds as ADORecordSetHelper;
													  if (rs != null)
													  {
														  MoveEventHandler onWillMove = null;
														  MoveCompleteEventHandler onMoveComplete = null;
														  //Events.After
														  EventHandler onAfterQuery = null;
														  if (onWillMove == null)
														  {
															  onWillMove = (sender, args) =>
															  {
																  var theRS = sender as ADORecordSetHelper;
																  if (theRS != null)
																  {
																	  if (theRS.CurrentRow != null)
																	  {
																		  applyAction(false);
																	  }
																  }
															  };
														  }

														  if (onMoveComplete == null)
														  {
															  onMoveComplete = (sender, args) => applyAction(true);
														  }
														  if (onAfterQuery == null)
														  {
															  onAfterQuery =
																  (sender, args) =>
																  applyAction(true);
														  }
														  rs.WillMove -= onWillMove;
														  rs.WillMove += onWillMove;
														  rs.MoveComplete -= onMoveComplete;
														  rs.MoveComplete += onMoveComplete;
														  rs.AfterQuery -= onAfterQuery;
														  rs.AfterQuery += onAfterQuery;
														  return;
													  }
													  throw new ArgumentException(
														  "SurrogateRegisterBinding object was expected to by of UpgradeHelpers.DB.ADO.ADORecordSetHelper type");
												  },
												  (ds, propertyName) =>
												  {
													  var rs = ds as ADORecordSetHelper;
													  if (rs != null)
													  {
														  propertyName = propertyName.Replace("Table.", "");
														  if (rs.Opened)
															  return rs[propertyName];
														  else
															  return null;
													  }
													  throw new ArgumentException(
														  "SurrogateRegisterBinding object was expected to by of UpgradeHelpers.DB.ADO.ADORecordSetHelper type");
												  },
												  (ds, propertyName, value) =>
												  {
													  var rs = ds as ADORecordSetHelper;
													  if (rs != null)
													  {
														  propertyName = propertyName.Replace("Table.", "");
														  var original_val = rs[propertyName];
														  if (!original_val.Equals(value))
														  {
															  rs[propertyName] = value;
														  }
														  return;
													  }
													  throw new ArgumentException(
														  "SurrogateRegisterBinding object was expected to by of UpgradeHelpers.DB.ADO.ADORecordSetHelper type");
												  }
				);
		}
	}
}