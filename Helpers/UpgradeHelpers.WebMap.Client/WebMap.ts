/// <reference path="kendo.all.d.ts" />
/// <reference path="jquery.blockUI.d.ts" />
/// <reference path="jquery.d.ts" />
/// <reference path="WebMap_Interfaces.ts" />
/// <reference path="WebMap_Helpers.ts" />
/// <reference path="jquery.caret.d.ts" />

interface Window {
    app: any;
}

declare module kendo.data {
    var binders: any;

}
module WebMap.Client {


    /// Custom bindings



    kendo.data.binders.checkState = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);

            var that = this;
            (<HTMLInputElement>that.element).addEventListener('click',() => {
                that.change();
            });
        },
        refresh: function () {
            var binding = this.bindings["checkState"];
            var vmelement = binding.source[binding.path.substr(0, binding.path.indexOf('.'))];
            if (vmelement !== undefined) {
                var currentState = vmelement.get('CheckState');
                if (currentState != 2) {
                    (this.element).checked = currentState == 1;
                } else {
                    (this.element).indeterminate = true;
                }
            }
        },
        change: function () {
            var value = this.element.checked;
            var binding = this.bindings["checkState"];
            var vmelement = binding.source[binding.path.substr(0, binding.path.indexOf('.'))];
            vmelement.set('Checked', value);
            vmelement.set('CheckState', value ? 1 : 0);
            this.bindings['checkState'].set(value ? 1 : 0);
        }

    });

    (kendo.data.binders).customChecked = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (kendo.data.Binder.fn).init.call(this, element, bindings, options);

            var that = this;
            (that.element).addEventListener('change', function () {
                that.change();
            });
        },
        refresh: function () {
            var binding = this.bindings["customChecked"];
            (this.element).checked = binding.get();
        },
        change: function () {
            var value = this.element.checked;
            var binding = this.bindings["customChecked"];
            if (binding.source.get(binding.path) !== undefined) {
                binding.set(true);
            }


            var source = binding.source;
            var relatedRadios = document.querySelectorAll('div[id="' + source.UniqueID + '"] input[name="' + this.element.name + '"]');
            for (var i in relatedRadios) {
                var e = <any>relatedRadios[i];
                if (e != this.element &&
                    e.kendoBindingTarget &&
                    e.kendoBindingTarget.source) {
                    if (e.kendoBindingTarget.source[e.id]) {
                        e.kendoBindingTarget.source[e.id].set('Checked', false);
                    } else {
                        // Check for control array updates
                        var controlArrayRefRegex = /^_(.+)_([0-9]+)/;
                        if (controlArrayRefRegex.test(e.id)) {
                            e.kendoBindingTarget.source.set(e.id.replace(controlArrayRefRegex, "$1[$2]") + ".Checked", false);
                        }
                    }
                }
            }
        }
    });

    kendo.data.binders.menuEnabled = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
        },
        refresh: function () {
            var binding = this.bindings["menuEnabled"];
            var parentMenu = $(this.element).parents('ul[data-role="menu"]').data('kendoMenu');
            parentMenu.enable(this.element, binding.get());
        }
    });

    kendo.data.binders.widget.dateTimePickerValue = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);

            var that = this;
            this.element.bind("change",
                function () {
                    that.change();
                });
        },
        refresh: function () {
            var binding = this.bindings["dateTimePickerValue"];
            var value = binding.get();
            if (typeof (value) === 'string') {
                value = kendo.parseDate(value);
            }
            this.element.value(value);
        },
        change: function () {
            var value = this.element.value();
            var binding = this.bindings["dateTimePickerValue"];

            binding.set(value);
        }

    });

    kendo.data.binders.buttonText = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
        },
        refresh: function () {
            var binding = this.bindings["buttonText"];
            var spans = $(this.element).find("span");
            if (!spans || !spans.length)
                $(this.element).html(binding.get());
            else {
                $(spans[0]).html(binding.get());
            }
        }
    });

    kendo.data.binders.widget.DateTimeMinDate = kendo.data.Binder.extend({
        refresh: function (widget) {
            var binding = this.bindings["DateTimeMinDate"];
            var value = binding.get();
            if (value != null) {
                var dataRole = this.element.element.attr('data-role');
                if (dataRole === "datetimepicker")
                    this.element.element.data("kendoDateTimePicker").min(kendo.parseDate(value));
                else if (dataRole === "datepicker")
                    this.element.element.data("kendoDatePicker").min(kendo.parseDate(value));
            }
        }
    });

    kendo.data.binders.timerTick = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
        },
        refresh: function () {
        },
    });

    kendo.data.binders.timerInterval = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
        },
        refresh: function () {
        },
    });

    kendo.data.binders.timerEnabled = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
            this.isTimerActive = false;
            this.currentTimeoutId = -1;
        },
        refresh: function () {
            var binding = this.bindings["timerEnabled"];
            var callbackBinding = this.bindings.events["timerTick"];
            var intervalBinding = this.bindings["timerInterval"];
            var source = binding.source;
            var logic = source.logic;
            var that = this;
            var interval = -1;
            if (intervalBinding !== undefined) {
                interval = intervalBinding.get();
            } else {
                interval = parseInt($(this.element).attr('data-timerinterval'));
                if (isNaN(interval)) {
                    interval = -1;
                }
            }
            if (logic && logic.RegisterTimer) {
                logic.RegisterTimer(that);
            }
            that.isTimerActive = binding.get() !== false;

            if (binding.get() !== false) {
                var theCallBack = callbackBinding.source.get(callbackBinding.path);
                function returnHere() {
                    theCallBack.call(binding.source).then(function () {
                        if (that.isTimerActive) {
                            that.currentTimeoutId = setTimeout(returnHere, interval);
                        }
                    });
                }
                that.currentTimeoutId = setTimeout(returnHere, interval);
            } else {

                that.clearTimer();
            }
        },
        clearTimer: function () {
            if (this.isTimerActive) {
                this.isTimerActive = false;
            }
            if (this.currentTimeoutId != -1) {
                clearTimeout(this.currentTimeoutId);
                this.currentTimeoutId = -1;
            }
        }
    });

    kendo.data.binders.widget.comboSelectedIndex = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);

            var that = this;
            that.indexCorrection = 0;
            this.element.bind("select",
                function (item) {
                    that.change(item);
                });
            if (this.element.ns === ".kendoDropDownList") {
                this.indexCorrection = 1;
            }
            this.ignoreRefresh = false;
        },
        refresh: function () {
            var that = this;
            if (!this.ignoreRefresh) {
                var binding = this.bindings["comboSelectedIndex"];
                var value = binding.get();
                if (typeof (value) === 'string') {
                    value = parseInt(value);
                }
                // Update the selected element, but be careful
                // the data in the Combo may not be available yet
                if (this.element.dataSource.data().length > 0) {
                    this.element.select(value + this.indexCorrection);
                } else {
                    // If the datasource is not available yet then delay the selection
                    setTimeout(function () {
                        that.element.select(value + that.indexCorrection);
                    },
                        1);
                }
            }
        },
        change: function (item) {
            var value = item.item.index();
            var binding = this.bindings["comboSelectedIndex"];
            try {
                this.ignoreRefresh = true;
                binding.set(value - this.indexCorrection);
            } finally {
                this.ignoreRefresh = false;
            }
        }
    });

    //////////////////////////////
    ///
    /// ListView support
    ///



    function createListViewGridAsChild(element: any, source: any, columnsObject: any, thisElement: any) {
        var newElement = document.createElement("div");
        var role = document.createAttribute("data-role");
        role.value = "grid";
        var db = $(element).attr('data-itemssource');
        var dbatt = document.createAttribute("data-bind");
        dbatt.value = db;
        newElement.setAttribute('data-role', 'grid');
        newElement.setAttribute('data-resizable', 'true');
        var dblclickhandler = $(element).attr('data-doubleclickhandler');
        if (dblclickhandler) {
            db = db + ",listViewDoubleClick: " + dblclickhandler;
        }
        var changeHandler = $(element).attr('data-changehandler');
        if (changeHandler) {
            source.logic["internalChangeHandler"] =
            function (e) {
                if (!WebMap.Client.App.Current.isSynchronizingClient()) {
                    this.logic[changeHandler].bind(this)(e);
                }
            }
            db = db + ",events: {change: logic.internalChangeHandler}";
        }


        var initialHeight = $(element).attr('data-initialheight');
        if (initialHeight !== undefined) {
            newElement.setAttribute('style', 'height: ' + initialHeight + 'px');
        }
        var selectIndices = $(element).attr('data-selectedIndices');
        var checkedIndices = $(element).attr('data-checkedIndices');
        newElement.setAttribute('data-bind', db + "," + selectIndices + "," + checkedIndices);

        element.appendChild(newElement);

        var selectionMode = $(element).data('selectionmode') == "multiple" ? "multiple, row" : "row";
        var allowReorder = $(element).data('allowreorder') === true ? true : false;
        $(newElement).kendoGrid({
            resizable: true,
            scrollable: true,
            reorderable: allowReorder,
            selectable: selectionMode,
            columns: transformColumnsToObj(columnsObject, thisElement),
            dataBound: function (e: any) {
                var items = e.sender.dataItems();
                if (items && items.length) {
                    var itemElements = e.sender.element.find("[class='k-grid-content']").find('tr');
                    var i = 0;
                    for (i = 0; i < items.length; i++) {
                        if (items[i].Checked) {
                            var checkbox = $(itemElements[i]).find('input');
                            if (checkbox) {
                                $(checkbox).attr("checked", "checked");
                            }
                        }
                        if (items[i].Selected) {
                            $(itemElements[i]).addClass("k-state-selected");
                            $(itemElements[i]).attr('selected', 'true');
                        }
                    }
                }
            },
            change: function (e: any) {
                var items = e.sender.dataItems();
                if (items && items.length) {
                    var itemElements = e.sender.element.find("[class='k-grid-content']").find('tr');
                    var i = 0;
                    for (i = 0; i < items.length; i++) {
                        var isSelected = $(itemElements[i]).hasClass('k-state-selected');
                        if (isSelected)
                            items[i].Selected = true;
                        else
                            items[i].Selected = false;
                    }
                }
            }
        });
        // setTimeout is required because the template may not be completely applied
        if (initialHeight === undefined) {
            setTimeout(function () {
                $(element).find('.k-widget').height("100%");
                $(newElement).find('.k-grid-content').height("87%");
                $(newElement).find('.k - grid - header').height("13%");
            }, 100);
        }
        kendo.bind($(element).children('div'), source);
    }

    function removeExistingListViewInnerElement(element: any) {
        $(element).children('div').each(function (index, subeelement) {
            var existingGrid = $(subeelement).data('kendoGrid');
            if (existingGrid) {
                existingGrid.destroy();
                $(subeelement).remove();
            }
            var existingLv = $(subeelement).data('kendoListView');
            if (existingLv) {
                existingLv.destroy();
                $(subeelement).remove();
            }
        });

    }

    function noImageTemplate(checkboxes) {
        var listTemplate = "";
        if (checkboxes)
            listTemplate = "<div style='float: left;'><div style='float:left; margin: 10px 10px 0 10px;'><input type='checkbox' value='#= ItemContent[0] #'/><p style='word-wrap:break-word; width: 5em; margin-top:0;'>#= ItemContent[0] #</p><br></div></div>";

        else


            listTemplate = "<div style='float: left;'><div style='float:left; margin: 10px 10px 0 10px;'><p style='word-wrap:break-word; width: 5em; margin-top:0;'>#= ItemContent[0] #</p><br></div></div>";
        return listTemplate;
    }

    function createTemplateForList(element: any, modevalue: number, checkboxes: boolean) {
        var listTemplate = "<div style='float: left'><div>#= ItemContent[0] #<br></div></div>";
        if (modevalue == 0) {
            var imageListPrefix = $(element).data('imagelistprefix');
        }
        if (modevalue == 2) {
            var imageListPrefix = $(element).data('smallimagelistprefix');
        }

        if (modevalue == 0 || modevalue == 2) {
            if (imageListPrefix) {
                imageListPrefix = imageListPrefix;
                if (checkboxes) {
                    listTemplate = "<div style='float: left;'><div style='float:left; margin: 10px 10px 0 10px;'><input type='checkbox' value='#= ItemContent[0] #'/>#if(ImageIndex != -1){#<img src='" + imageListPrefix + ".ImageStream#= ImageIndex #.png'>#}#<p style='word-wrap:break-word; width: 5em; margin-top:0;'>#= ItemContent[0] #</p><br></div></div>";
                }
                else {
                    listTemplate = "<div style='float: left;'><div style='float:left; margin: 10px 10px 0 10px;'>#if(ImageIndex != -1){#<img src='" + imageListPrefix + ".ImageStream#= ImageIndex #.png'>#}#<p style='word-wrap:break-word; width: 5em; margin-top:0;'>#= ItemContent[0] #</p><br></div></div>";
                }
            }
            else
                listTemplate = noImageTemplate(checkboxes);
        } else {
            var imageListPrefix = $(element).data('smallimagelistprefix');
            if (imageListPrefix) {
                imageListPrefix = imageListPrefix;
                if (checkboxes) {
                    listTemplate = "<div style='float: left'><div><input type='checkbox' value='#= ItemContent[0] #'/>#if(ImageIndex != -1){#<img src='" + imageListPrefix + ".ImageStream#= ImageIndex #.png'>#}##= ItemContent[0] #<br></div></div>";
                }
                else {
                    listTemplate = "<div style='float: left'><div>#if(ImageIndex != -1){#<img src='" + imageListPrefix + ".ImageStream#= ImageIndex #.png'>#}##= ItemContent[0] #<br></div></div>";
                }
            }
            else
                if (checkboxes) {
                    listTemplate = "<div style='float: left'><div><input type='checkbox' value='#= ItemContent[0] #'/>#= ItemContent[0] #<br></div></div>";
                }
                else {
                    listTemplate = "<div style='float: left'><div>#= ItemContent[0] #<br></div></div>";

                }
        }
        return listTemplate;
    }

    function refreshListViewTemplates(thisElement) {
        var modebinding = thisElement.bindings["listViewMode"];
        var modevalue = modebinding.get();
        var binding = thisElement.bindings["listViewColumns"];
        var value = binding.get();
        var element = thisElement.element.element[0];
        var checkBoxes = thisElement.bindings["CheckBoxesMode"].get();//Indicates if the template should have a checkbox element next to each Item
        removeExistingListViewInnerElement(element);

        if (modevalue === 1) {
            createListViewGridAsChild(element, binding.source, value, thisElement);
        } else {
            var newElement = document.createElement("div");
            var role = document.createAttribute("data-role");
            role.value = "listview";
            var db = $(element).attr('data-itemssource');
            var selectIndices = $(element).attr('data-selectedIndices');
            var checkedIndices = $(element).attr('data-checkedIndices');
            var selectedIndexChanged = $(element).attr('data-SelectedIndexChanged');
            newElement.setAttribute('data-role', 'listview');
            newElement.setAttribute('data-resizable', 'true');
            var events = "";
            if (selectedIndexChanged !== undefined) {
                events = "events : {" + selectedIndexChanged + "}";
            };
            newElement.setAttribute('data-bind', db + "," + selectIndices + "," + checkedIndices + "," + events);
            element.appendChild(newElement);
            var initialHeight = $(element).attr('data-initialheight');
            if (initialHeight !== undefined) {
                newElement.setAttribute('style', 'height: ' + initialHeight + 'px');
            }
            newElement.setAttribute('style', 'overflow:scroll' + (newElement.getAttribute('style') ? ';' + newElement.getAttribute('style') : ''));

            var selectionMode = $(element).data('selectionmode') == "single" ? "single" : "multiple";
            var listTemplate = createTemplateForList(element, modevalue, checkBoxes);

            $(newElement).kendoListView({
                navigatable: true,
                selectable: selectionMode,
                template: kendo.template($(listTemplate).html()),
                dataBound: function (e: any) {

                    var items = e.sender.dataItems();
                    if (items && items.length) {
                        var itemElements = e.sender.element.find("div");
                        var i = 0;
                        for (i = 0; i < items.length; i++) {
                            if (items[i].Checked) {
                                var checkbox = $(itemElements[i]).find('input');
                                if (checkbox) {
                                    $(checkbox).attr("checked", "checked");
                                }
                            }
                            if (items[i].Selected) {
                                $(itemElements[i]).addClass("k-state-selected");
                                $(itemElements[i]).attr('selected', 'true');
                            }
                        }
                    }
                },
                change: function (e: any) {
                    var items = e.sender.dataItems();
                    if (items && items.length) {
                        var itemElements = e.sender.element.find("div");
                        var i = 0;
                        for (i = 0; i < items.length; i++) {
                            var isSelected = $(itemElements[i]).hasClass('k-state-selected');
                            console.log(isSelected);
                            if (isSelected)
                                items[i].Selected = true;
                            else
                                items[i].Selected = false;
                        }
                    }
                }
            });
            kendo.bind($(element).children('div'), binding.source);
        }
    }



    kendo.data.binders.widget.listViewSelectedIndices = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
            this.innerUpdating = false;
            element.bind('change',() => {
                this.update();
            });

        },
        refresh: function () {
            if (!this.innerUpdating) {
                var selectedIndexBinding = this.bindings["listViewSelectedIndices"];
                var newIndex = selectedIndexBinding.get();
                var listview = this.element.element;
                if (listview)
                    listview = listview[0];
                else
                    listview = this.element[0];
                var isGrid = $(listview).find('table');
                var items = null;
                if (isGrid.length == 0) {
                    items = $(listview).find('div');
                }
                else
                    items = $(listview).find("[class='k-grid-content']").find('tr');
                this.selectionRestore(items, newIndex);
            }
        },
        update: function () {
            var listview = this.element.element;
            if (listview)
                listview = listview[0];
            else
                listview = this.element[0];
            var isGrid = $(listview).find('table');
            var items = null;
            if (isGrid.length == 0)
                items = $(listview).find('div');
            else
                items = $(listview).find("[class='k-grid-content']").find('tr');
            var newIndices: number[] = [];
            items.each((i, item) => {
                var isSelected = $(item).hasClass('k-state-selected');
                if (typeof isSelected !== typeof undefined && isSelected !== false) {
                    newIndices.push(i);
                }
            });
            var selectedIndexBinding = this.bindings["listViewSelectedIndices"];
            selectedIndexBinding.set(newIndices);
            this.innerUpdating = false;

        },
        destroy: function () {
            (<any>kendo.data.Binder.fn).destroy.call(this);
            this.element.unbind('dblclick');

        },
        selectionRestore: (children, indices) => {
            if (indices) {
                children.each((i, element) => {
                    var colIndex = indices.indexOf(i);
                    if (colIndex != -1) {
                        $(element).addClass('k-state-selected');
                        $(element).attr('selected', 'true');

                    } else if (colIndex == -1) {
                        $(element).removeClass('k-state-selected');
                        $(element).removeAttr('selected');

                    }
                });
            }
        }
    });

    kendo.data.binders.widget.listViewCheckedIndices = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
            this.innerUpdating = false;
            element.element.bind('click',() => {
                this.update();
            });
            element.element.bind('dataBound',() => {
                this.databound();
            });
        },
        refresh: function () {
            if (!this.innerUpdating) {
                var checkedIndexBinding = this.bindings["listViewCheckedIndices"];
                var newIndex = checkedIndexBinding.get();
                var listview = this.element.element;
                if (listview)
                    listview = listview[0];
                else
                    listview = this.element[0];
                var isGrid = $(listview).find('table');
                var items = null;
                if (isGrid.length == 0) {
                    items = $(listview).find('div');
                }
                else
                    items = $(listview).find("[class='k-grid-content']").find('tr');
                this.selectionRestore(items, newIndex);
            }


        },
        update: function () {
            var listview = this.element.element;
            if (listview)
                listview = listview[0];
            else
                listview = this.element[0];
            var isGrid = $(listview).find('table');
            var items = null;
            if (isGrid.length == 0)
                items = $(listview).find('div');
            else
                items = $(listview).find("[class='k-grid-content']").find('tr');
            var newIndices: number[] = [];
            items.each((i, item) => {
                var checkbox = $(item).find('input');
                var checked = false;
                if (checkbox.get(0) !== undefined) { //checkbox activated
                    checked = checkbox.prop('checked');
                    if (checked)
                        newIndices.push(i);
                }
            });
            var checkedIndexBinding = this.bindings["listViewCheckedIndices"];
            checkedIndexBinding.set(newIndices);
            this.innerUpdating = false;

        },
        destroy: function () {
            (<any>kendo.data.Binder.fn).destroy.call(this);
            this.element.unbind('dblclick');

        },
        selectionRestore: (children, indices) => {
            if (indices) {
                children.each((i, element) => {
                    var colIndex = indices.indexOf(i);
                    if (colIndex != -1) {
                        var checkbox = $(element).find('input');
                        if (checkbox) {
                            $(checkbox).prop("checked", "checked");
                        }
                    } else if (colIndex == -1) {
                        var checkbox = $(element).find('input');
                        if (checkbox)
                            checkbox.removeAttr('checked');
                    }
                });
            }
        }
    });

    kendo.data.binders.widget.listViewMode = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
        },

        refresh: function () {
            refreshListViewTemplates(this);
        },

    });


    kendo.data.binders.widget.CheckBoxesMode = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
        },

        refresh: function () {
            refreshListViewTemplates(this);
        },

    });

    function transformColumnsToObj(cols, thisElement) {
        var colsStr = [];
        var checkBoxes = thisElement.bindings["CheckBoxesMode"].get();
        if (checkBoxes) {
            colsStr.push(
                {
                    field: "select",
                    title: "&nbsp;",
                    template: '<input type=\'checkbox\' />',
                    sortable: false,
                    width: 25
                });
        }
        for (var idx = 0; idx < cols.Items.length; idx++) {
            var colDefinition = cols.Items[idx];
            var columnAlignment = getKendoAlignment(colDefinition.HorizontalAlignment);
            colsStr.push({
                title: colDefinition.Text,
                field: "ItemContent[" + idx.toString() + "]",
                attributes: {
                    style: "text-align: " + columnAlignment
                },
                headerAttributes: {
                    style: "text-align: " + columnAlignment
                },
                width: colDefinition.Width
            });
        }

        return colsStr;
    }

    function getKendoAlignment(alignment) {
        switch (alignment) {
            case 0: return "center";
            case 2: return "right";
            default: return "left";
        }
    }

    kendo.data.binders.widget.listViewColumns = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
            var binding = this.bindings["listViewColumns"];
            if (binding) {
                var that = this;
                var value = binding.get();
                if (value) {
                    value.bind('change', function () {
                        that.refresh();
                    });
                }
            }
        },
        refresh: function () {
            var modebinding = this.bindings["listViewMode"];
            var modevalue = modebinding.get();
            if (modevalue !== 1) {
                return;
            }
            var binding = this.bindings["listViewColumns"];
            var value = binding.get();
            var element = this.element.element[0];
            removeExistingListViewInnerElement(element);
            createListViewGridAsChild(element, binding.source, value, this);
        },

    });



    kendo.data.binders.widget.listViewDoubleClick = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);

        },
        refresh: function () {
            var handlerBinding = this.bindings["listViewDoubleClick"];
            var theCallBack = handlerBinding.source.get(handlerBinding.path);
            if (this.theHandler === undefined) {
                this.theHandler = function () {
                    theCallBack.call(handlerBinding.source);
                };
                $(this.element.table).last().dblclick(this.theHandler);
            }
        },
        destroy: function () {
            (<any>kendo.data.Binder.fn).destroy.call(this);
            $(this.element.table).last().unbind('dblclick');
        }

    });


    (function ($) {
        var ui = kendo.ui,
            Widget = ui.Widget
        var WMListView = Widget.extend({
            init: function (element, options) {
                Widget.fn.init.call(this, element, options);
            },
            options: {
                name: "WMListView",
                mode: 1
            }
        });

        ui.plugin(WMListView);
        kendo.init($(document.body));

    })(jQuery);

    kendo.data.binders.enabled = kendo.data.Binder.extend({
        previousBinder: kendo.data.binders.enabled,
        init: function (widget, bindings, options) {
            //call the base constructor
            kendo.data.Binder.fn.init.call(this, widget, bindings, options);
            this.refresh = this.setupTabStripPage(widget) ||
            this.setupMenuItem(widget) ||
            (this.previousBinder ? this.previousBinder.prototype.refresh : undefined);  // use default kendo enabled binder
        },
        setupTabStripPage: function (widget) {
            // check if the HTML element receiving bindings is a tabpage from a tabcontrol widget
            var tabControl, tmp;
            if (widget instanceof HTMLLIElement && (tmp = widget.parentElement) instanceof HTMLUListElement &&
                (tabControl = tmp.parentElement) instanceof HTMLDivElement && tabControl.getAttribute("data-role") === "tabstrip") {
                var tabWidget = $(tabControl).data("kendoTabStrip");
                var enabledBinding = this.bindings.enabled;
                return function () {
                    var index = enabledBinding.path.indexOf(".Items[")
                    if (index >= 0) {
                        var sinPath = enabledBinding.path.substring(0, index);
                        sinPath = sinPath.concat(".Enabled");
                    }
                    var widgetEnabledValue = enabledBinding.source.get(sinPath);
                    if (widgetEnabledValue) {
                        tabWidget.enable(this.element, enabledBinding.get());
                    }
                };
            }
            return null;
        },
        setupMenuItem: function (widget) {
            var parentMenuElement = $(this.element).parents('ul[data-role="menu"]');
            if (parentMenuElement && parentMenuElement.length > 0) {
                var binding = this.bindings["enabled"];
                var parentMenu = parentMenuElement.data('kendoMenu');
                return function () {
                    parentMenu.enable(this.element, binding.get());
                };
            }
            return null;
        }
    });

    /// Enabled custom binding for widgets 
    kendo.data.binders.widget.enabled = kendo.data.Binder.extend({
        previousBinder: kendo.data.binders.widget.enabled,
        init: function (widget, bindings, options) {
            //call the base constructor
            kendo.data.Binder.fn.init.call(this, widget, bindings, options);
            this.refresh = this.setupControls(widget) || (this.previousBinder ? this.previousBinder.prototype.refresh : undefined);  // use default kendo widget enabled binder
        },
        setupControls: function (widget) {
            var dataRole = $(widget.element).attr('data-role');
            if (dataRole === "treeview") {
                var treeWidget = widget;
                var enabledBinding = this.bindings.enabled;
                return function () {
                    treeWidget.enable(".k-item", enabledBinding.get());
                };
            } else if (dataRole === "dropdownlist"
                || dataRole === "datetimepicker"
                || dataRole === "timepicker"
                || dataRole === "datepicker"
                || dataRole === "combobox") {
                var enabledBinding = this.bindings.enabled;
                return function () {
                    widget.enable(enabledBinding.get());
                };
            } else if (dataRole === "wmlistview") {
                var enabledBinding = this.bindings.enabled;
                return function () {
                    var coverDiv = $(widget.element).find('.cover');
                    var enabledValue = enabledBinding.get();
                    if (!enabledValue) {
                        $("<div class='cover'>").css({
                            width: "100%",
                            height: "100%",
                            top: "0px",
                            left: "0px",
                            position: "absolute",
                            background: 'rgba(0,0,0,0.2)',
                            "z-index": $("div[data-role='grid']").css('z-index') + 1,
                        }).appendTo($(widget.element));
                    } else {
                        $(widget.element).find('.cover').remove();
                    }
                };
            } else if (dataRole === "tabstrip") {
                var enabledBinding = this.bindings.enabled;
                return function () {
                    var ulElem = $(widget.element).children('ul').first();
                    ulElem.children('li').each(function (index, li) {
                        if (enabledBinding.get()) {
                            var sinPath = enabledBinding.path.replace('.Enabled', '');
                            var itemEnabledValue = enabledBinding.source.get(sinPath + '.Items[' + index + '].Enabled');
                            widget.enable(li, itemEnabledValue);
                        }
                        else {
                            widget.enable(li, enabledBinding.get());
                        }
                    });
                }
            }
            return null;
        }
    });



    kendo.data.binders.visible = kendo.data.Binder.extend({
        previousBinder: kendo.data.binders.visible,
        init: function (widget, bindings, options) {
            //call the base constructor
            kendo.data.Binder.fn.init.call(this, widget, bindings, options);
            //WebMap.Kendo.setupBinding(this, kendo.data.binders.visible, [TabPageVisibleBindingSetup.prototype]);
            this.refresh = this.setupTabStripPage(widget) || (this.previousBinder ? this.previousBinder.prototype.refresh : undefined);  // use default kendo enabled binder
        },
        setupTabStripPage: function (widget) {
            // check if the HTML element receiving bindings is a tabpage from a tabcontrol widget
            var tabControl, tmp;
            if (widget instanceof HTMLLIElement && (tmp = widget.parentElement) instanceof HTMLUListElement &&
                (tabControl = tmp.parentElement) instanceof HTMLDivElement && tabControl.getAttribute("data-role") === "tabstrip") {
                var tabWidget = $(tabControl).data("kendoTabStrip");
                var visibleBinding = this.bindings.visible;
                return function () {
                    var cls = widget.getAttributeNode("class");
                    if (visibleBinding.get()) {
                        if (!WebMap.Utils.IsClassSet(cls.value, "k-item"))
                            cls.value = "k-item " + cls.value;
                        widget.removeAttribute("hidden");
                    }
                    else {
                        var newValue = WebMap.Utils.RemoveClass(cls.value, "k-item");
                        if (newValue != cls.value) {
                            if (tabWidget.select()[0] === widget)
                                tabWidget.deactivateTab(widget);
                            cls.value = newValue;
                            widget.setAttribute("hidden", "hidden");
                        }
                    }
                };
            }
            return null;
        }
    });

    //class TabPageVisibleBindingSetup extends WebMap.Kendo.BindingSetup {
    //    public setup(binding: kendo.data.Binder): boolean {
    //        // check if the HTML element receiving bindings is a tabpage from a tabcontrol widget
    //        var widget = binding.element;
    //        var tabControl;
    //        if (widget instanceof HTMLLIElement && widget.parentElement instanceof HTMLUListElement &&
    //            (tabControl = widget.parentElement.parentElement) instanceof HTMLDivElement &&
    //            tabControl.getAttribute("data-role") === "tabstrip") {
    //            var tabWidget = $(tabControl).data("kendoTabStrip");
    //            var visibleBinding = binding.bindings["visible"];
    //            binding.refresh = function () {
    //                var cls = widget.getAttributeNode("class");
    //                if (visibleBinding.get()) {
    //                    if (!WebMap.Utils.IsClassSet(cls.value, "k-item"))
    //                        cls.value = "k-item " + cls.value;
    //                    widget.removeAttribute("hidden");
    //                }
    //                else {
    //                    var newValue = WebMap.Utils.RemoveClass(cls.value, "k-item");
    //                    if (newValue != cls.value) {
    //                        if (tabWidget.select()[0] === widget)
    //                            tabWidget.deactivateTab(widget);
    //                        cls.value = newValue;
    //                        widget.setAttribute("hidden", "hidden");
    //                    }
    //                }
    //            };
    //            return true;
    //        }
    //        return false;
    //    }
    //}


    kendo.data.binders.widget.visible = kendo.data.Binder.extend({
        previousBinder: kendo.data.binders.visible,
        init: function (widget, bindings, options) {
            //call the base constructor
            kendo.data.Binder.fn.init.call(this, widget, bindings, options);
            this.refresh = this.setupCustomControl(widget) || (this.previousBinder ? this.previousBinder.prototype.refresh : undefined);  // use default kendo enabled binder
        },
        setupCustomControl: function (widget) {
            var dataRole = $(widget.element).attr('data-role');
            var visibleBinding = this.bindings.visible;
            if (dataRole === "wmlistview"
                || dataRole === "listbox"
                || dataRole === "grid"
                || dataRole === "wmflexgrid"
                || dataRole === "dropdownlist"
                || dataRole === "combobox"
                || dataRole === "menu"
                || dataRole === "treeview"
                || dataRole === "tabstrip"
                || dataRole === "datetimepicker"
                || dataRole === "timepicker"
                || dataRole === "datepicker") {
                return function () {
                    if (visibleBinding.get()) {
                        $(widget.element).css({ visibility: "inherit" });
                    } else {
                        $(widget.element).css({ visibility: "hidden" });
                    }
                };
            }
            return null;
        }
    });

    kendo.data.binders.widget.selectedIndex = kendo.data.Binder.extend({
        previousBinder: kendo.data.binders.widget.selectedIndex,
        init: function (widget, bindings, options) {
            //call the base constructor
            kendo.data.Binder.fn.init.call(this, widget, bindings, options);
            if (!this.setupTabStripPage(widget) && this.previousBinder) {
                this.refresh = this.previousBinder.prototype.refresh;
                this.destroy = this.previousBinder.proptotype.destroy;
            }
        },
        setupTabStripPage: function (widget) {
            // check if the HTML element receiving bindings is a tabpage from a tabcontrol widget
            var htmlElement = widget.element[0];
            if (htmlElement instanceof HTMLDivElement && htmlElement.getAttribute("data-role") === "tabstrip") {
                var selectedIndexBinding = this.bindings.selectedIndex;
                var busy = false;
                var updateModel = function () {
                    if (!busy)
                        try {
                            busy = true;
                            selectedIndexBinding.set(widget.select().index());
                        }
                        finally {
                            busy = false;
                        }
                };
                WebMap.Client.App.Current.addModelUpdateListener(updateModel);
                this.refresh = function () {
                    if (!busy)
                        try {
                            busy = true;
                            var selectedValue = selectedIndexBinding.get();
                            if (selectedValue == -1) {
                                widget.select(0);
                            }
                            else {
                                widget.select(selectedValue);
                            }
                        }
                        finally {
                            busy = false;
                        }
                };
                this.destroy = function () {
                    WebMap.Client.App.Current.removeModelUpdateListener(updateModel);
                };
                return true;
            }
            return false;
        }
    });

    kendo.data.binders.widget.flexGridRefresh = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
        },
        refresh: function () {

            var binding = this.bindings["flexGridRefresh"];
            var value = binding.get();
            if (value === true) {
                try {
                    var grid = $(this.element.element.children('div')).data('kendoGrid');
                    grid.dataSource.read();
                } finally {
                    binding.set(false);
                }
            }
        }
    });


    kendo.data.binders.widget.flexGridPosition = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
            this.innerUpdating = false;
            var that = this;

            that.registerChange();

            this.element.bind('reconfigured', function () {
                that.registerChange();
            });
        },
        registerChange: function () {
            var that = this;
            var grid = $(this.element.element).find('div').data('kendoGrid');
            if (grid) {
                grid.bind('change', function () {
                    that.update(grid, that.bindings["flexGridPosition"]);
                    setTimeout(function () {
                        that.element.trigger("click");
                    }, 1);

                });
            }
        },
        refresh: function () {
            if (!this.innerUpdating) {
                var binding = this.bindings["flexGridPosition"];
                var grid = $(this.element.element).find('div').data('kendoGrid');
                var htmlRow = grid.tbody.children().eq(binding.get()[0] - this.rowAdjustment());
                if (htmlRow && htmlRow.length > 0) {
                    if ((grid.options && grid.options.selectable === "row")) {
                        grid.clearSelection();
                        grid.select(htmlCell);
                    } else {
                        var htmlCell = htmlRow.children('td').eq(binding.get()[1]);
                        if (htmlCell && !htmlCell.hasClass('k-state-selected')) {
                            grid.clearSelection();
                            grid.select(htmlCell);
                        }
                    }
                }
            }
        },
        update: function (grid, binding) {
            var dataItem = grid.dataItem(grid.select()) ||
                grid.dataItem($(grid.select()).parent());
            if (dataItem && dataItem.UniqueID) {
                var rowIndex = parseInt(dataItem.UniqueID.substring(0, dataItem.UniqueID.indexOf('#')));
                rowIndex = rowIndex + this.rowAdjustment();
                try {
                    this.innerUpdating = true;
                    var columnIndex =
                        (grid.options && grid.options.selectable === "row") ?
                            0 : $(grid.select()).index();
                    binding.set([rowIndex, columnIndex]);
                } finally {
                    this.innerUpdating = false;
                }
            }
        },
        rowAdjustment: function () {
            var result = 0;
            if (this.bindings['flexGridDefinition'] && this.bindings['flexGridDefinition'].get) {
                var binding = this.bindings['flexGridDefinition'];
                var columnsValue = binding.get();
                if (columnsValue &&
                    columnsValue.parent() &&
                    "FixedRowsCount" in columnsValue.parent()) {
                    result = columnsValue.parent().FixedRowsCount;
                }
            }
            return result;
        }
    });

    kendo.data.binders.widget.flexGridEndSelection = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
            this.innerUpdating = false;
            var that = this;
            var grid = $(element.element).find('div').data('kendoGrid');
            if (grid) {
                grid.bind('change', function () {
                    that.update(grid, that.bindings["flexGridEndSelection"]);
                });
            }
        },
        refresh: function () {
            if (!this.innerUpdating) {
                var binding = this.bindings["flexGridEndSelection"];
                var positionBinding = this.bindings["flexGridPosition"];
                if (positionBinding) {
                    if (positionBinding.get()[0] == binding.get()[0]
                        && positionBinding.get()[1] == binding.get()[1]) {
                        return;
                    }
                    var position = positionBinding.get();
                    position[0] = position[0] - this.rowAdjustment();
                    var destination = binding.get();
                    destination[0] = destination[0] - this.rowAdjustment();
                    var grid = $(this.element.element).find('div').data('kendoGrid');
                    var htmlRow = grid.tbody.children().eq(binding.get()[0]);
                    grid.clearSelection();
                    for (var y = position[1]; y <= destination[1]; y++) {
                        for (var x = position[0]; x <= destination[0]; x++) {
                            this.selectCell(x, y, grid);
                        }
                    }
                }
            }
        },
        selectCell: function (row: number, column: number, grid: any) {
            var htmlRow = grid.tbody.children().eq(row);
            if (htmlRow) {
                if ((grid.options && grid.options.selectable === "row")) {
                    grid.select(htmlCell);
                } else {
                    var htmlCell = htmlRow.children('td').eq(column);
                    if (htmlCell && !htmlCell.hasClass('k-state-selected')) {
                        grid.select(htmlCell);
                    }
                }
            }
        },
        rowAdjustment: function () {
            var result = 0;
            if (this.bindings['flexGridDefinition'] && this.bindings['flexGridDefinition'].get) {
                var binding = this.bindings['flexGridDefinition'];
                var columnsValue = binding.get();
                if (columnsValue &&
                    columnsValue.parent() &&
                    "FixedRowsCount" in columnsValue.parent()) {
                    result = columnsValue.parent().FixedRowsCount;
                }
            }
            return result;
        },
        update: function (grid, binding) {
            var dataItem = grid.dataItem(grid.select().last()) ||
                grid.dataItem($(grid.select().last()).parent());
            if (dataItem && dataItem.UniqueID) {
                var rowIndex = parseInt(dataItem.UniqueID.substring(0, dataItem.UniqueID.indexOf('#'))) + 1;
                try {
                    this.innerUpdating = true;
                    var columnIndex =
                        (grid.options && grid.options.selectable === "multiple row") ?
                            ($(grid.select().last()).children('td').length - 1) : $(grid.select().last()).index();
                    binding.set([rowIndex, columnIndex]);
                } finally {
                    this.innerUpdating = false;
                }
            }
        }
    });

    kendo.data.binders.widget.flexCellCoordinates = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
            this.innerUpdating = false;
            var that = this;
            var grid = $(element.element).find('div').data('kendoGrid');
            if (grid) {
                grid.bind('change', function () {
                    that.update(grid, that.bindings["flexCellCoordinates"]);
                });
            }
        },
        refresh: function () { },
        update: function (grid, binding) {
            var selected = grid.select();
            if (selected) {
                var height = Math.floor($(selected).height());
                var width = Math.floor($(selected).width());
                var pos = $(selected).position();
                binding.set([pos.left, pos.top, width, height]);
            }
        }
    });

    kendo.data.binders.widget.flexGridDefinition = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
            var binding = this.bindings["flexGridDefinition"];
            if (binding) {
                var that = this;
                var value = binding.get();
                if (value) {
                    value.bind('change', function () {
                        if (!that.isRefreshPending) {
                            that.isRefreshPending = true;
                            setTimeout(function () {
                                try {
                                    that.refresh();
                                    that.element.trigger('reconfigured');
                                } finally {
                                    that.isRefreshPending = false;
                                }
                            }, 1);
                        }
                    });
                }
            }
        },
        refresh: function () {

            var binding = this.bindings["flexGridDefinition"];
            var value = binding.get();
            var element = this.element.element[0];
            this.removeExistingInnerElement(element);
            this.createGridAsChild(element, binding.source, value);

        },
        transformColumnsToObj: function (cols) {
            var colsStr = [];

            for (var idx = 0; idx < cols.Items.length; idx++) {
                var colDefinition = cols.Items[idx];
                colsStr.push({
                    title: colDefinition.Text,
                    field: "RowContent[" + idx.toString() + "]",
                    width: colDefinition.Width
                });
            }
            return colsStr;
        },

        removeExistingInnerElement: function (element: any) {
            $(element).children('div').each(function (index, subeelement) {
                var existingGrid = $(subeelement).data('kendoGrid');
                if (existingGrid) {
                    existingGrid.destroy();
                    $(subeelement).remove();
                }
            });
            ///
        },
        createGridAsChild: function (element: any, source: any, columnsObject: any) {
            var that = this;
            var newElement = document.createElement("div");
            var role = document.createAttribute("data-role");
            role.value = "grid";
            var db = "source: " + $(element).attr('data-itemssource');
            var dbatt = document.createAttribute("data-bind");
            dbatt.value = db;
            newElement.setAttribute('data-role', 'grid');
            newElement.setAttribute('data-resizable', 'true');
            var pageSize = parseInt($(element).attr('data-pagesize')) || 15;

            var itemsSource = source.get($(element).attr('data-itemssource'));
            var itemsSourceUniqueId = itemsSource.UniqueID;

            var extraBindings = $(element).attr('data-gridbindings');
            if (extraBindings) {
                newElement.setAttribute('data-bind', extraBindings);
            }

            element.appendChild(newElement);
            var selectionMode = $(element).attr('data-selectionmode') || "multiple cell";

            $(newElement).kendoGrid({
                resizable: true,
                scrollable: {
                    virtual: true
                },
                selectable: selectionMode,
                //pageable: true,
                dataSource: {
                    serverPaging: true,
                    pageSize: pageSize,
                    schema: {
                        data: "data",
                        total: "total"
                    },
                    transport: {
                        read: function (options) {
                            var t = new Date();

                            $.ajax({
                                url: 'ResumeOperation/CollectiongPendingResults',
                                type: "POST",
                                headers:
                                {
                                    WM: true
                                },
                                dataType: "json",
                                data: { coluid: itemsSourceUniqueId, skip: options.data.skip, take: options.data.take },
                                success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                                    options.success(data);
                                }
                            });
                        }
                    }
                },
                columns: this.transformColumnsToObj(columnsObject)
            });
            // setTimeout is required because the template may not be completely applied
            setTimeout(function () {
                $(element).find('.k-widget').height("100%");
                $(element).find('table').last().dblclick(function () {
                    that.element.trigger('dblclick');
                });
            }, 100);
            kendo.bind($(element).children('div'), source);
        }
    });


    kendo.data.binders.widget.treeDataSource = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
            var that = this;
            // Create a new empty data source to start with
            element.setDataSource((<any>kendo).observableHierarchy([]));
            // Event handler used to synchronize checkboxes
            element.dataSource.bind('change', function (e) {
                that.syncCheckedNodes(that, e);
            });
            // Event handler to handle children
            element.bind('change', function (e) {
                var selected = element.dataItem(element.select());
                if (selected && selected.wmid) {
                    var dataSourceBinding = that.bindings["treeDataSource"];
                    var model = dataSourceBinding.get();
                    model.set('SelectedItemId', selected.wmid);
                }
            });
            element.element.bind('click', function (e) {
                var editable = $(this).attr('data-labeledit');
                if (editable && (editable == "true")) {
                    var modelObj = e.target;
                    $(modelObj).bind('click', function () {
                        $(modelObj).prop('contentEditable', true);
                    });
                    $(modelObj).bind('blur', function () {
                        $(modelObj).removeAttr('contentEditable');
                        var binding = that.bindings["treeDataSource"];
                        var tree = binding.get();
                        var ds = that.element.dataSource.data();
                        for (var i = 0; i < tree.Items.length; i++) {
                            var modelObj = tree.Items[i];
                            var htmlElement = ds[i];
                            that.updateValues(that, modelObj, htmlElement);
                        }
                    });
                }
            });
            that.initialHeight = $(element.element).attr('data-initialheight');
            that.imageListPrefix = $(element.element).data('imagelistprefix');
            that.imageIndex = $(element.element).data('imageIndex');


        },
        // Updating text when the labels are edited.
        updateValues: function (that, e, treeNode) {
            if (treeNode) {
                var node = $(that.element.element).find("[data-uid='" + treeNode.uid + "']").children()[0];
                if (node) {
                    var innerSpan = $(node).find("[class*='k-in']");
                    e.set('Text', innerSpan.text());
                }
                if (e.items)
                    for (var i = 0; i < e.Items.length; i++) {
                        this.updateValues(that, e.Items[i], treeNode.items[i]);
                    }
            }
        },
        // Synchronization of checkbox nodes
        syncCheckedNodes: function (that, e) {
            var binding = that.bindings["treeDataSource"];
            if (binding && binding.get()
                && (e.field === 'checked'
                    || e.field === 'selected'
                    || e.field === 'expanded'
                    || e.field === 'isVisible')
                && e.items
                && e.items.length > 0) {
                var tree = binding.get();
                var field = e.field;
                for (var i = 0; i < tree.Items.length; i++) {
                    that.syncingCheckedNodes(that, tree.Items[i], e.items, field);
                }
            }
        },

        syncingCheckedNodes: function (that, modelTreeNode, treeNodes, field) {
            for (var i = 0; i < treeNodes.length; i++) {
                var uiTreeNode = treeNodes[i];
                if (uiTreeNode.wmid === modelTreeNode.UniqueID) {
                    var ht = $(uiTreeNode).html();
                    switch (field) {
                        case "checked": { modelTreeNode.set('Checked', uiTreeNode.checked); break; }
                        case "selected": { modelTreeNode.set('IsSelected', uiTreeNode.selected); break; }
                        case "expanded": { modelTreeNode.set('IsExpanded', uiTreeNode.expanded); break; }
                        case "isVisible": { modelTreeNode.set('IsVisible', uiTreeNode.isVisible); break; }
                    }

                }
            }
            for (var i = 0; i < modelTreeNode.Items.length; i++) {
                this.syncingCheckedNodes(that, modelTreeNode.Items[i], treeNodes, field);
            }
        },

        createMirrorNodeFromModelNode: function (modelObj) {
            var that = this;
            var imageurl = undefined;
            if (that.imageListPrefix !== undefined)
                if (modelObj.ImageIndex != -1)
                    imageurl = that.imageListPrefix + '.ImageStream' + modelObj.ImageIndex + ".png"
                else if (that.imageIndex != -1)
                    imageurl = that.imageListPrefix + '.ImageStream' + that.imageIndex + ".png" //Default image
            return (<any>kendo).observableHierarchy([{
                text: modelObj.Text,
                wmid: modelObj.UniqueID,
                imageUrl: imageurl,
                checked: modelObj.Checked,
                expanded: modelObj.IsExpanded,
                items: []
            }])[0];
        },

        createMirrorStructure: function (parentItemsCollection, modelObj) {
            var that = this;
            var newObj = that.createMirrorNodeFromModelNode(modelObj);
            parentItemsCollection.push(newObj);
            var element = $(that.element.element).find("[data-uid='" + newObj.uid + "']");
            var visibility = $(element).is(":visible");
            var itemHeight = $(element).height();
            var dataSourceBinding = that.bindings["treeDataSource"];
            var model = dataSourceBinding.get();
            var vc = model.get('VisibleCount');
            if (!(vc && vc > 0) && (itemHeight > 0)) {
                var visibleCount = Math.floor(that.initialHeight / itemHeight);
                model.set('VisibleCount', visibleCount);
            }
            var newObj2 = parentItemsCollection[parentItemsCollection.length - 1];
            newObj2.set('isVisible', visibility);
            function syncItems(e) {
                if ((e.action === 'add' || e.action === undefined)
                    && newObj2.items.length < modelObj.Items.length) {
                    for (var k = newObj2.items.length; k < modelObj.Items.length; k++) {
                        var innerObj = modelObj.Items[k];
                        var newInnerObj = that.createMirrorNodeFromModelNode(innerObj);
                        that.createMirrorStructure(newObj2.items, innerObj);
                    }
                } else if (e.action == 'remove' && ('index' in e)) {
                    newObj2.items.remove(newObj2[e.index]);
                } else {
                    horizontalSyncItems();
                }
            }

            function horizontalSyncItems() {
                for (var i = 0;
                    i < newObj.items.length && i < modelObj.Items.length;
                    i++) {
                    if (newObj.items[i] && modelObj.Items[i]
                        && newObj.items[i].wmid
                        && modelObj.Items[i].UniqueID
                        && newObj.items[i].wmid !== modelObj.Items[i].UniqueID) {
                        newObj.items[i].set("wmid", modelObj.Items[i].UniqueID);
                        newObj.items[i].set("text", modelObj.Items[i].Text);
                        newObj.items[i].set("checked", modelObj.Items[i].Checked);
                        newObj.items[i].set("items", modelObj.Items[i].Items);
                    }
                }
            }

            var changeFunc = function (e) {
                if (e.field === "Text") {
                    newObj2.set('text', modelObj.get(e.field));
                } if (e.field === "Items") {
                    if (!(e.action && (e.action === 'itemchange'))) {
                        syncItems(e);
                    }
                } if (e.field === "IsExpanded") {
                    newObj2.set('expanded', modelObj.get(e.field));
                }
                else {
                    newObj2.set(e.field, modelObj.get(e.field));
                }
            };
            modelObj.bind('change', changeFunc);
            newObj2.changeFunc = changeFunc;

            // Add existing items if available
            if ('Items' in modelObj) {
                var itemsChangeFunc = function (e) {
                    if (e.action === "add") {
                        var idx = e.index === undefined ? 0 : e.index;
                        var innerObj = modelObj.Items[idx];
                        var newInnerObj = that.createMirrorNodeFromModelNode(innerObj);
                        that.createMirrorStructure(newObj2.items, modelObj.Items[idx])
                    }
                };
                newObj2.itemsChangeFunc = itemsChangeFunc;
                for (var k = 0; k < modelObj.Items.length; k++) {
                    this.createMirrorStructure(newObj2.items, modelObj.Items[k]);
                }
            }
        },

        refresh: function () {
            var that = this;
            var treeDataSourceBinding = this.bindings["treeDataSource"];
            var labelEdit = $(this.element.element).data("LabelEdit");
            that.labelEdit = labelEdit;
            var bvalue = treeDataSourceBinding.get().Items;
            var ds = this.element.dataSource;
            var data = ds.data();
            var callRefresh = function (f) {
                // Skip for child object property change
                if (f.action === "itemchange") {
                    return;
                } else if (f.action === "remove" && ('index' in f)) {
                    data.remove(data[f.index]);
                } else {
                    that.refresh();
                }
            };
            // Unregister a previous version of the 'refresh' callback if exist
            // (this is needed to avoid multiple calls)
            if (data.callRefresh) {
                bvalue.unbind('change', callRefresh);
            }
            bvalue.bind('change', callRefresh);

            // Keep a reference of the function in order to unregister it
            data.callRefresh = callRefresh;
            var oldCount = data.length
            for (var i = oldCount; i < bvalue.length; i++) {
                var modelObj = bvalue[i];
                that.createMirrorStructure(data, modelObj);
            }
        }
    });

    (function ($) {
        var ui = kendo.ui,
            Widget = ui.Widget
        var wmflexgrid = Widget.extend({
            init: function (element, options) {
                Widget.fn.init.call(this, element, options);
            },
            options: {
                name: "wmflexgrid",
                itemssource: ""
            },
            events: [
                "reconfigured", "dblclick", "click"
            ]
        });

        ui.plugin(wmflexgrid);
        kendo.init($(document.body));

    })(jQuery);


    kendo.data.binders.widget.listBoxSelectedIndices = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
            this.innerUpdating = false;
            element.bind('change',() => {
                this.update();
            });
            element.bind('dataBound',() => {
                this.databound();
            });
        },
        refresh: function () {
            if (!this.innerUpdating) {
                this.selectionRestore(this.element.items(), this.bindings["listBoxSelectedIndices"].get());
            }
        },
        update: function () {
            try {
                this.innerUpdating = true;
                var selectedIndices = $.map(this.element.select(), item => $(item).index());
                var listBoxSelectedIndicesBinding = this.bindings["listBoxSelectedIndices"];
                listBoxSelectedIndicesBinding.set(selectedIndices);
            } finally {
                this.innerUpdating = false;
            }
        },
        databound: function () {
            if (!this.innerUpdating) {
                this.selectionRestore(this.element.items(), this.bindings["listBoxSelectedIndices"].get());
            }
        },
        selectionRestore: (children, indices) => {
            if (indices && indices.indexOf) {
                children.each((i, element) => {
                    var isSelected = $(element).hasClass('k-state-selected');
                    var colIndex = indices.indexOf(i);
                    if (colIndex != -1 && !isSelected) {
                        $(element).addClass('k-state-selected');
                    } else if (colIndex == -1 && isSelected) {
                        $(element).removeClass('k-state-selected');
                    }
                });
            }
        }
    });

    export function checkedListBoxProc(element: any) {
        var listBox = $(element).parents('div[data-role="checkedlistbox"]').data('kendoCheckedListBox');
        var index = $(element).index();
        if (listBox && index >= 0) {
            var data = listBox.dataSource.data();
            if (data && data[index]) {
                data[index].set("Checked", element.checked);
            }
        }
    }

    kendo.data.binders.selectedRange = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
            var that = this;
            $(element).bind('focus', function () {
                var binding = that.bindings["selectedRange"];
                var bindingValue = binding.get();
                var selectionStart = bindingValue[0];
                var selectionEnd = bindingValue[1];
                $(that.element).caret({ start: selectionStart, end: selectionEnd });
            });
            $(element).bind('click', function () {
                that.update();
            });
            $(element).bind('keyup', function () {
                that.update();
            });
            $(element).bind('keydown', function () {
                that.update();
            });
            $(element).bind('keypress', function () {
                that.update();
            });
            $(element).bind('mouseup', function () {
                that.update();
            });
            $(element).bind('mousedown', function () {
                that.update();
            });
        },
        update: function () {
            var selectedRangeBinding = this.bindings["selectedRange"];
            var caret = $(this.element).caret();
            console.log("Update: " + caret.start + " " + caret.end);
            selectedRangeBinding.set([caret.start, caret.end]);
        },
        refresh: function () {
            var binding = this.bindings["selectedRange"];
            var bindingValue = binding.get()
            var selectionStart = bindingValue[0];
            var selectionEnd = bindingValue[1];
            $(this.element).caret({ start: selectionStart, end: selectionEnd });
            $(this.element).focus();
        },
    });
}

//NumericTextBox  Value
kendo.data.binders.widget.NumericUpDownValue = kendo.data.Binder.extend({
    init: function (widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
    },
    refresh: function () {
        var that = this,
            value = that.bindings["NumericUpDownValue"].get(); //get the value from the View-Model
        $(that.element).data("kendoNumericTextBox").value(value); //update the widget
    }
});

//NumericTextBox Max Value
kendo.data.binders.widget.NumericUpDownMax = kendo.data.Binder.extend({
    init: function (widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
    },
    refresh: function () {
        var that = this,
            value = that.bindings["NumericUpDownMax"].get(); //get the value from the View-Model
        $(that.element).data("kendoNumericTextBox").max(value); //update the widget
    }
});

//NumericTextBox Min Value
kendo.data.binders.widget.NumericUpDownMin = kendo.data.Binder.extend({
    init: function (widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
    },
    refresh: function () {
        var that = this,
            value = that.bindings["NumericUpDownMin"].get(); //get the value from the View-Model
        $(that.element).data("kendoNumericTextBox").min(value); //update the widget
    }
});

//NumericTextBox Increment Value
kendo.data.binders.widget.NumericUpDownIncrementVal = kendo.data.Binder.extend({
    init: function (widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
    },
    refresh: function () {
        var that = this,
            value = that.bindings["NumericUpDownIncrementVal"].get(); //get the value from the View-Model
        $(that.element).data("kendoNumericTextBox").step(value); //update the widget
    }
});



kendo.data.binders.widget.refreshOnChange = kendo.data.Binder.extend({
    init: function (element, bindings, options) {
        (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
        this.timeout = 0;
    },
    refresh: function () {
        var that = this;
        /// The following code allows use to trigger just one refresh call for a batch of 
        // updates on the same list.
        if (that.timeout === 0) {
            that.timeout = setTimeout(function () {
                try {
                    that.element.refresh();
                } finally {
                    that.timeout = 0;
                }
            }, 1);
        }
    }
});

////Masked Textbox Support
//MaskedTextBox Mask 
kendo.data.binders.widget.maskedTextBoxMask = kendo.data.Binder.extend({
    init: function (widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
        var that = this;
        this.widget = widget;
    },
    refresh: function () {
        var that = this;
        var oldmask = this.widget.options.mask;
        var value = that.bindings["maskedTextBoxMask"].get();
        this.widget.setOptions({ mask: value });
        if (oldmask != "")
            $(this.element).trigger("maskChanged");
    }
});
 
//MaskedTextBox promptChar
kendo.data.binders.widget.maskedTextBoxPrompt = kendo.data.Binder.extend({
    init: function (widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
        var that = this;
        this.widget = widget;
    },
    refresh: function () {
        var that = this;
        var value = that.bindings["maskedTextBoxPrompt"].get();
        this.widget.setOptions({ promptChar: value });
    }
});


//MaskChangedEvent
kendo.data.binders.widget.MaskChanged = kendo.data.Binder.extend({
    init: function (element, bindings, options) {
        (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
        var that = this;
        $(element.element).bind("maskChanged", function (event) {
            var binding = that.bindings["MaskChanged"];
            var methodName = binding.path.substring((binding.path.indexOf('.') + 1));
            var method = binding.source.logic[methodName];
            method.apply(binding.source);
        });
    },
    refresh: function () { }
});


//TypeValidationEvent
kendo.data.binders.widget.TypeValidationCompleted = kendo.data.Binder.extend({
    init: function (element, bindings, options) {
        (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
        var that = this;
        $(element.element).bind("change", function (event) {
            var binding = that.bindings["TypeValidationCompleted"];
            var methodName = binding.path.substring((binding.path.indexOf('.') + 1));
            var method = binding.source.logic[methodName];
            method.apply(binding.source);
        });
    },
    refresh: function () { }
});

//MaskInputRejected 
kendo.data.binders.widget.MaskInputRejected = kendo.data.Binder.extend({
    init: function (element, bindings, options) {
        (<any>kendo.data.Binder.fn).init.call(this, element, bindings, options);
        var that = this;
        $(element.element).bind("keypress", function (event) {
            var binding = that.bindings["MaskInputRejected"];
            var methodName = binding.path.substring((binding.path.indexOf('.') + 1));
            var method = binding.source.logic[methodName];
            var newChar = String.fromCharCode(event.charCode);
            var position = ($(this).caret().start);
            var currentMask = that.options.mask;
            var match = maskToRegex(newChar, currentMask.charAt(position));
            if (!match) {
                method.apply(binding.source);
            }
        });
    },
    refresh: function () { }
});

function maskToRegex(newChar, maskChar) {
    switch (maskChar) {
        case "0": { return newChar.match(/\d/); }
        case "9": { return newChar.match(/\d|\s/); }
        case "#": { return newChar.match(/\d|\s|\+|\-/); }
        case "&": { return newChar.match(/\S/); }
        case "?": { return newChar.match(/[a-zA-Z]|\s/); }
        case "A": { return newChar.match(/[a-zA-Z0-9]/); }
        case "C": { return newChar.match(/./); }
        case "L": { return newChar.match(/[a-zA-Z]/); }
        case "a": { return newChar.match(/[a-zA-Z0-9]|\s/); }
        default: {
            return newChar.match(new RegExp("/\\" + maskChar + "/"));
        }

    }
}