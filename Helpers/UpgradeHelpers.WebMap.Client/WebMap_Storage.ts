/// <reference path="WebMap_Interfaces.ts" />

module WebMap.Client {

    class SimpleStorageSerializer implements IStorageSerializer {
        Serialize(obj: any) {
        }
    }

   /**
     * (Not implementet yet!) Provides an storage area where to serialize IStateObject objects to.
     * The default persistent layer would be the WebMap server side object.
     */
    export class StorageManager {
        _storageSerializer: IStorageSerializer;

        /**
          * Tries the gets an object from the storage area.
          * @param uniqueId The id of the object to get.
          */
        public TryGetValue(uniqueId: string): JQueryPromise<TryGetValueResult<IStateObject>> {
            var def = $.Deferred();
            def.resolve({ HasValue: false, Value: null });
            return def.promise();
        }

        /**
          * Persists the StateCache object into the storage area.
          * @param stateCache The StateCache object to persist.
          */
        public Persist(stateCache: StateCache): void {

        }

        /**
          * Saves an object to the storage area.
          * @param uniqueId  The id of ghe object to serialize
          * @param serializedValue The value to serialize.
          */
        public SaveEntry(uniqueID: string, serializedValue: any): void { }

    }


}

