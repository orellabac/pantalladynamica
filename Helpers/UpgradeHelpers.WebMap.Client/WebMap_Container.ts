// Module
module WebMap.Client {

    // IIocContainer implementation that does use of kendo objects to keep track of the
    // state of the elements it creates.
    export class Container implements IIocContainer {
        static Current: Container;

        // Initializes the singleton instance
        public static Init() {
            if (!Container.Current) {
                Container.Current = new Container();
            }
        }

        // Creates a new object according to the given options.  Standard calls are:
        // 1. Resolve({ vm: true, data: <JSON object>, dirty: false }).  Creates a new view model object with the 
        // given JSON data and marks it as not diry.
        // 2. Resolve({ "cons": constructor lambda) }).  Creates a new object usign the given lambda to create it.
        Resolve(options?: any): any {
            if (options) {
                if (options.vm) {  // creating a view model object?

                    var observable: kendo.Observable;
                    if (options.data['@arr']) {  // is it an array? then let's create an Observable array
                        observable = new kendo.data.ObservableArray(options.data);
                        (<any>observable).Count = options.data.Count;
                        (<any>observable).UniqueID = options.data.UniqueID;
                    }
                    else {
                        observable = new kendo.data.ObservableObject(options.data);
                    }

                    // the new view model objects defaults to not dirty, however its initial value can be changed by 
                    // using the dirty flag.
                    var dirty: boolean = false;
                    if (options.dirty != undefined)
                        dirty = options.dirty;
                    // adds the new object to cache
                    StateCache.Current.addNewObject(<IStateObject><any>observable, dirty);
                    return observable;
                }
                // Creates the object from the given constructor
                if (options.cons) {
                    return new options.cons();
                }
            }
            return null;
        }

    }

}
