module WebMap.Client {

    export interface TryGetValueResult<T> {
        HasValue: boolean;
        Value: T;
    }

    export interface IStorageSerializer {
        Serialize(obj: any);
    }

    export interface IPromise {
    }

    export interface DictionaryStringTo<TV> {
        [uniqueID: string]: TV;
    }

    export interface AppActionOptions {
        controller?: string;
        action?: string;
        mainobj: IStateObject;
        parameters?: DictionaryStringTo<any>;
        useblockui?: boolean;
        dialogResult?: string;
    }

    export interface Message {
        UniqueID: string;
        Text: string;
        Buttons?: number;
        Icons?: number;
        Continuation_UniqueID?: string;
        Caption?: string;

    }

    // Defines an Inversion of control container used to resolve object instances.

    export interface IIocContainer {
        Resolve(options?: any): any;
    }


    interface Window {
        WebMapClientModels: DictionaryStringTo<IStateObject>;
        timeoutForActions: any;
    }

    // Defines an object that defines state and is uniquely identified by an ID
    export interface IStateObject {
        UniqueID: string;
    }

    // Defines a view object (Views and ViewModels are both defined as IViewModel).
    export interface IViewModel extends IStateObject {
        Name: string;
    }

    // Defines and object that contains "logic"to attend IViewModel actions.
    export interface ILogicView<T extends IViewModel> {
        // The IViewModel object to handle
        ViewModel: T;
        // Intializes this object with the given actions.
        Init(): JQueryPromise<any>;
        // Closes the view elements.
        Close(): void;
    }

    // Defines an object responsible for dealing with the views cycle (show, hide, close them).
    export interface IViewManager {
        //void SetAsAlwaysOnTop(IViewModel view);
        //void BringToFront(IViewModel view);

        /**
          * Closes a view in the client side.  This method handles form closing when the user clicks
          * the close box control and no binding to server side events has been added.
          */
        CloseView(view: IViewModel);

        /**
          * Fills up the JSONWebMapRequest object with the delta information provided by the
          * ViewManager
          */
        PrepareDelta(requestInfo: JSONWebMapRequest);

        // Removes the views indicated byt the "RemovedViews" of the data object
        RemoveViews(data): void;

        // Updates the given view with the information sent from the server side.
        UpdateView(view: IViewModel, viewInfo: any): void;

        // Navigates to the view matching the given IViewModel.  Navigating implies to display a 
        // window containing the view information and intantiating its matching ILogicView object
        NavigateToView(viewModel: IViewModel): JQueryPromise<any>;

        // Not used
        ShowMessage(message: string, caption: string, buttons, boxIcons): IPromise;

        // Loads the view associated to the given ILogicView object.  Loading a view implies to 
        // execute a html request to get the speficic html to render.
        LoadView(logic: ILogicView<IViewModel>,domElement:any): JQueryPromise<any>;

        //void SetCurrentField(object control);
        getConstructor(vm: IViewModel): () => ILogicView<IViewModel>;
    }
}