module WebMap.Client {

    // Prints a message to the console for debugging purposes
    export function debug(e) {
        if (console) {
            console.error(e);
        }
    }

    // Checks that the given id is a new view
    export function IsNewView(id: string, VD): boolean {
    if (VD && VD.NewViews) {
        for (var i = 0; i < VD.NewViews.length; i++) {
            if (VD.NewViews[i] == id)
                return true;
        }
    }
    return false;
    }


    export function showMessage(msg:string) : void
    {
        var w = $("<div> <span> " + msg + "<span></div>").kendoWindow();
        var kendowWindow: kendo.ui.Window = w.data("kendoWindow");
        kendowWindow.open();
    }

}

module WebMap.Utils {

    export function IsClassSet(classes: string, cls: string): boolean {
        var idx = classes.indexOf(cls);
        var len = cls.length;
        return (idx == 0 && (classes.length == len || classes[len] == ' ')) || (idx > 0 && classes[idx - 1] == ' ' && ((idx += len) == classes.length || classes[idx] == ' '));
    }

    export function RemoveClass(classes: string, cls: string): string {
        var len = cls.length;
        var idx = classes.indexOf(cls);
        if (idx >= 0) {
            var idx2 = idx + len;
            var before = "", after = "";
            if (idx > 0) {
                if (classes[idx - 1] == ' ')
                    idx--;
                else
                    return classes;
                before = classes.substring(0, idx);
            }
            if (idx2 < classes.length) {
                if (classes[idx2] == ' ')
                    idx2++;
                else
                    return classes;
                after = classes.substring(idx2);
            }
            return (before + ' ' + after).trim();
        }
        return classes;
    }

}

module WebMap.Kendo {

    export function setupBinding(binding: kendo.data.Binder, previousBinding: kendo.data.Binder, setups: BindingSetup[]): void {
        for (var i = 0, len = setups.length; i < len; i++) {
            var s = setups[i];
            if (s.setup(binding))
                return;
        }
        if (previousBinding) {
            binding.refresh = previousBinding.refresh;
            binding.destroy = previousBinding.destroy;
        }
    }

    export class BindingSetup {
        public setup(binding: kendo.data.Binder): boolean {
            return false;
        }
    }
}
