
// Module
module WebMap.Client {

    // Defines a class that contains information about the View changes.  This class
    // is used to interchange data between client and server calls.
    export class ViewState {
        LoadedViews: IViewModel[];  // List of loaded views
    }


    export class JSONWebMapRequest {
        public JSONWebMapRequest() {
        }
        public dirty: DictionaryStringTo<any>;
        public vm: string;
        public closedViews: string[];
        public parameters: DictionaryStringTo<any>;
        public dialogResult: string;
    }

    // not in use
    export class ViewInfo {
        public UniqueID: string;
        public ZOrder: number;
        public Visible: boolean;
    }

    // not in use
    export class ViewsState {
        public LoadedViews: ViewInfo[];

    }

}
