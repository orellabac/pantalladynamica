/// <reference path="WebMap_Storage.ts" />

// Module
module WebMap.Client {

    export class StateCache {
        static Current: StateCache;
        private _storageManager: StorageManager;
        private _cache: DictionaryStringTo<IStateObject>;
        private _tracker: DeltaTracker;


        static Init() {
            if (!StateCache.Current) {
                StateCache.Current = new StateCache();
            }
        }

        constructor() {
            this._cache = {};
            this._tracker = new DeltaTracker();
        }


        organize(options?: any): void {
            var cacheEntries = Object.keys(this._cache);


            var cachedEntryKeyCount = -1;

            while (cacheEntries.length && cachedEntryKeyCount != cacheEntries.length) {
                cachedEntryKeyCount = cacheEntries.length;
                for (var i = 0; i < cacheEntries.length; i++) {//cachedEntryKey in cacheEntries) {
                    var cachedEntryKey = cacheEntries[i];
                    if (cachedEntryKey.indexOf("###") === -1) continue;
                    var value = this._cache[cachedEntryKey];
                    //We need to link data an entry
                    var accessPath = cachedEntryKey.split("###").reverse();
                    var root = this._cache[accessPath.shift()];
                    var field = accessPath.pop();
                    while (root && accessPath.length > 0) {
                        var shifted = accessPath.shift()
                        if (!(shifted in root) && shifted == "_items") {
                            root = root["Items"];
                        } else {
                            root = root[shifted];
                        }
                    }
                    if ((<any>root) instanceof kendo.data.ObservableArray) {
                        //This is a control array and field must be an index
                        //but this entries must be collected and then pushed in order
                        //the field holds the index, we also have the value and the owner control array
                        //for an owner we will collect the pairs of values and array index
                        cacheEntries[i] = undefined;
                    }
                    else {
                        if (root && (<any>root).set) {
                            if (field === "_items") {
                                (<any>root).set("Items", value);
                            }
                            else {
                                (<any>root).set(field, value);
                            }
                            cacheEntries[i] = undefined;

                        }
                    }
                }
                cacheEntries = cacheEntries.filter(function (x) { return x != undefined; });
            }
            if (options && options.itemsCollections) {
                for (var i = 0; i < options.itemsCollections.length; i++) {
                    var curr = options.itemsCollections[i];
                    var parent = this._cache[curr.Parent];
                    var items = this._cache[curr.Items];
                    if (parent && items) {
                        (<any>parent).set("Items", items);
                    }
                }
            }
        }

        processObject(obj: IStateObject): void {
            if (obj && obj.UniqueID) {
                this._cache[obj.UniqueID] = <IStateObject>obj;
                for (var prop in obj) {
                    this.processObject(obj[prop]);
                }
            }
        }

        public addNewObject(obj: IStateObject, markAsDirty?: boolean): any {

            this.processObject(obj);
            this._cache[obj.UniqueID] = <IStateObject>obj;
            if (markAsDirty === undefined)
                markAsDirty = true;
            this._tracker.attachModel(obj, markAsDirty);
            return obj;
        }

        public getDirty(): DictionaryStringTo<any> {
            var res: DictionaryStringTo<any> = {};

            for (var entryKey in this._cache) {
                var obj = this._cache[entryKey];

                var delta = this._tracker.getCalculatedDeltaFor(obj);
                if (delta) {
                    res[obj.UniqueID] = delta;
                }
            }
            if (Object.keys(res).length == 0)
                return undefined;
            this._tracker.start();
            return res;
        }

        public getObjectLocal(uniqueId: string): IStateObject {
            return this._cache[uniqueId];
        }


        public clearDirty() {
            this._tracker.start();
        }

        public getObject(uniqueId: string): JQueryPromise<any> {
            var def = $.Deferred();
            var obj: any = null;
            if ((obj = this._cache[uniqueId])) {
                def.resolve(obj);
            }
            else {
                //Cache failed!. Go to storage to see if can be recovered from there    
                if (this._storageManager) {
                    this._storageManager.TryGetValue(uniqueId)
                        .then(function (tmp: TryGetValueResult<any>) {
                            if (tmp.HasValue) {
                                obj = tmp.Value;
                                this._cache[uniqueId] = obj;
                                this._tracker.attachModel(obj);
                                def.resolve(obj);
                            }
                            else def.resolve(null);
                        });
                }
                else {
                    def.resolve(null);
                }

            }
            return def.promise();
        }

        // Removes all elements belonging to a specific view
        public RemoveView(id: string) {
            var cacheEntries = Object.keys(this._cache);
            delete this._cache[id];
            var key = "###" + id;
            for (var i = 0; i < cacheEntries.length; i++) {//cachedEntryKey in cacheEntries) {
                if (cacheEntries[i].lastIndexOf(key) != -1) {
                    delete this._cache[cacheEntries[i]];
                }
            }

        }

    }

}
