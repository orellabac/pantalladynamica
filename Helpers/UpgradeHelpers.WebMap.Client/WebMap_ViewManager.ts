/// <reference path="WebMap_Interfaces.ts" />
/// <reference path="WebMap_Model.ts" />

module WebMap.Client {

    export class ViewManager implements IViewManager {

        private _logic: DictionaryStringTo<ILogicView<IViewModel>>;
        public closedViews : string[];

        constructor() {
            this._logic = <DictionaryStringTo<ILogicView<IViewModel>>>  {};
            this.closedViews = [];
        }

        CloseView(view: IViewModel) {
            this.closedViews[this.closedViews.length] = view.UniqueID; 
            this.DisposeView(view.UniqueID);
        }

        PrepareDelta(requestInfo: JSONWebMapRequest) {
            requestInfo.closedViews = this.closedViews;
        }

        UpdateView(view: IViewModel, viewInfo: any): void  {
            var viewId = view.UniqueID;
            // if the view has been removed lets ignored the update process
            var isRemoved = this.IsRemovedView(viewId, viewInfo);
            if (!isRemoved) {
                // lets add the code to refresh the view state here
            }
        }

        RemoveViews(data) : void {
            if (data.RemovedViews) {
                for (var i = 0; i < data.RemovedViews.length; i++) {
                    var viewModelId = data.RemovedViews[i];
                    this.DisposeView(viewModelId);
                }
            }
        }

        NavigateToView(viewModel: IViewModel): JQueryPromise<any> {
            var logic = <BaseLogic<IViewModel>> this._logic[viewModel.UniqueID];
            if (!logic) {
                logic = Container.Current.Resolve({ "cons": this.getConstructor(viewModel) });
                this._logic[viewModel.UniqueID] = logic
            }
            logic.ViewModel = viewModel;
            (<any>viewModel).ViewModel = viewModel;
            (<any>viewModel).logic = logic;
            this._logic[viewModel.UniqueID] = logic
            return logic.Init();
        }

        ShowMessage(message: string, caption: string, buttons, boxIcons): IPromise {
            return null;
        }
        
        private static DESIGNERSUPPORT: string = '<designersupport/>';

        public static removeDesignerSupportSnippet(document: string): string {
            var index = -1;
            if ((index = document.indexOf(ViewManager.DESIGNERSUPPORT)) != -1)
            {
                document = document.substring(index + ViewManager.DESIGNERSUPPORT.length);
            }
            return document;
        }

        LoadView(logic: ILogicView<IViewModel>,domElement:any=undefined): JQueryPromise<any> {

            
            var isLib: boolean = <boolean> logic["isLib"];
            var viewName = logic.ViewModel.Name;
            if (logic["usercontrol"]) {
                viewName = viewName.substring(viewName.lastIndexOf(".") + 1);
            }


            var cssUrl = ViewManager.getViewHtmlURL(viewName, ".css", isLib);
            var that = this;
            ViewManager.AddCssIfMissing(cssUrl);
            console.log("LoadView 1. " + viewName);
            var def = 
                $.get(ViewManager.getViewHtmlURL(viewName, ".html", isLib)).then(
                    function (document: string) {
                        try {
                            console.log("Laod View 2. " + viewName);
                                document = ViewManager.removeDesignerSupportSnippet(document);
                                var template = kendo.template(document);
                                var appliedTemplate = template(logic.ViewModel);
                                var isUserControl = logic["usercontrol"];
                                if (isUserControl) {
                                    var ucDOMId = domElement;
                                    var uc = $(ucDOMId);
                                    if (uc.length > 0) {
                                        uc.append(appliedTemplate);
                                    }
                                }
                                else {
                                    if (logic.ViewModel['IsMdiParent']) {
                                        $("#mainContent").replaceWith(appliedTemplate);
                                    }
                                    else {
                                        $("#mainContent").append(appliedTemplate);
                                    }
                                }
                            }
                            catch (e) {
                                debug(e);
                                throw e;
                            }
                        })
            return def;
        }

        public static AddCssIfMissing(href) : void {
            if (!$("head link[href='" + href + "']").length) {
                var link = $("<link rel='stylesheet' href='" + href + "'>");
                $("head").append(link);
            }
        }

        // Disposes the given view by removing the ILogicView object and calling its Close method.
        private DisposeView(viewModelId: string): void {
            var baseLogic = <BaseLogic<IViewModel>> this._logic[viewModelId];
            delete this._logic[viewModelId];
            baseLogic.Close();
        }

        // Gets a lambda responsible for creating a new ILogicView<IViewModel> object for the given
        // view model
        public getConstructor(vm : IViewModel): () => ILogicView<IViewModel> {
            var root = window;
            var current = null;
            if (vm.Name) {
                var name: string = vm.Name;
                if (name.indexOf('#') > 0) {
                    name = name.substring(name.indexOf('#')+1);
                }
                var parts = name.split(".");
				for (var i = 0; i < parts.length; i++) {
					var currentPart = parts[i];
					if (root === undefined) {
						throw "Error while looking for constructor of " + vm.Name + " possible cause is that the JS code for that class has not been loaded, if plugins used check if the script is processed";
					}
                    current = root[currentPart];
                    root = current;
                }
            }
            return current;
        }


        public static getViewHtmlURL(viewName, extension: string, isLib: boolean = false): string {
            if (isLib) {
                return document.location.protocol + "//" +
                    document.location.hostname + ":" +
                    document.location.port + document.location.pathname + "Resources/libs/" + viewName + extension;
            }
            else {
                return document.location.protocol + "//" +
                    document.location.hostname + ":" +
                    document.location.port + document.location.pathname + "Resources/" + viewName + extension;
            }
        }

        // Checks that the given id is a removed view
        private IsRemovedView(id: string, VD): boolean {
            if (VD && VD.RemovedViews) {
                for (var i = 0; i < VD.RemovedViews.length; i++) {
                    if (VD.RemovedViews[i] == id)
                        return true;
                }
            }
            return false;
        }
    }
}
