﻿/// <reference path="WebMap_Interfaces.ts" />
/// <reference path="WebMap_Helpers.ts" />

module WebMap.Client {

    // Implements an ILogicView<T> interface.  
    export class BaseLogic<TViewModel extends IViewModel> implements ILogicView<TViewModel>
    {
        public ViewModel: TViewModel;
        private timersToCleanup: any[] = []; 

        Close(): void {
            var domID = this.getDOMID()
            if (!this.ViewModel['IsMdiParent']) {
                $(domID).data("kendoWindow").close();
                $(domID).data("kendoWindow").destroy();
            }
            else {
                window.close();
            }
            this.CleanupTimers();
            StateCache.Current.RemoveView(this.ViewModel.UniqueID);
        }

        Init(domElement:any = undefined): JQueryPromise<any> {
            var that = this;
            // let's get the associated view from the ViewManager (it could imply a web call) and then
            // let's initalize it!
            var def = App.Current.ViewManager.LoadView(that,domElement);
            def.then(function () { that.CoreInit(domElement); });
            return def;
        }



        public RegisterTimer(timerInfo: any) {
            this.timersToCleanup.push(timerInfo);
        }

        public generic_Click(event): JQueryPromise<any> {
            if (event && event.data && event.target) {
                var options: AppActionOptions =
                    {
                        controller: event.data.Name.split(".").join("/"),
                        action: event.target.id + "_Click",
                        mainobj: <IStateObject>event.data
                    };
                App.Current.sendAction(options);
            }
            else {
                var def = $.Deferred();
                def.resolve();
                return def.promise();
            }
        }

        // Gets the id of the DOM object associated to the view
        private getDOMID(): string {
            return "#" + this.ViewModel.UniqueID;
        }

        CloseWindowLocally(e): void {
            if (e.userTriggered) {
                var id = this.getDOMID();
                App.Current.ViewManager.CloseView(this.ViewModel);
            }
        }     

        // Intializes the view elements using the given options
        private CoreInit(domElement:any) : void {

            var isUserControl = this["usercontrol"];
            if (isUserControl) {
                var ucDOMId = domElement;
                kendo.bind($(ucDOMId), this.ViewModel);
                return;
            }

            var domID = this.getDOMID();

            var width = $(domID).attr("data-width");
            var height = $(domID).attr("data-height");
            // renders the kendoWindow
            if (!this.ViewModel['IsMdiParent']) {
                if (width && height) {
                    $(domID).kendoWindow({ width: width, height: height, resizable: false });
                    $(domID).css("overflow", "hidden");  // fixes an extra scrollbar in chrome
                }
                else {
                    $(domID).kendoWindow();
                    $(domID).css("overflow", "hidden"); // fixes an extra scrollbar in chrome
                }
            }
			
            // binds the html element identified by gotten domID with the view model
            kendo.bind($(domID), this.ViewModel);

            //Set focus 
            $(domID + " *[tabindex=\"1\"]").first().focus();

            // lets add a close event if not added at migration time
            if (!this.ViewModel['IsMdiParent']) {
                var kendoInfo = $(domID).data("kendoWindow");
                if (!kendoInfo) {
                    throw "Possible initialization error. A kendoWindow was expected but it was not found";
                }
                var that = this;
                if (kendoInfo && !kendoInfo._events || !kendoInfo._events.close) {
                    kendoInfo.bind("close", (e) => {
                        if (e.userTriggered) {
                            var id = that.getDOMID();
                            App.Current.ViewManager.CloseView(that.ViewModel);
                        }
                    });
                }
            }
        }

        private CleanupTimers(): void {
            for (var i = 0; i < this.timersToCleanup.length; i++) {
                var timerInfo = this.timersToCleanup[i];
                if (timerInfo && timerInfo.clearTimer) {
                    timerInfo.clearTimer();
                }

            }
        }

    }


}
