var WebMap;
(function (WebMap) {
    /// <reference path="jquery.d.ts" />
    (function (Client) {
        var ViewState = (function () {
            function ViewState() {
            }
            return ViewState;
        })();
        Client.ViewState = ViewState;

        var BaseLogic = (function () {
            function BaseLogic() {
            }
            BaseLogic.prototype.Init = function () {
            };
            return BaseLogic;
        })();
        Client.BaseLogic = BaseLogic;

        var ViewInfo = (function () {
            function ViewInfo() {
            }
            return ViewInfo;
        })();
        Client.ViewInfo = ViewInfo;

        var ViewsState = (function () {
            function ViewsState() {
            }
            return ViewsState;
        })();
        Client.ViewsState = ViewsState;

        var ViewManager = (function () {
            function ViewManager() {
            }
            ViewManager.prototype.NavigateToView = function (logicObjectWithView) {
                ViewManager.LoadedLogicWithViews.push(logicObjectWithView);
                return null;
            };

            ViewManager.prototype.ShowMessage = function (message, caption, buttons, boxIcons) {
                return null;
            };

            ViewManager.prototype.DisposeView = function (logicObjectWithView) {
            };

            ViewManager.prototype.setupDisplay = function (viewsAsJSON) {
            };
            return ViewManager;
        })();

        var DeltaTracker = (function () {
            function DeltaTracker() {
            }
            DeltaTracker.prototype.attachModel = function (obj, markAsDirty) {
            };

            DeltaTracker.prototype.reset = function () {
            };

            DeltaTracker.prototype.start = function () {
            };

            DeltaTracker.prototype.getDeltas = function () {
            };

            DeltaTracker.prototype.wasModified = function (variable) {
                return true;
            };

            DeltaTracker.prototype.getCalculatedDeltaFor = function (variable) {
                return null;
            };
            return DeltaTracker;
        })();
        var StateCache = (function () {
            function StateCache() {
                this._cache = {};
                this._tracker = new DeltaTracker();
            }
            StateCache.prototype.addNewObject = function (obj) {
                this._cache[obj.UniqueID] = obj;
                this._tracker.attachModel(obj, true);
                return obj;
            };

            StateCache.prototype.getObjectAsync = function (uniqueId) {
                var obj = null;
                if ((obj = this._cache[uniqueId])) {
                    return obj;
                } else {
                    //Cache failed!. Go to storage to see if can be recovered from there
                    var tmp = this._storageManager.TryGetValue(uniqueId);
                    if (tmp.HasValue) {
                        obj = tmp.Value;
                        this._cache[uniqueId] = obj;
                        this._tracker.attachModel(obj);
                        return obj;
                    }
                }
                return null;
            };
            return StateCache;
        })();

        var SimpleStorageSerializer = (function () {
            function SimpleStorageSerializer() {
            }
            SimpleStorageSerializer.prototype.Serialize = function (obj) {
            };
            return SimpleStorageSerializer;
        })();

        var StorageManager = (function () {
            function StorageManager() {
            }
            StorageManager.prototype.TryGetValue = function (uniqueId) {
                return { HasValue: false, Value: null };
            };

            StorageManager.prototype.Persist = function (stateCache) {
                //for(var dirtyEntry in stateCache.DirtyEntries)
                // {
                //     var serializedValue = this._storageSerializer.Serialize(dirtyEntry.Value);
                //     SaveEntry(dirtyEntry.Value.UniqueID, serializedValue);
                // }
            };

            StorageManager.prototype.SaveEntry = function (uniqueID, serializedValue) {
            };
            return StorageManager;
        })();

        var Container = (function () {
            function Container() {
            }
            Container.prototype.Resolve = function (options) {
                if (options) {
                    if (options.vm) {
                        var observable = StateCache.Current.addNewObject(options.data);
                        return observable;
                    }
                    if (options.contructor) {
                        return new options.contructor();
                    }
                }
                return null;
            };

            Container.prototype.ResolveObject = function (options) {
            };
            return Container;
        })();
        Client.Container = Container;

        function getConstructor(vm) {
            var root = window;
            var current = null;
            if (vm.Name) {
                var name = vm.Name;
                var parts = name.split(".");
                for (var i = 0; i < parts.length; i++) {
                    current = root[parts[i]];
                    root = current;
                }
            }
        }

        var App = (function () {
            function App() {
                this._logic = {};
            }
            App.prototype.InitUI = function (viewState) {
                for (var key in this._logic) {
                    var value = this._logic[key];
                    value.Init();
                }
            };

            App.prototype.InitModels = function (models) {
                for (var i = 0; i < models.length; i++) {
                    var current = models[i];
                    var vm = Container.Current.Resolve({ vm: true, data: current });
                    var logic = Container.Current.Resolve({ "constructor": getConstructor(current) });
                    this._logic[vm.UniqueID] = logic;
                }
            };

            App.prototype.Init = function (models, viewsState) {
                //for (var i = 0; i < models.length; i++) {
                //    var currModel = models[i];
                //    AddToStateCache(currModel);
                //    // window.WebMapClientModels[currModel.UniqueID] = currModel;
                //}
                this.InitModels(models);
                this.InitUI(viewsState);
            };
            return App;
        })();
        Client.App = App;
    })(WebMap.Client || (WebMap.Client = {}));
    var Client = WebMap.Client;
})(WebMap || (WebMap = {}));
