/// <reference path="WebMap_Interfaces.ts" />
/// <reference path="WebMap_Model.ts" />

module WebMap.Client {

    interface IItemCollectionInfo {
        Parent: string;
        Items: string;
    }
    // This class represents a WebMap client side application, it is responsible for starting up the application and
    // manage the communication with de server side of a WebMap application
    export class App {

        public static Current: App;
        public ViewManager: IViewManager;
        public inActionExecution: boolean;
        public clientSynchronizationActive: boolean;
        private updateListeners: any[];
        private eventsQueue: any[];

        //private usercontrols: any[];

        // Static concstructor that creates singleton objects
        static Init() {
            if (!App.Current) {
                App.Current = new App();
                App.Current.ViewManager = new ViewManager();
            }
        }

        //  Inits the WebMap application before the first screen is rendered.  This method must be invoked from
        // the main html page just before any other action is performed.
        public Init(models, viewsState: ViewState) {
            Container.Init();
            StateCache.Init();
            this.InitModels(models);
            this.InitUI(viewsState);
            this.eventsQueue = new Array<string[]>();

        }

        public isSynchronizingClient(): any {
            return this.clientSynchronizationActive;
        }

        // Adds a listener to be called to update the model data
        public addModelUpdateListener(l: () => any) {
            if (l) {
                if (!this.updateListeners)
                    this.updateListeners = [l];
                else
                    this.updateListeners.push(l);
            }
        }

        // Removes the given listener
        public removeModelUpdateListener(l: () => any) {
            var ul = this.updateListeners;
            if (ul) {
                var idx = ul.indexOf(l);
                if (idx >= 0)
                    ul.splice(idx, 1);
            }
        }

        // Calls the registered listeners in order to update the model data
        private callModelUpdateListeners() {
            var ul = this.updateListeners;
            if (ul) {
                for (var i = 0, len = ul.length; i < len; i++)
                    ul[i]();
            }
        }



        // Sends a request to the WebMap server side application.  After call is made, the method updates the 
        // WebMap client side app in order to refresh all changes performed by server side logic, to achieve that the 
        // updateClientSide and UpdateMessages methods are called.
        public sendAction(options: AppActionOptions): JQueryPromise<any> {
            //var customMessage = options.customMessage;
            var url = this.buildActionUrl(options);
            this.callModelUpdateListeners();
            var actionParamStr = this.buildJSONRequest(options);

            if (this.inActionExecution) {
                var data = [url, actionParamStr];
                this.eventsQueue.push(data);
                var emptyPromess = $.Deferred();
                emptyPromess.resolve();
                return emptyPromess.promise();
            }
            this.inActionExecution = true;
            var jqxhr = this.doServerCall(this, url, actionParamStr, false);
            return jqxhr;
        }

        private doServerCall(that: App, url: string, actionParamStr: string, forced: boolean) : JQueryXHR {
            that.inActionExecution = true;
            var jqxhr = $.ajax({
                url: url,
                type: "POST",
                headers:
                {
                    WM: true
                },

                dataType: "json",
                data: actionParamStr,
                beforeSend: () => {
                    if (!forced)
                        (<any>window).timeoutForActions = setTimeout(function () { $.blockUI(); }, 1500);
                },

                complete: (jqXHR: JQueryXHR, textStatus: string) => {

                    if ((<any>window).timeoutForActions) {
                        clearTimeout((<any>window).timeoutForActions);
                    }
                    if (that.eventsQueue.length > 0) {
                        $.blockUI();
                        var opts = that.eventsQueue.pop();
                        that.doServerCall(that, opts[0], opts[1], true);
                    } else {
                        that.inActionExecution = false;
                        $.unblockUI();
                    }

                },

                success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                    if (data.ErrorOcurred) {
                        that.showGenericMessage(data.ExMessage + "<pre>" + data.ExStackTrace + "<pre\>");
                        return false;
                    }
                    if (that.checkFlag(data, "SessionTimeOut")) {
                        //showConfirmDialog("Session Timeout", "Your session has ended. You need to logon again");
                    }
                    else if (that.checkFlag(data, "DoLogoff")) {
                        //goHome();
                    }
                    else if (that.checkFlag(data, "CloseApp")) {
                        window.close();
                    }
                    else {
                        //TODO setViewsInOrder(data.viewsInOrder);
                        //Common.updateModules(data.modifiedModules);
                        //Common.loadViews(data.newViews);
                        //Common.removeViews(data.removedViews, data.removedViewsNames);
                        that.clientSynchronizationActive = true;
                        try {
                            that.updateClientSide(data);
                        }
                        finally {
                            that.clientSynchronizationActive = false;
                        }
                        if (data) {
                            this.updateMessages(data.VD);
                        }
                        StateCache.Current.clearDirty();
                    }
                    return false;
                },



                error: function (a, b, c) {
                    //Common.showMessagesDialog("Error while calling action", [a.responseText]);
                    return false;
                },
                contentType: 'application/json; charset=utf-8'
            });
            return jqxhr;
        }

        // updates the given model object with the information stored in delta.
        private applyDelta(model: IStateObject, delta: any) {
            for (var prop in delta) {
                if ((prop in model) && (<any>model).set) {
                    (<kendo.data.ObservableObject><any>model).set(prop, delta[prop]);
                }
            }
        }

        // Builds an url action request based in the values gieven in options.
        private buildActionUrl(options: AppActionOptions): string {
            var path1 = location.pathname;
            var path2 = options.controller || "Home";

            if (options.mainobj !== undefined && options.mainobj["area"]) {
                path2 = options.mainobj["area"] + "/" + path2;
            }

            if (path1.length > 1 && path1.substring(path1.length - 1) != '/') {
                path1 = path1 + "/";
            }
            if (path2.length > 1 && path2[0] === '/') {
                path2 = path2.substring(1);
            }

            options.action = options.action;

            var url = path1 + path2 + "/" + options.action + "/";
            return url;
        }

        // Builds a JSON request based in the values given in options
        private buildJSONRequest(options: AppActionOptions): string {
            var request = new JSONWebMapRequest();
            request.dirty = StateCache.Current.getDirty();
            if (options.mainobj) {
                request.vm = options.mainobj.UniqueID;
            }
            if (options.dialogResult) {
                request.dialogResult = options.dialogResult;
            }
            App.Current.ViewManager.PrepareDelta(request);
            request.parameters = options.parameters;

            var cache = [];
            var res = JSON.stringify(request, function (key, value) {
                if (typeof value === 'object' && value !== null) {
                    if (cache.indexOf(value) !== -1) {
                        // Circular reference found, discard key
                        return;
                    }
                    // Store value in our collection
                    cache.push(value);
                }
                return value;
            });
            cache = null; // Enable garbage collection

            return res;
        }

        private checkFlag(data: any, flagName: string): boolean {
            return false;
        }


        private extractItemsCollectionInfo(data: any[]): IItemCollectionInfo[] {
            var itemsCollections: IItemCollectionInfo[] = [];
            for (var i = 0; i < data.length; i++) {
                var current = data[i];
                if (current && current['@k']) {
                    itemsCollections.push({ Parent: current.Parent, Items: current.Items });
                    delete current['Parent'];
                    delete current['Items'];
                    delete current['@k'];
                }
            }
            return itemsCollections;
        }


        // Creates the IViewModel objects for every model object in "models".  Models is an array of json data sent from the 
        // server side, where every element contains a set of property/value describing the view model object.
        public InitModels(models: any[]) {
            var itemsCollections = this.extractItemsCollectionInfo(models);
            var controlArrays: any[] = [];
            for (var i = 0; i < models.length; i++) {
                var current = models[i];
                var vm: IViewModel = <IViewModel>Container.Current.Resolve({ vm: true, data: current, dirty: false });
                if (vm instanceof kendo.data.ObservableArray)
                    controlArrays.push({ controlArray: vm, delta: vm });
                if (vm.Name && vm.Name.indexOf("UserControl") == 0) {

                    var logic = vm["logic"] = Client.Container.Current.Resolve({ "cons": App.Current.ViewManager.getConstructor(vm) });
                    logic.ViewModel = vm;
                    //this.usercontrols = this.usercontrols || [];
                    //this.usercontrols.push(logic);
                }
            }
            this.syncControlArraysSize(controlArrays);
            var options = undefined;
            if (itemsCollections.length) {
                options = { itemsCollections: itemsCollections };
            }
            //Child models must be connected to entry in the cache
            StateCache.Current.organize(options);
        }

        // Initializes the UI object for the first time.  This method must be called when the user first access the 
        // WebMap app and it is responsible for creating the first loaded windows (main window and/or login screen probably!).
        // This method must be called after InitModels method because it requires view model to be already set.
        // viewState :  Json object sent from server side containing the information of loaded views.
        private InitUI(viewState: ViewState) {

            var that = this;
            this.showLoadingSplash();
            var def = $.Deferred();
            // let's iterate the loaded views in order to load them
            for (var i: number = 0; i < viewState.LoadedViews.length; i++) {
                var current: IViewModel = viewState.LoadedViews[i];
                // gets the matching view model object from the state cache and then navigates to the view.
                var awaitLogic = StateCache.Current.getObject(current.UniqueID).then(
                    function (viewModel: IViewModel) {
                        console.log("Init promess from InitUI for" + viewModel.Name); 
                        if (!viewModel) {
                            WebMap.Client.debug("FATAL ERROR: View model is null");
                            var ret = $.Deferred();
                            def.resolve({});
                            return def.promise();
                        }
                        return that.ViewManager.NavigateToView(viewModel);
                    });

                def.then(function () { return awaitLogic; });
            }

            //if (this.usercontrols) {
            //    for (var i: number = 0; i < this.usercontrols.length; i++) {
            //        var userControl = this.usercontrols[i];
            //        def.then(function () {
            //            console.log("Init promess from InitUI usercontrol" ); 
            //            userControl.Init();
            //        });
                    
            //    }
            //    this.usercontrols = undefined;
            //}


            def.resolve();
            def.done(this.removeLoadingSplash).done(this.updateMessages(viewState));
            return def.promise();
        }

        // removes the blocking UI message
        private removeLoadingSplash(): void {
            $.unblockUI();
        }

        // shows a blocking UI message
        private showLoadingSplash(): void {
            $.blockUI({ message: "Loading app views" });
        }

        // Updates the interaction messages sent from the server side in order to show them to the 
        // end user
        private updateMessages(viewData: any): JQueryPromiseCallback<any> {
            if (viewData && viewData.Messages) {
                var messageList = viewData.Messages;
                for (var i = 0; i < messageList.length; i++) {
                    var msg = <Message>messageList[i];
                    this.showMessageDialog(msg);
                }
            }
            return null;
        }


        private showGenericMessage(messageText: string): void {

            var msg = <Message> { UniqueID:"generic", Text: messageText, Buttons: 1, Caption: "Exception Occurred" };
            var msgBoxTemplate = this.preparteMessageBoxTemplate(msg);

            // Create the window to be displayed
            var w = $(msgBoxTemplate).kendoWindow(
                {
                    title: msg.Caption ? msg.Caption : "",
                    modal: true,
                    resizable: false
                }
                );
            var kendowWindow: kendo.ui.Window = w.data("kendoWindow");
            kendowWindow.center().open();

            // Add handlers to close the window
            (<any>w).find('.msgboxokbuttoncls,.msgboxcancelbuttoncls,.msgboxyesbuttoncls,.msgboxnobuttoncls')
                .click(function () {
                    var dialogResult = "cancel";
                    if ($(this).hasClass('msgboxokbuttoncls')) {
                        dialogResult = "ok";
                    } else if ($(this).hasClass('msgboxyesbuttoncls')) {
                        dialogResult = "yes";
                    } else if ($(this).hasClass('msgboxnobuttoncls')) {
                        dialogResult = "no";
                    } else {
                        dialogResult = "cancel";
                    }
                    (<any>window).app.sendAction({ controller: "ResumeOperation", action: "ResumePendingOperation", dialogResult: dialogResult });
                    (<any>w).data('kendoWindow').close();
                });
        }


        private showMessageDialog(msg: Message): void {

            var msgBoxTemplate = this.preparteMessageBoxTemplate(msg);

            // Create the window to be displayed
            var w = $(msgBoxTemplate).kendoWindow(
                {
                    title: msg.Caption ? msg.Caption : "",
                    modal: true,
                    resizable: false
                }
                );
            var kendowWindow: kendo.ui.Window = w.data("kendoWindow");
            kendowWindow.center().open();

            // Add handlers to close the window
            (<any>w).find('.msgboxokbuttoncls,.msgboxcancelbuttoncls,.msgboxyesbuttoncls,.msgboxnobuttoncls')
                .click(function () {
                    var dialogResult = "cancel";
                    if ($(this).hasClass('msgboxokbuttoncls')) {
                        dialogResult = "ok";
                    } else if ($(this).hasClass('msgboxyesbuttoncls')) {
                        dialogResult = "yes";
                    } else if ($(this).hasClass('msgboxnobuttoncls')) {
                        dialogResult = "no";
                    } else {
                        dialogResult = "cancel";
                    }
                    (<any>window).app.sendAction({ controller: "ResumeOperation", action: "ResumePendingOperation", dialogResult: dialogResult });
                    (<any>w).data('kendoWindow').close();
                });
        }

        private preparteMessageBoxTemplate(msg: Message): string {
            var msgBoxTemplate = "<div class='wmmsgbox __additional_classes__'>" +
                "<span class='msgboxiconscls' ></span><span class='msgboxmsgnclass'> " + msg.Text + "</span > " +
                "<div style='text-align:center'>";
            var iconClass = this.getMessageBoxIconCssClass(msg.Icons);

            if (msg.Buttons == 0 || msg.Buttons == 1) {
                msgBoxTemplate = msgBoxTemplate + "<button class='msgboxokbuttoncls' > OK </button > ";
            }
            if (msg.Buttons == 3 || msg.Buttons == 4) {
                msgBoxTemplate = msgBoxTemplate + "<button class='msgboxyesbuttoncls' > Yes </button > ";
                msgBoxTemplate = msgBoxTemplate + "<button class='msgboxnobuttoncls'>No</button>";
            }
            if (msg.Buttons == 5) {
                msgBoxTemplate = msgBoxTemplate + "<button class='msgboxretrybuttoncls'>Retry</button>";
            }
            if (msg.Buttons == 1 || msg.Buttons == 3 || msg.Buttons == 5) {
                msgBoxTemplate = msgBoxTemplate + "<button class='msgboxcancelbuttoncls'>Cancel</button>";
            }
            msgBoxTemplate = msgBoxTemplate + "</div>";
            msgBoxTemplate = msgBoxTemplate.replace('__additional_classes__', iconClass);

            return msgBoxTemplate;
        }

        private getMessageBoxIconCssClass(id: number): string {
            var iconClass = "";
            switch (id) {
                case 1:
                    iconClass = "msgboxquestion";
                    break;
                case 2:
                    iconClass = "msgboxwarning";
                    break;
                case 3:
                    iconClass = "msgboxerror";
                    break;
            }
            return iconClass;
        }

        private extractItemsCollectionInfoFromDeltas(MD: any): IItemCollectionInfo[] {
            var alldeltas = [];
            for (var i = 0; i < MD.length; i++) {
                alldeltas.push(MD[i].Delta);
            }
            return this.extractItemsCollectionInfo(alldeltas);
        }


        private syncControlArraysSize(controlArrays: any[]) {
            var element: IStateObject;
            for (var i = 0; i < controlArrays.length; i++) {
                var curr = controlArrays[i];
                var diff = curr.controlArray.Count - curr.delta.Count;
                var index;
                if (diff < 0) //New items needed
                {
                    while (diff < 0) {
                        diff = diff + 1;
                        curr.controlArray.push(StateCache.Current.getObjectLocal(0 + "###" + curr.controlArray.UniqueID));
                    }
                    for (var innerIndex = 0; innerIndex < curr.controlArray.length; innerIndex++) {
                        curr.controlArray[innerIndex] = StateCache.Current.getObjectLocal(innerIndex + "###" + curr.controlArray.UniqueID);
                    }
                    try {
                    curr.controlArray.trigger('change');
                    } catch (ex) {
                    }
                }
                else if (diff > 0) {
                    while (diff > 0) {
                        diff = diff - 1;
                        try {
                            curr.controlArray.pop();
                        } catch(ex) {
                        }
                    }
                }
                else {
                    //No new items needed?
                    if (curr.delta.Count > 0) {
                        for (var innerIndex = 0; innerIndex < curr.controlArray.length; innerIndex++) {
                            curr.controlArray[innerIndex] = StateCache.Current.getObjectLocal(innerIndex + "###" + curr.controlArray.UniqueID);
                        }
                        index = 0;
                        while (curr.controlArray.length === undefined || (curr.controlArray.length < curr.delta.Count)) {
                            var element = StateCache.Current.getObjectLocal(index + "###" + curr.controlArray.UniqueID);
                            if (!element) {
                                element = <IStateObject>{ UniqueID: "" };
                            };
                            curr.controlArray.push(element);
                            index = index + 1;
                        }
                    }

                    curr.controlArray.trigger('change');

                }
                curr.controlArray.Count = curr.delta.Count;
            }

        }



        private getDeltaWithId(uniqueID: string, deltaData: any): any {
            var result = undefined;
            if (deltaData !== undefined) {
                for (var i = 0; i < deltaData.length; i++) {
                    if (deltaData[i].UniqueID === uniqueID) {
                        result = kendo.observable(deltaData[i].Delta);
                        break;
                    }
                }
            }
            if (!result) {
                console.log('Not found:' + uniqueID);
                result = {
                    UniqueID: uniqueID
                };
            }
            return result;
        }


        private processRemovedIds(data: any): void {
            if (data.length) {
                for (var i = 0; i < data.length; i++) {
                    var id = data[i];
                    if (id in (<any>StateCache.Current)._cache) {
                        delete (<any>StateCache.Current)._cache[id];
                    }
                }
            }
        }

        private processSwitchedIds(data: any, deltaData: any): void {
            var collectionsToCheck = {};
            // the assumed format for 'data' is [ [id1_1,id1_2], [id2_1,id2_2] ... ]
            for (var i = 0; i < data.length; i++) {
                var pair = data[i];
                if (pair.length === 2) {
                    var id1 = pair[0];
                    var id2 = pair[1];
                    var id1Parts = id1.split('###');
                    var index1 = id1Parts.shift();
                    var id2Parts = id2.split('###');
                    var index2 = id2Parts.shift();
                    if (id1Parts.shift() == "_items" && id2Parts.shift() == "_items") {
                        var list1Id = id1Parts.join("###");
                        var list2Id = id2Parts.join("###");
                        // locate the collections
                        var collection1 = StateCache.Current.getObjectLocal(list1Id);
                        var collection2 = StateCache.Current.getObjectLocal(list2Id);
                        
                        // remmember this collectionto fill its gaps later
                        collectionsToCheck[list1Id] = collection1;

                        // switch the element inside the collection
                        var tmp = (<any>collection1).Items[index1];
                        (<any>collection1).Items[index2] = tmp;
                        // the origin slot is marked with 'undefined'
                        (<any>collection1).Items[index1] = undefined;
                        if (tmp) {
                            tmp.UniqueID = id2;
                        }
                        // Remove the origin slot from the cache
                        delete (<any>StateCache.Current)._cache[id1];
                        (<any>StateCache.Current)._cache[id2] = tmp;
                    }
                }
            }
            //Fill any gaps on the collections 
            for (var listId in collectionsToCheck) {
                var collection = collectionsToCheck[listId];
                for (var j = 0; j < collection.Items.length; j++) {
                    if (collection.Items[j] === undefined) {
                        var id = j.toString() + '###_items###' + list1Id;
                        var deltaObj = this.getDeltaWithId(id, deltaData);
                        if (deltaObj) {
                            collection.Items[j] = deltaObj;
                            (<any>StateCache.Current)._cache[id1] = deltaObj;
                        }
                    }
                }
            }
        }

        // This method must be called to handle the server side response to an action, it is responsible for updating
        // client side data out from server side changes.  Responsibilities include:
        // 1. Initialize view models for every new view 
        // 2. Update view model objects with server side changed information (delta)
        // 3. Display any shown view
        // 4. Update view changes (position, z-order, visibility)
        // 5. Remove any closed view
        private updateClientSide(data: any): void {
            var that = this;
            var areThereNewSubViews = false;
            if (data) {
                if (data.SessionTimeOut) {
	                this.showSeesionExpiredMessage();
                    return;
                }
                var def = $.Deferred();
                if (data.SW) {
                    this.processSwitchedIds(data.SW, data.MD);
                }
                if (data.MD) {
                    var controlArrays: any[] = [];

                    var itemsCollections = this.extractItemsCollectionInfoFromDeltas(data.MD);
                    for (var i = 0; i < data.MD.length; i++) {
                        var modelUpdateEntry = data.MD[i];

                        var delta = modelUpdateEntry.Delta;
                        var entry = StateCache.Current.getObjectLocal(modelUpdateEntry.UniqueID);
                        if (entry) {
                            if (entry instanceof kendo.data.ObservableArray)
                                controlArrays.push({ controlArray: entry, delta: delta });
                            else
                                that.applyDelta(entry, delta);
                        }
                        else {

                            areThereNewSubViews = true;
                            var vm: IViewModel = entry = <IViewModel>Container.Current.Resolve({ vm: true, data: delta, dirty: false });
                            if (vm instanceof kendo.data.ObservableArray)
                                controlArrays.push({ controlArray: vm, delta: delta });
                            if (vm.Name && vm.Name.indexOf("UserControl") == 0) {
                                var logic = vm["logic"] = WebMap.Client.Container.Current.Resolve({ "cons": App.Current.ViewManager.getConstructor(vm) });
                                logic.ViewModel = vm;
                            }
                            StateCache.Current.addNewObject(vm);
                        }
                    }
                    this.syncControlArraysSize(controlArrays);

                    if (data.VD && data.VD.NewViews) {
                        var newModels: IViewModel[] = [];
                        for (var i = 0; i < data.VD.NewViews.length; i++) {
                            var uid = data.VD.NewViews[i];
                            var obj = <IViewModel>StateCache.Current.getObjectLocal(uid);
                            if (obj) {
                                newModels.push(obj);
                            }
                            var promise = that.ViewManager.NavigateToView(obj);
                            def.then(function () { return promise; });
                        }
                    }
                    //Sets focus to the current control
                    if (data.VD && data.VD.CurrentFocusedControl) {
                        //var id = document.getElementById(data.VD.CurrentFocusedControl);
                        //id.focus();
                        $('#' + data.VD.CurrentFocusedControl).focus();
                    }

                    if (areThereNewSubViews) {
                        var options = undefined;
                        if (itemsCollections.length) {
                            options = { itemsCollections: itemsCollections };
                        }
                        //Child models must be connected to entry in the cache
                        StateCache.Current.organize(options);
                    }

                }
                def.resolve();
                if (data.VD) {
                    this.ViewManager.RemoveViews(data.VD);
                }

                if (data.RM) {
                    this.processRemovedIds(data.RM);
                }
            }
		}

		private showSeesionExpiredMessage(): void {
			var msg = <Message> { UniqueID: "generic", Text: "The application session has timeout! You must reload the application.", Buttons: 0, Caption: "Session Expired" };
			var msgBoxTemplate = this.preparteMessageBoxTemplate(msg);

			// Create the window to be displayed
			var w = $(msgBoxTemplate).kendoWindow(
			{
				title: msg.Caption ? msg.Caption : "",
				modal: true,
				resizable: false,
				width: 300
			});
			var kendowWindow: kendo.ui.Window = w.data("kendoWindow");
			kendowWindow.center().open();

			// Add handlers to close the window
			(<any>w).find('.msgboxokbuttoncls,.msgboxcancelbuttoncls,.msgboxyesbuttoncls,.msgboxnobuttoncls')
				.click(function () {
				location.reload(true);
				});

			// Add handlers to close the window
			(<any>w).find('.msgboxokbuttoncls,.msgboxcancelbuttoncls,.msgboxyesbuttoncls,.msgboxnobuttoncls').html("Reload");

		}

    }

}
