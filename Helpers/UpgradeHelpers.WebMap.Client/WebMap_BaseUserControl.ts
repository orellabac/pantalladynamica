﻿/// <reference path="kendo.all.d.ts" />

(function ($) {

	var DATABINDING = "dataBinding",
		DATABOUND = "dataBound",
		CHANGE = "change";

	// shorten references to variables. this is better for uglification 
	var kendo = (<any>window).kendo,
		ui = kendo.ui,
		Widget = ui.Widget;

	var BaseUserControl = Widget.extend({

		applyTemplate: function (value) {
			var that = this;
			var proto = that.constructor.prototype;

			if (!value["$$$parent"]) {
				var parent = value.parent();
				var ancestorPath = parent["$$$parent"] || "";
				if (parent) {
					var props = Object.getOwnPropertyNames(parent);
					for (var i = 0; i < props.length; i++) {
						var propName = props[i];
						if (parent[propName] == value) {
							if (ancestorPath == "") {
								value["$$$parent"] = propName;
							} else {
								value["$$$parent"] = ancestorPath + "." + propName;
							}
							break;
						}
					}
				}
			}

			if (!proto._compiledTemplate) {
				proto._compiledTemplate = kendo.template(proto.options.template);
			}
			this.initStyles();
			this.initClientMethods(value);
			return proto._compiledTemplate(value);
		},

		initStyles: function () {
			var that = this;
			//ADD CSS RULES DYNAMICALLY 

			var addCssRule = function (styles, id) {
				if (!document.getElementById(id)) {
					var style = document.createElement('style');
					var styleMark = document.createElement("script");
					var styleIdAtt = document.createAttribute("id");
					styleIdAtt.value = id;
					style.type = 'text/css';
					if (style.styleSheet) (<any>style.styleSheet).cssText = styles; //IE 
					else style.innerHTML = styles; //OTHERS 

					document.getElementsByTagName('head')[0].appendChild(style);
				}
			};
			addCssRule(that.options.css, '' + that.options.name);
		},

		initClientMethods: function (value) {
			var that = value;
		},

		init: function (element, options) {
			// base call to initialize widget 
			options._uiinitialized = false;
			Widget.fn.init.call(this, element, options);
		},

		refresh: function () { },

		buildUI: function () {
			var that = this;
			that.options.uiinitialized = true;
			that.setOptions(that.options);
			var that = this,
				view = that._value;

			var parent = that._value.parent();
			var ancestorPath = parent["$$$parent"] || "";
			if (parent) {
				var props = Object.getOwnPropertyNames(parent);
				for (var i = 0; i < props.length; i++) {
					var propName = props[i];
					if (parent[propName] == that._value) {
						if (ancestorPath == "") {
							that._value["$$$parent"] = propName;
						}
						else {
							that._value["$$$parent"] = ancestorPath + "." + propName;
						}
						break;
					}
				}

				var html = this.applyTemplate(view);
				//that.element.html(html); 
				// trigger the dataBinding event 
				//that.trigger(DATABINDING); 
				// mutate the DOM (AKA build the widget UI) 
				that.element.html(html);
				// trigger the dataBound event 
				//that.trigger(DATABOUND); 
			}
		},
		//MVVM framework calls 'value' when the viewmodel 'value' binding changes 
		value: function (value) {
			var that = this;
			if (value === undefined) {
				return that._value;
			}
			that._update(value);
			that._old = that._value;
		},

		_update: function (value) {
			var that = this;
			that._value = value;
			if (!that.options.uiinitialized) {
				that.buildUI();
			}
			that.refresh();
		},

		options: {
			// the name is what it will appear as off the kendo namespace(i.e. kendo.ui.UserControl1). 
			// The jQuery plugin would be jQuery.fn.kendoUserControl1. 
			name: "BaseUserControl",
			value: null,
			css: "",
			// other options go here 
			template: "<div></div>"
		},
		// events are used by other widgets / developers - API for other purposes 
		// these events support MVVM bound items in the template. for loose coupling with MVVM. 
		events: [
		// call before mutating DOM. 
		// mvvm will traverse DOM, unbind any bound elements or widgets 
			DATABINDING,
		// call after mutating DOM 
		// traverses DOM and binds ALL THE THINGS 
			DATABOUND,
			CHANGE
		],
	});
	ui.plugin(BaseUserControl);
})(jQuery);
