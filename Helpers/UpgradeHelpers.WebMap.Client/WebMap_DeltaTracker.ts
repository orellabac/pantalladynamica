// Module
module WebMap.Client {

    export class DeltaTracker {

        attachedObjs: DictionaryStringTo<IStateObject>;
        dirtyTable: DictionaryStringTo<boolean>;
        dirtyPropertiesTable: DictionaryStringTo<DictionaryStringTo<boolean>>;

        public isDirty(obj: IStateObject): boolean {
            return <boolean>(this.dirtyTable[obj.UniqueID] ||
                this.dirtyPropertiesTable[obj.UniqueID]);
        }


        constructor() {
            this.dirtyTable = {};
            this.attachedObjs = {};
            this.dirtyPropertiesTable = {};
        }
        //Update the Dirty properties table 
        updateDirtyPropertiesTable(fieldName, uid): void {
            var propertiesChangesMap = this.dirtyPropertiesTable[uid];
            if (!propertiesChangesMap) {
                this.dirtyPropertiesTable[uid] = {};
                propertiesChangesMap = this.dirtyPropertiesTable[uid]
            }
            propertiesChangesMap[fieldName] = true;

        }

        changeTracker(e): any {
            var fieldName: string = e.field;
            if (fieldName && e.sender.UniqueID) {
                var uid = e.sender.UniqueID;// e.sender.get(actualSenderNameParts.join('.')).UniqueID;
                this.updateDirtyPropertiesTable(fieldName, uid);
            }
        }



        _changeDelegate: any;

        public attachModel(obj: IStateObject, markAsDirty?: boolean): void {
            this.attachedObjs[obj.UniqueID] = obj;
            var events = obj["_events"];
            var that = this;
            if (!this._changeDelegate) {
                this._changeDelegate = (e) => {
                    if (!((e.action && e.action == "itemchange") || (e.field && e.field.indexOf('.') > -1))) { //Filter the itemchange action
                        that.changeTracker(e);
                    }
                };
            }

            if (events && events.change) {
                var found: boolean = false;
                for (var i = 0; i < events.change.length; i++) {
                    if (events.change[i] == this.changeTracker)
                        found = true;
                }
                if (!found) {
                    (<kendo.Observable><any>obj).bind("change", this._changeDelegate);
                }
            }
            else {

                (<kendo.Observable><any>obj).bind("change", this._changeDelegate);
            }
            if (markAsDirty) {
                this.dirtyTable[obj.UniqueID] = true;
            }

        }

        public reset(): void {

        }

        public start(): void {
            this.dirtyTable = {};
            this.dirtyPropertiesTable = {};
        }

        public getDeltas(): void {

        }

        wasModified(variable: IStateObject): boolean {
            return true;
        }

        getJSONFromFullObject(obj): any {
            var value = obj.toJSON();
            for (var prop in value) {
                var elementUniqueID: IStateObject;
                if (value[prop] && (elementUniqueID = value[prop]) && elementUniqueID.UniqueID) {
                    //this is another viewmode so just decouple
                    delete value[prop];
                }
            }
            return value;
        }

        getCalculatedDeltaFor(variable: IStateObject): any {
            var isDirty = this.dirtyTable[variable.UniqueID];
            if (isDirty)
                return this.getJSONFromFullObject(variable);
            var hasDirtyProperties = this.dirtyPropertiesTable[variable.UniqueID];
            if (hasDirtyProperties) {
                var delta = {};
                for (var prop in hasDirtyProperties) {
                    delta[prop] = variable[prop];
                }
                return delta;
            }
            return undefined;
        }
    }

}
