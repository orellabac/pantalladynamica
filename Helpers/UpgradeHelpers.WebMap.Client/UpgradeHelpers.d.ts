declare module UpgradeHelpers {
    class ControlArray {
        static indexOf(ctrlArray: any, e: any): number;
    }
    class Events {
        static getEventSender(e: any): any;
    }
    class EventKey {
        static getKeyChar(e: any): string;
        static setKeyChar(e: any, key: string): void;
        static getKeyCode(e: any): number;
        static setKeyCode(e: any, key: number): void;
        static handleEvent(e: any, value: boolean): void;
    }
    enum Keys {
        A = 65,
        Add = 43,
        Alt = 18,
        B = 66,
        Back = 8,
        C = 67,
        Capital = 20,
        CapsLock = 20,
        Clear = 46,
        Control = 17,
        ControlKey = 17,
        D = 68,
        D0 = 48,
        D1 = 49,
        D2 = 50,
        D3 = 51,
        D4 = 52,
        D5 = 53,
        D6 = 54,
        D7 = 55,
        D8 = 56,
        D9 = 57,
        Decimal = 44,
        Delete = 46,
        Divide = 47,
        Down = 40,
        E = 69,
        End = 35,
        Enter = 13,
        Escape = 27,
        F = 70,
        F1 = 112,
        F10 = 121,
        F11 = 122,
        F12 = 123,
        F2 = 113,
        F3 = 114,
        F4 = 115,
        F5 = 116,
        F6 = 117,
        F7 = 118,
        F8 = 119,
        F9 = 120,
        G = 71,
        H = 72,
        Home = 36,
        I = 73,
        Insert = 45,
        J = 74,
        K = 75,
        L = 76,
        LControlKey = 17,
        Left = 37,
        LShiftKey = 16,
        M = 77,
        Multiply = 42,
        N = 78,
        NumLock = 144,
        NumPad0 = 48,
        NumPad1 = 49,
        NumPad2 = 50,
        NumPad3 = 51,
        NumPad4 = 52,
        NumPad5 = 53,
        NumPad6 = 54,
        NumPad7 = 55,
        NumPad8 = 56,
        NumPad9 = 57,
        O = 79,
        P = 80,
        PageDown = 34,
        PageUp = 33,
        Q = 81,
        R = 82,
        RControlKey = 17,
        Return = 13,
        Right = 39,
        RShiftKey = 16,
        S = 83,
        Shift = 16,
        ShiftKey = 16,
        Space = 32,
        Subtract = 45,
        T = 84,
        Tab = 9,
        U = 85,
        Up = 38,
        V = 86,
        W = 87,
        X = 88,
        Y = 89,
        Z = 90,
    }
    class Sound {
        static getBeep(): Sound;
        public Play(): void;
    }
    class Strings {
        static convertToString(obj: any): string;
        static format(obj: any, format: string): string;
    }
}
