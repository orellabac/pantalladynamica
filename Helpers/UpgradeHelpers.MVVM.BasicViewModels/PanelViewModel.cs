﻿using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
	/// <summary>
	/// Represents a view model that deals with panel in the view.
	/// </summary>
    public class PanelViewModel : IDependantViewModel
	{
		public string UniqueID { get; set; }
		public void Build(IIocContainer ctx)
		{
			// Enabled DefaultValue
			Enabled = true;

			// Visible DefaultValue
			Visible = true;
		}

		#region Data Members

		/// <summary>
		/// Gets or sets a value indicating whether in the view the element that represents this model can respond to user interaction
		/// </summary>
		public virtual bool Enabled { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether in the view the element that represent this model is displayed
		/// </summary>
		public virtual bool Visible { get; set; }

		/// <summary>
		/// Gets or sets the tab order of the element in the view that  that will represent this model 
		/// </summary>
		public virtual int TabIndex { get; set; }

		/// <summary>
		/// Gets or sets the background color that will be used by the element tin
		/// the view that will represent this model
		/// </summary>
		public virtual Color BackColor { get; set; }

		/// <summary>
		/// Gets or sets the foreground color that will be used by the element tin
		/// the view that will represent this model
		/// </summary>
		public virtual Color ForeColor { get; set; }

		/// <summary>
		/// Gets or sets the text associated with this model
		/// </summary>
		public virtual string Text { get; set; }

		public virtual object Tag { get; set; }
		#endregion
	}
}
