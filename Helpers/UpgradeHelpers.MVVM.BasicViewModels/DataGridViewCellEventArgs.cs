﻿
using System;
namespace UpgradeHelpers.BasicViewModels
{
    /// <summary>
    /// Provides data to events related to row an cell events
    /// </summary>
    public class DataGridViewCellEventArgs : EventArgs
    {
        private int columnIndex;
        private int rowIndex;

        public DataGridViewCellEventArgs(int columnIndex, int rowIndex)
        {
            this.columnIndex = columnIndex;
            this.rowIndex = rowIndex;
        }
    }
}
