using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
	/// <summary>
	/// Manages a the state for a related set of tab pages
	/// </summary>
	public class TabControlViewModel : ViewModelCollection<TabPageViewModel>
	{
		/// <summary>
		/// Returns a name that will be used in to view that presents this model
		/// </summary>
		public virtual string Name { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether in the view the element that represents this model can respond to user interaction
		/// </summary>
		public virtual bool Enabled { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether in the view the element that represent this model is displayed
		/// </summary>
		public virtual bool Visible { get; set; }

		/// <summary>
		/// Gets or sets the font of the text that the view will use to display text
		/// </summary>
		public virtual Font Font { get; set; }

		/// <summary>
		/// Gets or sets the distance in pixels between the top edge of the the element of the view that represents this model 
		/// and the top edge of the element in the view that contains it
		/// and the
		/// </summary>
		public virtual int Top { get; set; }

		/// <summary>
		/// Gets or sets the distance in pixels, between the 
		/// left edge of the element in the view that represents this model 
		/// and the left edge of the element of the view that contains it
		/// </summary>
		public virtual int Left { get; set; }

		/// <summary>
		/// Gets or sets the height in pixels for the element in the view that will represent this model
		/// </summary>
		public virtual int Height { get; set; }

		/// <summary>
		/// Gets or sets the Width in pixels for the element in the view that will represent this model
		/// </summary>
		public virtual int Width { get; set; }

		/// <summary>
		/// Gets or sets the tab order of the element in the view that  that will represent this model 
		/// </summary>
		public virtual int TabIndex { get; set; }

		/// <summary>
		/// Gets or sets the background color that will be used by the element tin
		/// the view that will represent this model
		/// </summary>
		public virtual Color BackColor { get; set; }

		/// <summary>
		/// Gets or sets the foreground color that will be used by the element tin
		/// the view that will represent this model
		/// </summary>
		public virtual Color ForeColor { get; set; }

		/// <summary>
		/// Gets or sets generic data
		/// </summary>
		public virtual string Tag { get; set; }

		/// <summary>
		/// Gets or sets the model for the currently selected tab page in the view
		/// </summary>
		//      public virtual TabPageViewModel SelectedTab { get; set;}
		private int _selectedIndex = -1;
		public virtual int SelectedIndex
		{
			get
			{
				return _selectedIndex;
			}
			set
			{
				if (base.Items != null)
				{
					if (value >= 0 && value < base.Items.Count)
					{
						if (_selectedIndex >= 0)
						{
							base.Items[_selectedIndex].Focused = false;
						}
						_selectedIndex = value;
						base.Items[_selectedIndex].Focused = true;
					}
				}
				else
				{
					_selectedIndex = value;
				}
			}
		}

		public override IStateObject CreateItem(object item)
		{
			return Container.Resolve<TabPageViewModel>();
		}

		public override void Build(IIocContainer ctx)
		{
			base.Build(ctx);
			Enabled = true;
			Visible = true;
		}
	}
}
