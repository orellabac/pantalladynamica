﻿using System.Collections.Generic;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
	public class ToolStripStatusLabelViewModel : IDependantViewModel
	{
		public void Build(IIocContainer ctx)
		{
			this.Visible = true;
			this.Enabled = true;
			this.Text = "";
		}

		public string UniqueID
		{
			get;
			set;
		}

    #region Data Members

		/// <summary>
		/// Reference to the ToolStripViewModel object owning this item.
		/// </summary>
		public virtual ToolStripViewModel Owner { get; set; }

        /// <summary>
        /// Returns a name that will be used in to view that presents this model
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether in the view the element that represents this model can respond to user interaction
        /// </summary>
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether in the view the element that represent this model is displayed
        /// </summary>
        public virtual bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the text value of the element
        /// </summary>
        public virtual string Text { get; set; }

        /// <summary>
        /// Gets or sets the index value of the image that is displayed on the item.
        /// </summary>
        public virtual int ImageIndex { get; set; }

        /// <summary>
        /// Gets or sets the text that appears as a ToolTip for a control.
        /// </summary>
        public virtual string ToolTipText { get; set; }

        public virtual string Tag { get; set; }

    }

        #endregion
}
