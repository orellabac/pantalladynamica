﻿


using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
    /// <summary>
    /// A view model to hold state for ToolTips
    /// </summary>
    public class ToolTipViewModel : IDependantViewModel
    {

        #region Data Members

        #endregion



        public void Build(IIocContainer ctx)
        {
            
        }

        public string UniqueID { get; set; }
    }
}
