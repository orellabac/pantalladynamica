﻿
namespace UpgradeHelpers.BasicViewModels
{
    /// <summary>
    /// Represents the method that will handle events related to cell and row operations.
    /// </summary>
    /// <param name="sender">The source of the event</param>
    /// <param name="e">An object that contains the event data</param>
    public delegate void DataGridViewCellEventHandler(object sender, DataGridViewCellEventArgs e);

}
