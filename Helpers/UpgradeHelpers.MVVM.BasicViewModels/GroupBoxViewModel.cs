﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
	/// <summary>
	/// Represents a view model that deals with panel in the view.
	/// </summary>
	public class GroupBoxViewModel : IDependantViewModel
	{
		public string UniqueID { get; set; }
		public void Build(IIocContainer ctx)
		{
			// Enabled DefaultValue
			Enabled = true;

			// Visible DefaultValue
			Visible = true;
		}

		#region Data Members

		/// <summary>
		/// Gets or sets a value indicating whether in the view the element that represents this model can respond to user interaction
		/// </summary>
		public virtual bool Enabled { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether in the view the element that represent this model is displayed
		/// </summary>
		public virtual bool Visible { get; set; }

		/// <summary>
		/// Gets or sets the distance in pixels between the top edge of the the element of the view that represents this model 
		/// and the top edge of the element in the view that contains it
		/// and the
		/// </summary>
		public virtual int Top { get; set; }

		/// <summary>
		/// Gets or sets the distance in pixels, between the 
		/// left edge of the element in the view that represents this model 
		/// and the left edge of the element of the view that contains it
		/// </summary>
		public virtual int Left { get; set; }

		/// <summary>
		/// Gets or sets the height in pixels for the element in the view that will represent this model
		/// </summary>
		public virtual int Height { get; set; }

		/// <summary>
		/// Gets or sets the Width in pixels for the element in the view that will represent this model
		/// </summary>
		public virtual int Width { get; set; }

		/// <summary>
		/// Gets or sets the tab order of the element in the view that  that will represent this model 
		/// </summary>
		public virtual int TabIndex { get; set; }

		/// <summary>
		/// Gets or sets the background color that will be used by the element tin
		/// the view that will represent this model
		/// </summary>
		public virtual Color BackColor { get; set; }

		/// <summary>
		/// Gets or sets the foreground color that will be used by the element tin
		/// the view that will represent this model
		/// </summary>
		public virtual Color ForeColor { get; set; }

		/// <summary>
		/// Gets or sets the text associated with this model
		/// </summary>
		public virtual string Text { get; set; }

		/// <summary>
		/// Gets or sets the Tag associated with this model
		/// </summary>
		public virtual string Tag { get; set; }

		#endregion
	}
}
