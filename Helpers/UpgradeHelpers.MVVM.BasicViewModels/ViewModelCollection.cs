using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.WebMap.Server.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
	public abstract class ViewModelCollection<T> : IViewModelCollection
		where T: IStateObject
    {
        public string UniqueID { get; set; }
        public IViewManager ViewManager { get; set; }
        public IIocContainer Container { get; set; }

        [StateManagement()]
        public virtual IModelList<IStateObject> _items { get; set; }

        public virtual void Build(IIocContainer ctx)
        {
            _items = ctx.Resolve<IModelList<IStateObject>>();
        }

        public abstract IStateObject CreateItem(object item);

		private ItemsCollection<T> _collection;

        [StateManagement(false)]
		public virtual ItemsCollection<T> Items
        {
			get { return (_collection ?? (_collection = new ItemsCollection<T>(this))); }
        }

    }
}