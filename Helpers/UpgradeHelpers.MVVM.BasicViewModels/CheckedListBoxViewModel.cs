﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
	/// <summary>
	///  View model of each entry of a CheckedListBox
	/// </summary>
	public class CheckedListBoxItem : IStateObjectWithInitialization
	{
		public virtual string UniqueID { get; set; }
		public virtual string Text { get; set; }
		public virtual int Value { get; set; }
		public virtual bool Checked { get; set; }

		public void Build(IIocContainer ctx)
		{
			Text = string.Empty;
			Value = 0;
		}

	}

	/// <summary>
	///   View Model for List boxes with check box items
	/// </summary>
	public class CheckedListBoxViewModel : ViewModelCollection<CheckedListBoxItem>
	{


		/// <summary>
		///     Gets or sets a value indicating whether in the view the element that represents this model can respond to user interaction
		/// </summary>
		public virtual bool Enabled { get; set; }

		/// <summary>
		///     Gets or sets a value indicating whether in the view the element that represent this model is displayed
		/// </summary>
		public virtual bool Visible { get; set; }

		/// <summary>
		///     Gets or sets the font of the text that the view will use to display text
		/// </summary>
		public virtual Font Font { get; set; }

		/// <summary>
		///     Gets or sets the distance in pixels between the top edge of the the element of the view that represents this model
		///     and the top edge of the element in the view that contains it
		///     and the
		/// </summary>
		public virtual int Top { get; set; }

		/// <summary>
		///     Gets or sets the distance in pixels, between the
		///     left edge of the element in the view that represents this model
		///     and the left edge of the element of the view that contains it
		/// </summary>
		public virtual int Left { get; set; }

		/// <summary>
		///     Gets or sets the height in pixels for the element in the view that will represent this model
		/// </summary>
		public virtual int Height { get; set; }

		/// <summary>
		///     Gets or sets the Width in pixels for the element in the view that will represent this model
		/// </summary>
		public virtual int Width { get; set; }

		/// <summary>
		///     Gets or sets the tab order of the element in the view that  that will represent this model
		/// </summary>
		public virtual int TabIndex { get; set; }

		/// <summary>
		///     Gets or sets the background color that will be used by the element tin
		///     the view that will represent this model
		/// </summary>
		public virtual Color BackColor { get; set; }


		/// <summary>
		///     Gets or sets the foreground color that will be used by the element tin
		///     the view that will represent this model
		/// </summary>
		public virtual Color ForeColor { get; set; }

		/// <summary>
		///  Tag associated with this control
		/// </summary>
		public virtual string Tag { get; set; }

		/// <summary>
		///  Selected element
		/// </summary>
		public int SelectedIndex
		{
			set;
			get;
		}

		/// <summary>
		///    CheckedListBox selection mode
		/// </summary>
		public virtual SelectionMode SelectionMode { get; set; }


		/// <summary>
		///  Initialization for view model
		/// </summary>
		/// <param name="ctx"></param>
		public override void Build(IIocContainer ctx)
		{
			base.Build(ctx);
			Enabled = true;
			Visible = true;
		}

		public override Interfaces.IStateObject CreateItem(object item)
		{
			var result =  Container.Resolve<CheckedListBoxItem>();
			result.Text = item.ToString();
			return result;
		}

		/// <summary>
		///  Adds an element to the ListBox
		/// </summary>
		/// <param name="itemText">item text</param>
		public void AddItem(string itemText)
		{
			this.Items.Add(itemText);
		}

		/// <summary>
		///   Returns the text of the specified item
		/// </summary>
		/// <param name="itemIndex">item to get the text from</param>
		/// <returns></returns>
		public string GetListItem(int itemIndex)
		{
			return this.Items[itemIndex].Text;
		}

		/// <summary>
		///   Gets the check box state of the specified item
		/// </summary>
		/// <param name="itemIndex">item index to get the 'checked' state from</param>
		/// <returns></returns>
		public bool GetItemChecked(int itemIndex)
		{
			return this.Items[itemIndex].Checked;
		}

		/// <summary>
		///   Sets the 'checked' stated
		/// </summary>
		/// <param name="itemIndex"></param>
		/// <param name="checkedValue"></param>
		public void SetItemChecked(int itemIndex, bool checkedValue)
		{
			this.Items[itemIndex].Checked = checkedValue;
		}

		public CheckedListBoxCheckedCollection CheckedIndices
		{
			get
			{
				return new CheckedListBoxCheckedCollection(this);
			}
			set
			{
			}
		}

		/// <summary>
		/// Sets the ItemData of a specific item
		/// </summary>
		/// <param name="index">The index of the item which ItemData will be set</param>
		/// <param name="value">The value of the ItemData to set</param>
		public void SetItemData(int index, int value)
		{
			if (Items.Count > 0 && Items.Count > index)
			{
				this.Items[index].Value = value;
			}
		}


		/// <summary>
		/// Gets the ItemData of a specific item
		/// </summary>
		/// <param name="index">The index of the item which ItemData will be returned</param>
		/// <returns>Return the ItemData of the item in the "index" position</returns>
		public int GetItemData(int index)
		{
			if (Items.Count > 0 && Items.Count > index)
			{
				return Items[index].Value;
			}
			return 0;
		}




		public class CheckedListBoxCheckedCollection : IList<CheckedListBoxItem>
		{
			CheckedListBoxViewModel _viewModel;
			internal CheckedListBoxCheckedCollection(CheckedListBoxViewModel viewModel)
			{
				this._viewModel = viewModel;
			}

			private IEnumerable<CheckedListBoxItem> AllItems()
			{
				for (int i = 0; i < this._viewModel.Items.Count; i++)
				{
					yield return _viewModel.Items[i];
				}
			}
			private IEnumerable<CheckedListBoxItem> SelectedItems()
			{
				for (int i = 0; i < this._viewModel.Items.Count; i++)
				{
					if (this._viewModel.Items[i].Checked)
					{
						yield return _viewModel.Items[i];
					}
				}
			}

			public int IndexOf(CheckedListBoxItem item)
			{
				int result = -1;
				int i = 0;
				foreach (var innerItem in SelectedItems())
				{
					if (innerItem == item)
					{
						result = i;
						break;
					}
					i++;
				}
				return result;
			}

			public void Insert(int index, CheckedListBoxItem item)
			{
				int innerIndex = IndexOfItem(item);
				this._viewModel.Items[innerIndex].Checked =  true;
			}

			private int IndexOfItem(CheckedListBoxItem item)
			{
				int innerIndex = -1;
				for (int i = 0; i < this._viewModel.Items.Count; i++)
				{
					if (item == _viewModel.Items[i])
					{
						innerIndex = i;
					}
				}
				return innerIndex;
			}

			public void RemoveAt(int index)
			{
				this._viewModel.Items[index].Checked = false;
			}

			public CheckedListBoxItem this[int index]
			{
				get
				{
					return SelectedItems().ElementAt(index);
				}
				set
				{
					throw new NotImplementedException();
				}
			}

			public void Add(CheckedListBoxItem item)
			{
				int innerIndex = IndexOfItem(item);
				this._viewModel.Items[innerIndex].Checked = true;
			}

			public void Clear()
			{
				for (int i = 0; i < this._viewModel.Items.Count; i++)
				{
					if (this._viewModel.Items[i].Checked)
					{
						_viewModel.Items[i].Checked = false;
					}
				}
			}

			public bool Contains(CheckedListBoxItem item)
			{
				return SelectedItems().Any(innerItem => innerItem == item);
			}

			public void CopyTo(CheckedListBoxItem[] array, int arrayIndex)
			{
				throw new NotImplementedException();
			}

			public int Count
			{
				get { return SelectedItems().Count(); }
			}

			public bool IsReadOnly
			{
				get { return true; }
			}

			public bool Remove(CheckedListBoxItem item)
			{
				this.RemoveAt(IndexOfItem(item));
				return true;
			}

			public IEnumerator<CheckedListBoxItem> GetEnumerator()
			{
				return this.SelectedItems().GetEnumerator();
			}

			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
			{
				return this.SelectedItems().GetEnumerator();
			}
		}
	}
}
