#region

using System;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

#endregion

namespace UpgradeHelpers.BasicViewModels
{
    /// <summary>
    ///     Represents a model that can be used on the view to display or edit unformatted text
    /// </summary>
    public class TextBoxViewModel : IDependantViewModel
    {
        /// <summary>
        ///     Setup the model properties with its default values
        /// </summary>
        public void Build(IIocContainer container)
        {
            // Enabled DefaultValue
            Enabled = true;

			//ReadOnly DefaultValue
			ReadOnly = false;

            // Visible DefaultValue
            Visible = true;

            // SelectedRange DefaultValue
            SelectedRange = new int[] { 0, 0 };

            Text = String.Empty;
        }

        #region Data Members

        /// <summary>
        ///     Returns unique identifier for ViewModel
        /// </summary>
        public string UniqueID { get; set; }

        /// <summary>
        ///     Returns a name that will be used in to view that presents this model
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether in the view the element that represents this model can respond to user interaction
        /// </summary>
        public virtual bool Enabled { get; set; }


		/// <summary>
		///     Gets or sets a value indicating whether in the view the element that represents this model can modified the Text.
		/// </summary>
		public virtual bool ReadOnly { get; set; }


        /// <summary>
        ///     Gets or sets a value indicating whether in the view the element that represent this model is displayed
        /// </summary>
        public virtual bool Visible { get; set; }

        /// <summary>
        ///     Gets or sets the font of the text that the view will use to display text
        /// </summary>
        public virtual Font Font { get; set; }

        /// <summary>
        ///     Gets or sets the distance in pixels between the top edge of the the element of the view that represents this model
        ///     and the top edge of the element in the view that contains it
        ///     and the
        /// </summary>
        public virtual int Top { get; set; }

        /// <summary>
        ///     Gets or sets the distance in pixels, between the
        ///     left edge of the element in the view that represents this model
        ///     and the left edge of the element of the view that contains it
        /// </summary>
        public virtual int Left { get; set; }

        /// <summary>
        ///     Gets or sets the height in pixels for the element in the view that will represent this model
        /// </summary>
        public virtual int Height { get; set; }

        /// <summary>
        ///     Gets or sets the Width in pixels for the element in the view that will represent this model
        /// </summary>
        public virtual int Width { get; set; }

        /// <summary>
        ///     Gets or sets the tab order of the element in the view that  that will represent this model
        /// </summary>
        public virtual int TabIndex { get; set; }

        /// <summary>
        ///     Gets or sets the background color that will be used by the element tin
        ///     the view that will represent this model
        /// </summary>
        public virtual Color BackColor { get; set; }

        /// <summary>
        ///     Gets or sets the foreground color that will be used by the element tin
        ///     the view that will represent this model
        /// </summary>
        public virtual Color ForeColor { get; set; }

        /// <summary>
        ///     Gets or sets generic data
        /// </summary>
        public virtual string Tag { get; set; }

        public virtual int MaxLength { get; set; }

        public virtual int[] SelectedRange { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating the currently selected text in the control.
        /// </summary>
        [StateManagement(false)]
        public string SelectedText
        {
            get
            {
                return Text.Substring(SelectedRange[0], SelectedRange[1] - SelectedRange[0]);
            }
            set
            {
                Text.Remove(SelectedRange[0], SelectedRange[1] - SelectedRange[0]);
                Text.Insert(SelectedRange[0], value);
                SelectedRange[1] = SelectedRange[0] + value.Length;
            }
        }

        /// <summary>
        ///     Gets or sets the starting point of text selected in the text box.
        /// </summary>
        [StateManagement(false)]
        public virtual int SelectionStart
        {
            get
            {
                return SelectedRange[0];
            }
            set
            {
				var tmpLength = SelectionLength;
                SelectedRange[0] = value;
				SelectedRange[1] = value + tmpLength;
				UpdateSelectedRangeToSetModified();
            }
        }

		private void UpdateSelectedRangeToSetModified()
		{
			var tmp = SelectedRange;
			SelectedRange = null;
			SelectedRange = tmp;
		}

        /// <summary>
        ///     Gets or sets the number of characters selected in the text box.
        /// </summary>
        [StateManagement(false)]
        public virtual int SelectionLength
        {
            get
            {
                return SelectedRange[1] - SelectedRange[0];
            }
            set
            {
				SelectedRange[1] = SelectedRange[0] + value;
				UpdateSelectedRangeToSetModified();
            }
        }

        /// <summary>
        ///     Gets or sets the text associated with this model
        /// </summary>
        public virtual string Text { get; set; }

        #endregion

        #region Events

        private event EventHandler _TextChanged;

        public event EventHandler TextChanged
        {
            add { _TextChanged += value; }
            remove { _TextChanged -= value; }
        }

        public void OnTextChanged()
        {
            if (_TextChanged != null)
            {
                _TextChanged(this, new EventArgs());
            }
        }

        #endregion
    }
}