﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
    public class NumericUpDownViewModel : IDependantViewModel
    {
        /// <summary>
        ///     Setup the model properties with its default values
        /// </summary>
        public void Build(IIocContainer container)
        {
            // Enabled DefaultValue
            Enabled = true;

            // ReadOnly DefaultValue
            ReadOnly = false;

            // Visible DefaultValue
            Visible = true;

            //Maximum DefaultValue
            Maximum = decimal.MaxValue;

            //Minimum DefaultValue
            Minimum = decimal.MinValue;

            //DecimalPlaces DefaultValue
            DecimalPlaces = 0;

            //Increment Default Value;
            Increment = 1;

        }

        #region Data Members

        /// <summary>
        ///     Returns unique identifier for ViewModel
        /// </summary>
        public string UniqueID { get; set; }

        /// <summary>
        ///     Returns a name that will be used in to view that presents this model
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether in the view the element that represents this model can respond to user interaction
        /// </summary>
        public virtual bool Enabled { get; set; }


        /// <summary>
        ///     Gets or sets a value indicating whether in the view the element that represents this model can modified the Text.
        /// </summary>
        public virtual bool ReadOnly { get; set; }


        /// <summary>
        ///     Gets or sets a value indicating whether in the view the element that represent this model is displayed
        /// </summary>
        public virtual bool Visible { get; set; }

        /// <summary>
        ///     Gets or sets the font of the text that the view will use to display text
        /// </summary>
        public virtual Font Font { get; set; }

        /// <summary>
        ///     Gets or sets the distance in pixels between the top edge of the the element of the view that represents this model
        ///     and the top edge of the element in the view that contains it
        ///     and the
        /// </summary>
        public virtual int Top { get; set; }

        /// <summary>
        ///     Gets or sets the distance in pixels, between the
        ///     left edge of the element in the view that represents this model
        ///     and the left edge of the element of the view that contains it
        /// </summary>
        public virtual int Left { get; set; }

        /// <summary>
        ///     Gets or sets the height in pixels for the element in the view that will represent this model
        /// </summary>
        public virtual int Height { get; set; }

        /// <summary>
        ///     Gets or sets the Width in pixels for the element in the view that will represent this model
        /// </summary>
        public virtual int Width { get; set; }

        /// <summary>
        ///     Gets or sets the tab order of the element in the view that  that will represent this model
        /// </summary>
        public virtual int TabIndex { get; set; }

        /// <summary>
        ///     Gets or sets the background color that will be used by the element tin
        ///     the view that will represent this model
        /// </summary>
        public virtual Color BackColor { get; set; }

        /// <summary>
        ///     Gets or sets the foreground color that will be used by the element in
        ///     the view that will represent this model
        /// </summary>
        public virtual Color ForeColor { get; set; }

        /// <summary>
        ///     Gets or sets generic data
        /// </summary>
        public virtual string Tag { get; set; }

        /// <summary>
        ///     Gets or sets the value associated with this model
        /// </summary>
        public virtual decimal Value { get; set; }

        /// <summary>
        ///     Gets or sets the maximum value associated with this model
        /// </summary>
        public virtual decimal Maximum { get; set; }

        /// <summary>
        ///     Gets or sets the minimum value associated with this model
        /// </summary>
        public virtual decimal Minimum { get; set; }

        /// <summary>
        ///     Gets or sets the increment or decrement value associated with this model.
        /// </summary>
        public virtual decimal Increment { get; set; }
                   
        /// <summary>
        ///     Gets or sets the number of decimal places to be displayed 
        ///     by the element of the view that represents this model
        /// </summary>
        public virtual int DecimalPlaces { get; set; }
        
        #endregion

        #region events
        /// <summary>
        ///  Increases the value of the control when the Upbutton is pressed
        /// </summary>
        ///
        public void UpButton()
        {
            Value += Increment;
        }

        /// <summary>
        ///  Decreases the value of the control when the Downbutton is pressed
        /// </summary>
        ///
        public void DownButton()
        {
            Value -= Increment;
        }


        #endregion
    }
}