﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
	public class ToolStripSplitButtonViewModel : ToolStripItemViewModel , IDependantViewModel
	{
		public string UniqueID
		{
			get;
			set;
		}

		/// <summary>
		/// Setup the model properties with its default values
		/// </summary>
		public void Build(IIocContainer ctx)
		{
			// Enabled DefaultValue
			Enabled = true;

			// Visible DefaultValue
			Visible = true;

			// tooltipText DefaultValue
			ToolTipText = "";
		}
		
	}
}