﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
	public class ProgressBarViewModel : IDependantViewModel
	{
		/// <summary>
		/// Gets / Sets the current progress value
		/// </summary>
		public virtual int Value { get; set; }

		/// <summary>
		/// Gets/Sets the maximum progress value
		/// </summary>
		public virtual int Maximum { get; set; }


		/// <summary>
		/// Gets/Sets the minimum progress value
		/// </summary>
		public virtual int Minimum { get; set; }


		public virtual bool Enable { get; set; }
		public virtual bool Visible { get; set; }

		public void Build(IIocContainer ctx)
		{
		}

		public string UniqueID
		{
			get;
			set;
		}
	}
}
