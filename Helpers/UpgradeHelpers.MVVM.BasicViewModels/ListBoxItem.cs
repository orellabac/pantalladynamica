using System;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
	public class ListBoxItem : IDependantViewModel
	{
		public string UniqueID { get; set; }

		public void Build(IIocContainer ctx)
		{
			Text = String.Empty;
			Value = null;
		}

		public virtual string Text { get; set; }

		public virtual object Value { get; set; }

		public virtual int? Data { get; set; }

		public virtual object Item { get; set; }

		public override string ToString()
		{
			return Text;
		}
	}
}