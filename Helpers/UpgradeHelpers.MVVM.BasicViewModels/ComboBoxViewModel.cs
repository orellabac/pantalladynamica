﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.WebMap.Server;

namespace UpgradeHelpers.BasicViewModels
{
	/// <summary>
	///  View model for the element for ComboBox elements
	/// </summary>
	public class ComboBoxItem : IDependantViewModel
	{
		public string UniqueID { get; set; }

		public void Build(IIocContainer ctx)
		{
			Text = String.Empty;
			Value = null;
		}

		public virtual string Text { get; set; }
		public virtual object Value { get; set; }
		public virtual int? Data { get; set; }
		public virtual object Item { get; set; }

		public override string ToString()
		{
			return Text;
		}
	}

	/// <summary>
	/// ViewModel to represent the state of a ComboBox
	/// </summary>
	public class ComboBoxViewModel : IDependantViewModel, ICreatesObjects, IInteractsWithView
	{
		public ComboBoxData InternalData;
		private ComboBoxItemCollection _items;

		public string UniqueID { get; set; }

		public IViewManager ViewManager { get; set; }

		public IIocContainer Container { get; set; }

		/// <summary>
		///   Tag associated with this control
		/// </summary>
		public virtual string Tag { get; set; }

		/// <summary>
		/// Setup the model properties with its default values
		/// </summary>
		public void Build(IIocContainer container)
		{
			// Enabled DefaultValue
			Enabled = true;

			// Visible DefaultValue
			Visible = true;

			// SelectedIndex DefaultValue
			SelectedIndex = -1;

			ComboBoxItems = new ObservableCollection<ComboBoxItem>();
		}

		public virtual ObservableCollection<ComboBoxItem> ComboBoxItems { get; set; }

		public ComboBoxItemCollection Items
		{
			get { return _items ?? (_items = new ComboBoxItemCollection(this)); }
		}

		private int _selectedIndex;

		/// <summary>
		///    The index of the selected element
		/// </summary>
		/// 
		public virtual int SelectedIndex
		{
			get
			{
				return _selectedIndex;
			}

			set
			{
				_selectedIndex = value;
				if (ViewManager != null)
					ViewManager.Events.Publish("SELECTEDINDEXCHANGED", this, new EventArgs());
			}
		}


		/// <summary>
		///  Text typed in a combobox that accepts typing
		/// </summary>
		public virtual string CustomText { get; set; }

		/// <summary>
		/// Returns a name that will be used in to view that presents this model
		/// </summary>
		public virtual string Name { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether in the view the element that represents this model can respond to user interaction
		/// </summary>
		public virtual bool Enabled { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether in the view the element that represent this model is displayed
		/// </summary>
		public virtual bool Visible { get; set; }

		/// <summary>
		/// Gets or sets the tab order of the element in the view that  that will represent this model 
		/// </summary>
		public virtual int TabIndex { get; set; }

		/// <summary>
		/// Returns a DisplayMember that will be used in to view that presents this model
		/// </summary>
		public virtual string DisplayMember { get; set; }

		/// <summary>
		/// Returns a ValueMember that will be used in to view that presents this model
		/// </summary>
		public virtual string ValueMember { get; set; }

		/// <summary>
		/// Gets or sets the font of the text that the view will use to display text
		/// </summary>
		public virtual Font Font { get; set; }

		/// <summary>
		/// Gets or sets the distance in pixels between the top edge of the the element of the view that represents this model 
		/// and the top edge of the element in the view that contains it
		/// and the
		/// </summary>
		public virtual int Top { get; set; }

		/// <summary>
		/// Gets or sets the distance in pixels, between the 
		/// left edge of the element in the view that represents this model 
		/// and the left edge of the element of the view that contains it
		/// </summary>
		public virtual int Left { get; set; }

		/// <summary>
		/// Gets or sets the height in pixels for the element in the view that will represent this model
		/// </summary>
		public virtual int Height { get; set; }

		/// <summary>
		/// Gets or sets the Width in pixels for the element in the view that will represent this model
		/// </summary>
		public virtual int Width { get; set; }

		/// <summary>
		/// Gets or sets the background color that will be used by the element tin
		/// the view that will represent this model
		/// </summary>
		public virtual Color BackColor { get; set; }

		/// <summary>
		/// Gets or sets the foreground color that will be used by the element tin
		/// the view that will represent this model
		/// </summary>
		public virtual Color ForeColor { get; set; }

		[StateManagement(false)]
		public object DropDownStyle { get; set; }

		[StateManagement(false)]
		public object AutoCompleteMode { get; set; }

		[StateManagement(false)]
		public int SelectionLength { get; set; }

		[StateManagement(false)]
		public string ToolTipText { get; set; }

		/// <summary>
		/// Gets the selected item
		/// </summary>
		[StateManagement(false)]
		public object SelectedItem
		{
			get
			{
				if (SelectedIndex >= 0)
				{
					var comboBoxItem = ComboBoxItems[SelectedIndex];

					if (comboBoxItem != null)
					{
						return comboBoxItem.Item ?? comboBoxItem.Text;
					}
					return comboBoxItem;
				}
				return null;
			}
			set
			{
				if (value != null)
				{
					bool selectedIndexChanged = false;
					for (int i = 0; i < ComboBoxItems.Count; i++)
					{
						var comboItem = ComboBoxItems[i];

						if (comboItem != null)
						{
							if (comboItem.Item != null)
							{
								if ((comboItem.Item).Equals(value))
								{
									SelectedIndex = i;
									selectedIndexChanged = true;
									break;
								}
							}
							else
							{
								if (comboItem.Value == value || comboItem.Text == (string) value ||
									(string) comboItem.Value == value.ToString())
								{
									SelectedIndex = i;
									selectedIndexChanged = true;
									break;
								}
							}
						}
						else
						{
							if (comboItem != null && comboItem.Equals(value))
							{
								SelectedIndex = i;
								selectedIndexChanged = true;
								break;
							}
						}
					}

					if (!selectedIndexChanged)
					{
						SelectedIndex = -1;
					}
				}
				else
					SelectedIndex = -1;
			}
		}

		/// <summary>
		/// Gets the selected value
		/// </summary>
		[StateManagement(false)]
		public virtual object SelectedValue
		{
			get
			{
				if (SelectedIndex >= 0)
				{
					if (ComboBoxItems[SelectedIndex] != null)
						return ComboBoxItems[SelectedIndex].Value;
				}
				return null;
			}
			set
			{
				if (value != null)
				{
					for (int itemIndex = 0; itemIndex < ComboBoxItems.Count; itemIndex++)
					{
						if (ComboBoxItems[itemIndex].Value.Equals(value))
						{
							SelectedIndex = itemIndex;
							break;
						}
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the text associated with this model
		/// </summary>
		[StateManagement(false)]
		public string Text
		{
			get
			{
				if (SelectedIndex >= 0)
				{
					var comboBoxItem = ComboBoxItems[SelectedIndex];

					if (comboBoxItem != null)
					{
						return comboBoxItem.Text;
					}
				}
				return String.Empty;
			}
			set
			{
				if (value == null) throw new ArgumentNullException("value");
				for (var i = 0; i < Items.Count; i++)
				{
					if (ComboBoxItems[i].Text.Equals(value))
					{
						SelectedIndex = i;
						return;
					}
				}
				CustomText = value;
			}
		}

		[StateManagement(false)]
		public IListSource DataSource
		{
			get { return InternalData.DataSource; }
			set
			{
				if (value != null)
				{
					foreach (DataRow r in ((DataTable)value).Rows)
					{
						var item = Container.Resolve<ComboBoxItem>();
						item.Text = r[DisplayMember].ToString();
						item.Value = r[ValueMember];
						item.Item = r[DisplayMember].ToString();
						ComboBoxItems.Add(item);
					}
				}

				//
				if (InternalData == null)
					InternalData = new ComboBoxData();
				InternalData.DataSource = value;
			}
		}

		private object GetMemberValue(object item, string member)
		{
			if (String.IsNullOrEmpty(member))
				return item.ToString();
			// lets get the property
			var type = item.GetType();
			var prop = type.GetProperty(member);
			if (prop != null)
			{
				return prop.GetValue(item, null);
			}
			return null;
		}

		/// <summary>
		///  Creates an inner element for the given object
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public ComboBoxItem CreateItem(object item)
		{
			var newItem = Container.Resolve<ComboBoxItem>();

			if (!(item is string))
			{
				newItem.Item = item;
				if (!String.IsNullOrEmpty(DisplayMember) && !String.IsNullOrEmpty(ValueMember))
				{
					newItem.Text = GetMemberValue(item, DisplayMember).ToString();
					newItem.Value = GetMemberValue(item, ValueMember);
				}
				else
				{
					if (!String.IsNullOrEmpty(DisplayMember) || !String.IsNullOrEmpty(ValueMember))
					{
						var property = String.IsNullOrEmpty(DisplayMember) ? ValueMember : DisplayMember;
						newItem.Value = GetMemberValue(item, property);
						newItem.Text = newItem.Value.ToString();
					}
					else
					{
						newItem.Text = item.ToString();
						newItem.Value = item.ToString();
					}
				}
			}
			else
			{
				newItem.Text = item.ToString();
				newItem.Value = item.ToString();
			}
			return newItem;
		}

		public ComboBoxItem FindByValue(object dataValue)
		{
			for (var i = 0; i < ComboBoxItems.Count; i++)
			{
				if (ComboBoxItems[i].Value.Equals(dataValue))
					return ComboBoxItems[i];
			}
			return null;
		}

		/// <summary>
		/// Adds a new element to the combo
		/// </summary>
		/// <param name="item">item to add</param>
		/// <param name="index">position to adding</param>
		public int AddItem(object item, int index = -1)
		{
			var newItem = CreateItem(item);
			if (index == -1 || index == ComboBoxItems.Count)
			{
				ComboBoxItems.Add(newItem);
				return ComboBoxItems.Count - 1;
			}
			ComboBoxItems.Insert(index, newItem);
			return index;
		}

		/// <summary>
		///  Removes an element from the ComboBox
		/// </summary>
		/// <param name="itemIndex"></param>
		public void RemoveItem(int itemIndex)
		{
			ComboBoxItems.RemoveAt(itemIndex);
		}
		
		/// <summary>
		///   Returns the value at the specified index
		/// </summary>
		/// <param name="index">index of th requested element</param>
		/// <returns>the value of the requested element</returns>
		public string GetListItem(int index)
		{
			string result = "";
			if (index >= 0 && index < ComboBoxItems.Count)
			{
				result = ComboBoxItems[index].Text;
			}
			return result;
		}

		/// <summary>
		///  Sets the text of a combo box element
		/// </summary>
		/// <param name="index"></param>
		/// <param name="value"></param>
		public void SetListItem(int index, string value)
		{
			ComboBoxItems[index].Text = value;
		}

		/// <summary>
		/// Sets the ItemData of the combo
		/// </summary>
		/// <param name="itemData">The new ItemData for the elements of the collection</param>
		public void SetItemData(int[] itemData)
		{
			for (var i = 0; i < itemData.Length; i++)
			{
				if (i < ComboBoxItems.Count)
					ComboBoxItems[i].Data = itemData[i];
			}
		}

		/// <summary>
		/// Sets the ItemData of a specific item
		/// </summary>
		/// <param name="index">The index of the item which ItemData will be set</param>
		/// <param name="value">The value of the ItemData to set</param>
		public void SetItemData(int index, int value)
		{
			if (ComboBoxItems.Count > 0 && ComboBoxItems.Count > index)
			{
				ComboBoxItems[index].Data = value;
			}
		}

		/// <summary>
		/// Gets the ItemData of the combo
		/// </summary>
		/// <returns>The ItemData of the elements of the collection</returns>
		public int[] GetItemData()
		{
			var items = new int[ComboBoxItems.Count];
			for (var i = 0; i < ComboBoxItems.Count; i++)
			{
				var data = ComboBoxItems[i].Data;
				items[i] = data.HasValue ? data.Value : -1;
			}
			return items;
		}

		/// <summary>
		/// Gets the ItemData of a specific item
		/// </summary>
		/// <param name="index">The index of the item which ItemData will be returned</param>
		/// <returns>Return the ItemData of the item in the "index" position</returns>
		public int GetItemData(int index)
		{
			if (ComboBoxItems.Count > 0 && ComboBoxItems.Count > index)
			{
				var itemData = ComboBoxItems[index].Data;
				return itemData.HasValue ? itemData.Value : -1;
			}
			return 0;
		}

		/// <summary>
		/// Gets the index of the last added element
		/// </summary>
		/// <returns>The index of the last added element</returns>
		public int GetNewIndex()
		{
			return ComboBoxItems.Count - 1;
		}

		public class ComboBoxData : IInternalData
		{
			public IList Data = new ArrayList();

			public IListSource DataSource;
		}

		[JsonObject]
		public class ComboBoxItemCollection : IEnumerable
		{
			private readonly ComboBoxViewModel _comboBoxViewModel;

			public ComboBoxItemCollection(ComboBoxViewModel comboBoxViewModel)
			{
				_comboBoxViewModel = comboBoxViewModel;
			}

			public void Add(object item)
			{
				_comboBoxViewModel.AddItem(item);
			}

			public void AddRange(object[] items)
			{
				foreach (var item in items)
				{
					_comboBoxViewModel.AddItem(item);
				}
			}

			public void Clear()
			{
				_comboBoxViewModel.ComboBoxItems.Clear();
			}

			public bool Contains(object value)
			{
				var comboBoxItem = value is ComboBoxItem ? value as ComboBoxItem : _comboBoxViewModel.FindByValue(value);
				return _comboBoxViewModel.ComboBoxItems.Contains(comboBoxItem);
			}

			public int Count
			{
				get { return _comboBoxViewModel.ComboBoxItems.Count; }
			}

			public int IndexOf(object value)
			{
				var comboBoxItem = value is ComboBoxItem ? value as ComboBoxItem : _comboBoxViewModel.FindByValue(value);
				return _comboBoxViewModel.ComboBoxItems.IndexOf(comboBoxItem);
			}

			public void Insert(int index, object item)
			{
				_comboBoxViewModel.AddItem(item, index);
			}

			public void Remove(object value)
			{
				var comboBoxItem = value is ComboBoxItem ? value as ComboBoxItem : _comboBoxViewModel.FindByValue(value);
				_comboBoxViewModel.ComboBoxItems.Remove(comboBoxItem);
			}

			public void RemoveAt(int index)
			{
				_comboBoxViewModel.ComboBoxItems.RemoveAt(index);
			}

			public object this[int index]
			{
				get { return _comboBoxViewModel.ComboBoxItems[index].Item ?? _comboBoxViewModel.ComboBoxItems[index].Text; }
				set { _comboBoxViewModel.ComboBoxItems[index].Text = value.ToString(); }
			}

			public IEnumerator GetEnumerator()
			{
				return _comboBoxViewModel.ComboBoxItems.GetEnumerator();
			}

		}

	}
}
