﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UpgradeHelpers.Events;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
    /// <summary>
    /// Defines how to format the control's text
    /// </summary>
    public enum MaskFormat
    {
        // Summary:
        //     Return only text input by the user.
        ExcludePromptAndLiterals = 0,
        //
        // Summary:
        //     Return text input by the user as well as any instances of the prompt character.
        IncludePrompt = 1,
        //
        // Summary:
        //     Return text input by the user as well as any literal characters defined in
        //     the mask.
        IncludeLiterals = 2,
        //
        // Summary:
        //     Return text input by the user as well as any literal characters defined in
        //     the mask and any instances of the prompt character.
        IncludePromptAndLiterals = 3,
    }
    public class MaskedTextBoxViewModel : IDependantViewModel, IInteractsWithView
    {
        public IIocContainer Container { get; set; }
        public IViewManager ViewManager { get; set; }
        private const string TypeValidationEvent = "TypeValidationCompleted";
        /// <summary>
        ///     Setup the model properties with its default values
        /// </summary>
        public void Build(IIocContainer container)
        {
            // Enabled DefaultValue
            Enabled = true;

            // ReadOnly DefaultValue
            ReadOnly = false;

            // Visible DefaultValue
            Visible = true;

            //PromptChar DefaultValue
            PromptChar = '_';

            //TextMaskFormat DefaultValue
            TextMaskFormat = MaskFormat.IncludeLiterals;
        }

        #region Data Members

        /// <summary>
        ///     Returns unique identifier for ViewModel
        /// </summary>
        public string UniqueID { get; set; }

        /// <summary>
        ///     Returns a name that will be used in to view that presents this model
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether in the view the element that represents this model can respond to user interaction
        /// </summary>
        public virtual bool Enabled { get; set; }


        /// <summary>
        ///     Gets or sets a value indicating whether in the view the element that represents this model can modified the Text.
        /// </summary>
        public virtual bool ReadOnly { get; set; }


        /// <summary>
        ///     Gets or sets a value indicating whether in the view the element that represent this model is displayed
        /// </summary>
        public virtual bool Visible { get; set; }

        /// <summary>
        ///     Gets or sets the font of the text that the view will use to display text
        /// </summary>
        public virtual Font Font { get; set; }

        /// <summary>
        ///     Gets or sets the distance in pixels between the top edge of the the element of the view that represents this model
        ///     and the top edge of the element in the view that contains it
        ///     and the
        /// </summary>
        public virtual int Top { get; set; }

        /// <summary>
        ///     Gets or sets the distance in pixels, between the
        ///     left edge of the element in the view that represents this model
        ///     and the left edge of the element of the view that contains it
        /// </summary>
        public virtual int Left { get; set; }

        /// <summary>
        ///     Gets or sets the height in pixels for the element in the view that will represent this model
        /// </summary>
        public virtual int Height { get; set; }

        /// <summary>
        ///     Gets or sets the Width in pixels for the element in the view that will represent this model
        /// </summary>
        public virtual int Width { get; set; }

        /// <summary>
        ///     Gets or sets the tab order of the element in the view that  that will represent this model
        /// </summary>
        public virtual int TabIndex { get; set; }

        /// <summary>
        ///     Gets or sets the background color that will be used by the element tin
        ///     the view that will represent this model
        /// </summary>
        public virtual Color BackColor { get; set; }

        /// <summary>
        ///     Gets or sets the foreground color that will be used by the element in
        ///     the view that will represent this model
        /// </summary>
        public virtual Color ForeColor { get; set; }

        /// <summary>
        ///     Gets or sets generic data
        /// </summary>
        public virtual string Tag { get; set; }

        /// <summary>
        ///     Gets or sets the text value used by the element in the view that will represent this model
        /// </summary>
        private string _text = string.Empty;
        public virtual string _unmodifiedText { get; set; }
        public virtual string Text
        {
            get
            {
              
                switch (TextMaskFormat)
                {
                    case MaskFormat.IncludePromptAndLiterals:
                        {
                            break;
                        }
                    case MaskFormat.IncludePrompt:
                        {
                            Regex rgx = new Regex("[^a-zA-Z0-9" + PromptChar + "]");
                            _text = rgx.Replace(_text, "");
                            break;

                        }
                    case MaskFormat.IncludeLiterals:
                        {
                            _text = _text.Replace("" + PromptChar, "");
                            break;

                        }
                    case MaskFormat.ExcludePromptAndLiterals:
                        {
                            Regex rgx = new Regex("[^a-zA-Z0-9]");
                            _text = rgx.Replace(_text, "");
                            break;

                        }

                }
                ValidateText();
                return _text;
               

            }
            set
            {
                _text = value;
                if (Mask != null && (Mask.Length == value.Length))
                    this._unmodifiedText = value;
            }


        }


        /// <summary>
        ///     Gets or sets the prompt character value used by the element in the view that will represent this model
        /// </summary>
        public virtual char PromptChar { get; set; }

        /// <summary>
        /// Gets or sets the input mask
        /// </summary>
        public virtual string Mask { get; set; }

        /// <summary>
        /// Gets or Sets the value wheter literals and prompt
        /// </summary>
        public virtual MaskFormat TextMaskFormat { get; set; }

        /// <summary>
        /// Gets or sets the datatype used to verify tha data input by the user
        /// </summary>
        public virtual Type ValidatingType { get; set; }
        /// <summary>
        /// Gets a value indicating whether the formatted input string was successfully converted to the validating type.
        /// </summary>
        public bool IsValidInput { get; set; }


        #endregion



        #region Methods
        /// <summary>
        /// Clears all text from the control
        /// </summary>
        public void Clear()
        {
            this.Text = "";
        }
        /// <summary>
        /// Converts the user input string to an instance of the validating type 
        /// </summary>
        /// <returns></returns>
        public Object ValidateText()
        {
            try
            {
                if (ValidatingType != null)
                {
                    TypeConverter typeConverter = TypeDescriptor.GetConverter(ValidatingType);
                    object propValue = typeConverter.ConvertFromString(_unmodifiedText);

                    IsValidInput = true;
                    if (ViewManager != null)
                        ViewManager.Events.Publish(TypeValidationEvent, this, new TypeValidationEventArgs(ValidatingType, IsValidInput, propValue, string.Empty));
                    return propValue;
                }
                else
                    return null;

            }
            catch (Exception e)
            {
                if (ViewManager != null)
                    ViewManager.Events.Publish(TypeValidationEvent, this, new TypeValidationEventArgs(ValidatingType, IsValidInput, null, e.ToString()));
                return null;
            }
        }
        #endregion

        #region extensionMethods

        #endregion
    }
}