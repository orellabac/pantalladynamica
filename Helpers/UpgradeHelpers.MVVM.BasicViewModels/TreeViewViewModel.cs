﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.WebMap.Server.Common;

namespace UpgradeHelpers.BasicViewModels
{
    /// <summary>
    ///   ViewModel for the elements of a TreeView
    /// </summary>
    public class TreeNodeViewModel : ViewModelCollection<TreeNodeViewModel>
    {

        private string _fullpath = "";
        public virtual bool Checked { get; set; }
        public virtual string Name { get; set; }
        public virtual string Text { get; set; }
        public virtual int ImageIndex { get; set; }
        public virtual string ImageListPrefix { get; set; }
        public virtual bool IsSelected { get; set; }
        public virtual bool IsExpanded { get; set; }
        public virtual int Index { get; set; }
        public virtual bool IsVisible { get; set; }

        public virtual int Level { get; set; }

        public override Interfaces.IStateObject CreateItem(object item)
        {
            return Container.Resolve<TreeNodeViewModel>();
        }

        public TreeNodeViewModel Add(string text)
        {
            return Add("", text);
        }

        public TreeNodeViewModel Add(string name, string text, int imageIndex = -1)
        {
            var result = Container.Resolve<TreeNodeViewModel>();

            result.Name = name;
            result.Text = text;
            result.ImageIndex = imageIndex;
            result.ImageListPrefix = ImageListPrefix;
            result.Level = this.Level + 1;
            result.Index = this.GetNodeCount(false);
            Items.Add(result);

            return result;
        }

        public string ImageUrl
        {
            get
            {
                if (ImageIndex == -1)
                {
                    return null;
                }
                else
                {
                    return "Resources/images/" + (ImageListPrefix ?? "") + ImageIndex + ".png";
                }
            }
            set { }
        }


        ////////////////////////////////////
        ///Calculated Properties
        ///
        [StateManagement(false)]
        public virtual string FullPath
        {
            get
            {
                if (this.Parent == null)
                    _fullpath = this.Text;
                else
                    _fullpath = Parent.FullPath + "\\" + this.Text;

                return _fullpath;
            }

        }

        [StateManagement(false)]
        public virtual TreeNodeViewModel FirstNode
        {
            get
            {
                if (this.Items.Count > 0)
                    return this.Items[0];
                else
                    return null;
            }
            set { }

        }

        [StateManagement(false)]
        public virtual TreeNodeViewModel NextNode
        {
            get
            {
                var _parent = this.Parent;
                if (Index + 1 < _parent.GetNodeCount(false))
                {
                    return _parent.Items[Index + 1];
                }
                else
                    return null;
            }
        }

        [StateManagement(false)]
        public virtual TreeNodeViewModel PrevNode
        {
            get
            {
                var _parent = this.Parent;
                var prevIndex = Index - 1;
                if (prevIndex >= 0 && prevIndex < _parent.GetNodeCount(false))
                {
                    return _parent.Items[prevIndex];
                }
                else
                    return null;
            }
        }

        [StateManagement(false)]
        public TreeNodeViewModel Parent
        {
            get
            {
                try
                {
                    return ((TreeNodeViewModel)ViewManager.GetParentViewModel(getParentUID));
                }
                catch (InvalidCastException e)
                { //NullCast means there is no Parent
                    return null;
                }
            }

        }

        private string getParentUID()
        {
            Regex rgx = new Regex("\\d+###_items###");
            return rgx.Replace(this.UniqueID, "", 1);
        }

        /// <summary>
        /// Gets The NextVisibleNode can be a child or a sibling
        /// </summary>
        [StateManagement(false)]
        public TreeNodeViewModel NextVisibleNode
        {
            get
            {
                return NextVisibleChild() ?? NextVisibleSibling();
            }
        }

        /// <summary>
        /// Gets The PrevVisibleNode can be a child or a sibling
        /// </summary>
        [StateManagement(false)]
        public TreeNodeViewModel PrevVisibleNode
        {
            get
            {
                var prevVisibleSibling = this.PrevVisibleSibling();
                if (prevVisibleSibling != null)
                {
                    var prevChild = prevVisibleSibling.LastVisibleChild();
                    if (prevChild != null)
                        return prevChild;
                    else
                        return prevVisibleSibling;
                }
                else
                    return null;
            }
        }


        private TreeNodeViewModel NextVisibleChild()
        {
            for (int i = 0; i < this.Items.Count; i++)
            {
                var current = this.Items[i];
                if (current.IsVisible)
                    return current;
                else
                    this.Items[i].NextVisibleChild();
            }
            return null;
        }

        private TreeNodeViewModel NextVisibleSibling()
        {
            var _parent = this.Parent;
            for (int i = this.Index; i < _parent.Items.Count; i++)
            {
                var current = _parent.Items[i];
                if (current.IsVisible)
                {
                    return current;
                }
            }
            return null;
        }

        private TreeNodeViewModel PrevVisibleSibling()
        {
            var _parent = this.Parent;
            for (int i = 0; i < _parent.Items.Count && i < this.Index; i++)
            {
                var current = _parent.Items[i];
                if (current.IsVisible)
                {
                    return current;
                }
            }
            return null;
        }

        private TreeNodeViewModel LastVisibleChild()
        {
            TreeNodeViewModel result = null;
            for (int i = 0; i < this.Items.Count; i++)
            {
                var current = this.Items[i];
                if (current.IsVisible)
                    result = current;
                else
                    this.Items[i].NextVisibleChild();
            }
            return result;
        }

        #region Methods
        public int GetNodeCount(bool includeSubTrees)
        {
            var result = this.Items.Count;
            if (includeSubTrees)
            {
                for (int i = 0; i < this.Items.Count; i++)
                {
                    result += this.Items[i].GetNodeCount(includeSubTrees);
                }
            }
            return result;
        }
        /// <summary>
        /// Collapse the tree node
        /// </summary>
        public void Collapse()
        {
            IsExpanded = false;
        }
        /// <summary>
        /// Expands the tree node
        /// </summary>
        public void Expand()
        {
            IsExpanded = true;
        }

        /// <summary>
        /// Expand all the child tree nodes
        /// </summary>
        public void ExpandAll()
        {
            this.IsExpanded = true;
            for (int i = 0; i < this.Items.Count; i++)
            {
                this.Items[i].ExpandAll();
            }
        }


        public override void Build(IIocContainer ctx)
        {
            base.Build(ctx);
            Name = "";
            Text = "";
            ImageListPrefix = "";
        }
    }
        #endregion
    /// <summary>
    ///   View model for the TreeView
    /// </summary>
    public class TreeViewViewModel : ViewModelCollection<TreeNodeViewModel>
    {
        /// <summary>
        ///  Prefix used for the url of image list items
        /// </summary>
        public virtual string ImageListPrefix { get; set; }
        /// <summary>
        /// Default image that is displayed by the tree nodes
        /// </summary>
        public virtual int ImageIndex { get; set; }
        /// <summary>
        ///  Control enabled state
        /// </summary>
        public virtual bool Enabled { get; set; }

        /// <summary>
        ///  Control visibility
        /// </summary>
        public virtual bool Visible { get; set; }

        /// <summary>
        ///  The UniqueID of the selected node (mainly used for client/server communication)
        /// </summary>
        public virtual string SelectedItemId
        {
            get;
            set;
        }
        /// <summary>
        /// true if the label text of the tree nodes can be edited; otherwise, false.
        /// </summary>
        public bool LabelEdit { get; set; }

        /// <summary>
        ///   Tag associated with this control
        /// </summary>
        public virtual string Tag { get; set; }
        /// <summary>
        /// Gets the amount of visible nodes on the control.
        /// </summary>
        public virtual int VisibleCount { get; set; }
        /// <summary>
        /// Gets or Sets the key of the default image key shown by each tree node
        /// </summary>
        public virtual string ImageKey { get; set; }

        /// <summary>
        ///  Transient field used to cache the selected node
        /// </summary>
        TreeNodeViewModel tmpSelectedNode;


        public override void Build(IIocContainer ctx)
        {
            base.Build(ctx);
            ImageListPrefix = "";
            Visible = true;
            Enabled = true;
            VisibleCount = 0;
        }

        public override Interfaces.IStateObject CreateItem(object item)
        {
            var result = Container.Resolve<TreeNodeViewModel>();
            if (item is Tuple<string, string>)
            {
                var tuple = (Tuple<string, string>)item;
                result.Name = tuple.Item1;
                result.Text = tuple.Item2;
            }
            return result;
        }

        /// <summary>
        ///    Add a new node to the top level of the tree with the specified text
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public TreeNodeViewModel Add(string text)
        {
            return Add("", text);
        }


        /// <summary>
        ///    Add a new node to the top level of the tree with the specified text and id
        /// </summary>
        /// <param name="name"></param>
        /// <param name="text"></param>
        /// <returns></returns>				
        public TreeNodeViewModel Add(string name, string text, int imageIndex = -1)
        {
            var result = Container.Resolve<TreeNodeViewModel>();
            result.Name = name;
            result.Text = text;
            result.ImageListPrefix = ImageListPrefix;
            result.ImageIndex = imageIndex;
            this.Items.Add(result);
            return result;
        }


        //////////////////////////////////////////////
        ///  Calculated properties
        ///  

        [StateManagement(false)]
        public TreeNodeViewModel SelectedNode
        {
            get
            {

                if (tmpSelectedNode == null
                    || (tmpSelectedNode != null && tmpSelectedNode.UniqueID != this.SelectedItemId))
                {
                    tmpSelectedNode = null;
                    var tmp = this.Items.FindById(this.SelectedItemId, true);
                    if (tmp != null && tmp.Length > 0)
                    {
                        tmpSelectedNode = tmp[0];
                    }
                }
                return tmpSelectedNode;
            }
            set
            {

            }
        }





    }

    /// <summary>
    ///   Extension methods to access the TreeView
    /// </summary>
    public static class TreeViewExtensions
    {
        public static TreeNodeViewModel[] FindById(this ItemsCollection<TreeNodeViewModel> nodes, string nodeUniqueId, bool searchAllChildren)
        {
            return nodes.InnerFind((TreeNodeViewModel node) => node.UniqueID == nodeUniqueId, searchAllChildren);
        }

        public static TreeNodeViewModel[] Find(this ItemsCollection<TreeNodeViewModel> nodes, string nodeName, bool searchAllChildren)
        {
            return nodes.InnerFind((TreeNodeViewModel node) => node.Name == nodeName, searchAllChildren);
        }

        private static void InnerFinding(this TreeNodeViewModel node, Predicate<TreeNodeViewModel> predicate, bool searchAllChildren, List<TreeNodeViewModel> results)
        {

            for (int i = 0; i < node.Items.Count; i++)
            {
                var child = node.Items[i];
                InnerSearchInsideChild(predicate, searchAllChildren, results, child);
            }
        }

        private static void InnerSearchInsideChild(Predicate<TreeNodeViewModel> predicate, bool searchAllChildren, List<TreeNodeViewModel> results, TreeNodeViewModel child)
        {
            if (predicate(child))
            {
                results.Add(child);
            }
            else
            {
                child.InnerFinding(predicate, searchAllChildren, results);
            }
        }

        private static TreeNodeViewModel[] InnerFind(this ItemsCollection<TreeNodeViewModel> nodes, Predicate<TreeNodeViewModel> predicate, bool searchAllChildren)
        {
            var results = new List<TreeNodeViewModel>();
            for (int i = 0; i < nodes.Count; i++)
            {
                var child = nodes[i];
                InnerSearchInsideChild(predicate, searchAllChildren, results, child);
            }
            return results.ToArray();
        }



    }
}
