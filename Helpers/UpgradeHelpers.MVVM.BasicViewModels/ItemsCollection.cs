using System;
using System.Linq;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
    
	public class ItemsCollection<T> where T : IStateObject
    {
		internal ViewModelCollection<T> _parent;
		internal ItemsCollection(ViewModelCollection<T> parent)
        {
            this._parent = parent;
        }

        public void Clear()
        {
            this._parent._items.Clear();
        }

		public T Add(object item)
        {
			T newItem;
            if (item == null) item = "";

            if (item is string || item.GetType().IsPrimitive)
            {
				newItem = (T)this._parent.CreateItem(item);
                _parent._items.Add(newItem);
            }
            else
            {
				_parent._items.Add(newItem = (T)item);
            }
			return newItem;
        }

		public T Insert(int index, object item)
		{
			T newItem;
			if (item == null) item = "";

			if (item is string || item.GetType().IsPrimitive)
			{
				newItem = (T)this._parent.CreateItem(item);
				_parent._items.Insert(index, newItem);
			}
			else
			{
				_parent._items.Insert(index, newItem = (T)item);
			}
			return newItem;
		}

		public void RemoveAt(int index)
		{
			_parent._items.RemoveAt(index);
		}

        public int Count
        {
            get
            {
                if (_parent._items != null)
                    return _parent._items.Count;
                return 0;
            }

        }

		public T this[int index]
		{
			get
			{
				return (T)_parent._items[index];
			}
			set
			{
				_parent._items[index] = value;
			}
		}

		public T this[string key]
		{
			get
			{
				if (IsIdenfiableElement())
				{
					return (T) _parent._items.Cast<IIdentifiableCollectionItem>().FirstOrDefault(i => i.Key.ToLower() == key.ToLower());
				}
				throw new InvalidOperationException("Type element does not implement ");
			}
		}

		private bool IsIdenfiableElement()
		{
			return typeof(IIdentifiableCollectionItem).IsAssignableFrom(typeof(T));
		}
    }
}
