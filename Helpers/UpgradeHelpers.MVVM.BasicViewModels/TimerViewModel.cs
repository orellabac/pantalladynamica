﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
	public class TimerViewModel : IDependantViewModel
	{
		public void Build(IIocContainer ctx)
		{
			Enabled = false;
		}

		public string UniqueID
		{
			get;
			set;
		}

		public virtual bool Enabled
		{
			get;
			set;
		}

		public virtual int Interval
		{
			get;
			set;
		}
	}
}
