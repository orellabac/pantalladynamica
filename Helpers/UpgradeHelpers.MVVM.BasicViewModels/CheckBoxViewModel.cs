using System;
using UpgradeHelpers;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{

    /// <summary>
    /// ViewModel to represent the state of a component on the view
    /// that allows the user to select a true or false condition.
    /// </summary>
   public class CheckBoxViewModel : IStateObjectWithInitialization, IDependantViewModel
   {
	   CheckState _checkState;
	   bool _checked;
       

      #region Data Members
      /// <summary>
      /// Returns a name that will be used in to view that presents this model
      /// </summary>
      public virtual string Name { get; set;}

      /// <summary>
      /// Gets or sets a value indicating whether in the view the element that represents this model can respond to user interaction
      /// </summary>
      public virtual bool Enabled { get; set; }

      /// <summary>
      /// Gets or sets a value indicating whether in the view the element that represent this model is displayed
      /// </summary>
      public virtual bool Visible { get; set; }


      /// <summary>
      /// Gets or sets the font of the text that the view will use to display text
      /// </summary>
      public virtual Font Font { get; set; }

      /// <summary>
      /// Gets or sets the distance in pixels between the top edge of the the element of the view that represents this model 
      /// and the top edge of the element in the view that contains it
      /// and the
      /// </summary>
      public virtual int Top { get; set; }

      /// <summary>
      /// Gets or sets the distance in pixels, between the 
      /// left edge of the element in the view that represents this model 
      /// and the left edge of the element of the view that contains it
      /// </summary>
      public virtual int Left { get; set; }

      /// <summary>
      /// Gets or sets the height in pixels for the element in the view that will represent this model
      /// </summary>
      public virtual int Height { get; set; }

      /// <summary>
      /// Gets or sets the Width in pixels for the element in the view that will represent this model
      /// </summary>
      public virtual int Width { get; set; }

      /// <summary>
      /// Gets or sets the tab order of the element in the view that  that will represent this model 
      /// </summary>
      public virtual int TabIndex { get; set; }

      /// <summary>
      /// Gets or sets the background color that will be used by the element tin
      /// the view that will represent this model
      /// </summary>
      public virtual Color BackColor { get; set; }

      /// <summary>
      /// Gets or sets the foreground color that will be used by the element tin
      /// the view that will represent this model
      /// </summary>
      public virtual Color ForeColor { get; set; }

      /// <summary>
      /// Gets or sets generic data
      /// </summary>
      public virtual string Tag { get; set; }

      /// <summary>
      /// Gets or sets the index value of the image that is displayed on the item.
      /// </summary>
      public virtual int ImageIndex { get; set; }


      /// <summary>
      /// Gets or sets the image list that is displayed on the item.
      /// </summary>
      public virtual object ImageList { get; set; }
      


      /// <summary>
      /// Indicates whether the element on the view is checked or unchecked
      /// </summary>
	  public virtual CheckState CheckState
	  {
		  get
		  {
			  return _checkState;
		  }
		  set
		  {
			  if (_checkState != value )
			  {

				  _checkState = value;
				  if (!IsCompatibleCheckState(_checked, _checkState))
				  {
					  this.Checked = _checkState == UpgradeHelpers.Helpers.CheckState.Checked;
				  }
			  }
		  }
	  }

	  public virtual bool Checked
	  {
		  get
		  {
			  return _checked;
		  }
		  set
		  {
			  if (_checked != value)
			  {
				  _checked = value;
				  if (!IsCompatibleCheckState(_checked, _checkState))
				  {
					  this.CheckState = value ? UpgradeHelpers.Helpers.CheckState.Checked : UpgradeHelpers.Helpers.CheckState.Unchecked;
				  }
			  }
		  }
	  }

	  private bool IsCompatibleCheckState(bool checkedFlag, UpgradeHelpers.Helpers.CheckState checkState)
	  {
		  if (checkedFlag)
		  {
			  return checkState == UpgradeHelpers.Helpers.CheckState.Checked;
		  }
		  else
		  {
			  return checkState != UpgradeHelpers.Helpers.CheckState.Checked;
		  }
	  }

	  

      #endregion

      #region Events
      private event EventHandler _CheckStateChanged;

       /// <summary>
      /// Occurs when the value of the CheckState property changes.
       /// </summary>
      public event  EventHandler CheckStateChanged
      {
        add
        {
            _CheckStateChanged += value;
        }
        remove
        {
            _CheckStateChanged -= value;
        }
      }

      /// <summary>
      /// Triggers the CheckStateChanged event
      /// </summary>
      public void OnCheckStateChanged()
      {
         if (_CheckStateChanged != null)
         {
             _CheckStateChanged(this, new System.EventArgs());
         }
      }

      #endregion


	  public string UniqueID
	  {
		  get;
		  set;
	  }

      /// <summary>
      /// Setup the model properties with its default values
      /// </summary>
	  public void Build(IIocContainer ctx)
	  {
          // Enabled DefaultValue
          Enabled = true;

          // Visible DefaultValue
          Visible = true;

          // CheckState DefaultValue
          CheckState = CheckState.Unchecked;
	  }

	  public virtual string Text { get; set; }
  }
}
