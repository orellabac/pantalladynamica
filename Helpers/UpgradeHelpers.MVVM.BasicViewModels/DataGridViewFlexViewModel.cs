using System;
using System.Collections.ObjectModel;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.BasicViewModels;
using UpgradeHelpers.Events;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Collections.Generic;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.WebMap.Server;


namespace UpgradeHelpers.BasicViewModels
{ 
    // Summary:
    //     Defines constants that indicate the alignment of content within a System.Windows.Forms.DataGridView
    //     cell.
    public enum DataGridViewContentAlignment
    {
        NotSet = 0,
        TopLeft = 1,
        TopCenter = 2,
        TopRight = 4,
        MiddleLeft = 16,
        MiddleCenter = 32,
        MiddleRight = 64,
        BottomLeft = 256,
        BottomCenter = 512,
        BottomRight = 1024,
    }
    /// <summary>
    ///  ViewModel for DataGridViewFlex grids
    /// </summary>
    public class DataGridViewFlexViewModel
        : IDependantViewModel, ICreatesObjects
    {
       

        /// <summary>
        ///   Fixed rows number ( only 0 and 1 are supported in this version)
        /// </summary>
        public virtual int FixedRowsCount { get; set; }

        /// <summary>
        ///   Fixed column number ( only 0 is supported in this version)
        /// </summary>
        public virtual System.Int32 FixedColumns { get; set; }

        /// <summary>
        ///  Name of this control
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        ///   Rows of this grid
        /// </summary>
        public virtual GridContent Content { get; set; }

        /// <summary>
        ///  Flag to indicate visibility
        /// </summary>
        public virtual bool Visible { get; set; }


        /// <summary>
        ///   Storage attribute for rows count
        /// </summary>
        public virtual int _rowCount { get; set; }

        /// <summary>
        ///   Storage attribute for columnscount
        /// </summary>
        public virtual int _colCount { get; set; }

        /// <summary>
        ///  Flag to indicate if the grid's data must be refreshed 
        /// </summary>
        public virtual bool NeedsRefresh { get; set; }
        public virtual int[] GridPosition { get; set; }


        /// <summary>
        ///   Background color
        /// </summary>
        public virtual Color BackColor { get; set; }

        /// <summary>
        ///  Foreground color
        /// </summary>
        public virtual Color ForeColor { get; set; }

        /// <summary>
        ///  A flag to indicate if this control is enabled
        /// </summary>
        public virtual bool Enabled { get; set; }


        /// <summary>
        ///   Grid coordinates
        ///   
        ///   This propery holds an array of two element with the position of the current cell
        /// </summary>
        public virtual int[] GridCellCoordinates { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int[] GridEndSelectionCoordinates { get; set; }


        /// <summary>
        ///   Additional data associated with each column
        /// </summary>
        public virtual int[] ColData { get; set; }


        /// <summary>
        ///   Tag of this control
        /// </summary>
        public virtual string Tag { get; set; }
        /// <summary>
        /// Gets or sets the ColAlignment for the control
        /// </summary>
        private DataGridViewContentAlignment[] _colAlignment = new DataGridViewContentAlignment[0];
        public virtual DataGridViewContentAlignment[] ColAlignment
        {
            get
            {
                if (_colAlignment.Length < ColumnsCount)
                {
                    return new DataGridViewContentAlignment[ColumnsCount];
                }
                return _colAlignment;
            }
            set
            {
                _colAlignment = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DataGridViewFlexViewModel()
        {
        }

        /// <summary>
        ///  Control initalization
        /// </summary>
        /// <param name="ctx"></param>
        public void Build(IIocContainer ctx)
        {

            // Name DefaultValue
            Name = "DataGridViewFlexViewModel";
            Columns = ctx.Resolve<FlexGridColumnsViewModel>();
            Content = ctx.Resolve<GridContent>();
            GridPosition = new int[2];
            GridEndSelectionCoordinates = new int[2];
            GridCellCoordinates = new int[4];
            FixedRowsCount = 1;
            Visible = true;


        }

        [StateManagement(false)]
        public virtual System.Int32 FixedRows
        {
            get
            {
                return FixedRowsCount;
            }
            set
            {
                if (FixedRowsCount != value)
                {

                    FixedRowsCount = value;
                    if (FixedRowsCount == 0)
                    {
                        RemoveColumnDefinitions();
                    }
                    else
                    {
                        //WARNING only FixedRows == 1 is supported
                        FixedRowsCount = value;
                        TakeFirstRowAsHeaders();
                    }
                    NeedsRefresh = true;
                }

            }
        }

        private void TakeFirstRowAsHeaders()
        {
            if (this.Items.Count > 0)
            {
                for (int i = 0; i < this.Columns.Items.Count; i++)
                {
                    var columnDefinition = this.Columns.Items[i];
                    columnDefinition.Text = this.Items[0].RowContent[i];
                    this.Columns.Items[i] = null;
                    this.Columns.Items[i] = columnDefinition;
                }

                // Reorganize rows

                for (int i = 0; i < _rowCount; i++)
                {
                    if ((i + 1) < _rowCount)
                    {
                        for (int j = 0; j < _colCount; j++)
                        {
                            this.Items[i].SetColumnValue(j, this.Items[i + 1].RowContent[j]);
                        }
                    }
                }

                for (int j = 0; j < _colCount; j++)
                {
                    this.Items[_rowCount - 1].SetColumnValue(j, "");
                }

            }
        }

        private void RemoveColumnDefinitions()
        {
            if (this.Items.Count >= 1)
            {
                // Reorganize rows
                for (int i = _rowCount - 1; i > 0; i--)
                {
                    if ((i - 1) >= 0)
                    {
                        for (int j = 0; j < _colCount; j++)
                        {
                            this.Items[i].SetColumnValue(j, this.Items[i - 1].RowContent[j]);
                        }
                    }
                }

                for (int i = 0; i < this.Columns.Items.Count; i++)
                {
                    var columnDefinition = this.Columns.Items[i];
                    this.Items[0].SetColumnValue(i, columnDefinition.Text);
                    columnDefinition.Text = "\t";
                    this.Columns.Items[i] = null;
                    this.Columns.Items[i] = columnDefinition;
                }
            }
        }




        [StateManagement(false)]
        public int RowsCount
        {
            get
            {
                return _rowCount;
            }
            set
            {
                _rowCount = value;
                SyncRowsWithRowCount();

            }
        }

        private void SyncRowsWithRowCount()
        {
            if ((this.Items.Count + this.FixedRows) != _rowCount)
            {
                if ((this.Items.Count + this.FixedRows) > _rowCount)
                {
                    while (Items.Count > 0 && (Items.Count) > (_rowCount - this.FixedRows))
                    {
                        Items.RemoveAt(Items.Count - 1);
                    }
                    if (FixedRows > _rowCount)
                    {
                        for (int i = 0; i < this.Columns.Items.Count; i++)
                        {
                            var columnDefinition = this.Columns.Items[i];
                            columnDefinition.Text = " ";
                            this.Columns.Items[i] = null;
                            this.Columns.Items[i] = columnDefinition;
                        }
                    }
                }
                else if ((this.Items.Count + this.FixedRows) < _rowCount)
                {
                    var elementsToAdd = _rowCount - (this.Items.Count + this.FixedRows);
                    for (int i = 0; i < elementsToAdd; i++)
                    {
                        var newRow = Container.Resolve<FlexGridRowViewModel>();
                        newRow.Resize(_colCount);
                        Items.Add(newRow);
                    }
                }
                NeedsRefresh = true;
            }
        }

        [StateManagement(false)]
        public int ColumnsCount
        {
            get
            {
                return _colCount;
            }
            set
            {
                if (_colCount != value)
                {
                    _colCount = value;
                    for (int i = 0; i < this.Items.Count; i++)
                    {
                        var row = this.Items[i];
                        row.Resize(_colCount);
                    }

                    SyncColumnDefinitions();

                    AdjustColumnData();
                    NeedsRefresh = true;

                }

            }
        }

        private void AdjustColumnData()
        {
            var newColumnCount = this.ColumnsCount;
            var newColumnData = new int[newColumnCount];
            if (ColData != null)
            {
                Array.Copy(ColData,
                        newColumnData,
                        Math.Min(ColData.Length, newColumnData.Length));
            }

            ColData = newColumnData;
        }

        private void SyncColumnDefinitions()
        {
            if (_colCount != Columns.Items.Count)
            {
                if (_colCount < Columns.Items.Count && _colCount > 0)
                {
                    var columnCountToRemove = Columns.Items.Count - _colCount;
                    for (int i = 0; i < columnCountToRemove; i++)
                    {
                        Columns.Items.RemoveAt(Columns.Items.Count - 1);
                    }
                }
                else if (_colCount > Columns.Items.Count)
                {
                    var columnCountToAdd = _colCount - Columns.Items.Count;
                    for (int i = 0; i < columnCountToAdd; i++)
                    {
                        var columnDefinition = Container.Resolve<FlexGridColumnViewModel>();
                        Columns.Items.Add(columnDefinition);
                        columnDefinition.Text = " ";
                    }
                }
            }
        }


        public virtual FlexGridColumnsViewModel Columns
        {
            get;
            set;
        }


        [StateManagement(false)]
        public int CellWidth
        {
            get
            {
                return GetGridCellCoordinatesValue(2);
            }
            set
            {
                SetGridCellCoordinatesValue(value, 2);
            }
        }
        [StateManagement(false)]
        public int CellHeight
        {
            get
            {
                return GetGridCellCoordinatesValue(3);
            }
            set
            {
                SetGridCellCoordinatesValue(value, 3);
            }
        }

        [StateManagement(false)]
        public int CellTop
        {
            get
            {
                return GetGridCellCoordinatesValue(1);
            }
            set
            {
                SetGridCellCoordinatesValue(value, 1);
            }
        }
        [StateManagement(false)]
        public int CellLeft
        {
            get
            {
                return GetGridCellCoordinatesValue(0);
            }
            set
            {
                SetGridCellCoordinatesValue(value, 0);
            }
        }

        private int GetGridCellCoordinatesValue(int idx)
        {
            return this.GridCellCoordinates != null && this.GridCellCoordinates.Length > idx ? this.GridCellCoordinates[idx] : 0;
        }

        private void SetGridCellCoordinatesValue(int value, int idx)
        {
            if (this.GridCellCoordinates != null && this.GridCellCoordinates.Length > idx)
            {
                this.GridCellCoordinates[idx] = value;
            }
        }

        #region Events
        private event System.EventHandler _EnterCell;
        public event System.EventHandler EnterCell
        {
            add
            {
                _EnterCell += value;
            }
            remove
            {
                _EnterCell -= value;
            }
        }

        public void OnEnterCell()
        {
            if (_EnterCell != null)
            {
                _EnterCell(this, new System.EventArgs());
            }
        }

        #endregion



        internal void AddItem(string lineToAdd, object p2)
        {
            var parts = lineToAdd.Split('\t');
            var newRow = Container.Resolve<FlexGridRowViewModel>();
            newRow.Resize(_colCount);
            for (int i = 0; i < _colCount && i < parts.Length; i++)
            {
                newRow.SetColumnValue(i, parts[i]);
            }
            Items.Add(newRow);

        }

        [StateManagement(false)]
        public string this[int i, int j]
        {
            get
            {
                int index = i;
                if (this.FixedRows > 0 && index > 0)
                {
                    index = index - this.FixedRows;
                }
                return Items[index].RowContent[j];
            }
            set
            {
                set_TextMatrix(i, j, value);
            }
        }


        internal void set_TextMatrix(int i, int j, string p)
        {
            if (FixedRows == 1 && i == 0)
            {
                this.Columns.Items[j].Text = p;
            }
            else
            {
                var index = i;
                if (this.FixedRows > 0 && index > 0)
                {
                    index = index - this.FixedRows;
                }
                this.Items[index].SetColumnValue(j, p);
            }
            NeedsRefresh = true;
        }



        public void SetColumnWidth(int j, double p)
        {
            this.Columns.Items[j].Width = Convert.ToInt32(p);
        }

        public string UniqueID
        {
            get;
            set;
        }

        public IIocContainer Container
        {
            get;
            set;
        }

        [StateManagement(false)]
        public ItemsCollection<FlexGridRowViewModel> Items
        {
            get
            {
                return this.Content.Items;
            }
            set
            {
            }
        }

        [StateManagement(false)]
        public int CurrentColumnIndex
        {
            get
            {
                return GridPosition[1];
            }
            set
            {
                RowSel = value;
                ColSel = value;

                GridPosition[1] = value;
                UpdateGridPosition();
            }
        }

        private void UpdateGridPosition()
        {
            var tmp = GridPosition;
            GridPosition = null;
            GridPosition = tmp;
        }

        [StateManagement(false)]
        public int CurrentRowIndex
        {
            get
            {
                return GridPosition[0];
            }
            set
            {
                RowSel = value;
                ColSel = value;
                GridPosition[0] = value;
                UpdateGridPosition();
            }
        }


        [StateManagement(false)]
        public int ColSel
        {
            get
            {
                return GridEndSelectionCoordinates[1];
            }
            set
            {

                GridEndSelectionCoordinates[1] = value;
                UpdateEndSelectionCoordinates();

            }
        }

        [StateManagement(false)]
        public int RowSel
        {
            get
            {
                return GridEndSelectionCoordinates[0];
            }
            set
            {

                GridEndSelectionCoordinates[0] = value;
                UpdateEndSelectionCoordinates();

            }
        }

        private void UpdateEndSelectionCoordinates()
        {
            var tmp = GridEndSelectionCoordinates;
            GridEndSelectionCoordinates = null;
            GridEndSelectionCoordinates = tmp;
        }


        public void AddItem(string data)
        {
            var parts = data.Split('\t');
            this.RowsCount = this.RowsCount + 1;
            var newRow = this.RowsCount - 1;
            var columnsCount = this.ColumnsCount;
            for (int i = 0; i < parts.Length && i < columnsCount; i++)
            {
                this[newRow, i] = parts[i];
            }
        }



        public void Clear()
        {
            var rows = this.RowsCount;
            var cols = this.ColumnsCount;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    this[i, j] = i == 0 ? " " : "";
                }
            }
        }

        public void RemoveItem(int rowNumber)
        {
            rowNumber = rowNumber - FixedRows;
            if (rowNumber < RowsCount)
            {
                var rows = RowsCount - FixedRows;
                for (int i = rowNumber; i < rows - 1; i++)
                {
                    this.Items[i].RowContent = this.Items[i + 1].RowContent;
                }
                RowsCount = RowsCount - 1;
                NeedsRefresh = true;
            }
        }


    }

    public class GridContent : ViewModelCollection<FlexGridRowViewModel>, IInternalData
    {

        public override IStateObject CreateItem(object item)
        {
            return Container.Resolve<FlexGridRowViewModel>();
        }
    }
    public class FlexGridColumnViewModel : IDependantViewModel
    {
        public string UniqueID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Text { get; set; }
        public virtual string Tag { get; set; }
        public virtual int Width { get; set; }

        public void Build(IIocContainer ctx)
        {
            Name = "";
            Width = 65;
        }

    }


    public class FlexGridColumnsViewModel : ViewModelCollection<FlexGridColumnViewModel>
    {
        public override IStateObject CreateItem(object item)
        {
            var result = Container.Resolve<ColumnHeaderViewModel>();
            result.Name = item.ToString();
            return result;
        }
    }


    /// <summary>
    /// </summary>
    public class FlexGridRowViewModel : IDependantViewModel, IInternalData
    {
        public string UniqueID { get; set; }
        public virtual string[] RowContent { get; set; }


        public void Build(IIocContainer ctx)
        {
            RowContent = new string[0];
        }

        //public string Text
        //{
        //	get
        //	{
        //		return RowContent[0];
        //	}
        //	set
        //	{
        //		var tmpArray = RowContent;
        //		tmpArray[0] = value;
        //		// Force marking this property as 'dirty'
        //		RowContent = null;
        //		RowContent = tmpArray;
        //	}
        //}

        public void SetColumnValue(int item, string value)
        {
            if (RowContent.Length > item)
            {
                var tmpArray = RowContent;
                tmpArray[item] = value;
                // Force marking this property as 'dirty'
                RowContent = null;
                RowContent = tmpArray;
            }
            else
            {
                string[] tmp = RowContent;
                string[] newArray = new string[item + 1];
                Array.Copy(tmp, newArray, tmp.Length);
                newArray[item] = value;
                RowContent = newArray;
            }

        }


        internal void Resize(int ColCont)
        {
            if (RowContent.Length != ColCont)
            {
                if (RowContent.Length > ColCont)
                {
                    string[] tmp = RowContent;
                    string[] newArray = new string[ColCont];
                    Array.Copy(tmp, newArray, ColCont);
                    RowContent = newArray;
                }
                else if (RowContent.Length < ColCont)
                {
                    string[] tmp = RowContent;
                    string[] newArray = new string[ColCont];

                    Array.Copy(tmp, newArray, tmp.Length);
                    RowContent = newArray;
                    for (int i = tmp.Length; i < ColCont; i++)
                    {
                        newArray[i] = "";
                    }

                }
            }

        }
    }
}
