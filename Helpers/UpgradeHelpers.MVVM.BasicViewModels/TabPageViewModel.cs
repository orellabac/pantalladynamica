﻿using System;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
	/// <summary>
	/// Represents a single tab page in a TabControlViewModel.
	/// </summary>
	public class TabPageViewModel : IDependantViewModel
	{
		/// <summary>
		/// Gets or sets the text to display on the tab
		/// </summary>
		public string UniqueID { get; set; }

		public virtual bool Enabled { get; set; }

		public virtual bool Focused { get; internal set; }

		public virtual string Text { get; set; }

		public virtual bool Visible { get; set; }

		public void Build(IIocContainer ctx)
		{
			Enabled = true;
			Visible = true;
		}
	}
}
