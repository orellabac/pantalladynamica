﻿using System;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.Interfaces;

namespace UpgradeHelpers.BasicViewModels
{
	public class RadioButtonViewModel : IDependantViewModel
	{

		public RadioButtonViewModel()
		{
			
		}
		public virtual bool Visible { get; set; }
		public virtual bool Enabled { get; set; }
		public virtual bool Checked { get; set; }
		public virtual Font Font { get; set; }


		public void Build(IIocContainer ctx)
		{
			Enabled = true;
			Visible = true;
		}

		public string UniqueID
		{
			get;
			set;
		}

		public virtual string Text { get; set; }
        /// <summary>
        /// Gets or sets generic data
        /// </summary>
        public virtual string Tag { get; set; }

		/// <summary>
		///     Gets or sets the background color that will be used by the element tin
		///     the view that will represent this model
		/// </summary>
		public virtual Color BackColor { get; set; }

		/// <summary>
		///     Gets or sets the foreground color that will be used by the element tin
		///     the view that will represent this model
		/// </summary>
		public virtual Color ForeColor { get; set; }

		/// <summary>
		///     Gets or sets the distance in pixels between the top edge of the the element of the view that represents this model
		///     and the top edge of the element in the view that contains it
		///     and the
		/// </summary>
		public virtual int Top { get; set; }

		/// <summary>
		///     Gets or sets the distance in pixels, between the
		///     left edge of the element in the view that represents this model
		///     and the left edge of the element of the view that contains it
		/// </summary>
		public virtual int Left { get; set; }

		/// <summary>
		///     Gets or sets the height in pixels for the element in the view that will represent this model
		/// </summary>
		public virtual int Height { get; set; }

		/// <summary>
		///     Gets or sets the Width in pixels for the element in the view that will represent this model
		/// </summary>
		public virtual int Width { get; set; }

	}
}
