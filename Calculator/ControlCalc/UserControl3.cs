using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UpgradeHelpers;
using UpgradeHelpers.BasicViewModels;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.Events;
using UpgradeHelpers.Helpers;

namespace ControlCalc
{

	public class UserControl3
		: UserControl2, UpgradeHelpers.Interfaces.IUserControl, ICreatesObjects
	{

		public virtual IModelList<TextBoxViewModel> textboxes { get; set; }
		public virtual IModelList<LabelViewModel> labels { get; set; }

		public override void Build(UpgradeHelpers.Interfaces.IIocContainer ctx)
		{
			base.Build(ctx);
			this.button1 = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			textboxes = ctx.Resolve<IModelList<TextBoxViewModel>>();
			labels = ctx.Resolve<IModelList<LabelViewModel>>();
			button1.Text = "Add a Control";
		}


		public UserControl3()
		{
		}
		[UpgradeHelpers.Helpers.Watchable(typeof(UpgradeHelpers.WebMap.Server.Common.StructsWatcher))]
		[UpgradeHelpers.Helpers.StateManagement(UpgradeHelpers.Helpers.StateManagementValues.ServerOnly)]
		public Patito Feo;

		[Handler()]
		internal void button1_Click(object sender, System.EventArgs e)
		{
			Feo.Nombre = "Patricio";
			Feo.Edad = 45;
			var newTextbox = Container.Resolve<TextBoxViewModel>();
			newTextbox.Text = Feo.Nombre;
			textboxes.Add(newTextbox);
			ViewManager.ShowMessage("Event executed");
		}


		[Handler()]
		internal void button2_Click(object sender, System.EventArgs e)
		{
			Feo.Nombre = "Bob";
			Feo.Edad = 45;

			var newLabel = Container.Resolve<LabelViewModel>();
			newLabel.Text = Feo.Nombre;
			labels.Add(newLabel);
			ViewManager.ShowMessage("Event executed 2");
		}


		public struct Patito
		{
			public string Nombre;
			public int Edad;
		}

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel button1 { get; set; }

		public override void Init()
		{
			base.Init();
		}

	}

	public class UserControl3Controller
		: System.Web.Mvc.Controller
	{

		public System.Web.Mvc.ActionResult button1_Click(ControlCalc.UserControl3 usercontrol)
		{
			usercontrol.button1_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}


		public System.Web.Mvc.ActionResult button2_Click(ControlCalc.UserControl3 usercontrol)
		{
			usercontrol.button2_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

	}
}