using System.Web;
using System.Web.Optimization;

namespace WebSite
{
	public class BundleConfig
	{
		// For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725

		private static void AddScriptsByPrefix(ScriptBundle codeBundle, string serverPath, string projectCodePrefix)
		{
			var folderPath = HttpContext.Current.Server.MapPath(serverPath);

			foreach (var file in System.IO.Directory.GetFiles(folderPath, projectCodePrefix + ".*.js", System.IO.SearchOption.TopDirectoryOnly))
			{
				codeBundle.Include("~/Scripts" + file.Replace(folderPath, string.Empty).Replace("\\", "/"));
			}

		}

		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));
			bundles.Add(new ScriptBundle("~/bundles/extras").Include("~/Scripts/extras/*.js"));
			bundles.Add(new StyleBundle("~/bundles/extrascss").Include("~/Scripts/extrascss/*.css"));

			bundles.Add(new ScriptBundle("~/bundles/guilibs").Include(
				"~/Scripts/ie8polyfills.js",
				"~/Scripts/jquery.js",
				"~/Scripts/jquery.blockUI.js",
				"~/Scripts/kendo.web.js",
				"~/Scripts/helpers/jquery.caret.js",
				"~/Scripts/helpers/kendo.listbox.js",
				"~/Scripts/helpers/WebMap.all.js",
				"~/Scripts/progress-polyfill.js"));
			bundles.Add(new StyleBundle("~/bundles/guicss").Include(
				"~/Content/kendo.common.css",
				"~/Content/kendo.default.css",
				"~/Content/progress-polyfill.css"));
			//Adding client side code for project:
			var codeBundle = new ScriptBundle("~/bundles/code");
			bundles.Add(codeBundle);
			bundles.Add(new ScriptBundle("~/bundles/libs").Include("~/Scripts/libs/*.js"));
			bundles.Add(new ScriptBundle("~/bundles/usercontrols").Include("~/Scripts/usercontrols/*.js"));
		}
	}
}


