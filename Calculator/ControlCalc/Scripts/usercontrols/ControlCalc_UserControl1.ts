/// <reference path="../helpers/WebMap.all.d.ts" />

(function ($) {

	var DATABINDING = "dataBinding",
		DATABOUND = "dataBound",
		CHANGE = "change";

	// shorten references to variables. this is better for uglification
	var kendo = (<any>window).kendo,
		ui = kendo.ui,
		Widget = ui.Widget;
	var ControlCalc_UserControl1 = ui.BaseUserControl.extend({

		initClientMethods: function (value) {
			var that = value;
			value.logic = {};
			value.logic.btn7_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btn7_Click"});
			};
			value.logic.btn8_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btn8_Click"});
			};
			value.logic.btn9_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btn9_Click"});
			};
			value.logic.btnDiv_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btnDiv_Click"});
			};
			value.logic.btn4_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btn4_Click"});
			};
			value.logic.btn5_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btn5_Click"});
			};
			value.logic.btn6_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btn6_Click"});
			};
			value.logic.btnMult_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btnMult_Click"});
			};
			value.logic.btn1_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btn1_Click"});
			};
			value.logic.btn2_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btn2_Click"});
			};
			value.logic.btn3_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btn3_Click"});
			};
			value.logic.btnMin_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btnMin_Click"});
			};
			value.logic.btn0_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btn0_Click"});
			};
			value.logic.btndot_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btndot_Click"});
			};
			value.logic.btnMax_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btnMax_Click"});
			};
			value.logic.btnEqual_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btnEqual_Click"});
			};
			value.logic.btnclean_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl1",action:"btnclean_Click"});
			};

		},
		options: {
			// the name is what it will appear as off the kendo namespace(i.e. kendo.ui.WindowsFormsApplication1_UserControl1). 
			// The jQuery plugin would be jQuery.fn.kendoUserControl1. 
			name: "ControlCalc_UserControl1", 
			value: null, 
			css: 
				".ControlCalc_UserControl1 {\
				}\
				.ControlCalc_UserControl1 .UserControl1 {\
					background-color: Gray;\
					width: 233px;\
					height: 235px;\
				}\
				.ControlCalc_UserControl1 .txtResult {\
					background-color: White;\
					font-family: Microsoft Sans Serif;\
					font-size: 16.8px;\
					font-weight: bold;\
					left: 3px;\
					top: 3px;\
					position: absolute;\
					width: 226px;\
					height: 59px;\
					text-align: right;\
				}\
				.ControlCalc_UserControl1 .btn7 {\
					background-color: rgba(227, 227, 227, 1);\
					left: 3px;\
					top: 68px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btn8 {\
					background-color: rgba(227, 227, 227, 1);\
					left: 61px;\
					top: 68px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btn9 {\
					background-color: rgba(227, 227, 227, 1);\
					left: 119px;\
					top: 68px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btnDiv {\
					background-color: SkyBlue;\
					left: 177px;\
					top: 68px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btn4 {\
					background-color: rgba(227, 227, 227, 1);\
					left: 3px;\
					top: 102px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btn5 {\
					background-color: rgba(227, 227, 227, 1);\
					left: 61px;\
					top: 102px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btn6 {\
					background-color: rgba(227, 227, 227, 1);\
					left: 119px;\
					top: 102px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btnMult {\
					background-color: SkyBlue;\
					left: 177px;\
					top: 102px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btn1 {\
					background-color: rgba(227, 227, 227, 1);\
					left: 3px;\
					top: 136px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btn2 {\
					background-color: rgba(227, 227, 227, 1);\
					left: 61px;\
					top: 136px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btn3 {\
					background-color: rgba(227, 227, 227, 1);\
					left: 119px;\
					top: 136px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btnMin {\
					background-color: SkyBlue;\
					left: 177px;\
					top: 136px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btn0 {\
					background-color: rgba(227, 227, 227, 1);\
					left: 3px;\
					top: 170px;\
					position: absolute;\
					width: 110px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btndot {\
					background-color: rgba(227, 227, 227, 1);\
					left: 119px;\
					top: 170px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btnMax {\
					background-color: SkyBlue;\
					left: 177px;\
					top: 170px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btnEqual {\
					background-color: SkyBlue;\
					left: 3px;\
					top: 204px;\
					position: absolute;\
					width: 168px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}\
				.ControlCalc_UserControl1 .btnclean {\
					background-color: Coral;\
					left: 177px;\
					top: 204px;\
					position: absolute;\
					width: 52px;\
					height: 28px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}",
			// other options go here 
			template: 
				"<div class=\"ControlCalc_UserControl1\">\
					<div name=\"UserControl1\" class=\"UserControl1\">\
						<textarea disabled=\"disabled\" id=\"txtResult\" tabindex=\"2\" class=\" txtResult\" data-bind=\"value : #= $$$parent #.txtResult.Text\"></textarea>\
						<button id=\"btn7\" tabindex=\"1\" class=\" btn7\" type=\"button\" data-bind=\"buttonText : #= $$$parent #.btn7.Text, events : { click : #= $$$parent #.logic.btn7_Click }, \">7</button>\
						<button id=\"btn8\" tabindex=\"3\" class=\" btn8\" type=\"button\" data-bind=\"buttonText : #= $$$parent #.btn8.Text, events : { click : #= $$$parent #.logic.btn8_Click }, \">8</button>\
						<button id=\"btn9\" tabindex=\"4\" class=\" btn9\" type=\"button\" data-bind=\"buttonText : #= $$$parent #.btn9.Text, events : { click : #= $$$parent #.logic.btn9_Click }, \">9</button>\
						<button id=\"btnDiv\" tabindex=\"5\" class=\" btnDiv\" type=\"button\" data-bind=\"events : { click : #= $$$parent #.logic.btnDiv_Click }, \">/</button>\
						<button id=\"btn4\" tabindex=\"6\" class=\" btn4\" type=\"button\" data-bind=\"buttonText : #= $$$parent #.btn4.Text, events : { click : #= $$$parent #.logic.btn4_Click }, \">4</button>\
						<button id=\"btn5\" tabindex=\"7\" class=\" btn5\" type=\"button\" data-bind=\"buttonText : #= $$$parent #.btn5.Text, events : { click : #= $$$parent #.logic.btn5_Click }, \">5</button>\
						<button id=\"btn6\" tabindex=\"8\" class=\" btn6\" type=\"button\" data-bind=\"buttonText : #= $$$parent #.btn6.Text, events : { click : #= $$$parent #.logic.btn6_Click }, \">6</button>\
						<button id=\"btnMult\" tabindex=\"9\" class=\" btnMult\" type=\"button\" data-bind=\"events : { click : #= $$$parent #.logic.btnMult_Click }, \">x</button>\
						<button id=\"btn1\" tabindex=\"10\" class=\" btn1\" type=\"button\" data-bind=\"buttonText : #= $$$parent #.btn1.Text, events : { click : #= $$$parent #.logic.btn1_Click }, \">1</button>\
						<button id=\"btn2\" tabindex=\"11\" class=\" btn2\" type=\"button\" data-bind=\"buttonText : #= $$$parent #.btn2.Text, events : { click : #= $$$parent #.logic.btn2_Click }, \">2</button>\
						<button id=\"btn3\" tabindex=\"12\" class=\" btn3\" type=\"button\" data-bind=\"buttonText : #= $$$parent #.btn3.Text, events : { click : #= $$$parent #.logic.btn3_Click }, \">3</button>\
						<button id=\"btnMin\" tabindex=\"13\" class=\" btnMin\" type=\"button\" data-bind=\"events : { click : #= $$$parent #.logic.btnMin_Click }, \">-</button>\
						<button id=\"btn0\" tabindex=\"14\" class=\" btn0\" type=\"button\" data-bind=\"buttonText : #= $$$parent #.btn0.Text, events : { click : #= $$$parent #.logic.btn0_Click }, \">0</button>\
						<button id=\"btndot\" tabindex=\"15\" class=\" btndot\" type=\"button\" data-bind=\"buttonText : #= $$$parent #.btndot.Text, events : { click : #= $$$parent #.logic.btndot_Click }, \">.</button>\
						<button id=\"btnMax\" tabindex=\"16\" class=\" btnMax\" type=\"button\" data-bind=\"events : { click : #= $$$parent #.logic.btnMax_Click }, \">+</button>\
						<button id=\"btnEqual\" tabindex=\"17\" class=\" btnEqual\" type=\"button\" data-bind=\"events : { click : #= $$$parent #.logic.btnEqual_Click }, \">=</button>\
						<button id=\"btnclean\" tabindex=\"18\" class=\" btnclean\" type=\"button\" data-bind=\"events : { click : #= $$$parent #.logic.btnclean_Click }, \">AC</button>\
					</div>\
				</div>"
		},

		// events are used by other widgets / developers - API for other purposes
		// these events support MVVM bound items in the template. for loose coupling with MVVM.
		events: [
		// call before mutating DOM.
		// mvvm will traverse DOM, unbind any bound elements or widgets
			DATABINDING,
		// call after mutating DOM
		// traverses DOM and binds ALL THE THINGS
			DATABOUND,
			CHANGE
		]
	});
	ui.plugin(ControlCalc_UserControl1);
})(jQuery); 


