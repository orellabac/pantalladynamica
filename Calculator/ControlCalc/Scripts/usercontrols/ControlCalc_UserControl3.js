/// <reference path="../helpers/WebMap.all.d.ts" />
(function ($) {
    var DATABINDING = "dataBinding", DATABOUND = "dataBound", CHANGE = "change";
    // shorten references to variables. this is better for uglification
    var kendo = window.kendo, ui = kendo.ui, Widget = ui.Widget;
    //WARNING: UserControls Heritage can present problems when loading the respective .js files due to the order that need to be loaded to run
    var ControlCalc_UserControl3 = ui.ControlCalc_UserControl2.extend({
        buildUI: function () {
            var that = this;
            that.options.uiinitialized = true;
            that.setOptions(that.options);
            var that = this, view = that._value;
            var parent = that._value.parent();
            var ancestorPath = parent["$$$parent"] || "";
            if (parent) {
                var props = Object.getOwnPropertyNames(parent);
                for (var i = 0; i < props.length; i++) {
                    var propName = props[i];
                    if (parent[propName] == that._value) {
                        if (ancestorPath == "") {
                            that._value["$$$parent"] = propName;
                        }
                        else {
                            that._value["$$$parent"] = ancestorPath + "." + propName;
                        }
                        break;
                    }
                }
                var html = this.applyTemplate(view);
                that.element.html(html);
                var that = that._value;
                if (that.textboxes)
                    for (var i = 0; i < that.textboxes.length; i++) {
                        //is present?
                        var uid = that.textboxes[i].UniqueID;
                        var item = $("input[id='" + uid + "']");
                        if (item.length == 0) {
                            $("#divfordynamic").append("<input id='" + uid + "' type='text' data-bind='value: " + that["$$$parent"] + ".textboxes[" + i + "].Text' style='float:left;clear:both'/>");
                            item = $("input[id='" + uid + "']");
                        }
                    }
                if (that.labels)
                    for (var i = 0; i < that.labels.length; i++) {
                        //is present?
                        var uid = that.labels[i].UniqueID;
                        var item = $("span[id='" + uid + "']");
                        if (item.length == 0) {
                            $("#divfordynamic").append("<span id='" + uid + "'  data-bind='text: " + that["$$$parent"] + ".labels[ + " + i + "].Text ' style='color:white;float:left;clear:both' />");
                            item = $("span[id='" + uid + "']");
                        }
                    }
            }
        },
        initClientMethods: function (value) {
            var element = this.element;
            var that = this;
            value.textboxes.bind("change", function (e) {
                if (element) {
                    for (var i = 0; i < that.textboxes.length; i++) {
                        //is present?
                        var uid = that.textboxes[i].UniqueID;
                        var item = $("input[id='" + uid + "']");
                        if (item.length == 0) {
                            $("#divfordynamic").append("<input id='" + uid + "' type='text' data-bind='value: Text' style='float:left;clear:both'/>");
                            item = $("input[id='" + uid + "']");
                            kendo.bind(item, that.textboxes[i]);
                        }
                    }
                }
                else {
                    alert(" widget got crazy!");
                }
            });
            value.labels.bind("change", function (e) {
                if (element) {
                    for (var i = 0; i < that.labels.length; i++) {
                        //is present?
                        var uid = that.labels[i].UniqueID;
                        var item = $("span[id='" + uid + "']");
                        if (item.length == 0) {
                            $("#divfordynamic").append("<span id='" + uid + "'  data-bind='text: Text' style='color:white;float:left;clear:both' />");
                            item = $("span[id='" + uid + "']");
                            kendo.bind(item, that.labels[i]);
                        }
                    }
                }
                else {
                    alert(" widget got crazy!");
                }
            });
            ui.ControlCalc_UserControl2.prototype.initClientMethods(value);
            var that = value;
            value.logic = {};
            value.logic.button1_Click = function (e) {
                return window.app.sendAction({ mainobj: that, controller: "UserControl3", action: "button1_Click" });
            };
            value.logic.button2_Click = function (e) {
                return window.app.sendAction({ mainobj: that, controller: "UserControl3", action: "button2_Click" });
            };
        },
        options: {
            // the name is what it will appear as off the kendo namespace(i.e. kendo.ui.WindowsFormsApplication1_UserControl1). 
            // The jQuery plugin would be jQuery.fn.kendoUserControl1. 
            name: "ControlCalc_UserControl3",
            value: null,
            css: ui.ControlCalc_UserControl2.prototype.options.css + ".ControlCalc_UserControl3 {\
				}\
				.ControlCalc_UserControl3 .UserControl3 {\
					width: 237px;\
					height: 345px;\
				}\
                .dynamic { background: black;position: fixed;left: 10px;top: 300px;height: 400px;}\
				.ControlCalc_UserControl3 .button1 {\
					left: 100px;\
					top: 316px;\
					position: absolute;\
					width: 100px;\
					height: 23px;\
					padding: 0px 0px 0px 0px;\
					display: table-cell;\
					vertical-align: middle;\
					display: table-cell;\
				}",
            // other options go here 
            template: ui.ControlCalc_UserControl2.prototype.options.template + "<div class=\"ControlCalc_UserControl3\">\
					<div name=\"UserControl3\" class=\"UserControl3\">\
						<button id=\"button1\" tabindex=\"1\" class=\" button1\" type=\"button\" data-bind=\"events : { click : #= $$$parent #.logic.button1_Click }, \">Add Textbox</button>\
						<button id=\"button2\" tabindex=\"1\" class=\" button2\" type=\"button\" data-bind=\"events : { click : #= $$$parent #.logic.button2_Click }, \">Add Label</button>\
                        <div id='divfordynamic' class='dynamic'>PlaceHolder For Dynamic elements</div>\
					</div>\
				</div>"
        },
        // events are used by other widgets / developers - API for other purposes
        // these events support MVVM bound items in the template. for loose coupling with MVVM.
        events: [
            DATABINDING,
            DATABOUND,
            CHANGE
        ]
    });
    ui.plugin(ControlCalc_UserControl3);
})(jQuery);
//# sourceMappingURL=ControlCalc_UserControl3.js.map