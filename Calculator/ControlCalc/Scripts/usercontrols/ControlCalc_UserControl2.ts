/// <reference path="../helpers/WebMap.all.d.ts" />

(function ($) {

	var DATABINDING = "dataBinding",
		DATABOUND = "dataBound",
		CHANGE = "change";

	// shorten references to variables. this is better for uglification
	var kendo = (<any>window).kendo,
		ui = kendo.ui,
		Widget = ui.Widget;
	var ControlCalc_UserControl2 = ui.BaseUserControl.extend({

        initClientMethods: function (value) {


			var that = value;
			value.logic = {};
			value.logic.pictureBox1_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl2",action:"pictureBox1_Click"});
			};
			value.logic.pictureBox2_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl2",action:"pictureBox2_Click"});
			};
			value.logic.pictureBox3_Click = function(e:any) {
				return window.app.sendAction({mainobj:that,controller:"UserControl2",action:"pictureBox3_Click"});
			};

		},
		options: {
			// the name is what it will appear as off the kendo namespace(i.e. kendo.ui.WindowsFormsApplication1_UserControl1). 
			// The jQuery plugin would be jQuery.fn.kendoUserControl1. 
			name: "ControlCalc_UserControl2", 
			value: null, 
			css: 
				".ControlCalc_UserControl2 {\
				}\
				.ControlCalc_UserControl2 .UserControl2 {\
					background-color: White;\
					width: 237px;\
					height: 312px;\
				}\
				.ControlCalc_UserControl2 .userControl11 {\
					background-color: Blue;\
					left: 3px;\
					top: 3px;\
					position: absolute;\
					width: 231px;\
					height: 235px;\
					overflow: hidden;\
				}\
				.ControlCalc_UserControl2 .pictureBox1 {\
					background-image: url(\"/Resources/images/UserControl2.pictureBox1.Image.png\");\
					background-repeat: no-repeat;\
					left: 3px;\
					top: 244px;\
					position: absolute;\
					width: 65px;\
					height: 66px;\
				}\
				.ControlCalc_UserControl2 .pictureBox2 {\
					background-image: url(\"/Resources/images/UserControl2.pictureBox2.Image.png\");\
					background-repeat: no-repeat;\
					left: 86px;\
					top: 244px;\
					position: absolute;\
					width: 65px;\
					height: 66px;\
				}\
				.ControlCalc_UserControl2 .pictureBox3 {\
					background-image: url(\"/Resources/images/UserControl2.pictureBox3.Image.png\");\
					background-repeat: no-repeat;\
					left: 169px;\
					top: 244px;\
					position: absolute;\
					width: 65px;\
					height: 66px;\
				}",
			// other options go here 
			template: 
				"<div class=\"ControlCalc_UserControl2\">\
					<div name=\"UserControl2\" class=\"UserControl2\">\
						<div id=\"userControl11\" class=\" userControl11\"> #= kendo.ui.ControlCalc_UserControl1.prototype.applyTemplate(userControl11) # </div>\
						<fieldset id=\"pictureBox1\" class=\" pictureBox1\" data-bind=\"enabled : #= $$$parent #.pictureBox1.Enabled, events : { click : #= $$$parent #.logic.pictureBox1_Click }, \"></fieldset>\
						<fieldset id=\"pictureBox2\" class=\" pictureBox2\" data-bind=\"enabled : #= $$$parent #.pictureBox2.Enabled, events : { click : #= $$$parent #.logic.pictureBox2_Click }, \"></fieldset>\
						<fieldset id=\"pictureBox3\" class=\" pictureBox3\" data-bind=\"enabled : #= $$$parent #.pictureBox3.Enabled, events : { click : #= $$$parent #.logic.pictureBox3_Click }, \"></fieldset>\
					</div>\
				</div>"
		},

		// events are used by other widgets / developers - API for other purposes
		// these events support MVVM bound items in the template. for loose coupling with MVVM.
		events: [
		// call before mutating DOM.
		// mvvm will traverse DOM, unbind any bound elements or widgets
			DATABINDING,
		// call after mutating DOM
		// traverses DOM and binds ALL THE THINGS
			DATABOUND,
			CHANGE
		]
	});
	ui.plugin(ControlCalc_UserControl2);
})(jQuery); 


