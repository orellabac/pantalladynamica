using System;
using UpgradeHelpers;
using UpgradeHelpers.BasicViewModels;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.Events;
using UpgradeHelpers.Helpers;

namespace ControlCalc
{

	public class UserControl2
		: UpgradeHelpers.Interfaces.IUserControl
	{

		public virtual void Build(UpgradeHelpers.Interfaces.IIocContainer ctx)
		{
			this.pictureBox1 = ctx.Resolve<UpgradeHelpers.BasicViewModels.PictureBoxViewModel>();
			this.pictureBox2 = ctx.Resolve<UpgradeHelpers.BasicViewModels.PictureBoxViewModel>();
			this.pictureBox3 = ctx.Resolve<UpgradeHelpers.BasicViewModels.PictureBoxViewModel>();
			this.userControl11 = ctx.Resolve<ControlCalc.UserControl1>();
		}


		public UserControl2()
		{
		}

		[Handler()]
		internal protected void pictureBox1_Click(object sender, System.EventArgs e)
		{
			ViewManager.ShowMessage("Click over img calc #1");
		}

		[Handler()]
		internal protected void pictureBox2_Click(object sender, System.EventArgs e)
		{
			ViewManager.ShowMessage("Event Click img calc #2");
		}

		[Handler()]
		internal protected void pictureBox3_Click(object sender, System.EventArgs e)
		{
			ViewManager.ShowMessage("Click Calculator #3");
		}

		public string UniqueID { get; set; }

		public IViewManager ViewManager { get; set; }

		public IIocContainer Container { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.PictureBoxViewModel pictureBox1 { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.PictureBoxViewModel pictureBox2 { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.PictureBoxViewModel pictureBox3 { get; set; }

		public virtual ControlCalc.UserControl1 userControl11 { get; set; }

		public virtual void Init()
		{
		}

	}

	public class UserControl2Controller
		: System.Web.Mvc.Controller
	{

		public System.Web.Mvc.ActionResult pictureBox1_Click(ControlCalc.UserControl2 usercontrol)
		{
			usercontrol.pictureBox1_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult pictureBox2_Click(ControlCalc.UserControl2 usercontrol)
		{
			usercontrol.pictureBox2_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult pictureBox3_Click(ControlCalc.UserControl2 usercontrol)
		{
			usercontrol.pictureBox3_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

	}
}