using System;
using UpgradeHelpers;
using UpgradeHelpers.BasicViewModels;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.Events;
using UpgradeHelpers.Helpers;

namespace ControlCalc
{

	public class UserControl1
		: UpgradeHelpers.Interfaces.IUserControl
	{

		public virtual void Build(UpgradeHelpers.Interfaces.IIocContainer ctx)
		{
			this.txtResult = ctx.Resolve<UpgradeHelpers.BasicViewModels.TextBoxViewModel>();
			this.btn7 = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btn7.Text = "7";
			this.btn8 = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btn8.Text = "8";
			this.btn9 = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btn9.Text = "9";
			this.btn4 = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btn4.Text = "4";
			this.btn5 = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btn5.Text = "5";
			this.btn6 = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btn6.Text = "6";
			this.btn1 = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btn1.Text = "1";
			this.btn2 = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btn2.Text = "2";
			this.btn3 = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btn3.Text = "3";
			this.btn0 = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btn0.Text = "0";
			this.btndot = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btndot.Text = ".";
			this.result = 0.0;
			this.btnDiv = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btnDiv.Text = "/";
			this.btnMult = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btnMult.Text = "x";
			this.btnMin = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btnMin.Text = "-";
			this.btnMax = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btnMax.Text = "+";
			this.btnEqual = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btnEqual.Text = "=";
			this.btnclean = ctx.Resolve<UpgradeHelpers.BasicViewModels.ButtonViewModel>();
			btnclean.Text = "AC";
		}


		public UserControl1()
		{
		}

		public virtual string input { get; set; }

		public virtual string operand1 { get; set; }

		public virtual string operand2 { get; set; }

		public virtual char operation { get; set; }

		public virtual double result { get; set; }

		public void Load()
		{
		    //txtResult.Text = "0";
		}

		[Handler()]
		internal void btnclean_Click(object sender, System.EventArgs e)
		{
		    this.txtResult.Text = "";
		    this.input = string.Empty;
		    this.operand1 = string.Empty;
		    this.operand2 = string.Empty;
		}

		[Handler()]
		internal void btn7_Click(object sender, System.EventArgs e)
		{
		    this.txtResult.Text = "";
		    input += btn7.Text;
		    txtResult.Text += input;
		}

		[Handler()]
		internal void btn8_Click(object sender, System.EventArgs e)
		{
		    txtResult.Text = "";
		    input += btn8.Text;
		    txtResult.Text += input;
		}

		[Handler()]
		internal void btn9_Click(object sender, System.EventArgs e)
		{
		    txtResult.Text = "";
		    input += btn9.Text;
		    txtResult.Text += input;
		}

		[Handler()]
		internal void btn4_Click(object sender, System.EventArgs e)
		{
		    txtResult.Text = "";
		    input += btn4.Text;
		    txtResult.Text += input;
		}

		[Handler()]
		internal void btn5_Click(object sender, System.EventArgs e)
		{
		    txtResult.Text = "";
		    input += btn5.Text;
		    txtResult.Text += input;
		}

		[Handler()]
		internal void btn6_Click(object sender, System.EventArgs e)
		{
		    txtResult.Text = "";
		    input += btn6.Text;
		    txtResult.Text += input;
		}

		[Handler()]
		internal void btn1_Click(object sender, System.EventArgs e)
		{
		    txtResult.Text = "";
		    input += btn1.Text;
		    txtResult.Text += input;
		}

		[Handler()]
		internal void btn2_Click(object sender, System.EventArgs e)
		{
		    txtResult.Text = "";
		    input += btn2.Text;
		    txtResult.Text += input;
		}

		[Handler()]
		internal void btn3_Click(object sender, System.EventArgs e)
		{
		    txtResult.Text = "";
		    input += btn3.Text;
		    txtResult.Text += input;
		}

		[Handler()]
		internal void btn0_Click(object sender, System.EventArgs e)
		{
		    txtResult.Text = "";
		    input += btn0.Text;
		    txtResult.Text += input;
		}

		[Handler()]
		internal void btndot_Click(object sender, System.EventArgs e)
		{
		    txtResult.Text = "";
		    input += btndot.Text;
		    txtResult.Text += input;
		}

		[Handler()]
		internal void btnMax_Click(object sender, System.EventArgs e)
		{

		    operand1 = input;
		    operation = '+';
		    input = string.Empty;


		}

		[Handler()]
		internal void btnEqual_Click(object sender, System.EventArgs e)
		{
		    operand2 = input;
		    double num1, num2;
		    double.TryParse(operand1, out num1);
		    double.TryParse(operand2, out num2);

		    if (operation == '+')
		    {
		        result = num1 + num2;
		        txtResult.Text = result.ToString("N");
		    }
		    else if (operation == '-')
		    {
		        result = num1 - num2;
		        txtResult.Text = result.ToString("N");
		    }
		    else if (operation == '*')
		    {
		        result = num1 * num2;
		        txtResult.Text = result.ToString("N");
		    }
		    else if (operation == '/')
		    {
		        if (num2 != 0)
		        {
		            result = num1 / num2;
		            txtResult.Text = result.ToString("N");
		        }
		        else
		        {
		            txtResult.Text = "DIV/Zero";
		        }
		    }


		}

		[Handler()]
		internal void btnMin_Click(object sender, System.EventArgs e)
		{
		    operand1 = input;
		    operation = '-';
		    input = string.Empty;
		}

		[Handler()]
		internal void btnDiv_Click(object sender, System.EventArgs e)
		{
		    operand1 = input;
		    operation = '/';
		    input = string.Empty;
		}

		[Handler()]
		internal void btnMult_Click(object sender, System.EventArgs e)
		{
		    operand1 = input;
		    operation = '*';
		    input = string.Empty;
		}

		public string UniqueID { get; set; }

		public IViewManager ViewManager { get; set; }

		public IIocContainer Container { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.TextBoxViewModel txtResult { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btn7 { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btn8 { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btn9 { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btn4 { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btn5 { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btn6 { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btn1 { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btn2 { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btn3 { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btn0 { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btndot { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btnDiv { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btnMult { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btnMin { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btnMax { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btnEqual { get; set; }

		public virtual UpgradeHelpers.BasicViewModels.ButtonViewModel btnclean { get; set; }

		public virtual void Init()
		{
		}

	}

	public class UserControl1Controller
		: System.Web.Mvc.Controller
	{

		public System.Web.Mvc.ActionResult btn7_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btn7_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btn8_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btn8_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btn9_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btn9_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btnDiv_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btnDiv_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btn4_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btn4_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btn5_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btn5_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btn6_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btn6_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btnMult_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btnMult_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btn1_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btn1_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btn2_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btn2_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btn3_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btn3_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btnMin_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btnMin_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btn0_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btn0_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btndot_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btndot_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btnMax_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btnMax_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btnEqual_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btnEqual_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

		public System.Web.Mvc.ActionResult btnclean_Click(ControlCalc.UserControl1 usercontrol)
		{
			usercontrol.btnclean_Click(null, null);
			return new UpgradeHelpers.WebMap.Server.AppChanges();
		}

	}
}