
using System.Web.Mvc;
using Project1.Controllers;
using UpgradeHelpers.WebMap.Server;

namespace SimpleWinForm.Areas
{
    public class Project1AreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SimpleWinForm";
            }
        }


        public override void RegisterArea(AreaRegistrationContext context)
        {
            var route = context.MapRoute("SimpleWinForm_default", "SimpleWinForm/{controller}/{action}/{id}", new {id = UrlParameter.Optional },new [] { "SimpleWinForm.Controllers"} );
            route.RouteHandler = new DefaultEventHandlerRouteHandler<HomeController>();
        }
    }
}


