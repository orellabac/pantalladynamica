using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using UpgradeHelpers.WebMap.Server;

namespace WebSite
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public partial class MvcApplication : System.Web.HttpApplication
    {
        
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ExternalConfig.RegisterExternalLinks();
            SurrogatesConfig.RegisterSurrogates();
            LibsConfig.RegisterLibs();
            Bootstrapper.Initialise();
            ValueProviderFactories.Factories.Remove(ValueProviderFactories.Factories.OfType<JsonValueProviderFactory>().FirstOrDefault());
            ValueProviderFactories.Factories.Add(new JSONAjaxValueProvider());
        }

        protected void Session_Start()
        {
            var container = IocContainerImplWithUnity.Current;
            container.Resolve<Defaults>().Program.Main(container);
        }
    }
}

