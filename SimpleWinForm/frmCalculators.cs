using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UpgradeHelpers;
using UpgradeHelpers.BasicViewModels;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.Events;
using UpgradeHelpers.Helpers;

namespace SimpleWinForm
{

	public class frmCalculators
		: UpgradeHelpers.Interfaces.ILogicView<SimpleWinForm.ViewModels.frmCalculatorsViewModel>
	{

		public SimpleWinForm.ViewModels.frmCalculatorsViewModel ViewModel { get; set; }

		public IViewManager ViewManager { get; set; }

		public IIocContainer Container { get; set; }

		public virtual string Text { get; set; }


		public frmCalculators()
		{
		}

		private void userControl21_Click(object sender, System.EventArgs e)
		{
			ViewModel._pp.name = "ffsdf";
			ViewModel._pp.value = "sdfjhjkf";
			ShowMessageBox();
			}

		private void ShowMessageBox()
		{
			ViewManager.ShowMessage("Click User Control with images");
		}


		public struct Ejemplo
		{
			public string value;
			public string name;
		}

		public virtual void Init()
		{
			var t = Container.Resolve<SharedState>();
		}

	}


	[UpgradeHelpers.Helpers.Singlenton()]
	public class SharedState
		: UpgradeHelpers.Interfaces.ILogicClass, UpgradeHelpers.Interfaces.ICreatesObjects, UpgradeHelpers.Interfaces.IInteractsWithView
	{

		public string UniqueID { get; set; }

		public IIocContainer Container { get; set; }

		public IViewManager ViewManager { get; set; }

		public void Init()
		{
		}

		
	}
}