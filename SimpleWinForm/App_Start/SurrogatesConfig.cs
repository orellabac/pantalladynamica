﻿using UpgradeHelpers.DB.ADO.Surrogates;
namespace WebSite
{
	public class SurrogatesConfig
	{
		public static void RegisterSurrogates()
		{
			new ADOSurrogates().Register();
		}
	}
}