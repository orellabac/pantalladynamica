﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebSite
{
    public class RouteConfig
    {

        public class MyRoutHandler : IRouteHandler
        {

            public IHttpHandler GetHttpHandler(RequestContext requestContext)
            {
               return new MvcHandler(requestContext);
            }
        }


        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },namespaces: new string[] {"WebSite.Controllers"}
            );

 

        }
    }
}