﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;

namespace WebSite
{
	public class ExternalConfig
	{
		public static List<string> ExternalLinks = new List<string>();

		public static void RegisterExternalLinks()
		{
			var assemblies = System.Web.Compilation.BuildManager.GetReferencedAssemblies()
				.Cast<Assembly>()
				.Where(
					a =>
						 a.GetName().Name.Equals("UpgradeHelpers.WebMap.Controls") || (!a.GetName().Name.StartsWith("System") && !a.GetName().Name.StartsWith("UpgradeHelpers") &&
						!a.GetName().Name.StartsWith("Microsoft"))).OrderBy(a => a.FullName);
			
			var resourceNames = assemblies.SelectMany(assemblyFile => assemblyFile.GetManifestResourceNames().SelectMany(s =>
			{
				if (!s.EndsWith(".g.resources")) return new string[0];
				var stream = new ResourceReader(assemblyFile.GetManifestResourceStream(s));
				return (from DictionaryEntry resource in stream select resource.Key.ToString()).ToArray();
			}));

			var extensionToInclude = new[] { ".js" };
			foreach (var resource in resourceNames)
			{
				var extension = System.IO.Path.GetExtension(resource);
				if (extensionToInclude.Contains(extension))
				{
					var resolvedPath = "~/" + resource;
					if (!System.IO.File.Exists(HttpContext.Current.Server.MapPath(resolvedPath)))
					{
						ExternalLinks.Add("/" + resource);
					}
				}
			}
		}
	}
}