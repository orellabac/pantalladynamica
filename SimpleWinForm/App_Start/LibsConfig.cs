﻿using System;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Web.Hosting;
using UpgradeHelpers.WebMap.Controls.Controllers;

namespace WebSite
{
	public class LibsConfig
	{
		#region Utils
		private static void CopyResources(Assembly assembly, String[] resources, String resourceKey, DirectoryInfo targetDir, bool cleanTargetDir = false)
		{
			var prefix = assembly.GetName().Name + '.' + resourceKey + '.';
			var content = resources.Where(x => x.StartsWith(prefix));
			
			if (cleanTargetDir)
				DeleteDir(targetDir);

			var templatesTargetDir = targetDir.FullName;
			while (!Directory.CreateDirectory(templatesTargetDir).Exists || System.Runtime.InteropServices.Marshal.GetLastWin32Error() == 5) 	
			{
				// patch: it seems that IIS lock something (error code 5 is ERROR_ACCESS_DENIED)
			}
			foreach (var c in content)
			{
				var targetFileName = c.Replace(prefix, "");
				if (targetFileName.StartsWith("images"))
				{
					var imagesFolder = Path.Combine(templatesTargetDir, "images");
					if (!Directory.Exists(imagesFolder))
						Directory.CreateDirectory(imagesFolder);
					targetFileName = targetFileName.Replace("images.", "images\\");
				}
				
				using (var s = assembly.GetManifestResourceStream(c))
				{
					if (s == null)
						continue;
					var fullTargetFileName = Path.Combine(templatesTargetDir, targetFileName);
					using (var fs = new FileStream(fullTargetFileName, FileMode.Create, FileAccess.Write))
					{
						s.CopyTo(fs);
					}
				}
			}
		}

        private static void DeleteDir(DirectoryInfo targetDir)
        {
            var templatesTargetDir = targetDir.FullName;
            if (Directory.Exists(templatesTargetDir)) Directory.Delete(templatesTargetDir, true);
        }

		private static void ExtractAllResources(Assembly assembly, DirectoryInfo HtmlTemplatesFolder, DirectoryInfo ScriptsFolder, DirectoryInfo ImagesFolder)
		{
			var resources = assembly.GetManifestResourceNames();
			//Copy html templates
			CopyResources(assembly, resources, "Resources", HtmlTemplatesFolder);
			//Copy scripts
			CopyResources(assembly, resources, "Scripts", ScriptsFolder);
		}
		#endregion

		public static void RegisterLibs()
		{
			var libTemplatesTempPath = HostingEnvironment.MapPath("~/Resources/libs");
			var libScriptsTempPath = HostingEnvironment.MapPath("~/Scripts/libs");
			var libImagesTempPath = HostingEnvironment.MapPath("~/Resources/libs");

			var ScriptsFolder = new DirectoryInfo(libScriptsTempPath);
			var HtmlTemplatesFolder = new DirectoryInfo(libTemplatesTempPath);
			var ImagesFolder = new DirectoryInfo(libImagesTempPath);

			Directory.CreateDirectory(HtmlTemplatesFolder.FullName);
			Directory.CreateDirectory(ScriptsFolder.FullName);

            DeleteDir(HtmlTemplatesFolder);
            DeleteDir(HtmlTemplatesFolder);
            DeleteDir(ImagesFolder);
			//Extract resources for assembly1
			//Example: ExtractAllResources(typeInAssembly1.Assembly, HtmlTemplatesFolder, ScriptsFolder, ImagesFolder);
		}
	}
}
