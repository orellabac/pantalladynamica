using UpgradeHelpers.Events;

namespace SimpleWinForm.ViewModels
{

	public class frmCalculatorsViewModel
		: UpgradeHelpers.Interfaces.IViewModel
	{

		public virtual void Build(UpgradeHelpers.Interfaces.IIocContainer ctx)
		{
			this.userControl21 = ctx.Resolve<ControlCalc.UserControl2>();
			this.userControl11 = ctx.Resolve<ControlCalc.UserControl1>();
			this.userControl31 = ctx.Resolve<ControlCalc.UserControl3>();
			Name = "SimpleWinForm.frmCalculators";
		}

		public string UniqueID { get; set; }

		[UpgradeHelpers.Helpers.Watchable(typeof(UpgradeHelpers.WebMap.Server.Common.StructsWatcher))]
		[UpgradeHelpers.Helpers.StateManagement(UpgradeHelpers.Helpers.StateManagementValues.ServerOnly)]
		public SimpleWinForm.frmCalculators.Ejemplo _pp;

		public virtual string Text { get; set; }

		public virtual ControlCalc.UserControl2 userControl21 { get; set; }

		public virtual ControlCalc.UserControl1 userControl11 { get; set; }

		public virtual ControlCalc.UserControl3 userControl31 { get; set; }

		public virtual string Name { get; set; }

		public virtual bool Visible { get; set; }

		public virtual bool Enabled { get; set; }

	}

}