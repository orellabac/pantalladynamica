/// <reference path="helpers/WebMap.all.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SimpleWinForm;
(function (SimpleWinForm) {
    var frmCalculators = (function (_super) {
        __extends(frmCalculators, _super);
        function frmCalculators(json) {
            _super.call(this);
        }
        frmCalculators.prototype.Init = function () {
            return _super.prototype.Init.call(this);
            //Rebinding  Content
        };
        return frmCalculators;
    })(WebMap.Client.BaseLogic);
    SimpleWinForm.frmCalculators = frmCalculators;
})(SimpleWinForm || (SimpleWinForm = {}));
//# sourceMappingURL=SimpleWinForm.frmCalculators.js.map