/// <reference path="kendo.all.d.ts" />
(function ($) {
    var DATABINDING = "dataBinding", DATABOUND = "dataBound", CHANGE = "change";
    // shorten references to variables. this is better for uglification 
    var kendo = window.kendo, ui = kendo.ui, Widget = ui.Widget;
    var BaseUserControl = Widget.extend({
        applyTemplate: function (value) {
            var that = this;
            var proto = that.constructor.prototype;
            if (!value["$$$parent"]) {
                var parent = value.parent();
                var ancestorPath = parent["$$$parent"] || "";
                if (parent) {
                    var props = Object.getOwnPropertyNames(parent);
                    for (var i = 0; i < props.length; i++) {
                        var propName = props[i];
                        if (parent[propName] == value) {
                            if (ancestorPath == "") {
                                value["$$$parent"] = propName;
                            }
                            else {
                                value["$$$parent"] = ancestorPath + "." + propName;
                            }
                            break;
                        }
                    }
                }
            }
            if (!proto._compiledTemplate) {
                proto._compiledTemplate = kendo.template(proto.options.template);
            }
            this.initStyles();
            this.initClientMethods(value);
            return proto._compiledTemplate(value);
        },
        initStyles: function () {
            var that = this;
            //ADD CSS RULES DYNAMICALLY 
            var addCssRule = function (styles, id) {
                if (!document.getElementById(id)) {
                    var style = document.createElement('style');
                    var styleMark = document.createElement("script");
                    var styleIdAtt = document.createAttribute("id");
                    styleIdAtt.value = id;
                    style.type = 'text/css';
                    if (style.styleSheet)
                        style.styleSheet.cssText = styles; //IE 
                    else
                        style.innerHTML = styles; //OTHERS 
                    document.getElementsByTagName('head')[0].appendChild(style);
                }
            };
            addCssRule(that.options.css, '' + that.options.name);
        },
        initClientMethods: function (value) {
            var that = value;
        },
        init: function (element, options) {
            // base call to initialize widget 
            options._uiinitialized = false;
            Widget.fn.init.call(this, element, options);
        },
        refresh: function () {
        },
        buildUI: function () {
            var that = this;
            that.options.uiinitialized = true;
            that.setOptions(that.options);
            var that = this, view = that._value;
            var parent = that._value.parent();
            var ancestorPath = parent["$$$parent"] || "";
            if (parent) {
                var props = Object.getOwnPropertyNames(parent);
                for (var i = 0; i < props.length; i++) {
                    var propName = props[i];
                    if (parent[propName] == that._value) {
                        if (ancestorPath == "") {
                            that._value["$$$parent"] = propName;
                        }
                        else {
                            that._value["$$$parent"] = ancestorPath + "." + propName;
                        }
                        break;
                    }
                }
                var html = this.applyTemplate(view);
                //that.element.html(html); 
                // trigger the dataBinding event 
                //that.trigger(DATABINDING); 
                // mutate the DOM (AKA build the widget UI) 
                that.element.html(html);
            }
        },
        //MVVM framework calls 'value' when the viewmodel 'value' binding changes 
        value: function (value) {
            var that = this;
            if (value === undefined) {
                return that._value;
            }
            that._update(value);
            that._old = that._value;
        },
        _update: function (value) {
            var that = this;
            that._value = value;
            if (!that.options.uiinitialized) {
                that.buildUI();
            }
            that.refresh();
        },
        options: {
            // the name is what it will appear as off the kendo namespace(i.e. kendo.ui.UserControl1). 
            // The jQuery plugin would be jQuery.fn.kendoUserControl1. 
            name: "BaseUserControl",
            value: null,
            css: "",
            // other options go here 
            template: "<div></div>"
        },
        // events are used by other widgets / developers - API for other purposes 
        // these events support MVVM bound items in the template. for loose coupling with MVVM. 
        events: [
            DATABINDING,
            DATABOUND,
            CHANGE
        ]
    });
    ui.plugin(BaseUserControl);
})(jQuery);
var UpgradeHelpers;
(function (UpgradeHelpers) {
    var ControlArray = (function () {
        function ControlArray() {
        }
        ControlArray.indexOf = function (ctrlArray, e) {
            return ctrlArray.indexOf(Events.getEventSender(e));
        };
        return ControlArray;
    })();
    UpgradeHelpers.ControlArray = ControlArray;
    var Events = (function () {
        function Events() {
        }
        Events.getEventSender = function (e) {
            var isEvent = (e.target != undefined);
            var id = isEvent ? e.target.id : e.id;
            var viewModel = isEvent ? e.data : e.ViewModel;
            var controlArrayRefRegex = /^_(.+)_([0-9]+)/;
            var eventSender = undefined;
            if (controlArrayRefRegex.test(id)) {
                eventSender = viewModel.get(id.replace(controlArrayRefRegex, "$1[$2]"));
            }
            else
                eventSender = viewModel.get(id);
            return eventSender;
        };
        return Events;
    })();
    UpgradeHelpers.Events = Events;
    var EventKey = (function () {
        function EventKey() {
        }
        EventKey.getKeyChar = function (e) {
            return String.fromCharCode(this.getKeyCode(e));
        };
        EventKey.setKeyChar = function (e, key) {
            this.setKeyCode(e, key.charCodeAt(0));
        };
        EventKey.getKeyCode = function (e) {
            return (e.keyCode ? e.keyCode : e.which);
        };
        EventKey.setKeyCode = function (e, key) {
            if (key != 0 && key != this.getKeyCode(e)) {
                var eventSender = Events.getEventSender(e);
                eventSender.set("Text", eventSender.Text + String.fromCharCode(key));
                this.handleEvent(e, true);
            }
        };
        EventKey.handleEvent = function (e, value) {
            if (value) {
                e.preventDefault();
            }
        };
        return EventKey;
    })();
    UpgradeHelpers.EventKey = EventKey;
    (function (Keys) {
        Keys[Keys["A"] = 65] = "A";
        Keys[Keys["Add"] = 43] = "Add";
        Keys[Keys["Alt"] = 18] = "Alt";
        /*        Apps,
                Attn,*/
        Keys[Keys["B"] = 66] = "B";
        Keys[Keys["Back"] = 8] = "Back";
        /*        BrowserBack,
                BrowserFavorites,
                BrowserForward,
                BrowserHome,
                BrowserRefresh,
                BrowserSearch,
                BrowserStop,*/
        Keys[Keys["C"] = 67] = "C";
        /*        Cancel,*/
        Keys[Keys["Capital"] = 20] = "Capital";
        Keys[Keys["CapsLock"] = 20] = "CapsLock";
        Keys[Keys["Clear"] = 46] = "Clear";
        Keys[Keys["Control"] = 17] = "Control";
        Keys[Keys["ControlKey"] = 17] = "ControlKey";
        /*        Crsel,*/
        Keys[Keys["D"] = 68] = "D";
        Keys[Keys["D0"] = 48] = "D0";
        Keys[Keys["D1"] = 49] = "D1";
        Keys[Keys["D2"] = 50] = "D2";
        Keys[Keys["D3"] = 51] = "D3";
        Keys[Keys["D4"] = 52] = "D4";
        Keys[Keys["D5"] = 53] = "D5";
        Keys[Keys["D6"] = 54] = "D6";
        Keys[Keys["D7"] = 55] = "D7";
        Keys[Keys["D8"] = 56] = "D8";
        Keys[Keys["D9"] = 57] = "D9";
        Keys[Keys["Decimal"] = 44] = "Decimal";
        Keys[Keys["Delete"] = 46] = "Delete";
        Keys[Keys["Divide"] = 47] = "Divide";
        Keys[Keys["Down"] = 40] = "Down";
        Keys[Keys["E"] = 69] = "E";
        Keys[Keys["End"] = 35] = "End";
        Keys[Keys["Enter"] = 13] = "Enter";
        /*        EraseEof,*/
        Keys[Keys["Escape"] = 27] = "Escape";
        /*        Execute,
                Exsel,*/
        Keys[Keys["F"] = 70] = "F";
        Keys[Keys["F1"] = 112] = "F1";
        Keys[Keys["F10"] = 121] = "F10";
        Keys[Keys["F11"] = 122] = "F11";
        Keys[Keys["F12"] = 123] = "F12";
        /*        F13,
                F14,
                F15,
                F16,
                F17,
                F18,
                F19,*/
        Keys[Keys["F2"] = 113] = "F2";
        /*        F20,
                F21,
                F22,
                F23,
                F24,*/
        Keys[Keys["F3"] = 114] = "F3";
        Keys[Keys["F4"] = 115] = "F4";
        Keys[Keys["F5"] = 116] = "F5";
        Keys[Keys["F6"] = 117] = "F6";
        Keys[Keys["F7"] = 118] = "F7";
        Keys[Keys["F8"] = 119] = "F8";
        Keys[Keys["F9"] = 120] = "F9";
        /*        FinalMode,*/
        Keys[Keys["G"] = 71] = "G";
        Keys[Keys["H"] = 72] = "H";
        /*        HanguelMode,
                HangulMode,
                HanjaMode,
                Help,*/
        Keys[Keys["Home"] = 36] = "Home";
        Keys[Keys["I"] = 73] = "I";
        /*        IMEAccept,
                IMEAceept,
                IMEConvert,
                IMEModeChange,
                IMENonconvert,*/
        Keys[Keys["Insert"] = 45] = "Insert";
        Keys[Keys["J"] = 74] = "J";
        /*        JunjaMode,*/
        Keys[Keys["K"] = 75] = "K";
        /*        KanaMode,
                KanjiMode,
                KeyCode,*/
        Keys[Keys["L"] = 76] = "L";
        /*        LaunchApplication1,
                LaunchApplication2,
                LaunchMail,
                LButton,*/
        Keys[Keys["LControlKey"] = 17] = "LControlKey";
        Keys[Keys["Left"] = 37] = "Left";
        /*        LineFeed,
                LMenu,*/
        Keys[Keys["LShiftKey"] = 16] = "LShiftKey";
        /*        LWin,*/
        Keys[Keys["M"] = 77] = "M";
        /*        MButton,
                MediaNextTrack,
                MediaPlayPause,
                MediaPreviousTrack,
                MediaStop,
                Menu,
                Modifiers,*/
        Keys[Keys["Multiply"] = 42] = "Multiply";
        Keys[Keys["N"] = 78] = "N";
        /*        Next,
                NoName,
                None,*/
        Keys[Keys["NumLock"] = 144] = "NumLock";
        Keys[Keys["NumPad0"] = 48] = "NumPad0";
        Keys[Keys["NumPad1"] = 49] = "NumPad1";
        Keys[Keys["NumPad2"] = 50] = "NumPad2";
        Keys[Keys["NumPad3"] = 51] = "NumPad3";
        Keys[Keys["NumPad4"] = 52] = "NumPad4";
        Keys[Keys["NumPad5"] = 53] = "NumPad5";
        Keys[Keys["NumPad6"] = 54] = "NumPad6";
        Keys[Keys["NumPad7"] = 55] = "NumPad7";
        Keys[Keys["NumPad8"] = 56] = "NumPad8";
        Keys[Keys["NumPad9"] = 57] = "NumPad9";
        Keys[Keys["O"] = 79] = "O";
        /*        Oem1,
                Oem102,
                Oem2,
                Oem3,
                Oem4,
                Oem5,
                Oem6,
                Oem7,
                Oem8,
                OemBackslash,
                OemClear,
                OemCloseBrackets,
                Oemcomma,
                OemMinus,
                OemOpenBrackets,
                OemPeriod,
                OemPipe,
                Oemplus,
                OemQuestion,
                OemQuotes,
                OemSemicolon,
                Oemtilde,*/
        Keys[Keys["P"] = 80] = "P";
        /*        Pa1,
                Packet,*/
        Keys[Keys["PageDown"] = 34] = "PageDown";
        Keys[Keys["PageUp"] = 33] = "PageUp";
        /*        Pause,
                Play,
                Print,
                PrintScreen,
                Prior,
                ProcessKey,*/
        Keys[Keys["Q"] = 81] = "Q";
        Keys[Keys["R"] = 82] = "R";
        /*        RButton,*/
        Keys[Keys["RControlKey"] = 17] = "RControlKey";
        Keys[Keys["Return"] = 13] = "Return";
        Keys[Keys["Right"] = 39] = "Right";
        /*        RMenu,*/
        Keys[Keys["RShiftKey"] = 16] = "RShiftKey";
        /*        RWin,*/
        Keys[Keys["S"] = 83] = "S";
        /*        Scroll,
                Select,
                SelectMedia,
                Separator,*/
        Keys[Keys["Shift"] = 16] = "Shift";
        Keys[Keys["ShiftKey"] = 16] = "ShiftKey";
        /*        Sleep,
                Snapshot,*/
        Keys[Keys["Space"] = 32] = "Space";
        Keys[Keys["Subtract"] = 45] = "Subtract";
        Keys[Keys["T"] = 84] = "T";
        Keys[Keys["Tab"] = 9] = "Tab";
        Keys[Keys["U"] = 85] = "U";
        Keys[Keys["Up"] = 38] = "Up";
        Keys[Keys["V"] = 86] = "V";
        /*        VolumeDown,
                VolumeMute,
                VolumeUp,*/
        Keys[Keys["W"] = 87] = "W";
        Keys[Keys["X"] = 88] = "X";
        /*        XButton1,
                XButton2,*/
        Keys[Keys["Y"] = 89] = "Y";
        Keys[Keys["Z"] = 90] = "Z";
    })(UpgradeHelpers.Keys || (UpgradeHelpers.Keys = {}));
    var Keys = UpgradeHelpers.Keys;
    var Sound = (function () {
        function Sound() {
        }
        Sound.getBeep = function () {
            return new Sound();
        };
        Sound.prototype.Play = function () {
            // TODO: to be implemented
            //throw "Sound.Play needs to be implemented";
        };
        return Sound;
    })();
    UpgradeHelpers.Sound = Sound;
    var Strings = (function () {
        function Strings() {
        }
        Strings.convertToString = function (obj) {
            return obj == null ? null : obj.toString();
        };
        Strings.format = function (obj, format) {
            // TODO: to be implemented
            //throw "String.format needs to be implemented";
            return obj.toString();
        };
        return Strings;
    })();
    UpgradeHelpers.Strings = Strings;
})(UpgradeHelpers || (UpgradeHelpers = {}));
var WebMap;
(function (WebMap) {
    var Client;
    (function (Client) {
        // Prints a message to the console for debugging purposes
        function debug(e) {
            if (console) {
                console.error(e);
            }
        }
        Client.debug = debug;
        // Checks that the given id is a new view
        function IsNewView(id, VD) {
            if (VD && VD.NewViews) {
                for (var i = 0; i < VD.NewViews.length; i++) {
                    if (VD.NewViews[i] == id)
                        return true;
                }
            }
            return false;
        }
        Client.IsNewView = IsNewView;
        function showMessage(msg) {
            var w = $("<div> <span> " + msg + "<span></div>").kendoWindow();
            var kendowWindow = w.data("kendoWindow");
            kendowWindow.open();
        }
        Client.showMessage = showMessage;
    })(Client = WebMap.Client || (WebMap.Client = {}));
})(WebMap || (WebMap = {}));
var WebMap;
(function (WebMap) {
    var Utils;
    (function (Utils) {
        function IsClassSet(classes, cls) {
            var idx = classes.indexOf(cls);
            var len = cls.length;
            return (idx == 0 && (classes.length == len || classes[len] == ' ')) || (idx > 0 && classes[idx - 1] == ' ' && ((idx += len) == classes.length || classes[idx] == ' '));
        }
        Utils.IsClassSet = IsClassSet;
        function RemoveClass(classes, cls) {
            var len = cls.length;
            var idx = classes.indexOf(cls);
            if (idx >= 0) {
                var idx2 = idx + len;
                var before = "", after = "";
                if (idx > 0) {
                    if (classes[idx - 1] == ' ')
                        idx--;
                    else
                        return classes;
                    before = classes.substring(0, idx);
                }
                if (idx2 < classes.length) {
                    if (classes[idx2] == ' ')
                        idx2++;
                    else
                        return classes;
                    after = classes.substring(idx2);
                }
                return (before + ' ' + after).trim();
            }
            return classes;
        }
        Utils.RemoveClass = RemoveClass;
    })(Utils = WebMap.Utils || (WebMap.Utils = {}));
})(WebMap || (WebMap = {}));
var WebMap;
(function (WebMap) {
    var Kendo;
    (function (Kendo) {
        function setupBinding(binding, previousBinding, setups) {
            for (var i = 0, len = setups.length; i < len; i++) {
                var s = setups[i];
                if (s.setup(binding))
                    return;
            }
            if (previousBinding) {
                binding.refresh = previousBinding.refresh;
                binding.destroy = previousBinding.destroy;
            }
        }
        Kendo.setupBinding = setupBinding;
        var BindingSetup = (function () {
            function BindingSetup() {
            }
            BindingSetup.prototype.setup = function (binding) {
                return false;
            };
            return BindingSetup;
        })();
        Kendo.BindingSetup = BindingSetup;
    })(Kendo = WebMap.Kendo || (WebMap.Kendo = {}));
})(WebMap || (WebMap = {}));
// Module
var WebMap;
(function (WebMap) {
    var Client;
    (function (Client) {
        // IIocContainer implementation that does use of kendo objects to keep track of the
        // state of the elements it creates.
        var Container = (function () {
            function Container() {
            }
            // Initializes the singleton instance
            Container.Init = function () {
                if (!Container.Current) {
                    Container.Current = new Container();
                }
            };
            // Creates a new object according to the given options.  Standard calls are:
            // 1. Resolve({ vm: true, data: <JSON object>, dirty: false }).  Creates a new view model object with the 
            // given JSON data and marks it as not diry.
            // 2. Resolve({ "cons": constructor lambda) }).  Creates a new object usign the given lambda to create it.
            Container.prototype.Resolve = function (options) {
                if (options) {
                    if (options.vm) {
                        var observable;
                        if (options.data['@arr']) {
                            observable = new kendo.data.ObservableArray(options.data);
                            observable.Count = options.data.Count;
                            observable.UniqueID = options.data.UniqueID;
                        }
                        else {
                            observable = new kendo.data.ObservableObject(options.data);
                        }
                        // the new view model objects defaults to not dirty, however its initial value can be changed by 
                        // using the dirty flag.
                        var dirty = false;
                        if (options.dirty != undefined)
                            dirty = options.dirty;
                        // adds the new object to cache
                        Client.StateCache.Current.addNewObject(observable, dirty);
                        return observable;
                    }
                    // Creates the object from the given constructor
                    if (options.cons) {
                        return new options.cons();
                    }
                }
                return null;
            };
            return Container;
        })();
        Client.Container = Container;
    })(Client = WebMap.Client || (WebMap.Client = {}));
})(WebMap || (WebMap = {}));
/// <reference path="kendo.all.d.ts" />
/// <reference path="jquery.blockUI.d.ts" />
/// <reference path="jquery.d.ts" />
/// <reference path="WebMap_Interfaces.ts" />
var WebMap;
(function (WebMap) {
    var Client;
    (function (Client) {
        // DataBinding: stores a list of Bindings and provides common operations
        var DataBinding = (function () {
            function DataBinding() {
                this.bindings = new Array();
            }
            DataBinding.prototype.addBinding = function (propertyName, dataSource, memberName) {
                var newBinding = new Binding();
                newBinding.propertyName = propertyName;
                newBinding.dataSource = dataSource;
                newBinding.memberName = memberName;
                this.bindings.push(newBinding);
            };
            return DataBinding;
        })();
        var Binding = (function () {
            function Binding() {
            }
            return Binding;
        })();
        // DataSource: stores a "RecordSet" with the common operations
        var DataSource = (function () {
            function DataSource() {
                this.data = new Array();
                this.index = 0;
                this.page = 1;
            }
            DataSource.prototype.loadData = function (rsJSON) {
                var dataRows = new Array();
                for (var row in rsJSON) {
                    var dataRow = new DataRow();
                    var dataCells = new Array();
                    for (var cell in row) {
                        var dataCell = new DataCell();
                        // TODO: review
                        dataCell.propertyName = row;
                        dataCell.value = row[cell];
                        dataCells.push(dataCell);
                    }
                    dataRow.cells = dataCells;
                    dataRows.push(dataRow);
                }
                this.data = dataRows;
            };
            DataSource.prototype.refresh = function () {
                //TODO
            };
            DataSource.prototype.update = function () {
                //TODO
            };
            return DataSource;
        })();
        var DataRow = (function () {
            function DataRow() {
            }
            return DataRow;
        })();
        var DataCell = (function () {
            function DataCell() {
            }
            return DataCell;
        })();
    })(Client = WebMap.Client || (WebMap.Client = {}));
})(WebMap || (WebMap = {}));
// Module
var WebMap;
(function (WebMap) {
    var Client;
    (function (Client) {
        // Defines a class that contains information about the View changes.  This class
        // is used to interchange data between client and server calls.
        var ViewState = (function () {
            function ViewState() {
            }
            return ViewState;
        })();
        Client.ViewState = ViewState;
        var JSONWebMapRequest = (function () {
            function JSONWebMapRequest() {
            }
            JSONWebMapRequest.prototype.JSONWebMapRequest = function () {
            };
            return JSONWebMapRequest;
        })();
        Client.JSONWebMapRequest = JSONWebMapRequest;
        // not in use
        var ViewInfo = (function () {
            function ViewInfo() {
            }
            return ViewInfo;
        })();
        Client.ViewInfo = ViewInfo;
        // not in use
        var ViewsState = (function () {
            function ViewsState() {
            }
            return ViewsState;
        })();
        Client.ViewsState = ViewsState;
    })(Client = WebMap.Client || (WebMap.Client = {}));
})(WebMap || (WebMap = {}));
/// <reference path="WebMap_Interfaces.ts" />
/// <reference path="WebMap_Model.ts" />
var WebMap;
(function (WebMap) {
    var Client;
    (function (Client) {
        // This class represents a WebMap client side application, it is responsible for starting up the application and
        // manage the communication with de server side of a WebMap application
        var App = (function () {
            function App() {
            }
            //private usercontrols: any[];
            // Static concstructor that creates singleton objects
            App.Init = function () {
                if (!App.Current) {
                    App.Current = new App();
                    App.Current.ViewManager = new Client.ViewManager();
                }
            };
            //  Inits the WebMap application before the first screen is rendered.  This method must be invoked from
            // the main html page just before any other action is performed.
            App.prototype.Init = function (models, viewsState) {
                Client.Container.Init();
                Client.StateCache.Init();
                this.InitModels(models);
                this.InitUI(viewsState);
                this.eventsQueue = new Array();
            };
            App.prototype.isSynchronizingClient = function () {
                return this.clientSynchronizationActive;
            };
            // Adds a listener to be called to update the model data
            App.prototype.addModelUpdateListener = function (l) {
                if (l) {
                    if (!this.updateListeners)
                        this.updateListeners = [l];
                    else
                        this.updateListeners.push(l);
                }
            };
            // Removes the given listener
            App.prototype.removeModelUpdateListener = function (l) {
                var ul = this.updateListeners;
                if (ul) {
                    var idx = ul.indexOf(l);
                    if (idx >= 0)
                        ul.splice(idx, 1);
                }
            };
            // Calls the registered listeners in order to update the model data
            App.prototype.callModelUpdateListeners = function () {
                var ul = this.updateListeners;
                if (ul) {
                    for (var i = 0, len = ul.length; i < len; i++)
                        ul[i]();
                }
            };
            // Sends a request to the WebMap server side application.  After call is made, the method updates the 
            // WebMap client side app in order to refresh all changes performed by server side logic, to achieve that the 
            // updateClientSide and UpdateMessages methods are called.
            App.prototype.sendAction = function (options) {
                //var customMessage = options.customMessage;
                var url = this.buildActionUrl(options);
                this.callModelUpdateListeners();
                var actionParamStr = this.buildJSONRequest(options);
                if (this.inActionExecution) {
                    var data = [url, actionParamStr];
                    this.eventsQueue.push(data);
                    var emptyPromess = $.Deferred();
                    emptyPromess.resolve();
                    return emptyPromess.promise();
                }
                this.inActionExecution = true;
                var jqxhr = this.doServerCall(this, url, actionParamStr, false);
                return jqxhr;
            };
            App.prototype.doServerCall = function (that, url, actionParamStr, forced) {
                var _this = this;
                that.inActionExecution = true;
                var jqxhr = $.ajax({
                    url: url,
                    type: "POST",
                    headers: {
                        WM: true
                    },
                    dataType: "json",
                    data: actionParamStr,
                    beforeSend: function () {
                        if (!forced)
                            window.timeoutForActions = setTimeout(function () {
                                $.blockUI();
                            }, 1500);
                    },
                    complete: function (jqXHR, textStatus) {
                        if (window.timeoutForActions) {
                            clearTimeout(window.timeoutForActions);
                        }
                        if (that.eventsQueue.length > 0) {
                            $.blockUI();
                            var opts = that.eventsQueue.pop();
                            that.doServerCall(that, opts[0], opts[1], true);
                        }
                        else {
                            that.inActionExecution = false;
                            $.unblockUI();
                        }
                    },
                    success: function (data, textStatus, jqXHR) {
                        if (data.ErrorOcurred) {
                            that.showGenericMessage(data.ExMessage + "<pre>" + data.ExStackTrace + "<pre\>");
                            return false;
                        }
                        if (that.checkFlag(data, "SessionTimeOut")) {
                        }
                        else if (that.checkFlag(data, "DoLogoff")) {
                        }
                        else if (that.checkFlag(data, "CloseApp")) {
                            window.close();
                        }
                        else {
                            //TODO setViewsInOrder(data.viewsInOrder);
                            //Common.updateModules(data.modifiedModules);
                            //Common.loadViews(data.newViews);
                            //Common.removeViews(data.removedViews, data.removedViewsNames);
                            that.clientSynchronizationActive = true;
                            try {
                                that.updateClientSide(data);
                            }
                            finally {
                                that.clientSynchronizationActive = false;
                            }
                            if (data) {
                                _this.updateMessages(data.VD);
                            }
                            Client.StateCache.Current.clearDirty();
                        }
                        return false;
                    },
                    error: function (a, b, c) {
                        //Common.showMessagesDialog("Error while calling action", [a.responseText]);
                        return false;
                    },
                    contentType: 'application/json; charset=utf-8'
                });
                return jqxhr;
            };
            // updates the given model object with the information stored in delta.
            App.prototype.applyDelta = function (model, delta) {
                for (var prop in delta) {
                    if ((prop in model) && model.set) {
                        model.set(prop, delta[prop]);
                    }
                }
            };
            // Builds an url action request based in the values gieven in options.
            App.prototype.buildActionUrl = function (options) {
                var path1 = location.pathname;
                var path2 = options.controller || "Home";
                if (options.mainobj !== undefined && options.mainobj["area"]) {
                    path2 = options.mainobj["area"] + "/" + path2;
                }
                if (path1.length > 1 && path1.substring(path1.length - 1) != '/') {
                    path1 = path1 + "/";
                }
                if (path2.length > 1 && path2[0] === '/') {
                    path2 = path2.substring(1);
                }
                options.action = options.action;
                var url = path1 + path2 + "/" + options.action + "/";
                return url;
            };
            // Builds a JSON request based in the values given in options
            App.prototype.buildJSONRequest = function (options) {
                var request = new Client.JSONWebMapRequest();
                request.dirty = Client.StateCache.Current.getDirty();
                if (options.mainobj) {
                    request.vm = options.mainobj.UniqueID;
                }
                if (options.dialogResult) {
                    request.dialogResult = options.dialogResult;
                }
                App.Current.ViewManager.PrepareDelta(request);
                request.parameters = options.parameters;
                var cache = [];
                var res = JSON.stringify(request, function (key, value) {
                    if (typeof value === 'object' && value !== null) {
                        if (cache.indexOf(value) !== -1) {
                            // Circular reference found, discard key
                            return;
                        }
                        // Store value in our collection
                        cache.push(value);
                    }
                    return value;
                });
                cache = null; // Enable garbage collection
                return res;
            };
            App.prototype.checkFlag = function (data, flagName) {
                return false;
            };
            App.prototype.extractItemsCollectionInfo = function (data) {
                var itemsCollections = [];
                for (var i = 0; i < data.length; i++) {
                    var current = data[i];
                    if (current && current['@k']) {
                        itemsCollections.push({ Parent: current.Parent, Items: current.Items });
                        delete current['Parent'];
                        delete current['Items'];
                        delete current['@k'];
                    }
                }
                return itemsCollections;
            };
            // Creates the IViewModel objects for every model object in "models".  Models is an array of json data sent from the 
            // server side, where every element contains a set of property/value describing the view model object.
            App.prototype.InitModels = function (models) {
                var itemsCollections = this.extractItemsCollectionInfo(models);
                var controlArrays = [];
                for (var i = 0; i < models.length; i++) {
                    var current = models[i];
                    var vm = Client.Container.Current.Resolve({ vm: true, data: current, dirty: false });
                    if (vm instanceof kendo.data.ObservableArray)
                        controlArrays.push({ controlArray: vm, delta: vm });
                    if (vm.Name && vm.Name.indexOf("UserControl") == 0) {
                        var logic = vm["logic"] = Client.Container.Current.Resolve({ "cons": App.Current.ViewManager.getConstructor(vm) });
                        logic.ViewModel = vm;
                    }
                }
                this.syncControlArraysSize(controlArrays);
                var options = undefined;
                if (itemsCollections.length) {
                    options = { itemsCollections: itemsCollections };
                }
                //Child models must be connected to entry in the cache
                Client.StateCache.Current.organize(options);
            };
            // Initializes the UI object for the first time.  This method must be called when the user first access the 
            // WebMap app and it is responsible for creating the first loaded windows (main window and/or login screen probably!).
            // This method must be called after InitModels method because it requires view model to be already set.
            // viewState :  Json object sent from server side containing the information of loaded views.
            App.prototype.InitUI = function (viewState) {
                var that = this;
                this.showLoadingSplash();
                var def = $.Deferred();
                for (var i = 0; i < viewState.LoadedViews.length; i++) {
                    var current = viewState.LoadedViews[i];
                    // gets the matching view model object from the state cache and then navigates to the view.
                    var awaitLogic = Client.StateCache.Current.getObject(current.UniqueID).then(function (viewModel) {
                        console.log("Init promess from InitUI for" + viewModel.Name);
                        if (!viewModel) {
                            WebMap.Client.debug("FATAL ERROR: View model is null");
                            var ret = $.Deferred();
                            def.resolve({});
                            return def.promise();
                        }
                        return that.ViewManager.NavigateToView(viewModel);
                    });
                    def.then(function () {
                        return awaitLogic;
                    });
                }
                //if (this.usercontrols) {
                //    for (var i: number = 0; i < this.usercontrols.length; i++) {
                //        var userControl = this.usercontrols[i];
                //        def.then(function () {
                //            console.log("Init promess from InitUI usercontrol" ); 
                //            userControl.Init();
                //        });
                //    }
                //    this.usercontrols = undefined;
                //}
                def.resolve();
                def.done(this.removeLoadingSplash).done(this.updateMessages(viewState));
                return def.promise();
            };
            // removes the blocking UI message
            App.prototype.removeLoadingSplash = function () {
                $.unblockUI();
            };
            // shows a blocking UI message
            App.prototype.showLoadingSplash = function () {
                $.blockUI({ message: "Loading app views" });
            };
            // Updates the interaction messages sent from the server side in order to show them to the 
            // end user
            App.prototype.updateMessages = function (viewData) {
                if (viewData && viewData.Messages) {
                    var messageList = viewData.Messages;
                    for (var i = 0; i < messageList.length; i++) {
                        var msg = messageList[i];
                        this.showMessageDialog(msg);
                    }
                }
                return null;
            };
            App.prototype.showGenericMessage = function (messageText) {
                var msg = { UniqueID: "generic", Text: messageText, Buttons: 1, Caption: "Exception Occurred" };
                var msgBoxTemplate = this.preparteMessageBoxTemplate(msg);
                // Create the window to be displayed
                var w = $(msgBoxTemplate).kendoWindow({
                    title: msg.Caption ? msg.Caption : "",
                    modal: true,
                    resizable: false
                });
                var kendowWindow = w.data("kendoWindow");
                kendowWindow.center().open();
                // Add handlers to close the window
                w.find('.msgboxokbuttoncls,.msgboxcancelbuttoncls,.msgboxyesbuttoncls,.msgboxnobuttoncls').click(function () {
                    var dialogResult = "cancel";
                    if ($(this).hasClass('msgboxokbuttoncls')) {
                        dialogResult = "ok";
                    }
                    else if ($(this).hasClass('msgboxyesbuttoncls')) {
                        dialogResult = "yes";
                    }
                    else if ($(this).hasClass('msgboxnobuttoncls')) {
                        dialogResult = "no";
                    }
                    else {
                        dialogResult = "cancel";
                    }
                    window.app.sendAction({ controller: "ResumeOperation", action: "ResumePendingOperation", dialogResult: dialogResult });
                    w.data('kendoWindow').close();
                });
            };
            App.prototype.showMessageDialog = function (msg) {
                var msgBoxTemplate = this.preparteMessageBoxTemplate(msg);
                // Create the window to be displayed
                var w = $(msgBoxTemplate).kendoWindow({
                    title: msg.Caption ? msg.Caption : "",
                    modal: true,
                    resizable: false
                });
                var kendowWindow = w.data("kendoWindow");
                kendowWindow.center().open();
                // Add handlers to close the window
                w.find('.msgboxokbuttoncls,.msgboxcancelbuttoncls,.msgboxyesbuttoncls,.msgboxnobuttoncls').click(function () {
                    var dialogResult = "cancel";
                    if ($(this).hasClass('msgboxokbuttoncls')) {
                        dialogResult = "ok";
                    }
                    else if ($(this).hasClass('msgboxyesbuttoncls')) {
                        dialogResult = "yes";
                    }
                    else if ($(this).hasClass('msgboxnobuttoncls')) {
                        dialogResult = "no";
                    }
                    else {
                        dialogResult = "cancel";
                    }
                    window.app.sendAction({ controller: "ResumeOperation", action: "ResumePendingOperation", dialogResult: dialogResult });
                    w.data('kendoWindow').close();
                });
            };
            App.prototype.preparteMessageBoxTemplate = function (msg) {
                var msgBoxTemplate = "<div class='wmmsgbox __additional_classes__'>" + "<span class='msgboxiconscls' ></span><span class='msgboxmsgnclass'> " + msg.Text + "</span > " + "<div style='text-align:center'>";
                var iconClass = this.getMessageBoxIconCssClass(msg.Icons);
                if (msg.Buttons == 0 || msg.Buttons == 1) {
                    msgBoxTemplate = msgBoxTemplate + "<button class='msgboxokbuttoncls' > OK </button > ";
                }
                if (msg.Buttons == 3 || msg.Buttons == 4) {
                    msgBoxTemplate = msgBoxTemplate + "<button class='msgboxyesbuttoncls' > Yes </button > ";
                    msgBoxTemplate = msgBoxTemplate + "<button class='msgboxnobuttoncls'>No</button>";
                }
                if (msg.Buttons == 5) {
                    msgBoxTemplate = msgBoxTemplate + "<button class='msgboxretrybuttoncls'>Retry</button>";
                }
                if (msg.Buttons == 1 || msg.Buttons == 3 || msg.Buttons == 5) {
                    msgBoxTemplate = msgBoxTemplate + "<button class='msgboxcancelbuttoncls'>Cancel</button>";
                }
                msgBoxTemplate = msgBoxTemplate + "</div>";
                msgBoxTemplate = msgBoxTemplate.replace('__additional_classes__', iconClass);
                return msgBoxTemplate;
            };
            App.prototype.getMessageBoxIconCssClass = function (id) {
                var iconClass = "";
                switch (id) {
                    case 1:
                        iconClass = "msgboxquestion";
                        break;
                    case 2:
                        iconClass = "msgboxwarning";
                        break;
                    case 3:
                        iconClass = "msgboxerror";
                        break;
                }
                return iconClass;
            };
            App.prototype.extractItemsCollectionInfoFromDeltas = function (MD) {
                var alldeltas = [];
                for (var i = 0; i < MD.length; i++) {
                    alldeltas.push(MD[i].Delta);
                }
                return this.extractItemsCollectionInfo(alldeltas);
            };
            App.prototype.syncControlArraysSize = function (controlArrays) {
                var element;
                for (var i = 0; i < controlArrays.length; i++) {
                    var curr = controlArrays[i];
                    var diff = curr.controlArray.Count - curr.delta.Count;
                    var index;
                    if (diff < 0) {
                        while (diff < 0) {
                            diff = diff + 1;
                            curr.controlArray.push(Client.StateCache.Current.getObjectLocal(0 + "###" + curr.controlArray.UniqueID));
                        }
                        for (var innerIndex = 0; innerIndex < curr.controlArray.length; innerIndex++) {
                            curr.controlArray[innerIndex] = Client.StateCache.Current.getObjectLocal(innerIndex + "###" + curr.controlArray.UniqueID);
                        }
                        try {
                            curr.controlArray.trigger('change');
                        }
                        catch (ex) {
                        }
                    }
                    else if (diff > 0) {
                        while (diff > 0) {
                            diff = diff - 1;
                            try {
                                curr.controlArray.pop();
                            }
                            catch (ex) {
                            }
                        }
                    }
                    else {
                        //No new items needed?
                        if (curr.delta.Count > 0) {
                            for (var innerIndex = 0; innerIndex < curr.controlArray.length; innerIndex++) {
                                curr.controlArray[innerIndex] = Client.StateCache.Current.getObjectLocal(innerIndex + "###" + curr.controlArray.UniqueID);
                            }
                            index = 0;
                            while (curr.controlArray.length === undefined || (curr.controlArray.length < curr.delta.Count)) {
                                var element = Client.StateCache.Current.getObjectLocal(index + "###" + curr.controlArray.UniqueID);
                                if (!element) {
                                    element = { UniqueID: "" };
                                }
                                ;
                                curr.controlArray.push(element);
                                index = index + 1;
                            }
                        }
                        curr.controlArray.trigger('change');
                    }
                    curr.controlArray.Count = curr.delta.Count;
                }
            };
            App.prototype.getDeltaWithId = function (uniqueID, deltaData) {
                var result = undefined;
                if (deltaData !== undefined) {
                    for (var i = 0; i < deltaData.length; i++) {
                        if (deltaData[i].UniqueID === uniqueID) {
                            result = kendo.observable(deltaData[i].Delta);
                            break;
                        }
                    }
                }
                if (!result) {
                    console.log('Not found:' + uniqueID);
                    result = {
                        UniqueID: uniqueID
                    };
                }
                return result;
            };
            App.prototype.processRemovedIds = function (data) {
                if (data.length) {
                    for (var i = 0; i < data.length; i++) {
                        var id = data[i];
                        if (id in Client.StateCache.Current._cache) {
                            delete Client.StateCache.Current._cache[id];
                        }
                    }
                }
            };
            App.prototype.processSwitchedIds = function (data, deltaData) {
                var collectionsToCheck = {};
                for (var i = 0; i < data.length; i++) {
                    var pair = data[i];
                    if (pair.length === 2) {
                        var id1 = pair[0];
                        var id2 = pair[1];
                        var id1Parts = id1.split('###');
                        var index1 = id1Parts.shift();
                        var id2Parts = id2.split('###');
                        var index2 = id2Parts.shift();
                        if (id1Parts.shift() == "_items" && id2Parts.shift() == "_items") {
                            var list1Id = id1Parts.join("###");
                            var list2Id = id2Parts.join("###");
                            // locate the collections
                            var collection1 = Client.StateCache.Current.getObjectLocal(list1Id);
                            var collection2 = Client.StateCache.Current.getObjectLocal(list2Id);
                            // remmember this collectionto fill its gaps later
                            collectionsToCheck[list1Id] = collection1;
                            // switch the element inside the collection
                            var tmp = collection1.Items[index1];
                            collection1.Items[index2] = tmp;
                            // the origin slot is marked with 'undefined'
                            collection1.Items[index1] = undefined;
                            if (tmp) {
                                tmp.UniqueID = id2;
                            }
                            // Remove the origin slot from the cache
                            delete Client.StateCache.Current._cache[id1];
                            Client.StateCache.Current._cache[id2] = tmp;
                        }
                    }
                }
                for (var listId in collectionsToCheck) {
                    var collection = collectionsToCheck[listId];
                    for (var j = 0; j < collection.Items.length; j++) {
                        if (collection.Items[j] === undefined) {
                            var id = j.toString() + '###_items###' + list1Id;
                            var deltaObj = this.getDeltaWithId(id, deltaData);
                            if (deltaObj) {
                                collection.Items[j] = deltaObj;
                                Client.StateCache.Current._cache[id1] = deltaObj;
                            }
                        }
                    }
                }
            };
            // This method must be called to handle the server side response to an action, it is responsible for updating
            // client side data out from server side changes.  Responsibilities include:
            // 1. Initialize view models for every new view 
            // 2. Update view model objects with server side changed information (delta)
            // 3. Display any shown view
            // 4. Update view changes (position, z-order, visibility)
            // 5. Remove any closed view
            App.prototype.updateClientSide = function (data) {
                var that = this;
                var areThereNewSubViews = false;
                if (data) {
                    if (data.SessionTimeOut) {
                        this.showSeesionExpiredMessage();
                        return;
                    }
                    var def = $.Deferred();
                    if (data.SW) {
                        this.processSwitchedIds(data.SW, data.MD);
                    }
                    if (data.MD) {
                        var controlArrays = [];
                        var itemsCollections = this.extractItemsCollectionInfoFromDeltas(data.MD);
                        for (var i = 0; i < data.MD.length; i++) {
                            var modelUpdateEntry = data.MD[i];
                            var delta = modelUpdateEntry.Delta;
                            var entry = Client.StateCache.Current.getObjectLocal(modelUpdateEntry.UniqueID);
                            if (entry) {
                                if (entry instanceof kendo.data.ObservableArray)
                                    controlArrays.push({ controlArray: entry, delta: delta });
                                else
                                    that.applyDelta(entry, delta);
                            }
                            else {
                                areThereNewSubViews = true;
                                var vm = entry = Client.Container.Current.Resolve({ vm: true, data: delta, dirty: false });
                                if (vm instanceof kendo.data.ObservableArray)
                                    controlArrays.push({ controlArray: vm, delta: delta });
                                if (vm.Name && vm.Name.indexOf("UserControl") == 0) {
                                    var logic = vm["logic"] = WebMap.Client.Container.Current.Resolve({ "cons": App.Current.ViewManager.getConstructor(vm) });
                                    logic.ViewModel = vm;
                                }
                                Client.StateCache.Current.addNewObject(vm);
                            }
                        }
                        this.syncControlArraysSize(controlArrays);
                        if (data.VD && data.VD.NewViews) {
                            var newModels = [];
                            for (var i = 0; i < data.VD.NewViews.length; i++) {
                                var uid = data.VD.NewViews[i];
                                var obj = Client.StateCache.Current.getObjectLocal(uid);
                                if (obj) {
                                    newModels.push(obj);
                                }
                                var promise = that.ViewManager.NavigateToView(obj);
                                def.then(function () {
                                    return promise;
                                });
                            }
                        }
                        //Sets focus to the current control
                        if (data.VD && data.VD.CurrentFocusedControl) {
                            //var id = document.getElementById(data.VD.CurrentFocusedControl);
                            //id.focus();
                            $('#' + data.VD.CurrentFocusedControl).focus();
                        }
                        if (areThereNewSubViews) {
                            var options = undefined;
                            if (itemsCollections.length) {
                                options = { itemsCollections: itemsCollections };
                            }
                            //Child models must be connected to entry in the cache
                            Client.StateCache.Current.organize(options);
                        }
                    }
                    def.resolve();
                    if (data.VD) {
                        this.ViewManager.RemoveViews(data.VD);
                    }
                    if (data.RM) {
                        this.processRemovedIds(data.RM);
                    }
                }
            };
            App.prototype.showSeesionExpiredMessage = function () {
                var msg = { UniqueID: "generic", Text: "The application session has timeout! You must reload the application.", Buttons: 0, Caption: "Session Expired" };
                var msgBoxTemplate = this.preparteMessageBoxTemplate(msg);
                // Create the window to be displayed
                var w = $(msgBoxTemplate).kendoWindow({
                    title: msg.Caption ? msg.Caption : "",
                    modal: true,
                    resizable: false,
                    width: 300
                });
                var kendowWindow = w.data("kendoWindow");
                kendowWindow.center().open();
                // Add handlers to close the window
                w.find('.msgboxokbuttoncls,.msgboxcancelbuttoncls,.msgboxyesbuttoncls,.msgboxnobuttoncls').click(function () {
                    location.reload(true);
                });
                // Add handlers to close the window
                w.find('.msgboxokbuttoncls,.msgboxcancelbuttoncls,.msgboxyesbuttoncls,.msgboxnobuttoncls').html("Reload");
            };
            return App;
        })();
        Client.App = App;
    })(Client = WebMap.Client || (WebMap.Client = {}));
})(WebMap || (WebMap = {}));
/// <reference path="WebMap_Interfaces.ts" />
var WebMap;
(function (WebMap) {
    var Client;
    (function (Client) {
        var SimpleStorageSerializer = (function () {
            function SimpleStorageSerializer() {
            }
            SimpleStorageSerializer.prototype.Serialize = function (obj) {
            };
            return SimpleStorageSerializer;
        })();
        /**
          * (Not implementet yet!) Provides an storage area where to serialize IStateObject objects to.
          * The default persistent layer would be the WebMap server side object.
          */
        var StorageManager = (function () {
            function StorageManager() {
            }
            /**
              * Tries the gets an object from the storage area.
              * @param uniqueId The id of the object to get.
              */
            StorageManager.prototype.TryGetValue = function (uniqueId) {
                var def = $.Deferred();
                def.resolve({ HasValue: false, Value: null });
                return def.promise();
            };
            /**
              * Persists the StateCache object into the storage area.
              * @param stateCache The StateCache object to persist.
              */
            StorageManager.prototype.Persist = function (stateCache) {
            };
            /**
              * Saves an object to the storage area.
              * @param uniqueId  The id of ghe object to serialize
              * @param serializedValue The value to serialize.
              */
            StorageManager.prototype.SaveEntry = function (uniqueID, serializedValue) {
            };
            return StorageManager;
        })();
        Client.StorageManager = StorageManager;
    })(Client = WebMap.Client || (WebMap.Client = {}));
})(WebMap || (WebMap = {}));
// Module
var WebMap;
(function (WebMap) {
    var Client;
    (function (Client) {
        var DeltaTracker = (function () {
            function DeltaTracker() {
                this.dirtyTable = {};
                this.attachedObjs = {};
                this.dirtyPropertiesTable = {};
            }
            DeltaTracker.prototype.isDirty = function (obj) {
                return (this.dirtyTable[obj.UniqueID] || this.dirtyPropertiesTable[obj.UniqueID]);
            };
            //Update the Dirty properties table 
            DeltaTracker.prototype.updateDirtyPropertiesTable = function (fieldName, uid) {
                var propertiesChangesMap = this.dirtyPropertiesTable[uid];
                if (!propertiesChangesMap) {
                    this.dirtyPropertiesTable[uid] = {};
                    propertiesChangesMap = this.dirtyPropertiesTable[uid];
                }
                propertiesChangesMap[fieldName] = true;
            };
            DeltaTracker.prototype.changeTracker = function (e) {
                var fieldName = e.field;
                if (fieldName && e.sender.UniqueID) {
                    var uid = e.sender.UniqueID; // e.sender.get(actualSenderNameParts.join('.')).UniqueID;
                    this.updateDirtyPropertiesTable(fieldName, uid);
                }
            };
            DeltaTracker.prototype.attachModel = function (obj, markAsDirty) {
                this.attachedObjs[obj.UniqueID] = obj;
                var events = obj["_events"];
                var that = this;
                if (!this._changeDelegate) {
                    this._changeDelegate = function (e) {
                        if (!((e.action && e.action == "itemchange") || (e.field && e.field.indexOf('.') > -1))) {
                            that.changeTracker(e);
                        }
                    };
                }
                if (events && events.change) {
                    var found = false;
                    for (var i = 0; i < events.change.length; i++) {
                        if (events.change[i] == this.changeTracker)
                            found = true;
                    }
                    if (!found) {
                        obj.bind("change", this._changeDelegate);
                    }
                }
                else {
                    obj.bind("change", this._changeDelegate);
                }
                if (markAsDirty) {
                    this.dirtyTable[obj.UniqueID] = true;
                }
            };
            DeltaTracker.prototype.reset = function () {
            };
            DeltaTracker.prototype.start = function () {
                this.dirtyTable = {};
                this.dirtyPropertiesTable = {};
            };
            DeltaTracker.prototype.getDeltas = function () {
            };
            DeltaTracker.prototype.wasModified = function (variable) {
                return true;
            };
            DeltaTracker.prototype.getJSONFromFullObject = function (obj) {
                var value = obj.toJSON();
                for (var prop in value) {
                    var elementUniqueID;
                    if (value[prop] && (elementUniqueID = value[prop]) && elementUniqueID.UniqueID) {
                        //this is another viewmode so just decouple
                        delete value[prop];
                    }
                }
                return value;
            };
            DeltaTracker.prototype.getCalculatedDeltaFor = function (variable) {
                var isDirty = this.dirtyTable[variable.UniqueID];
                if (isDirty)
                    return this.getJSONFromFullObject(variable);
                var hasDirtyProperties = this.dirtyPropertiesTable[variable.UniqueID];
                if (hasDirtyProperties) {
                    var delta = {};
                    for (var prop in hasDirtyProperties) {
                        delta[prop] = variable[prop];
                    }
                    return delta;
                }
                return undefined;
            };
            return DeltaTracker;
        })();
        Client.DeltaTracker = DeltaTracker;
    })(Client = WebMap.Client || (WebMap.Client = {}));
})(WebMap || (WebMap = {}));
/// <reference path="WebMap_Interfaces.ts" />
/// <reference path="WebMap_Model.ts" />
var WebMap;
(function (WebMap) {
    var Client;
    (function (Client) {
        var ViewManager = (function () {
            function ViewManager() {
                this._logic = {};
                this.closedViews = [];
            }
            ViewManager.prototype.CloseView = function (view) {
                this.closedViews[this.closedViews.length] = view.UniqueID;
                this.DisposeView(view.UniqueID);
            };
            ViewManager.prototype.PrepareDelta = function (requestInfo) {
                requestInfo.closedViews = this.closedViews;
            };
            ViewManager.prototype.UpdateView = function (view, viewInfo) {
                var viewId = view.UniqueID;
                // if the view has been removed lets ignored the update process
                var isRemoved = this.IsRemovedView(viewId, viewInfo);
                if (!isRemoved) {
                }
            };
            ViewManager.prototype.RemoveViews = function (data) {
                if (data.RemovedViews) {
                    for (var i = 0; i < data.RemovedViews.length; i++) {
                        var viewModelId = data.RemovedViews[i];
                        this.DisposeView(viewModelId);
                    }
                }
            };
            ViewManager.prototype.NavigateToView = function (viewModel) {
                var logic = this._logic[viewModel.UniqueID];
                if (!logic) {
                    logic = Client.Container.Current.Resolve({ "cons": this.getConstructor(viewModel) });
                    this._logic[viewModel.UniqueID] = logic;
                }
                logic.ViewModel = viewModel;
                viewModel.ViewModel = viewModel;
                viewModel.logic = logic;
                this._logic[viewModel.UniqueID] = logic;
                return logic.Init();
            };
            ViewManager.prototype.ShowMessage = function (message, caption, buttons, boxIcons) {
                return null;
            };
            ViewManager.removeDesignerSupportSnippet = function (document) {
                var index = -1;
                if ((index = document.indexOf(ViewManager.DESIGNERSUPPORT)) != -1) {
                    document = document.substring(index + ViewManager.DESIGNERSUPPORT.length);
                }
                return document;
            };
            ViewManager.prototype.LoadView = function (logic, domElement) {
                if (domElement === void 0) { domElement = undefined; }
                var isLib = logic["isLib"];
                var viewName = logic.ViewModel.Name;
                if (logic["usercontrol"]) {
                    viewName = viewName.substring(viewName.lastIndexOf(".") + 1);
                }
                var cssUrl = ViewManager.getViewHtmlURL(viewName, ".css", isLib);
                var that = this;
                ViewManager.AddCssIfMissing(cssUrl);
                console.log("LoadView 1. " + viewName);
                var def = $.get(ViewManager.getViewHtmlURL(viewName, ".html", isLib)).then(function (document) {
                    try {
                        console.log("Laod View 2. " + viewName);
                        document = ViewManager.removeDesignerSupportSnippet(document);
                        var template = kendo.template(document);
                        var appliedTemplate = template(logic.ViewModel);
                        var isUserControl = logic["usercontrol"];
                        if (isUserControl) {
                            var ucDOMId = domElement;
                            var uc = $(ucDOMId);
                            if (uc.length > 0) {
                                uc.append(appliedTemplate);
                            }
                        }
                        else {
                            if (logic.ViewModel['IsMdiParent']) {
                                $("#mainContent").replaceWith(appliedTemplate);
                            }
                            else {
                                $("#mainContent").append(appliedTemplate);
                            }
                        }
                    }
                    catch (e) {
                        Client.debug(e);
                        throw e;
                    }
                });
                return def;
            };
            ViewManager.AddCssIfMissing = function (href) {
                if (!$("head link[href='" + href + "']").length) {
                    var link = $("<link rel='stylesheet' href='" + href + "'>");
                    $("head").append(link);
                }
            };
            // Disposes the given view by removing the ILogicView object and calling its Close method.
            ViewManager.prototype.DisposeView = function (viewModelId) {
                var baseLogic = this._logic[viewModelId];
                delete this._logic[viewModelId];
                baseLogic.Close();
            };
            // Gets a lambda responsible for creating a new ILogicView<IViewModel> object for the given
            // view model
            ViewManager.prototype.getConstructor = function (vm) {
                var root = window;
                var current = null;
                if (vm.Name) {
                    var name = vm.Name;
                    if (name.indexOf('#') > 0) {
                        name = name.substring(name.indexOf('#') + 1);
                    }
                    var parts = name.split(".");
                    for (var i = 0; i < parts.length; i++) {
                        var currentPart = parts[i];
                        if (root === undefined) {
                            throw "Error while looking for constructor of " + vm.Name + " possible cause is that the JS code for that class has not been loaded, if plugins used check if the script is processed";
                        }
                        current = root[currentPart];
                        root = current;
                    }
                }
                return current;
            };
            ViewManager.getViewHtmlURL = function (viewName, extension, isLib) {
                if (isLib === void 0) { isLib = false; }
                if (isLib) {
                    return document.location.protocol + "//" + document.location.hostname + ":" + document.location.port + document.location.pathname + "Resources/libs/" + viewName + extension;
                }
                else {
                    return document.location.protocol + "//" + document.location.hostname + ":" + document.location.port + document.location.pathname + "Resources/" + viewName + extension;
                }
            };
            // Checks that the given id is a removed view
            ViewManager.prototype.IsRemovedView = function (id, VD) {
                if (VD && VD.RemovedViews) {
                    for (var i = 0; i < VD.RemovedViews.length; i++) {
                        if (VD.RemovedViews[i] == id)
                            return true;
                    }
                }
                return false;
            };
            ViewManager.DESIGNERSUPPORT = '<designersupport/>';
            return ViewManager;
        })();
        Client.ViewManager = ViewManager;
    })(Client = WebMap.Client || (WebMap.Client = {}));
})(WebMap || (WebMap = {}));
/// <reference path="WebMap_Storage.ts" />
// Module
var WebMap;
(function (WebMap) {
    var Client;
    (function (Client) {
        var StateCache = (function () {
            function StateCache() {
                this._cache = {};
                this._tracker = new Client.DeltaTracker();
            }
            StateCache.Init = function () {
                if (!StateCache.Current) {
                    StateCache.Current = new StateCache();
                }
            };
            StateCache.prototype.organize = function (options) {
                var cacheEntries = Object.keys(this._cache);
                var cachedEntryKeyCount = -1;
                while (cacheEntries.length && cachedEntryKeyCount != cacheEntries.length) {
                    cachedEntryKeyCount = cacheEntries.length;
                    for (var i = 0; i < cacheEntries.length; i++) {
                        var cachedEntryKey = cacheEntries[i];
                        if (cachedEntryKey.indexOf("###") === -1)
                            continue;
                        var value = this._cache[cachedEntryKey];
                        //We need to link data an entry
                        var accessPath = cachedEntryKey.split("###").reverse();
                        var root = this._cache[accessPath.shift()];
                        var field = accessPath.pop();
                        while (root && accessPath.length > 0) {
                            var shifted = accessPath.shift();
                            if (!(shifted in root) && shifted == "_items") {
                                root = root["Items"];
                            }
                            else {
                                root = root[shifted];
                            }
                        }
                        if (root instanceof kendo.data.ObservableArray) {
                            //This is a control array and field must be an index
                            //but this entries must be collected and then pushed in order
                            //the field holds the index, we also have the value and the owner control array
                            //for an owner we will collect the pairs of values and array index
                            cacheEntries[i] = undefined;
                        }
                        else {
                            if (root && root.set) {
                                if (field === "_items") {
                                    root.set("Items", value);
                                }
                                else {
                                    root.set(field, value);
                                }
                                cacheEntries[i] = undefined;
                            }
                        }
                    }
                    cacheEntries = cacheEntries.filter(function (x) {
                        return x != undefined;
                    });
                }
                if (options && options.itemsCollections) {
                    for (var i = 0; i < options.itemsCollections.length; i++) {
                        var curr = options.itemsCollections[i];
                        var parent = this._cache[curr.Parent];
                        var items = this._cache[curr.Items];
                        if (parent && items) {
                            parent.set("Items", items);
                        }
                    }
                }
            };
            StateCache.prototype.processObject = function (obj) {
                if (obj && obj.UniqueID) {
                    this._cache[obj.UniqueID] = obj;
                    for (var prop in obj) {
                        this.processObject(obj[prop]);
                    }
                }
            };
            StateCache.prototype.addNewObject = function (obj, markAsDirty) {
                this.processObject(obj);
                this._cache[obj.UniqueID] = obj;
                if (markAsDirty === undefined)
                    markAsDirty = true;
                this._tracker.attachModel(obj, markAsDirty);
                return obj;
            };
            StateCache.prototype.getDirty = function () {
                var res = {};
                for (var entryKey in this._cache) {
                    var obj = this._cache[entryKey];
                    var delta = this._tracker.getCalculatedDeltaFor(obj);
                    if (delta) {
                        res[obj.UniqueID] = delta;
                    }
                }
                if (Object.keys(res).length == 0)
                    return undefined;
                this._tracker.start();
                return res;
            };
            StateCache.prototype.getObjectLocal = function (uniqueId) {
                return this._cache[uniqueId];
            };
            StateCache.prototype.clearDirty = function () {
                this._tracker.start();
            };
            StateCache.prototype.getObject = function (uniqueId) {
                var def = $.Deferred();
                var obj = null;
                if ((obj = this._cache[uniqueId])) {
                    def.resolve(obj);
                }
                else {
                    //Cache failed!. Go to storage to see if can be recovered from there    
                    if (this._storageManager) {
                        this._storageManager.TryGetValue(uniqueId).then(function (tmp) {
                            if (tmp.HasValue) {
                                obj = tmp.Value;
                                this._cache[uniqueId] = obj;
                                this._tracker.attachModel(obj);
                                def.resolve(obj);
                            }
                            else
                                def.resolve(null);
                        });
                    }
                    else {
                        def.resolve(null);
                    }
                }
                return def.promise();
            };
            // Removes all elements belonging to a specific view
            StateCache.prototype.RemoveView = function (id) {
                var cacheEntries = Object.keys(this._cache);
                delete this._cache[id];
                var key = "###" + id;
                for (var i = 0; i < cacheEntries.length; i++) {
                    if (cacheEntries[i].lastIndexOf(key) != -1) {
                        delete this._cache[cacheEntries[i]];
                    }
                }
            };
            return StateCache;
        })();
        Client.StateCache = StateCache;
    })(Client = WebMap.Client || (WebMap.Client = {}));
})(WebMap || (WebMap = {}));
/// <reference path="WebMap_Interfaces.ts" />
/// <reference path="WebMap_Helpers.ts" />
var WebMap;
(function (WebMap) {
    var Client;
    (function (Client) {
        // Implements an ILogicView<T> interface.  
        var BaseLogic = (function () {
            function BaseLogic() {
                this.timersToCleanup = [];
            }
            BaseLogic.prototype.Close = function () {
                var domID = this.getDOMID();
                if (!this.ViewModel['IsMdiParent']) {
                    $(domID).data("kendoWindow").close();
                    $(domID).data("kendoWindow").destroy();
                }
                else {
                    window.close();
                }
                this.CleanupTimers();
                Client.StateCache.Current.RemoveView(this.ViewModel.UniqueID);
            };
            BaseLogic.prototype.Init = function (domElement) {
                if (domElement === void 0) { domElement = undefined; }
                var that = this;
                // let's get the associated view from the ViewManager (it could imply a web call) and then
                // let's initalize it!
                var def = Client.App.Current.ViewManager.LoadView(that, domElement);
                def.then(function () {
                    that.CoreInit(domElement);
                });
                return def;
            };
            BaseLogic.prototype.RegisterTimer = function (timerInfo) {
                this.timersToCleanup.push(timerInfo);
            };
            BaseLogic.prototype.generic_Click = function (event) {
                if (event && event.data && event.target) {
                    var options = {
                        controller: event.data.Name.split(".").join("/"),
                        action: event.target.id + "_Click",
                        mainobj: event.data
                    };
                    Client.App.Current.sendAction(options);
                }
                else {
                    var def = $.Deferred();
                    def.resolve();
                    return def.promise();
                }
            };
            // Gets the id of the DOM object associated to the view
            BaseLogic.prototype.getDOMID = function () {
                return "#" + this.ViewModel.UniqueID;
            };
            BaseLogic.prototype.CloseWindowLocally = function (e) {
                if (e.userTriggered) {
                    var id = this.getDOMID();
                    Client.App.Current.ViewManager.CloseView(this.ViewModel);
                }
            };
            // Intializes the view elements using the given options
            BaseLogic.prototype.CoreInit = function (domElement) {
                var isUserControl = this["usercontrol"];
                if (isUserControl) {
                    var ucDOMId = domElement;
                    kendo.bind($(ucDOMId), this.ViewModel);
                    return;
                }
                var domID = this.getDOMID();
                var width = $(domID).attr("data-width");
                var height = $(domID).attr("data-height");
                // renders the kendoWindow
                if (!this.ViewModel['IsMdiParent']) {
                    if (width && height) {
                        $(domID).kendoWindow({ width: width, height: height, resizable: false });
                        $(domID).css("overflow", "hidden"); // fixes an extra scrollbar in chrome
                    }
                    else {
                        $(domID).kendoWindow();
                        $(domID).css("overflow", "hidden"); // fixes an extra scrollbar in chrome
                    }
                }
                // binds the html element identified by gotten domID with the view model
                kendo.bind($(domID), this.ViewModel);
                //Set focus 
                $(domID + " *[tabindex=\"1\"]").first().focus();
                // lets add a close event if not added at migration time
                if (!this.ViewModel['IsMdiParent']) {
                    var kendoInfo = $(domID).data("kendoWindow");
                    if (!kendoInfo) {
                        throw "Possible initialization error. A kendoWindow was expected but it was not found";
                    }
                    var that = this;
                    if (kendoInfo && !kendoInfo._events || !kendoInfo._events.close) {
                        kendoInfo.bind("close", function (e) {
                            if (e.userTriggered) {
                                var id = that.getDOMID();
                                Client.App.Current.ViewManager.CloseView(that.ViewModel);
                            }
                        });
                    }
                }
            };
            BaseLogic.prototype.CleanupTimers = function () {
                for (var i = 0; i < this.timersToCleanup.length; i++) {
                    var timerInfo = this.timersToCleanup[i];
                    if (timerInfo && timerInfo.clearTimer) {
                        timerInfo.clearTimer();
                    }
                }
            };
            return BaseLogic;
        })();
        Client.BaseLogic = BaseLogic;
    })(Client = WebMap.Client || (WebMap.Client = {}));
})(WebMap || (WebMap = {}));
/// <reference path="kendo.all.d.ts" />
/// <reference path="jquery.blockUI.d.ts" />
/// <reference path="jquery.d.ts" />
/// <reference path="WebMap_Interfaces.ts" />
/// <reference path="WebMap_Helpers.ts" />
/// <reference path="jquery.caret.d.ts" />
var WebMap;
(function (WebMap) {
    var Client;
    (function (Client) {
        /// Custom bindings
        kendo.data.binders.checkState = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                var that = this;
                that.element.addEventListener('click', function () {
                    that.change();
                });
            },
            refresh: function () {
                var binding = this.bindings["checkState"];
                var vmelement = binding.source[binding.path.substr(0, binding.path.indexOf('.'))];
                if (vmelement !== undefined) {
                    var currentState = vmelement.get('CheckState');
                    if (currentState != 2) {
                        (this.element).checked = currentState == 1;
                    }
                    else {
                        (this.element).indeterminate = true;
                    }
                }
            },
            change: function () {
                var value = this.element.checked;
                var binding = this.bindings["checkState"];
                var vmelement = binding.source[binding.path.substr(0, binding.path.indexOf('.'))];
                vmelement.set('Checked', value);
                vmelement.set('CheckState', value ? 1 : 0);
                this.bindings['checkState'].set(value ? 1 : 0);
            }
        });
        (kendo.data.binders).customChecked = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                (kendo.data.Binder.fn).init.call(this, element, bindings, options);
                var that = this;
                (that.element).addEventListener('change', function () {
                    that.change();
                });
            },
            refresh: function () {
                var binding = this.bindings["customChecked"];
                (this.element).checked = binding.get();
            },
            change: function () {
                var value = this.element.checked;
                var binding = this.bindings["customChecked"];
                if (binding.source.get(binding.path) !== undefined) {
                    binding.set(true);
                }
                var source = binding.source;
                var relatedRadios = document.querySelectorAll('div[id="' + source.UniqueID + '"] input[name="' + this.element.name + '"]');
                for (var i in relatedRadios) {
                    var e = relatedRadios[i];
                    if (e != this.element && e.kendoBindingTarget && e.kendoBindingTarget.source) {
                        if (e.kendoBindingTarget.source[e.id]) {
                            e.kendoBindingTarget.source[e.id].set('Checked', false);
                        }
                        else {
                            // Check for control array updates
                            var controlArrayRefRegex = /^_(.+)_([0-9]+)/;
                            if (controlArrayRefRegex.test(e.id)) {
                                e.kendoBindingTarget.source.set(e.id.replace(controlArrayRefRegex, "$1[$2]") + ".Checked", false);
                            }
                        }
                    }
                }
            }
        });
        kendo.data.binders.menuEnabled = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
            },
            refresh: function () {
                var binding = this.bindings["menuEnabled"];
                var parentMenu = $(this.element).parents('ul[data-role="menu"]').data('kendoMenu');
                parentMenu.enable(this.element, binding.get());
            }
        });
        kendo.data.binders.widget.dateTimePickerValue = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                var that = this;
                this.element.bind("change", function () {
                    that.change();
                });
            },
            refresh: function () {
                var binding = this.bindings["dateTimePickerValue"];
                var value = binding.get();
                if (typeof (value) === 'string') {
                    value = kendo.parseDate(value);
                }
                this.element.value(value);
            },
            change: function () {
                var value = this.element.value();
                var binding = this.bindings["dateTimePickerValue"];
                binding.set(value);
            }
        });
        kendo.data.binders.buttonText = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
            },
            refresh: function () {
                var binding = this.bindings["buttonText"];
                var spans = $(this.element).find("span");
                if (!spans || !spans.length)
                    $(this.element).html(binding.get());
                else {
                    $(spans[0]).html(binding.get());
                }
            }
        });
        kendo.data.binders.widget.DateTimeMinDate = kendo.data.Binder.extend({
            refresh: function (widget) {
                var binding = this.bindings["DateTimeMinDate"];
                var value = binding.get();
                if (value != null) {
                    var dataRole = this.element.element.attr('data-role');
                    if (dataRole === "datetimepicker")
                        this.element.element.data("kendoDateTimePicker").min(kendo.parseDate(value));
                    else if (dataRole === "datepicker")
                        this.element.element.data("kendoDatePicker").min(kendo.parseDate(value));
                }
            }
        });
        kendo.data.binders.timerTick = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
            },
            refresh: function () {
            }
        });
        kendo.data.binders.timerInterval = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
            },
            refresh: function () {
            }
        });
        kendo.data.binders.timerEnabled = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                this.isTimerActive = false;
                this.currentTimeoutId = -1;
            },
            refresh: function () {
                var binding = this.bindings["timerEnabled"];
                var callbackBinding = this.bindings.events["timerTick"];
                var intervalBinding = this.bindings["timerInterval"];
                var source = binding.source;
                var logic = source.logic;
                var that = this;
                var interval = -1;
                if (intervalBinding !== undefined) {
                    interval = intervalBinding.get();
                }
                else {
                    interval = parseInt($(this.element).attr('data-timerinterval'));
                    if (isNaN(interval)) {
                        interval = -1;
                    }
                }
                if (logic && logic.RegisterTimer) {
                    logic.RegisterTimer(that);
                }
                that.isTimerActive = binding.get() !== false;
                if (binding.get() !== false) {
                    var theCallBack = callbackBinding.source.get(callbackBinding.path);
                    function returnHere() {
                        theCallBack.call(binding.source).then(function () {
                            if (that.isTimerActive) {
                                that.currentTimeoutId = setTimeout(returnHere, interval);
                            }
                        });
                    }
                    that.currentTimeoutId = setTimeout(returnHere, interval);
                }
                else {
                    that.clearTimer();
                }
            },
            clearTimer: function () {
                if (this.isTimerActive) {
                    this.isTimerActive = false;
                }
                if (this.currentTimeoutId != -1) {
                    clearTimeout(this.currentTimeoutId);
                    this.currentTimeoutId = -1;
                }
            }
        });
        kendo.data.binders.widget.comboSelectedIndex = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                var that = this;
                that.indexCorrection = 0;
                this.element.bind("select", function (item) {
                    that.change(item);
                });
                if (this.element.ns === ".kendoDropDownList") {
                    this.indexCorrection = 1;
                }
                this.ignoreRefresh = false;
            },
            refresh: function () {
                var that = this;
                if (!this.ignoreRefresh) {
                    var binding = this.bindings["comboSelectedIndex"];
                    var value = binding.get();
                    if (typeof (value) === 'string') {
                        value = parseInt(value);
                    }
                    // Update the selected element, but be careful
                    // the data in the Combo may not be available yet
                    if (this.element.dataSource.data().length > 0) {
                        this.element.select(value + this.indexCorrection);
                    }
                    else {
                        // If the datasource is not available yet then delay the selection
                        setTimeout(function () {
                            that.element.select(value + that.indexCorrection);
                        }, 1);
                    }
                }
            },
            change: function (item) {
                var value = item.item.index();
                var binding = this.bindings["comboSelectedIndex"];
                try {
                    this.ignoreRefresh = true;
                    binding.set(value - this.indexCorrection);
                }
                finally {
                    this.ignoreRefresh = false;
                }
            }
        });
        //////////////////////////////
        ///
        /// ListView support
        ///
        function createListViewGridAsChild(element, source, columnsObject, thisElement) {
            var newElement = document.createElement("div");
            var role = document.createAttribute("data-role");
            role.value = "grid";
            var db = $(element).attr('data-itemssource');
            var dbatt = document.createAttribute("data-bind");
            dbatt.value = db;
            newElement.setAttribute('data-role', 'grid');
            newElement.setAttribute('data-resizable', 'true');
            var dblclickhandler = $(element).attr('data-doubleclickhandler');
            if (dblclickhandler) {
                db = db + ",listViewDoubleClick: " + dblclickhandler;
            }
            var changeHandler = $(element).attr('data-changehandler');
            if (changeHandler) {
                source.logic["internalChangeHandler"] = function (e) {
                    if (!WebMap.Client.App.Current.isSynchronizingClient()) {
                        this.logic[changeHandler].bind(this)(e);
                    }
                };
                db = db + ",events: {change: logic.internalChangeHandler}";
            }
            var initialHeight = $(element).attr('data-initialheight');
            if (initialHeight !== undefined) {
                newElement.setAttribute('style', 'height: ' + initialHeight + 'px');
            }
            var selectIndices = $(element).attr('data-selectedIndices');
            var checkedIndices = $(element).attr('data-checkedIndices');
            newElement.setAttribute('data-bind', db + "," + selectIndices + "," + checkedIndices);
            element.appendChild(newElement);
            var selectionMode = $(element).data('selectionmode') == "multiple" ? "multiple, row" : "row";
            var allowReorder = $(element).data('allowreorder') === true ? true : false;
            $(newElement).kendoGrid({
                resizable: true,
                scrollable: true,
                reorderable: allowReorder,
                selectable: selectionMode,
                columns: transformColumnsToObj(columnsObject, thisElement),
                dataBound: function (e) {
                    var items = e.sender.dataItems();
                    if (items && items.length) {
                        var itemElements = e.sender.element.find("[class='k-grid-content']").find('tr');
                        var i = 0;
                        for (i = 0; i < items.length; i++) {
                            if (items[i].Checked) {
                                var checkbox = $(itemElements[i]).find('input');
                                if (checkbox) {
                                    $(checkbox).attr("checked", "checked");
                                }
                            }
                            if (items[i].Selected) {
                                $(itemElements[i]).addClass("k-state-selected");
                                $(itemElements[i]).attr('selected', 'true');
                            }
                        }
                    }
                },
                change: function (e) {
                    var items = e.sender.dataItems();
                    if (items && items.length) {
                        var itemElements = e.sender.element.find("[class='k-grid-content']").find('tr');
                        var i = 0;
                        for (i = 0; i < items.length; i++) {
                            var isSelected = $(itemElements[i]).hasClass('k-state-selected');
                            if (isSelected)
                                items[i].Selected = true;
                            else
                                items[i].Selected = false;
                        }
                    }
                }
            });
            // setTimeout is required because the template may not be completely applied
            if (initialHeight === undefined) {
                setTimeout(function () {
                    $(element).find('.k-widget').height("100%");
                    $(newElement).find('.k-grid-content').height("87%");
                    $(newElement).find('.k - grid - header').height("13%");
                }, 100);
            }
            kendo.bind($(element).children('div'), source);
        }
        function removeExistingListViewInnerElement(element) {
            $(element).children('div').each(function (index, subeelement) {
                var existingGrid = $(subeelement).data('kendoGrid');
                if (existingGrid) {
                    existingGrid.destroy();
                    $(subeelement).remove();
                }
                var existingLv = $(subeelement).data('kendoListView');
                if (existingLv) {
                    existingLv.destroy();
                    $(subeelement).remove();
                }
            });
        }
        function noImageTemplate(checkboxes) {
            var listTemplate = "";
            if (checkboxes)
                listTemplate = "<div style='float: left;'><div style='float:left; margin: 10px 10px 0 10px;'><input type='checkbox' value='#= ItemContent[0] #'/><p style='word-wrap:break-word; width: 5em; margin-top:0;'>#= ItemContent[0] #</p><br></div></div>";
            else
                listTemplate = "<div style='float: left;'><div style='float:left; margin: 10px 10px 0 10px;'><p style='word-wrap:break-word; width: 5em; margin-top:0;'>#= ItemContent[0] #</p><br></div></div>";
            return listTemplate;
        }
        function createTemplateForList(element, modevalue, checkboxes) {
            var listTemplate = "<div style='float: left'><div>#= ItemContent[0] #<br></div></div>";
            if (modevalue == 0) {
                var imageListPrefix = $(element).data('imagelistprefix');
            }
            if (modevalue == 2) {
                var imageListPrefix = $(element).data('smallimagelistprefix');
            }
            if (modevalue == 0 || modevalue == 2) {
                if (imageListPrefix) {
                    imageListPrefix = imageListPrefix;
                    if (checkboxes) {
                        listTemplate = "<div style='float: left;'><div style='float:left; margin: 10px 10px 0 10px;'><input type='checkbox' value='#= ItemContent[0] #'/>#if(ImageIndex != -1){#<img src='" + imageListPrefix + ".ImageStream#= ImageIndex #.png'>#}#<p style='word-wrap:break-word; width: 5em; margin-top:0;'>#= ItemContent[0] #</p><br></div></div>";
                    }
                    else {
                        listTemplate = "<div style='float: left;'><div style='float:left; margin: 10px 10px 0 10px;'>#if(ImageIndex != -1){#<img src='" + imageListPrefix + ".ImageStream#= ImageIndex #.png'>#}#<p style='word-wrap:break-word; width: 5em; margin-top:0;'>#= ItemContent[0] #</p><br></div></div>";
                    }
                }
                else
                    listTemplate = noImageTemplate(checkboxes);
            }
            else {
                var imageListPrefix = $(element).data('smallimagelistprefix');
                if (imageListPrefix) {
                    imageListPrefix = imageListPrefix;
                    if (checkboxes) {
                        listTemplate = "<div style='float: left'><div><input type='checkbox' value='#= ItemContent[0] #'/>#if(ImageIndex != -1){#<img src='" + imageListPrefix + ".ImageStream#= ImageIndex #.png'>#}##= ItemContent[0] #<br></div></div>";
                    }
                    else {
                        listTemplate = "<div style='float: left'><div>#if(ImageIndex != -1){#<img src='" + imageListPrefix + ".ImageStream#= ImageIndex #.png'>#}##= ItemContent[0] #<br></div></div>";
                    }
                }
                else if (checkboxes) {
                    listTemplate = "<div style='float: left'><div><input type='checkbox' value='#= ItemContent[0] #'/>#= ItemContent[0] #<br></div></div>";
                }
                else {
                    listTemplate = "<div style='float: left'><div>#= ItemContent[0] #<br></div></div>";
                }
            }
            return listTemplate;
        }
        function refreshListViewTemplates(thisElement) {
            var modebinding = thisElement.bindings["listViewMode"];
            var modevalue = modebinding.get();
            var binding = thisElement.bindings["listViewColumns"];
            var value = binding.get();
            var element = thisElement.element.element[0];
            var checkBoxes = thisElement.bindings["CheckBoxesMode"].get(); //Indicates if the template should have a checkbox element next to each Item
            removeExistingListViewInnerElement(element);
            if (modevalue === 1) {
                createListViewGridAsChild(element, binding.source, value, thisElement);
            }
            else {
                var newElement = document.createElement("div");
                var role = document.createAttribute("data-role");
                role.value = "listview";
                var db = $(element).attr('data-itemssource');
                var selectIndices = $(element).attr('data-selectedIndices');
                var checkedIndices = $(element).attr('data-checkedIndices');
                var selectedIndexChanged = $(element).attr('data-SelectedIndexChanged');
                newElement.setAttribute('data-role', 'listview');
                newElement.setAttribute('data-resizable', 'true');
                var events = "";
                if (selectedIndexChanged !== undefined) {
                    events = "events : {" + selectedIndexChanged + "}";
                }
                ;
                newElement.setAttribute('data-bind', db + "," + selectIndices + "," + checkedIndices + "," + events);
                element.appendChild(newElement);
                var initialHeight = $(element).attr('data-initialheight');
                if (initialHeight !== undefined) {
                    newElement.setAttribute('style', 'height: ' + initialHeight + 'px');
                }
                newElement.setAttribute('style', 'overflow:scroll' + (newElement.getAttribute('style') ? ';' + newElement.getAttribute('style') : ''));
                var selectionMode = $(element).data('selectionmode') == "single" ? "single" : "multiple";
                var listTemplate = createTemplateForList(element, modevalue, checkBoxes);
                $(newElement).kendoListView({
                    navigatable: true,
                    selectable: selectionMode,
                    template: kendo.template($(listTemplate).html()),
                    dataBound: function (e) {
                        var items = e.sender.dataItems();
                        if (items && items.length) {
                            var itemElements = e.sender.element.find("div");
                            var i = 0;
                            for (i = 0; i < items.length; i++) {
                                if (items[i].Checked) {
                                    var checkbox = $(itemElements[i]).find('input');
                                    if (checkbox) {
                                        $(checkbox).attr("checked", "checked");
                                    }
                                }
                                if (items[i].Selected) {
                                    $(itemElements[i]).addClass("k-state-selected");
                                    $(itemElements[i]).attr('selected', 'true');
                                }
                            }
                        }
                    },
                    change: function (e) {
                        var items = e.sender.dataItems();
                        if (items && items.length) {
                            var itemElements = e.sender.element.find("div");
                            var i = 0;
                            for (i = 0; i < items.length; i++) {
                                var isSelected = $(itemElements[i]).hasClass('k-state-selected');
                                console.log(isSelected);
                                if (isSelected)
                                    items[i].Selected = true;
                                else
                                    items[i].Selected = false;
                            }
                        }
                    }
                });
                kendo.bind($(element).children('div'), binding.source);
            }
        }
        kendo.data.binders.widget.listViewSelectedIndices = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                var _this = this;
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                this.innerUpdating = false;
                element.bind('change', function () {
                    _this.update();
                });
            },
            refresh: function () {
                if (!this.innerUpdating) {
                    var selectedIndexBinding = this.bindings["listViewSelectedIndices"];
                    var newIndex = selectedIndexBinding.get();
                    var listview = this.element.element;
                    if (listview)
                        listview = listview[0];
                    else
                        listview = this.element[0];
                    var isGrid = $(listview).find('table');
                    var items = null;
                    if (isGrid.length == 0) {
                        items = $(listview).find('div');
                    }
                    else
                        items = $(listview).find("[class='k-grid-content']").find('tr');
                    this.selectionRestore(items, newIndex);
                }
            },
            update: function () {
                var listview = this.element.element;
                if (listview)
                    listview = listview[0];
                else
                    listview = this.element[0];
                var isGrid = $(listview).find('table');
                var items = null;
                if (isGrid.length == 0)
                    items = $(listview).find('div');
                else
                    items = $(listview).find("[class='k-grid-content']").find('tr');
                var newIndices = [];
                items.each(function (i, item) {
                    var isSelected = $(item).hasClass('k-state-selected');
                    if (typeof isSelected !== typeof undefined && isSelected !== false) {
                        newIndices.push(i);
                    }
                });
                var selectedIndexBinding = this.bindings["listViewSelectedIndices"];
                selectedIndexBinding.set(newIndices);
                this.innerUpdating = false;
            },
            destroy: function () {
                kendo.data.Binder.fn.destroy.call(this);
                this.element.unbind('dblclick');
            },
            selectionRestore: function (children, indices) {
                if (indices) {
                    children.each(function (i, element) {
                        var colIndex = indices.indexOf(i);
                        if (colIndex != -1) {
                            $(element).addClass('k-state-selected');
                            $(element).attr('selected', 'true');
                        }
                        else if (colIndex == -1) {
                            $(element).removeClass('k-state-selected');
                            $(element).removeAttr('selected');
                        }
                    });
                }
            }
        });
        kendo.data.binders.widget.listViewCheckedIndices = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                var _this = this;
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                this.innerUpdating = false;
                element.element.bind('click', function () {
                    _this.update();
                });
                element.element.bind('dataBound', function () {
                    _this.databound();
                });
            },
            refresh: function () {
                if (!this.innerUpdating) {
                    var checkedIndexBinding = this.bindings["listViewCheckedIndices"];
                    var newIndex = checkedIndexBinding.get();
                    var listview = this.element.element;
                    if (listview)
                        listview = listview[0];
                    else
                        listview = this.element[0];
                    var isGrid = $(listview).find('table');
                    var items = null;
                    if (isGrid.length == 0) {
                        items = $(listview).find('div');
                    }
                    else
                        items = $(listview).find("[class='k-grid-content']").find('tr');
                    this.selectionRestore(items, newIndex);
                }
            },
            update: function () {
                var listview = this.element.element;
                if (listview)
                    listview = listview[0];
                else
                    listview = this.element[0];
                var isGrid = $(listview).find('table');
                var items = null;
                if (isGrid.length == 0)
                    items = $(listview).find('div');
                else
                    items = $(listview).find("[class='k-grid-content']").find('tr');
                var newIndices = [];
                items.each(function (i, item) {
                    var checkbox = $(item).find('input');
                    var checked = false;
                    if (checkbox.get(0) !== undefined) {
                        checked = checkbox.prop('checked');
                        if (checked)
                            newIndices.push(i);
                    }
                });
                var checkedIndexBinding = this.bindings["listViewCheckedIndices"];
                checkedIndexBinding.set(newIndices);
                this.innerUpdating = false;
            },
            destroy: function () {
                kendo.data.Binder.fn.destroy.call(this);
                this.element.unbind('dblclick');
            },
            selectionRestore: function (children, indices) {
                if (indices) {
                    children.each(function (i, element) {
                        var colIndex = indices.indexOf(i);
                        if (colIndex != -1) {
                            var checkbox = $(element).find('input');
                            if (checkbox) {
                                $(checkbox).prop("checked", "checked");
                            }
                        }
                        else if (colIndex == -1) {
                            var checkbox = $(element).find('input');
                            if (checkbox)
                                checkbox.removeAttr('checked');
                        }
                    });
                }
            }
        });
        kendo.data.binders.widget.listViewMode = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
            },
            refresh: function () {
                refreshListViewTemplates(this);
            }
        });
        kendo.data.binders.widget.CheckBoxesMode = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
            },
            refresh: function () {
                refreshListViewTemplates(this);
            }
        });
        function transformColumnsToObj(cols, thisElement) {
            var colsStr = [];
            var checkBoxes = thisElement.bindings["CheckBoxesMode"].get();
            if (checkBoxes) {
                colsStr.push({
                    field: "select",
                    title: "&nbsp;",
                    template: '<input type=\'checkbox\' />',
                    sortable: false,
                    width: 25
                });
            }
            for (var idx = 0; idx < cols.Items.length; idx++) {
                var colDefinition = cols.Items[idx];
                var columnAlignment = getKendoAlignment(colDefinition.HorizontalAlignment);
                colsStr.push({
                    title: colDefinition.Text,
                    field: "ItemContent[" + idx.toString() + "]",
                    attributes: {
                        style: "text-align: " + columnAlignment
                    },
                    headerAttributes: {
                        style: "text-align: " + columnAlignment
                    },
                    width: colDefinition.Width
                });
            }
            return colsStr;
        }
        function getKendoAlignment(alignment) {
            switch (alignment) {
                case 0: return "center";
                case 2: return "right";
                default: return "left";
            }
        }
        kendo.data.binders.widget.listViewColumns = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                var binding = this.bindings["listViewColumns"];
                if (binding) {
                    var that = this;
                    var value = binding.get();
                    if (value) {
                        value.bind('change', function () {
                            that.refresh();
                        });
                    }
                }
            },
            refresh: function () {
                var modebinding = this.bindings["listViewMode"];
                var modevalue = modebinding.get();
                if (modevalue !== 1) {
                    return;
                }
                var binding = this.bindings["listViewColumns"];
                var value = binding.get();
                var element = this.element.element[0];
                removeExistingListViewInnerElement(element);
                createListViewGridAsChild(element, binding.source, value, this);
            }
        });
        kendo.data.binders.widget.listViewDoubleClick = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
            },
            refresh: function () {
                var handlerBinding = this.bindings["listViewDoubleClick"];
                var theCallBack = handlerBinding.source.get(handlerBinding.path);
                if (this.theHandler === undefined) {
                    this.theHandler = function () {
                        theCallBack.call(handlerBinding.source);
                    };
                    $(this.element.table).last().dblclick(this.theHandler);
                }
            },
            destroy: function () {
                kendo.data.Binder.fn.destroy.call(this);
                $(this.element.table).last().unbind('dblclick');
            }
        });
        (function ($) {
            var ui = kendo.ui, Widget = ui.Widget;
            var WMListView = Widget.extend({
                init: function (element, options) {
                    Widget.fn.init.call(this, element, options);
                },
                options: {
                    name: "WMListView",
                    mode: 1
                }
            });
            ui.plugin(WMListView);
            kendo.init($(document.body));
        })(jQuery);
        kendo.data.binders.enabled = kendo.data.Binder.extend({
            previousBinder: kendo.data.binders.enabled,
            init: function (widget, bindings, options) {
                //call the base constructor
                kendo.data.Binder.fn.init.call(this, widget, bindings, options);
                this.refresh = this.setupTabStripPage(widget) || this.setupMenuItem(widget) || (this.previousBinder ? this.previousBinder.prototype.refresh : undefined); // use default kendo enabled binder
            },
            setupTabStripPage: function (widget) {
                // check if the HTML element receiving bindings is a tabpage from a tabcontrol widget
                var tabControl, tmp;
                if (widget instanceof HTMLLIElement && (tmp = widget.parentElement) instanceof HTMLUListElement && (tabControl = tmp.parentElement) instanceof HTMLDivElement && tabControl.getAttribute("data-role") === "tabstrip") {
                    var tabWidget = $(tabControl).data("kendoTabStrip");
                    var enabledBinding = this.bindings.enabled;
                    return function () {
                        var index = enabledBinding.path.indexOf(".Items[");
                        if (index >= 0) {
                            var sinPath = enabledBinding.path.substring(0, index);
                            sinPath = sinPath.concat(".Enabled");
                        }
                        var widgetEnabledValue = enabledBinding.source.get(sinPath);
                        if (widgetEnabledValue) {
                            tabWidget.enable(this.element, enabledBinding.get());
                        }
                    };
                }
                return null;
            },
            setupMenuItem: function (widget) {
                var parentMenuElement = $(this.element).parents('ul[data-role="menu"]');
                if (parentMenuElement && parentMenuElement.length > 0) {
                    var binding = this.bindings["enabled"];
                    var parentMenu = parentMenuElement.data('kendoMenu');
                    return function () {
                        parentMenu.enable(this.element, binding.get());
                    };
                }
                return null;
            }
        });
        /// Enabled custom binding for widgets 
        kendo.data.binders.widget.enabled = kendo.data.Binder.extend({
            previousBinder: kendo.data.binders.widget.enabled,
            init: function (widget, bindings, options) {
                //call the base constructor
                kendo.data.Binder.fn.init.call(this, widget, bindings, options);
                this.refresh = this.setupControls(widget) || (this.previousBinder ? this.previousBinder.prototype.refresh : undefined); // use default kendo widget enabled binder
            },
            setupControls: function (widget) {
                var dataRole = $(widget.element).attr('data-role');
                if (dataRole === "treeview") {
                    var treeWidget = widget;
                    var enabledBinding = this.bindings.enabled;
                    return function () {
                        treeWidget.enable(".k-item", enabledBinding.get());
                    };
                }
                else if (dataRole === "dropdownlist" || dataRole === "datetimepicker" || dataRole === "timepicker" || dataRole === "datepicker" || dataRole === "combobox") {
                    var enabledBinding = this.bindings.enabled;
                    return function () {
                        widget.enable(enabledBinding.get());
                    };
                }
                else if (dataRole === "wmlistview") {
                    var enabledBinding = this.bindings.enabled;
                    return function () {
                        var coverDiv = $(widget.element).find('.cover');
                        var enabledValue = enabledBinding.get();
                        if (!enabledValue) {
                            $("<div class='cover'>").css({
                                width: "100%",
                                height: "100%",
                                top: "0px",
                                left: "0px",
                                position: "absolute",
                                background: 'rgba(0,0,0,0.2)',
                                "z-index": $("div[data-role='grid']").css('z-index') + 1
                            }).appendTo($(widget.element));
                        }
                        else {
                            $(widget.element).find('.cover').remove();
                        }
                    };
                }
                else if (dataRole === "tabstrip") {
                    var enabledBinding = this.bindings.enabled;
                    return function () {
                        var ulElem = $(widget.element).children('ul').first();
                        ulElem.children('li').each(function (index, li) {
                            if (enabledBinding.get()) {
                                var sinPath = enabledBinding.path.replace('.Enabled', '');
                                var itemEnabledValue = enabledBinding.source.get(sinPath + '.Items[' + index + '].Enabled');
                                widget.enable(li, itemEnabledValue);
                            }
                            else {
                                widget.enable(li, enabledBinding.get());
                            }
                        });
                    };
                }
                return null;
            }
        });
        kendo.data.binders.visible = kendo.data.Binder.extend({
            previousBinder: kendo.data.binders.visible,
            init: function (widget, bindings, options) {
                //call the base constructor
                kendo.data.Binder.fn.init.call(this, widget, bindings, options);
                //WebMap.Kendo.setupBinding(this, kendo.data.binders.visible, [TabPageVisibleBindingSetup.prototype]);
                this.refresh = this.setupTabStripPage(widget) || (this.previousBinder ? this.previousBinder.prototype.refresh : undefined); // use default kendo enabled binder
            },
            setupTabStripPage: function (widget) {
                // check if the HTML element receiving bindings is a tabpage from a tabcontrol widget
                var tabControl, tmp;
                if (widget instanceof HTMLLIElement && (tmp = widget.parentElement) instanceof HTMLUListElement && (tabControl = tmp.parentElement) instanceof HTMLDivElement && tabControl.getAttribute("data-role") === "tabstrip") {
                    var tabWidget = $(tabControl).data("kendoTabStrip");
                    var visibleBinding = this.bindings.visible;
                    return function () {
                        var cls = widget.getAttributeNode("class");
                        if (visibleBinding.get()) {
                            if (!WebMap.Utils.IsClassSet(cls.value, "k-item"))
                                cls.value = "k-item " + cls.value;
                            widget.removeAttribute("hidden");
                        }
                        else {
                            var newValue = WebMap.Utils.RemoveClass(cls.value, "k-item");
                            if (newValue != cls.value) {
                                if (tabWidget.select()[0] === widget)
                                    tabWidget.deactivateTab(widget);
                                cls.value = newValue;
                                widget.setAttribute("hidden", "hidden");
                            }
                        }
                    };
                }
                return null;
            }
        });
        //class TabPageVisibleBindingSetup extends WebMap.Kendo.BindingSetup {
        //    public setup(binding: kendo.data.Binder): boolean {
        //        // check if the HTML element receiving bindings is a tabpage from a tabcontrol widget
        //        var widget = binding.element;
        //        var tabControl;
        //        if (widget instanceof HTMLLIElement && widget.parentElement instanceof HTMLUListElement &&
        //            (tabControl = widget.parentElement.parentElement) instanceof HTMLDivElement &&
        //            tabControl.getAttribute("data-role") === "tabstrip") {
        //            var tabWidget = $(tabControl).data("kendoTabStrip");
        //            var visibleBinding = binding.bindings["visible"];
        //            binding.refresh = function () {
        //                var cls = widget.getAttributeNode("class");
        //                if (visibleBinding.get()) {
        //                    if (!WebMap.Utils.IsClassSet(cls.value, "k-item"))
        //                        cls.value = "k-item " + cls.value;
        //                    widget.removeAttribute("hidden");
        //                }
        //                else {
        //                    var newValue = WebMap.Utils.RemoveClass(cls.value, "k-item");
        //                    if (newValue != cls.value) {
        //                        if (tabWidget.select()[0] === widget)
        //                            tabWidget.deactivateTab(widget);
        //                        cls.value = newValue;
        //                        widget.setAttribute("hidden", "hidden");
        //                    }
        //                }
        //            };
        //            return true;
        //        }
        //        return false;
        //    }
        //}
        kendo.data.binders.widget.visible = kendo.data.Binder.extend({
            previousBinder: kendo.data.binders.visible,
            init: function (widget, bindings, options) {
                //call the base constructor
                kendo.data.Binder.fn.init.call(this, widget, bindings, options);
                this.refresh = this.setupCustomControl(widget) || (this.previousBinder ? this.previousBinder.prototype.refresh : undefined); // use default kendo enabled binder
            },
            setupCustomControl: function (widget) {
                var dataRole = $(widget.element).attr('data-role');
                var visibleBinding = this.bindings.visible;
                if (dataRole === "wmlistview" || dataRole === "listbox" || dataRole === "grid" || dataRole === "wmflexgrid" || dataRole === "dropdownlist" || dataRole === "combobox" || dataRole === "menu" || dataRole === "treeview" || dataRole === "tabstrip" || dataRole === "datetimepicker" || dataRole === "timepicker" || dataRole === "datepicker") {
                    return function () {
                        if (visibleBinding.get()) {
                            $(widget.element).css({ visibility: "inherit" });
                        }
                        else {
                            $(widget.element).css({ visibility: "hidden" });
                        }
                    };
                }
                return null;
            }
        });
        kendo.data.binders.widget.selectedIndex = kendo.data.Binder.extend({
            previousBinder: kendo.data.binders.widget.selectedIndex,
            init: function (widget, bindings, options) {
                //call the base constructor
                kendo.data.Binder.fn.init.call(this, widget, bindings, options);
                if (!this.setupTabStripPage(widget) && this.previousBinder) {
                    this.refresh = this.previousBinder.prototype.refresh;
                    this.destroy = this.previousBinder.proptotype.destroy;
                }
            },
            setupTabStripPage: function (widget) {
                // check if the HTML element receiving bindings is a tabpage from a tabcontrol widget
                var htmlElement = widget.element[0];
                if (htmlElement instanceof HTMLDivElement && htmlElement.getAttribute("data-role") === "tabstrip") {
                    var selectedIndexBinding = this.bindings.selectedIndex;
                    var busy = false;
                    var updateModel = function () {
                        if (!busy)
                            try {
                                busy = true;
                                selectedIndexBinding.set(widget.select().index());
                            }
                            finally {
                                busy = false;
                            }
                    };
                    WebMap.Client.App.Current.addModelUpdateListener(updateModel);
                    this.refresh = function () {
                        if (!busy)
                            try {
                                busy = true;
                                var selectedValue = selectedIndexBinding.get();
                                if (selectedValue == -1) {
                                    widget.select(0);
                                }
                                else {
                                    widget.select(selectedValue);
                                }
                            }
                            finally {
                                busy = false;
                            }
                    };
                    this.destroy = function () {
                        WebMap.Client.App.Current.removeModelUpdateListener(updateModel);
                    };
                    return true;
                }
                return false;
            }
        });
        kendo.data.binders.widget.flexGridRefresh = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
            },
            refresh: function () {
                var binding = this.bindings["flexGridRefresh"];
                var value = binding.get();
                if (value === true) {
                    try {
                        var grid = $(this.element.element.children('div')).data('kendoGrid');
                        grid.dataSource.read();
                    }
                    finally {
                        binding.set(false);
                    }
                }
            }
        });
        kendo.data.binders.widget.flexGridPosition = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                this.innerUpdating = false;
                var that = this;
                that.registerChange();
                this.element.bind('reconfigured', function () {
                    that.registerChange();
                });
            },
            registerChange: function () {
                var that = this;
                var grid = $(this.element.element).find('div').data('kendoGrid');
                if (grid) {
                    grid.bind('change', function () {
                        that.update(grid, that.bindings["flexGridPosition"]);
                        setTimeout(function () {
                            that.element.trigger("click");
                        }, 1);
                    });
                }
            },
            refresh: function () {
                if (!this.innerUpdating) {
                    var binding = this.bindings["flexGridPosition"];
                    var grid = $(this.element.element).find('div').data('kendoGrid');
                    var htmlRow = grid.tbody.children().eq(binding.get()[0] - this.rowAdjustment());
                    if (htmlRow && htmlRow.length > 0) {
                        if ((grid.options && grid.options.selectable === "row")) {
                            grid.clearSelection();
                            grid.select(htmlCell);
                        }
                        else {
                            var htmlCell = htmlRow.children('td').eq(binding.get()[1]);
                            if (htmlCell && !htmlCell.hasClass('k-state-selected')) {
                                grid.clearSelection();
                                grid.select(htmlCell);
                            }
                        }
                    }
                }
            },
            update: function (grid, binding) {
                var dataItem = grid.dataItem(grid.select()) || grid.dataItem($(grid.select()).parent());
                if (dataItem && dataItem.UniqueID) {
                    var rowIndex = parseInt(dataItem.UniqueID.substring(0, dataItem.UniqueID.indexOf('#')));
                    rowIndex = rowIndex + this.rowAdjustment();
                    try {
                        this.innerUpdating = true;
                        var columnIndex = (grid.options && grid.options.selectable === "row") ? 0 : $(grid.select()).index();
                        binding.set([rowIndex, columnIndex]);
                    }
                    finally {
                        this.innerUpdating = false;
                    }
                }
            },
            rowAdjustment: function () {
                var result = 0;
                if (this.bindings['flexGridDefinition'] && this.bindings['flexGridDefinition'].get) {
                    var binding = this.bindings['flexGridDefinition'];
                    var columnsValue = binding.get();
                    if (columnsValue && columnsValue.parent() && "FixedRowsCount" in columnsValue.parent()) {
                        result = columnsValue.parent().FixedRowsCount;
                    }
                }
                return result;
            }
        });
        kendo.data.binders.widget.flexGridEndSelection = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                this.innerUpdating = false;
                var that = this;
                var grid = $(element.element).find('div').data('kendoGrid');
                if (grid) {
                    grid.bind('change', function () {
                        that.update(grid, that.bindings["flexGridEndSelection"]);
                    });
                }
            },
            refresh: function () {
                if (!this.innerUpdating) {
                    var binding = this.bindings["flexGridEndSelection"];
                    var positionBinding = this.bindings["flexGridPosition"];
                    if (positionBinding) {
                        if (positionBinding.get()[0] == binding.get()[0] && positionBinding.get()[1] == binding.get()[1]) {
                            return;
                        }
                        var position = positionBinding.get();
                        position[0] = position[0] - this.rowAdjustment();
                        var destination = binding.get();
                        destination[0] = destination[0] - this.rowAdjustment();
                        var grid = $(this.element.element).find('div').data('kendoGrid');
                        var htmlRow = grid.tbody.children().eq(binding.get()[0]);
                        grid.clearSelection();
                        for (var y = position[1]; y <= destination[1]; y++) {
                            for (var x = position[0]; x <= destination[0]; x++) {
                                this.selectCell(x, y, grid);
                            }
                        }
                    }
                }
            },
            selectCell: function (row, column, grid) {
                var htmlRow = grid.tbody.children().eq(row);
                if (htmlRow) {
                    if ((grid.options && grid.options.selectable === "row")) {
                        grid.select(htmlCell);
                    }
                    else {
                        var htmlCell = htmlRow.children('td').eq(column);
                        if (htmlCell && !htmlCell.hasClass('k-state-selected')) {
                            grid.select(htmlCell);
                        }
                    }
                }
            },
            rowAdjustment: function () {
                var result = 0;
                if (this.bindings['flexGridDefinition'] && this.bindings['flexGridDefinition'].get) {
                    var binding = this.bindings['flexGridDefinition'];
                    var columnsValue = binding.get();
                    if (columnsValue && columnsValue.parent() && "FixedRowsCount" in columnsValue.parent()) {
                        result = columnsValue.parent().FixedRowsCount;
                    }
                }
                return result;
            },
            update: function (grid, binding) {
                var dataItem = grid.dataItem(grid.select().last()) || grid.dataItem($(grid.select().last()).parent());
                if (dataItem && dataItem.UniqueID) {
                    var rowIndex = parseInt(dataItem.UniqueID.substring(0, dataItem.UniqueID.indexOf('#'))) + 1;
                    try {
                        this.innerUpdating = true;
                        var columnIndex = (grid.options && grid.options.selectable === "multiple row") ? ($(grid.select().last()).children('td').length - 1) : $(grid.select().last()).index();
                        binding.set([rowIndex, columnIndex]);
                    }
                    finally {
                        this.innerUpdating = false;
                    }
                }
            }
        });
        kendo.data.binders.widget.flexCellCoordinates = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                this.innerUpdating = false;
                var that = this;
                var grid = $(element.element).find('div').data('kendoGrid');
                if (grid) {
                    grid.bind('change', function () {
                        that.update(grid, that.bindings["flexCellCoordinates"]);
                    });
                }
            },
            refresh: function () {
            },
            update: function (grid, binding) {
                var selected = grid.select();
                if (selected) {
                    var height = Math.floor($(selected).height());
                    var width = Math.floor($(selected).width());
                    var pos = $(selected).position();
                    binding.set([pos.left, pos.top, width, height]);
                }
            }
        });
        kendo.data.binders.widget.flexGridDefinition = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                var binding = this.bindings["flexGridDefinition"];
                if (binding) {
                    var that = this;
                    var value = binding.get();
                    if (value) {
                        value.bind('change', function () {
                            if (!that.isRefreshPending) {
                                that.isRefreshPending = true;
                                setTimeout(function () {
                                    try {
                                        that.refresh();
                                        that.element.trigger('reconfigured');
                                    }
                                    finally {
                                        that.isRefreshPending = false;
                                    }
                                }, 1);
                            }
                        });
                    }
                }
            },
            refresh: function () {
                var binding = this.bindings["flexGridDefinition"];
                var value = binding.get();
                var element = this.element.element[0];
                this.removeExistingInnerElement(element);
                this.createGridAsChild(element, binding.source, value);
            },
            transformColumnsToObj: function (cols) {
                var colsStr = [];
                for (var idx = 0; idx < cols.Items.length; idx++) {
                    var colDefinition = cols.Items[idx];
                    colsStr.push({
                        title: colDefinition.Text,
                        field: "RowContent[" + idx.toString() + "]",
                        width: colDefinition.Width
                    });
                }
                return colsStr;
            },
            removeExistingInnerElement: function (element) {
                $(element).children('div').each(function (index, subeelement) {
                    var existingGrid = $(subeelement).data('kendoGrid');
                    if (existingGrid) {
                        existingGrid.destroy();
                        $(subeelement).remove();
                    }
                });
                ///
            },
            createGridAsChild: function (element, source, columnsObject) {
                var that = this;
                var newElement = document.createElement("div");
                var role = document.createAttribute("data-role");
                role.value = "grid";
                var db = "source: " + $(element).attr('data-itemssource');
                var dbatt = document.createAttribute("data-bind");
                dbatt.value = db;
                newElement.setAttribute('data-role', 'grid');
                newElement.setAttribute('data-resizable', 'true');
                var pageSize = parseInt($(element).attr('data-pagesize')) || 15;
                var itemsSource = source.get($(element).attr('data-itemssource'));
                var itemsSourceUniqueId = itemsSource.UniqueID;
                var extraBindings = $(element).attr('data-gridbindings');
                if (extraBindings) {
                    newElement.setAttribute('data-bind', extraBindings);
                }
                element.appendChild(newElement);
                var selectionMode = $(element).attr('data-selectionmode') || "multiple cell";
                $(newElement).kendoGrid({
                    resizable: true,
                    scrollable: {
                        virtual: true
                    },
                    selectable: selectionMode,
                    //pageable: true,
                    dataSource: {
                        serverPaging: true,
                        pageSize: pageSize,
                        schema: {
                            data: "data",
                            total: "total"
                        },
                        transport: {
                            read: function (options) {
                                var t = new Date();
                                $.ajax({
                                    url: 'ResumeOperation/CollectiongPendingResults',
                                    type: "POST",
                                    headers: {
                                        WM: true
                                    },
                                    dataType: "json",
                                    data: { coluid: itemsSourceUniqueId, skip: options.data.skip, take: options.data.take },
                                    success: function (data, textStatus, jqXHR) {
                                        options.success(data);
                                    }
                                });
                            }
                        }
                    },
                    columns: this.transformColumnsToObj(columnsObject)
                });
                // setTimeout is required because the template may not be completely applied
                setTimeout(function () {
                    $(element).find('.k-widget').height("100%");
                    $(element).find('table').last().dblclick(function () {
                        that.element.trigger('dblclick');
                    });
                }, 100);
                kendo.bind($(element).children('div'), source);
            }
        });
        kendo.data.binders.widget.treeDataSource = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                var that = this;
                // Create a new empty data source to start with
                element.setDataSource(kendo.observableHierarchy([]));
                // Event handler used to synchronize checkboxes
                element.dataSource.bind('change', function (e) {
                    that.syncCheckedNodes(that, e);
                });
                // Event handler to handle children
                element.bind('change', function (e) {
                    var selected = element.dataItem(element.select());
                    if (selected && selected.wmid) {
                        var dataSourceBinding = that.bindings["treeDataSource"];
                        var model = dataSourceBinding.get();
                        model.set('SelectedItemId', selected.wmid);
                    }
                });
                element.element.bind('click', function (e) {
                    var editable = $(this).attr('data-labeledit');
                    if (editable && (editable == "true")) {
                        var modelObj = e.target;
                        $(modelObj).bind('click', function () {
                            $(modelObj).prop('contentEditable', true);
                        });
                        $(modelObj).bind('blur', function () {
                            $(modelObj).removeAttr('contentEditable');
                            var binding = that.bindings["treeDataSource"];
                            var tree = binding.get();
                            var ds = that.element.dataSource.data();
                            for (var i = 0; i < tree.Items.length; i++) {
                                var modelObj = tree.Items[i];
                                var htmlElement = ds[i];
                                that.updateValues(that, modelObj, htmlElement);
                            }
                        });
                    }
                });
                that.initialHeight = $(element.element).attr('data-initialheight');
                that.imageListPrefix = $(element.element).data('imagelistprefix');
                that.imageIndex = $(element.element).data('imageIndex');
            },
            // Updating text when the labels are edited.
            updateValues: function (that, e, treeNode) {
                if (treeNode) {
                    var node = $(that.element.element).find("[data-uid='" + treeNode.uid + "']").children()[0];
                    if (node) {
                        var innerSpan = $(node).find("[class*='k-in']");
                        e.set('Text', innerSpan.text());
                    }
                    if (e.items)
                        for (var i = 0; i < e.Items.length; i++) {
                            this.updateValues(that, e.Items[i], treeNode.items[i]);
                        }
                }
            },
            // Synchronization of checkbox nodes
            syncCheckedNodes: function (that, e) {
                var binding = that.bindings["treeDataSource"];
                if (binding && binding.get() && (e.field === 'checked' || e.field === 'selected' || e.field === 'expanded' || e.field === 'isVisible') && e.items && e.items.length > 0) {
                    var tree = binding.get();
                    var field = e.field;
                    for (var i = 0; i < tree.Items.length; i++) {
                        that.syncingCheckedNodes(that, tree.Items[i], e.items, field);
                    }
                }
            },
            syncingCheckedNodes: function (that, modelTreeNode, treeNodes, field) {
                for (var i = 0; i < treeNodes.length; i++) {
                    var uiTreeNode = treeNodes[i];
                    if (uiTreeNode.wmid === modelTreeNode.UniqueID) {
                        var ht = $(uiTreeNode).html();
                        switch (field) {
                            case "checked": {
                                modelTreeNode.set('Checked', uiTreeNode.checked);
                                break;
                            }
                            case "selected": {
                                modelTreeNode.set('IsSelected', uiTreeNode.selected);
                                break;
                            }
                            case "expanded": {
                                modelTreeNode.set('IsExpanded', uiTreeNode.expanded);
                                break;
                            }
                            case "isVisible": {
                                modelTreeNode.set('IsVisible', uiTreeNode.isVisible);
                                break;
                            }
                        }
                    }
                }
                for (var i = 0; i < modelTreeNode.Items.length; i++) {
                    this.syncingCheckedNodes(that, modelTreeNode.Items[i], treeNodes, field);
                }
            },
            createMirrorNodeFromModelNode: function (modelObj) {
                var that = this;
                var imageurl = undefined;
                if (that.imageListPrefix !== undefined)
                    if (modelObj.ImageIndex != -1)
                        imageurl = that.imageListPrefix + '.ImageStream' + modelObj.ImageIndex + ".png";
                    else if (that.imageIndex != -1)
                        imageurl = that.imageListPrefix + '.ImageStream' + that.imageIndex + ".png"; //Default image
                return kendo.observableHierarchy([{
                    text: modelObj.Text,
                    wmid: modelObj.UniqueID,
                    imageUrl: imageurl,
                    checked: modelObj.Checked,
                    expanded: modelObj.IsExpanded,
                    items: []
                }])[0];
            },
            createMirrorStructure: function (parentItemsCollection, modelObj) {
                var that = this;
                var newObj = that.createMirrorNodeFromModelNode(modelObj);
                parentItemsCollection.push(newObj);
                var element = $(that.element.element).find("[data-uid='" + newObj.uid + "']");
                var visibility = $(element).is(":visible");
                var itemHeight = $(element).height();
                var dataSourceBinding = that.bindings["treeDataSource"];
                var model = dataSourceBinding.get();
                var vc = model.get('VisibleCount');
                if (!(vc && vc > 0) && (itemHeight > 0)) {
                    var visibleCount = Math.floor(that.initialHeight / itemHeight);
                    model.set('VisibleCount', visibleCount);
                }
                var newObj2 = parentItemsCollection[parentItemsCollection.length - 1];
                newObj2.set('isVisible', visibility);
                function syncItems(e) {
                    if ((e.action === 'add' || e.action === undefined) && newObj2.items.length < modelObj.Items.length) {
                        for (var k = newObj2.items.length; k < modelObj.Items.length; k++) {
                            var innerObj = modelObj.Items[k];
                            var newInnerObj = that.createMirrorNodeFromModelNode(innerObj);
                            that.createMirrorStructure(newObj2.items, innerObj);
                        }
                    }
                    else if (e.action == 'remove' && ('index' in e)) {
                        newObj2.items.remove(newObj2[e.index]);
                    }
                    else {
                        horizontalSyncItems();
                    }
                }
                function horizontalSyncItems() {
                    for (var i = 0; i < newObj.items.length && i < modelObj.Items.length; i++) {
                        if (newObj.items[i] && modelObj.Items[i] && newObj.items[i].wmid && modelObj.Items[i].UniqueID && newObj.items[i].wmid !== modelObj.Items[i].UniqueID) {
                            newObj.items[i].set("wmid", modelObj.Items[i].UniqueID);
                            newObj.items[i].set("text", modelObj.Items[i].Text);
                            newObj.items[i].set("checked", modelObj.Items[i].Checked);
                            newObj.items[i].set("items", modelObj.Items[i].Items);
                        }
                    }
                }
                var changeFunc = function (e) {
                    if (e.field === "Text") {
                        newObj2.set('text', modelObj.get(e.field));
                    }
                    if (e.field === "Items") {
                        if (!(e.action && (e.action === 'itemchange'))) {
                            syncItems(e);
                        }
                    }
                    if (e.field === "IsExpanded") {
                        newObj2.set('expanded', modelObj.get(e.field));
                    }
                    else {
                        newObj2.set(e.field, modelObj.get(e.field));
                    }
                };
                modelObj.bind('change', changeFunc);
                newObj2.changeFunc = changeFunc;
                // Add existing items if available
                if ('Items' in modelObj) {
                    var itemsChangeFunc = function (e) {
                        if (e.action === "add") {
                            var idx = e.index === undefined ? 0 : e.index;
                            var innerObj = modelObj.Items[idx];
                            var newInnerObj = that.createMirrorNodeFromModelNode(innerObj);
                            that.createMirrorStructure(newObj2.items, modelObj.Items[idx]);
                        }
                    };
                    newObj2.itemsChangeFunc = itemsChangeFunc;
                    for (var k = 0; k < modelObj.Items.length; k++) {
                        this.createMirrorStructure(newObj2.items, modelObj.Items[k]);
                    }
                }
            },
            refresh: function () {
                var that = this;
                var treeDataSourceBinding = this.bindings["treeDataSource"];
                var labelEdit = $(this.element.element).data("LabelEdit");
                that.labelEdit = labelEdit;
                var bvalue = treeDataSourceBinding.get().Items;
                var ds = this.element.dataSource;
                var data = ds.data();
                var callRefresh = function (f) {
                    // Skip for child object property change
                    if (f.action === "itemchange") {
                        return;
                    }
                    else if (f.action === "remove" && ('index' in f)) {
                        data.remove(data[f.index]);
                    }
                    else {
                        that.refresh();
                    }
                };
                // Unregister a previous version of the 'refresh' callback if exist
                // (this is needed to avoid multiple calls)
                if (data.callRefresh) {
                    bvalue.unbind('change', callRefresh);
                }
                bvalue.bind('change', callRefresh);
                // Keep a reference of the function in order to unregister it
                data.callRefresh = callRefresh;
                var oldCount = data.length;
                for (var i = oldCount; i < bvalue.length; i++) {
                    var modelObj = bvalue[i];
                    that.createMirrorStructure(data, modelObj);
                }
            }
        });
        (function ($) {
            var ui = kendo.ui, Widget = ui.Widget;
            var wmflexgrid = Widget.extend({
                init: function (element, options) {
                    Widget.fn.init.call(this, element, options);
                },
                options: {
                    name: "wmflexgrid",
                    itemssource: ""
                },
                events: [
                    "reconfigured",
                    "dblclick",
                    "click"
                ]
            });
            ui.plugin(wmflexgrid);
            kendo.init($(document.body));
        })(jQuery);
        kendo.data.binders.widget.listBoxSelectedIndices = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                var _this = this;
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                this.innerUpdating = false;
                element.bind('change', function () {
                    _this.update();
                });
                element.bind('dataBound', function () {
                    _this.databound();
                });
            },
            refresh: function () {
                if (!this.innerUpdating) {
                    this.selectionRestore(this.element.items(), this.bindings["listBoxSelectedIndices"].get());
                }
            },
            update: function () {
                try {
                    this.innerUpdating = true;
                    var selectedIndices = $.map(this.element.select(), function (item) { return $(item).index(); });
                    var listBoxSelectedIndicesBinding = this.bindings["listBoxSelectedIndices"];
                    listBoxSelectedIndicesBinding.set(selectedIndices);
                }
                finally {
                    this.innerUpdating = false;
                }
            },
            databound: function () {
                if (!this.innerUpdating) {
                    this.selectionRestore(this.element.items(), this.bindings["listBoxSelectedIndices"].get());
                }
            },
            selectionRestore: function (children, indices) {
                if (indices && indices.indexOf) {
                    children.each(function (i, element) {
                        var isSelected = $(element).hasClass('k-state-selected');
                        var colIndex = indices.indexOf(i);
                        if (colIndex != -1 && !isSelected) {
                            $(element).addClass('k-state-selected');
                        }
                        else if (colIndex == -1 && isSelected) {
                            $(element).removeClass('k-state-selected');
                        }
                    });
                }
            }
        });
        function checkedListBoxProc(element) {
            var listBox = $(element).parents('div[data-role="checkedlistbox"]').data('kendoCheckedListBox');
            var index = $(element).index();
            if (listBox && index >= 0) {
                var data = listBox.dataSource.data();
                if (data && data[index]) {
                    data[index].set("Checked", element.checked);
                }
            }
        }
        Client.checkedListBoxProc = checkedListBoxProc;
        kendo.data.binders.selectedRange = kendo.data.Binder.extend({
            init: function (element, bindings, options) {
                kendo.data.Binder.fn.init.call(this, element, bindings, options);
                var that = this;
                $(element).bind('focus', function () {
                    var binding = that.bindings["selectedRange"];
                    var bindingValue = binding.get();
                    var selectionStart = bindingValue[0];
                    var selectionEnd = bindingValue[1];
                    $(that.element).caret({ start: selectionStart, end: selectionEnd });
                });
                $(element).bind('click', function () {
                    that.update();
                });
                $(element).bind('keyup', function () {
                    that.update();
                });
                $(element).bind('keydown', function () {
                    that.update();
                });
                $(element).bind('keypress', function () {
                    that.update();
                });
                $(element).bind('mouseup', function () {
                    that.update();
                });
                $(element).bind('mousedown', function () {
                    that.update();
                });
            },
            update: function () {
                var selectedRangeBinding = this.bindings["selectedRange"];
                var caret = $(this.element).caret();
                console.log("Update: " + caret.start + " " + caret.end);
                selectedRangeBinding.set([caret.start, caret.end]);
            },
            refresh: function () {
                var binding = this.bindings["selectedRange"];
                var bindingValue = binding.get();
                var selectionStart = bindingValue[0];
                var selectionEnd = bindingValue[1];
                $(this.element).caret({ start: selectionStart, end: selectionEnd });
                $(this.element).focus();
            }
        });
    })(Client = WebMap.Client || (WebMap.Client = {}));
})(WebMap || (WebMap = {}));
//NumericTextBox  Value
kendo.data.binders.widget.NumericUpDownValue = kendo.data.Binder.extend({
    init: function (widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
    },
    refresh: function () {
        var that = this, value = that.bindings["NumericUpDownValue"].get(); //get the value from the View-Model
        $(that.element).data("kendoNumericTextBox").value(value); //update the widget
    }
});
//NumericTextBox Max Value
kendo.data.binders.widget.NumericUpDownMax = kendo.data.Binder.extend({
    init: function (widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
    },
    refresh: function () {
        var that = this, value = that.bindings["NumericUpDownMax"].get(); //get the value from the View-Model
        $(that.element).data("kendoNumericTextBox").max(value); //update the widget
    }
});
//NumericTextBox Min Value
kendo.data.binders.widget.NumericUpDownMin = kendo.data.Binder.extend({
    init: function (widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
    },
    refresh: function () {
        var that = this, value = that.bindings["NumericUpDownMin"].get(); //get the value from the View-Model
        $(that.element).data("kendoNumericTextBox").min(value); //update the widget
    }
});
//NumericTextBox Increment Value
kendo.data.binders.widget.NumericUpDownIncrementVal = kendo.data.Binder.extend({
    init: function (widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
    },
    refresh: function () {
        var that = this, value = that.bindings["NumericUpDownIncrementVal"].get(); //get the value from the View-Model
        $(that.element).data("kendoNumericTextBox").step(value); //update the widget
    }
});
kendo.data.binders.widget.refreshOnChange = kendo.data.Binder.extend({
    init: function (element, bindings, options) {
        kendo.data.Binder.fn.init.call(this, element, bindings, options);
        this.timeout = 0;
    },
    refresh: function () {
        var that = this;
        /// The following code allows use to trigger just one refresh call for a batch of 
        // updates on the same list.
        if (that.timeout === 0) {
            that.timeout = setTimeout(function () {
                try {
                    that.element.refresh();
                }
                finally {
                    that.timeout = 0;
                }
            }, 1);
        }
    }
});
////Masked Textbox Support
//MaskedTextBox Mask 
kendo.data.binders.widget.maskedTextBoxMask = kendo.data.Binder.extend({
    init: function (widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
        var that = this;
        this.widget = widget;
    },
    refresh: function () {
        var that = this;
        var oldmask = this.widget.options.mask;
        var value = that.bindings["maskedTextBoxMask"].get();
        this.widget.setOptions({ mask: value });
        if (oldmask != "")
            $(this.element).trigger("maskChanged");
    }
});
//MaskedTextBox promptChar
kendo.data.binders.widget.maskedTextBoxPrompt = kendo.data.Binder.extend({
    init: function (widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
        var that = this;
        this.widget = widget;
    },
    refresh: function () {
        var that = this;
        var value = that.bindings["maskedTextBoxPrompt"].get();
        this.widget.setOptions({ promptChar: value });
    }
});
//MaskChangedEvent
kendo.data.binders.widget.MaskChanged = kendo.data.Binder.extend({
    init: function (element, bindings, options) {
        kendo.data.Binder.fn.init.call(this, element, bindings, options);
        var that = this;
        $(element.element).bind("maskChanged", function (event) {
            var binding = that.bindings["MaskChanged"];
            var methodName = binding.path.substring((binding.path.indexOf('.') + 1));
            var method = binding.source.logic[methodName];
            method.apply(binding.source);
        });
    },
    refresh: function () {
    }
});
//TypeValidationEvent
kendo.data.binders.widget.TypeValidationCompleted = kendo.data.Binder.extend({
    init: function (element, bindings, options) {
        kendo.data.Binder.fn.init.call(this, element, bindings, options);
        var that = this;
        $(element.element).bind("change", function (event) {
            var binding = that.bindings["TypeValidationCompleted"];
            var methodName = binding.path.substring((binding.path.indexOf('.') + 1));
            var method = binding.source.logic[methodName];
            method.apply(binding.source);
        });
    },
    refresh: function () {
    }
});
//MaskInputRejected 
kendo.data.binders.widget.MaskInputRejected = kendo.data.Binder.extend({
    init: function (element, bindings, options) {
        kendo.data.Binder.fn.init.call(this, element, bindings, options);
        var that = this;
        $(element.element).bind("keypress", function (event) {
            var binding = that.bindings["MaskInputRejected"];
            var methodName = binding.path.substring((binding.path.indexOf('.') + 1));
            var method = binding.source.logic[methodName];
            var newChar = String.fromCharCode(event.charCode);
            var position = ($(this).caret().start);
            var currentMask = that.options.mask;
            var match = maskToRegex(newChar, currentMask.charAt(position));
            if (!match) {
                method.apply(binding.source);
            }
        });
    },
    refresh: function () {
    }
});
function maskToRegex(newChar, maskChar) {
    switch (maskChar) {
        case "0": {
            return newChar.match(/\d/);
        }
        case "9": {
            return newChar.match(/\d|\s/);
        }
        case "#": {
            return newChar.match(/\d|\s|\+|\-/);
        }
        case "&": {
            return newChar.match(/\S/);
        }
        case "?": {
            return newChar.match(/[a-zA-Z]|\s/);
        }
        case "A": {
            return newChar.match(/[a-zA-Z0-9]/);
        }
        case "C": {
            return newChar.match(/./);
        }
        case "L": {
            return newChar.match(/[a-zA-Z]/);
        }
        case "a": {
            return newChar.match(/[a-zA-Z0-9]|\s/);
        }
        default: {
            return newChar.match(new RegExp("/\\" + maskChar + "/"));
        }
    }
}
//# sourceMappingURL=WebMap.all.js.map