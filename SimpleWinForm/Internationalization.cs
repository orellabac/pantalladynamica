﻿namespace InternationalHelpers
{

    public static class International
    {
        /// <summary>
        /// Returns the local translation of this piece of text.
        /// Shorthand for Internationalization.GetText.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string _(this object o, string text)
        {
            return text;
        }

        public static string T(this object o, string text)
        {
            return text;
        }

    }

}
