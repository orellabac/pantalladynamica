using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UpgradeHelpers;
using UpgradeHelpers.BasicViewModels;
using UpgradeHelpers.Interfaces;
using UpgradeHelpers.Events;
using UpgradeHelpers.Helpers;

namespace SimpleWinForm
{

	[UpgradeHelpers.Helpers.Singlenton()]
	public class Program
		: UpgradeHelpers.Interfaces.ILogicClass, UpgradeHelpers.Interfaces.ICreatesObjects, UpgradeHelpers.Interfaces.IInteractsWithView
	{

		public void Init()
		{
		}
		/// <summary>
		/// The main entry point for the application.
		/// </summary>

		[STAThread]
		public void Main(IIocContainer Container)
		{
			var ViewManager = Container.Resolve<IViewManager>();
			ViewManager.NavigateToView(Container.Resolve< frmCalculators>());
		}

		public string UniqueID { get; set; }

		public IViewManager ViewManager { get; set; }

		public IIocContainer Container { get; set; }

	}
}